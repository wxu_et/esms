package esms.etonenet.bossoa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TC_CITY database table.
 * 
 */
@Entity
@Table(name = "TC_CITY")
@NamedQuery(name = "City.findAll", query = "SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CITY_CODE")
	private String cityCode;

	@Column(name = "CITY_NAME")
	private String cityName;

	// bi-directional many-to-one association to Province
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROVINCE_CODE")
	private Province province;

	public City() {
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

}