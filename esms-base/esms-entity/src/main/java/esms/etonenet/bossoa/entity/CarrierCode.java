package esms.etonenet.bossoa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TC_CARRIER_CODE database table.
 * 
 */
@Entity
@Table(name = "TC_CARRIER_CODE")
@NamedQuery(name = "CarrierCode.findAll", query = "SELECT c FROM CarrierCode c")
public class CarrierCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CARRIER_CODE")
	private String carrierCode;

	@Column(name = "CARRIER_NAME")
	private String carrierName;

	public CarrierCode() {
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCarrierName() {
		return this.carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

}