package esms.etonenet.bossoa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TC_NETWORK_CODE database table.
 * 
 */
@Entity
@Table(name = "TC_NETWORK_CODE")
@NamedQuery(name = "NetworkCode.findAll", query = "SELECT n FROM NetworkCode n")
public class NetworkCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NETWORK_CODE")
	private String networkCode;

	@Column(name = "NETWORK_NAME")
	private String networkName;

	public NetworkCode() {
	}

	public String getNetworkCode() {
		return this.networkCode;
	}

	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}

	public String getNetworkName() {
		return this.networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

}