package esms.etonenet.oldboss.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the TM_SP_SERVICE database table.
 * 
 */
@Embeddable
public class OldSpServicePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "SP_ID")
	private String spId;

	@Column(name = "SP_SERVICE_CODE")
	private String spServiceCode;

	public OldSpServicePK() {
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof OldSpServicePK)) {
			return false;
		}
		OldSpServicePK castOther = (OldSpServicePK) other;
		return this.spId.equals(castOther.spId) && this.spServiceCode.equals(castOther.spServiceCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.spId.hashCode();
		hash = hash * prime + this.spServiceCode.hashCode();

		return hash;
	}
}