package esms.etonenet.oldboss.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TM_CHANNEL database table.
 * 
 */
@Entity
@Table(name = "TM_CHANNEL")
@NamedQuery(name = "OldChannel.findAll", query = "SELECT o FROM OldChannel o")
public class OldChannel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "CHANNEL_MESSAGE_LENGTH")
	private BigDecimal channelMessageLength;

	@Column(name = "CHANNEL_MESSAGE_LENGTH_EN")
	private BigDecimal channelMessageLengthEn;

	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH")
	private BigDecimal channelMessageOrgLength;

	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH_EN")
	private BigDecimal channelMessageOrgLengthEn;

	@Column(name = "CHANNEL_MESSAGE_SIGNATURE")
	private String channelMessageSignature;

	@Column(name = "CHANNEL_MESSAGE_SIGNATURE_EN")
	private String channelMessageSignatureEn;

	@Column(name = "CHANNEL_MTMO_FLAG")
	private BigDecimal channelMtmoFlag;

	@Column(name = "CHANNEL_NAME")
	private String channelName;

	@Column(name = "CHANNEL_NUMBER")
	private String channelNumber;

	@Column(name = "CHANNEL_NUMBER_LENGTH")
	private BigDecimal channelNumberLength;

	@Column(name = "CHANNEL_SPEED")
	private BigDecimal channelSpeed;

	@Column(name = "CHANNEL_STATE")
	private BigDecimal channelState;

	@Column(name = "LONG_SMS_FLAG")
	private BigDecimal longSmsFlag;

	@Column(name = "WHITE_LIST_POLICY")
	private BigDecimal whiteListPolicy;

	public OldChannel() {
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public BigDecimal getChannelMessageLength() {
		return this.channelMessageLength;
	}

	public void setChannelMessageLength(BigDecimal channelMessageLength) {
		this.channelMessageLength = channelMessageLength;
	}

	public BigDecimal getChannelMessageLengthEn() {
		return this.channelMessageLengthEn;
	}

	public void setChannelMessageLengthEn(BigDecimal channelMessageLengthEn) {
		this.channelMessageLengthEn = channelMessageLengthEn;
	}

	public BigDecimal getChannelMessageOrgLength() {
		return this.channelMessageOrgLength;
	}

	public void setChannelMessageOrgLength(BigDecimal channelMessageOrgLength) {
		this.channelMessageOrgLength = channelMessageOrgLength;
	}

	public BigDecimal getChannelMessageOrgLengthEn() {
		return this.channelMessageOrgLengthEn;
	}

	public void setChannelMessageOrgLengthEn(BigDecimal channelMessageOrgLengthEn) {
		this.channelMessageOrgLengthEn = channelMessageOrgLengthEn;
	}

	public String getChannelMessageSignature() {
		return this.channelMessageSignature;
	}

	public void setChannelMessageSignature(String channelMessageSignature) {
		this.channelMessageSignature = channelMessageSignature;
	}

	public String getChannelMessageSignatureEn() {
		return this.channelMessageSignatureEn;
	}

	public void setChannelMessageSignatureEn(String channelMessageSignatureEn) {
		this.channelMessageSignatureEn = channelMessageSignatureEn;
	}

	public BigDecimal getChannelMtmoFlag() {
		return this.channelMtmoFlag;
	}

	public void setChannelMtmoFlag(BigDecimal channelMtmoFlag) {
		this.channelMtmoFlag = channelMtmoFlag;
	}

	public String getChannelName() {
		return this.channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelNumber() {
		return this.channelNumber;
	}

	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}

	public BigDecimal getChannelNumberLength() {
		return this.channelNumberLength;
	}

	public void setChannelNumberLength(BigDecimal channelNumberLength) {
		this.channelNumberLength = channelNumberLength;
	}

	public BigDecimal getChannelSpeed() {
		return this.channelSpeed;
	}

	public void setChannelSpeed(BigDecimal channelSpeed) {
		this.channelSpeed = channelSpeed;
	}

	public BigDecimal getChannelState() {
		return this.channelState;
	}

	public void setChannelState(BigDecimal channelState) {
		this.channelState = channelState;
	}

	public BigDecimal getLongSmsFlag() {
		return this.longSmsFlag;
	}

	public void setLongSmsFlag(BigDecimal longSmsFlag) {
		this.longSmsFlag = longSmsFlag;
	}

	public BigDecimal getWhiteListPolicy() {
		return this.whiteListPolicy;
	}

	public void setWhiteListPolicy(BigDecimal whiteListPolicy) {
		this.whiteListPolicy = whiteListPolicy;
	}

}