package esms.etonenet.oldboss.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TM_SP_SERVICE database table.
 * 
 */
@Entity
@Table(name = "TM_SP_SERVICE")
@NamedQuery(name = "OldSpService.findAll", query = "SELECT o FROM OldSpService o")
public class OldSpService implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OldSpServicePK id;

	@Column(name = "CONTENT_FILTER_CATEGORY_FLAG")
	private BigDecimal contentFilterCategoryFlag;

	@Column(name = "CONTENT_FILTER_POLICY")
	private BigDecimal contentFilterPolicy;

	@Column(name = "CONTENT_POSTFIX_PATTERN")
	private String contentPostfixPattern;

	@Column(name = "CONTENT_PREFIX_PATTERN")
	private String contentPrefixPattern;

	@Column(name = "CONTENT_PROCESS_POLICY")
	private BigDecimal contentProcessPolicy;

	@Column(name = "CONTENT_SPLIT_PATTERN")
	private String contentSplitPattern;

	@Column(name = "MSISDN_FILTER_POLICY")
	private BigDecimal msisdnFilterPolicy;

	@Column(name = "PRIORITY_PATTERN")
	private BigDecimal priorityPattern;

	@Column(name = "PRIORITY_POLICY")
	private BigDecimal priorityPolicy;

	@Column(name = "SP_SERVICE_NAME")
	private String spServiceName;

	public OldSpService() {
	}

	public OldSpServicePK getId() {
		return this.id;
	}

	public void setId(OldSpServicePK id) {
		this.id = id;
	}

	public BigDecimal getContentFilterCategoryFlag() {
		return this.contentFilterCategoryFlag;
	}

	public void setContentFilterCategoryFlag(BigDecimal contentFilterCategoryFlag) {
		this.contentFilterCategoryFlag = contentFilterCategoryFlag;
	}

	public BigDecimal getContentFilterPolicy() {
		return this.contentFilterPolicy;
	}

	public void setContentFilterPolicy(BigDecimal contentFilterPolicy) {
		this.contentFilterPolicy = contentFilterPolicy;
	}

	public String getContentPostfixPattern() {
		return this.contentPostfixPattern;
	}

	public void setContentPostfixPattern(String contentPostfixPattern) {
		this.contentPostfixPattern = contentPostfixPattern;
	}

	public String getContentPrefixPattern() {
		return this.contentPrefixPattern;
	}

	public void setContentPrefixPattern(String contentPrefixPattern) {
		this.contentPrefixPattern = contentPrefixPattern;
	}

	public BigDecimal getContentProcessPolicy() {
		return this.contentProcessPolicy;
	}

	public void setContentProcessPolicy(BigDecimal contentProcessPolicy) {
		this.contentProcessPolicy = contentProcessPolicy;
	}

	public String getContentSplitPattern() {
		return this.contentSplitPattern;
	}

	public void setContentSplitPattern(String contentSplitPattern) {
		this.contentSplitPattern = contentSplitPattern;
	}

	public BigDecimal getMsisdnFilterPolicy() {
		return this.msisdnFilterPolicy;
	}

	public void setMsisdnFilterPolicy(BigDecimal msisdnFilterPolicy) {
		this.msisdnFilterPolicy = msisdnFilterPolicy;
	}

	public BigDecimal getPriorityPattern() {
		return this.priorityPattern;
	}

	public void setPriorityPattern(BigDecimal priorityPattern) {
		this.priorityPattern = priorityPattern;
	}

	public BigDecimal getPriorityPolicy() {
		return this.priorityPolicy;
	}

	public void setPriorityPolicy(BigDecimal priorityPolicy) {
		this.priorityPolicy = priorityPolicy;
	}

	public String getSpServiceName() {
		return this.spServiceName;
	}

	public void setSpServiceName(String spServiceName) {
		this.spServiceName = spServiceName;
	}

}