package esms.etonenet.oldboss.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TM_SP database table.
 * 
 */
@Entity
@Table(name = "TM_SP")
@NamedQuery(name = "OldSp.findAll", query = "SELECT o FROM OldSp o")
public class OldSp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SP_ID")
	private String spId;

	@Column(name = "ADDRESS_POLICY")
	private BigDecimal addressPolicy;

	@Column(name = "MT_TIME_WINDOW")
	private String mtTimeWindow;

	@Column(name = "QUOTA_POLICY")
	private BigDecimal quotaPolicy;

	@Column(name = "ROUTE_POLICY")
	private BigDecimal routePolicy;

	@Column(name = "SMS_MO_URL")
	private String smsMoUrl;

	@Column(name = "SMS_MT_URL")
	private String smsMtUrl;

	@Column(name = "SMS_RT_URL")
	private String smsRtUrl;

	@Column(name = "SP_MTMO_FLAG")
	private BigDecimal spMtmoFlag;

	@Column(name = "SP_NAME")
	private String spName;

	@Column(name = "SP_STATE")
	private BigDecimal spState;

	@Column(name = "TRANSMIT_CONNECTION_LIMIT")
	private BigDecimal transmitConnectionLimit;

	@Column(name = "TRANSMIT_IP_LIMIT")
	private String transmitIpLimit;

	@Column(name = "TRANSMIT_MODE")
	private BigDecimal transmitMode;

	@Column(name = "TRANSMIT_SPEED_LIMIT")
	private BigDecimal transmitSpeedLimit;

	@Column(name = "USER_ID")
	private BigDecimal userId;

	@Column(name = "WHITE_LIST_POLICY")
	private BigDecimal whiteListPolicy;

	public OldSp() {
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public BigDecimal getAddressPolicy() {
		return this.addressPolicy;
	}

	public void setAddressPolicy(BigDecimal addressPolicy) {
		this.addressPolicy = addressPolicy;
	}

	public String getMtTimeWindow() {
		return this.mtTimeWindow;
	}

	public void setMtTimeWindow(String mtTimeWindow) {
		this.mtTimeWindow = mtTimeWindow;
	}

	public BigDecimal getQuotaPolicy() {
		return this.quotaPolicy;
	}

	public void setQuotaPolicy(BigDecimal quotaPolicy) {
		this.quotaPolicy = quotaPolicy;
	}

	public BigDecimal getRoutePolicy() {
		return this.routePolicy;
	}

	public void setRoutePolicy(BigDecimal routePolicy) {
		this.routePolicy = routePolicy;
	}

	public String getSmsMoUrl() {
		return this.smsMoUrl;
	}

	public void setSmsMoUrl(String smsMoUrl) {
		this.smsMoUrl = smsMoUrl;
	}

	public String getSmsMtUrl() {
		return this.smsMtUrl;
	}

	public void setSmsMtUrl(String smsMtUrl) {
		this.smsMtUrl = smsMtUrl;
	}

	public String getSmsRtUrl() {
		return this.smsRtUrl;
	}

	public void setSmsRtUrl(String smsRtUrl) {
		this.smsRtUrl = smsRtUrl;
	}

	public BigDecimal getSpMtmoFlag() {
		return this.spMtmoFlag;
	}

	public void setSpMtmoFlag(BigDecimal spMtmoFlag) {
		this.spMtmoFlag = spMtmoFlag;
	}

	public String getSpName() {
		return this.spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public BigDecimal getSpState() {
		return this.spState;
	}

	public void setSpState(BigDecimal spState) {
		this.spState = spState;
	}

	public BigDecimal getTransmitConnectionLimit() {
		return this.transmitConnectionLimit;
	}

	public void setTransmitConnectionLimit(BigDecimal transmitConnectionLimit) {
		this.transmitConnectionLimit = transmitConnectionLimit;
	}

	public String getTransmitIpLimit() {
		return this.transmitIpLimit;
	}

	public void setTransmitIpLimit(String transmitIpLimit) {
		this.transmitIpLimit = transmitIpLimit;
	}

	public BigDecimal getTransmitMode() {
		return this.transmitMode;
	}

	public void setTransmitMode(BigDecimal transmitMode) {
		this.transmitMode = transmitMode;
	}

	public BigDecimal getTransmitSpeedLimit() {
		return this.transmitSpeedLimit;
	}

	public void setTransmitSpeedLimit(BigDecimal transmitSpeedLimit) {
		this.transmitSpeedLimit = transmitSpeedLimit;
	}

	public BigDecimal getUserId() {
		return this.userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public BigDecimal getWhiteListPolicy() {
		return this.whiteListPolicy;
	}

	public void setWhiteListPolicy(BigDecimal whiteListPolicy) {
		this.whiteListPolicy = whiteListPolicy;
	}

}