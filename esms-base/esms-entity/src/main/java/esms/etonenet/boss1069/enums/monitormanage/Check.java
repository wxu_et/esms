package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum Check {
	YES(BigDecimal.valueOf(1), "是"), NO(BigDecimal.valueOf(0), "否");

	private final BigDecimal value;
	private final String name;

	private Check(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public static Check fromValue(BigDecimal value) {
		if (value != null) {
			for (Check c : values()) {
				if (value.equals(c.value))
					return c;
			}
		}
		return YES;
	}

	public static Map<String, String> todisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (Check c : values()) {
			map.put(c.value.toString(), c.name);
		}
		return map;
	}

}
