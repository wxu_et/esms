package esms.etonenet.boss1069.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.etonenet.anno.LogEntity;

@LogEntity(remark = "sp监控配置表")
@Entity
@Table(name = "TT_SP_MONITORMANAGE")
public class SpMonitorManage implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键 编号
	 */
	@Id
	@Column(name = "SP_ID")
	private String spId;

	/**
	 * 发送量报警点
	 */
	@Column(name = "MT_TOTAL_ALARMPOINT")
	private Integer mtTotalAlarmpoint;

	/**
	 * 成功率报警点
	 */
	@Column(name = "MT_SUCCESS_ALARMPOINT")
	private String mtSuccesAlarmpoint;

	/**
	 * 失败率报警点
	 */
	@Column(name = "MT_FAIL_ALARMPOINT")
	private String mtFailAlarmpoint;

	/**
	 * 平台拦截率报警点
	 */
	@Column(name = "MT_FAIL_ET_ALARMPOINT")
	private String mtFailEtAlarmpoint;

	/**
	 * 平台拦截量报警点
	 */
	@Column(name = "MT_FAIL_ET_ALARMTOTAL")
	private Integer mtFailEtAlarmtotal;

	/**
	 * 运营商拦截率报警点
	 */
	@Column(name = "MT_FAIL_CARRIER_ALARMPOINT")
	private String mtFailCarrierAlarmpoint;

	/**
	 * 运营商拦截量报警点
	 */
	@Column(name = "MT_FAIL_CARRIER_ALARMTOTAL")
	private Integer mtFailCarrierAlarmtotal;

	/**
	 * 无状态率报警点
	 */
	@Column(name = "MT_NO_REPORT_ALARMPOINT")
	private String mtNoReportAlarmpoint;

	/**
	 * 重复发送被过滤量报警点
	 */
	@Column(name = "MT_FILTER_REP_ALARMTOTAL")
	private Integer mtFilterRepAlarmtotal;

	/**
	 * 重复发送被过滤率报警点
	 */
	@Column(name = "MT_FILTER_REP_ALARMPOINT")
	private String mtFilterRepAlarmpoint;

	/**
	 * 内容过滤量报警点
	 */
	@Column(name = "MT_FILTER_CONT_ALARMTOTAL")
	private Integer mtFilterContAlarmtotal;

	/**
	 * 内容过滤率报警点
	 */
	@Column(name = "MT_FILTER_CONT_ALARMPOINT")
	private String mtFilterContAlarmpoint;

	/**
	 * 路由错误量报警点
	 */
	@Column(name = "MT_ROUTER_ERROR_ALARMTOTAL")
	private Integer mtRouterErrorAlarmtotal;

	/**
	 * 路由错误率报警点
	 */
	@Column(name = "MT_ROUTER_ERROR_ALARMPOINT")
	private String mtRouterErrorAlarmpoint;

	/**
	 * 黑名单过滤量报警点
	 */
	@Column(name = "MT_FILTER_BLACKLIST_ALARMTOTAL")
	private Integer mtFilterBlackListAlarmtotal;

	/**
	 * 黑名单过滤率报警点
	 */
	@Column(name = "MT_FILTER_BLACKLIST_ALARMPOINT")
	private String mtFilterBlackListAlarmpoint;

	/**
	 * 备注
	 */
	@Column(name = "COMMENTS")
	private String comments;

	/**
	 * 重要性 1 red 2 orange 3 yellow 4 green
	 */
	@Column(name = "IMPORTANCE")
	private Integer importance;

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public Integer getMtTotalAlarmpoint() {
		return mtTotalAlarmpoint;
	}

	public void setMtTotalAlarmpoint(Integer mtTotalAlarmpoint) {
		this.mtTotalAlarmpoint = mtTotalAlarmpoint;
	}

	public String getMtSuccesAlarmpoint() {
		return mtSuccesAlarmpoint;
	}

	public void setMtSuccesAlarmpoint(String mtSuccesAlarmpoint) {
		this.mtSuccesAlarmpoint = mtSuccesAlarmpoint;
	}

	public String getMtFailEtAlarmpoint() {
		return mtFailEtAlarmpoint;
	}

	public void setMtFailEtAlarmpoint(String mtFailEtAlarmpoint) {
		this.mtFailEtAlarmpoint = mtFailEtAlarmpoint;
	}

	public Integer getMtFailEtAlarmtotal() {
		return mtFailEtAlarmtotal;
	}

	public void setMtFailEtAlarmtotal(Integer mtFailEtAlarmtotal) {
		this.mtFailEtAlarmtotal = mtFailEtAlarmtotal;
	}

	public String getMtFailCarrierAlarmpoint() {
		return mtFailCarrierAlarmpoint;
	}

	public void setMtFailCarrierAlarmpoint(String mtFailCarrierAlarmpoint) {
		this.mtFailCarrierAlarmpoint = mtFailCarrierAlarmpoint;
	}

	public Integer getMtFailCarrierAlarmtotal() {
		return mtFailCarrierAlarmtotal;
	}

	public void setMtFailCarrierAlarmtotal(Integer mtFailCarrierAlarmtotal) {
		this.mtFailCarrierAlarmtotal = mtFailCarrierAlarmtotal;
	}

	public String getMtNoReportAlarmpoint() {
		return mtNoReportAlarmpoint;
	}

	public void setMtNoReportAlarmpoint(String mtNoReportAlarmpoint) {
		this.mtNoReportAlarmpoint = mtNoReportAlarmpoint;
	}

	public Integer getMtFilterRepAlarmtotal() {
		return mtFilterRepAlarmtotal;
	}

	public void setMtFilterRepAlarmtotal(Integer mtFilterRepAlarmtotal) {
		this.mtFilterRepAlarmtotal = mtFilterRepAlarmtotal;
	}

	public String getMtFilterRepAlarmpoint() {
		return mtFilterRepAlarmpoint;
	}

	public void setMtFilterRepAlarmpoint(String mtFilterRepAlarmpoint) {
		this.mtFilterRepAlarmpoint = mtFilterRepAlarmpoint;
	}

	public Integer getMtFilterContAlarmtotal() {
		return mtFilterContAlarmtotal;
	}

	public void setMtFilterContAlarmtotal(Integer mtFilterContAlarmtotal) {
		this.mtFilterContAlarmtotal = mtFilterContAlarmtotal;
	}

	public String getMtFilterContAlarmpoint() {
		return mtFilterContAlarmpoint;
	}

	public void setMtFilterContAlarmpoint(String mtFilterContAlarmpoint) {
		this.mtFilterContAlarmpoint = mtFilterContAlarmpoint;
	}

	public Integer getMtRouterErrorAlarmtotal() {
		return mtRouterErrorAlarmtotal;
	}

	public void setMtRouterErrorAlarmtotal(Integer mtRouterErrorAlarmtotal) {
		this.mtRouterErrorAlarmtotal = mtRouterErrorAlarmtotal;
	}

	public String getMtRouterErrorAlarmpoint() {
		return mtRouterErrorAlarmpoint;
	}

	public void setMtRouterErrorAlarmpoint(String mtRouterErrorAlarmpoint) {
		this.mtRouterErrorAlarmpoint = mtRouterErrorAlarmpoint;
	}

	public Integer getMtFilterBlackListAlarmtotal() {
		return mtFilterBlackListAlarmtotal;
	}

	public void setMtFilterBlackListAlarmtotal(Integer mtFilterBlackListAlarmtotal) {
		this.mtFilterBlackListAlarmtotal = mtFilterBlackListAlarmtotal;
	}

	public String getMtFilterBlackListAlarmpoint() {
		return mtFilterBlackListAlarmpoint;
	}

	public void setMtFilterBlackListAlarmpoint(String mtFilterBlackListAlarmpoint) {
		this.mtFilterBlackListAlarmpoint = mtFilterBlackListAlarmpoint;
	}

	public Integer getImportance() {
		return importance;
	}

	public void setImportance(Integer importance) {
		this.importance = importance;
	}

	public String getMtFailAlarmpoint() {
		return mtFailAlarmpoint;
	}

	public void setMtFailAlarmpoint(String mtFailAlarmpoint) {
		this.mtFailAlarmpoint = mtFailAlarmpoint;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
