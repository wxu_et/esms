package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_SP_SC_CARRIERCODE database table.
 * 
 */
@LogEntity(remark = "长短信管理")
@Entity
@Table(name = "TM_SP_SC_CARRIERCODE")
public class SpScCarriercode implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SpScCarriercodePK id;

	@Column(name = "CHANNEL_MESSAGE_LENGTH")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal channelMessageLength;

	@Column(name = "SMS_PATTERN")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal smsPattern;

	// bi-directional many-to-one association to SpService
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "SP_ID", referencedColumnName = "SP_ID", insertable = false, updatable = false),
			@JoinColumn(name = "SP_SERVICE_CODE", referencedColumnName = "SP_SERVICE_CODE", insertable = false, updatable = false) })
	private SpService spService;

	public SpScCarriercode() {
	}

	public SpScCarriercodePK getId() {
		return this.id;
	}

	public void setId(SpScCarriercodePK id) {
		this.id = id;
	}

	public BigDecimal getChannelMessageLength() {
		return this.channelMessageLength;
	}

	public void setChannelMessageLength(BigDecimal channelMessageLength) {
		this.channelMessageLength = channelMessageLength;
	}

	public BigDecimal getSmsPattern() {
		return this.smsPattern;
	}

	public void setSmsPattern(BigDecimal smsPattern) {
		this.smsPattern = smsPattern;
	}

	public SpService getSpService() {
		return this.spService;
	}

	public void setSpService(SpService spService) {
		this.spService = spService;
	}

}