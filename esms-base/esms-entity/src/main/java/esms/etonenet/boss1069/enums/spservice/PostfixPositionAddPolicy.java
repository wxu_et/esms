package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum PostfixPositionAddPolicy {

	NONE(BigDecimal.valueOf(0), "不加后缀"), FISRT(BigDecimal.valueOf(256), "第一条加后缀"), LAST(BigDecimal.valueOf(512),
			"最后一条加后缀"), ALL(BigDecimal.valueOf(1024), "全部加后缀");

	private BigDecimal value;
	private String name;

	private PostfixPositionAddPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static PostfixPositionAddPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (PostfixPositionAddPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return NONE;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (PostfixPositionAddPolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (PostfixPositionAddPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
