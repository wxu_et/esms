package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL_ROUTE database table.
 * 
 */
@SuppressWarnings("serial")
@LogEntity(remark = "通道路由")
@Entity
@Table(name = "TM_CHANNEL_ROUTE")
@NamedQuery(name = "ChannelRoute.findAll", query = "SELECT c FROM ChannelRoute c")
public class ChannelRoute implements Serializable {

	@Id
	@SequenceGenerator(name = "TM_CHANNEL_ROUTEID_GENERATOR", sequenceName = "SEQ_CHANNEL_ROUTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_CHANNEL_ROUTEID_GENERATOR")
	@Column(name = "CHANNEL_ROUTE_ID", precision = 10, nullable = false, unique = true)
	private Long channelRouteId;

	@Size(max = 6)
	@Column(name = "CARRIER_CODE", length = 6)
	private String carrierCode;

	@Size(max = 6)
	@Column(name = "CITY_CODE", length = 6)
	private String cityCode;

	@Size(max = 6)
	@Column(name = "COUNTRY_CODE", length = 6)
	private String countryCode;

	@Size(max = 6)
	@Column(name = "PROVINCE_CODE", length = 6)
	private String provinceCode;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "ROUTE_STATE", precision = 2, scale = 0)
	private BigDecimal routeState;

	// bi-directional many-to-one association to Channel
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID")
	private Channel channel;

	public ChannelRoute() {
	}

	public Long getChannelRouteId() {
		return this.channelRouteId;
	}

	public void setChannelRouteId(Long channelRouteId) {
		this.channelRouteId = channelRouteId;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public BigDecimal getRouteState() {
		return this.routeState;
	}

	public void setRouteState(BigDecimal routeState) {
		this.routeState = routeState;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}