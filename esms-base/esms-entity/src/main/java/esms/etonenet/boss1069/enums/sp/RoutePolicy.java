package esms.etonenet.boss1069.enums.sp;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum RoutePolicy {

	AUTO(BigDecimal.valueOf(0), "路由为系统自动设置"), STATIC(BigDecimal.valueOf(1), "路由为静态设置"), DYNAMIC(BigDecimal.valueOf(2),
			"路由为动态设置"), CUSTOM(BigDecimal.valueOf(3), "自定义路由算法");

	private BigDecimal value;
	private String name;

	private RoutePolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static RoutePolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (RoutePolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return AUTO;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RoutePolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
