package esms.etonenet.boss1069.enums.spmonitor;

/**
 * 时间单位标志
 * 
 * @author zfang
 *
 */
public class TimeUnitFlag {
	/**
	 * 小时
	 */
	public static final int HOUR = 1;

	/**
	 * 天
	 */
	public static final int DAY = 2;

	/**
	 * 月
	 */
	public static final int MONTH = 3;

	/**
	 * 周
	 */
	public static final int WEEK = 4;

	/**
	 * 分钟
	 */
	public static final int MINUTE = 5;
}
