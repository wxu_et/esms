package esms.etonenet.boss1069.enums.channel;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ChannelMtmoFlag {

	MT(BigDecimal.valueOf(1), "可下行"), BATCH_MT(BigDecimal.valueOf(2), "可批量下行"), MO(BigDecimal.valueOf(4), "可上行"), RT(
			BigDecimal.valueOf(8),
			"返回状态报告"), ESM_CLASS(BigDecimal.valueOf(65536), "填写ESM_CLASS"), PROTOCOL_ID(BigDecimal.valueOf(131072),
					"填写PROTOCOL_ID"), PRIORITY(BigDecimal.valueOf(262144), "可填写优先级"), TIMING(BigDecimal.valueOf(524288),
							"填写定时发送时间"), EXPIRY(BigDecimal.valueOf(1048576), "填写有效时间");

	private BigDecimal value;
	private String name;

	private ChannelMtmoFlag(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ChannelMtmoFlag fromValue(BigDecimal value) {
		if (value != null) {
			for (ChannelMtmoFlag state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return MT;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ChannelMtmoFlag e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
