package esms.etonenet.boss1069.enums.sp;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SpMtmoFlag {

	F1(BigDecimal.valueOf(1), "可SMS单条下行"), F2(BigDecimal.valueOf(2), "可SMS多条下行"), F3(BigDecimal.valueOf(4),
			"可SMS批量下行"), F4(BigDecimal.valueOf(8), "可SMS上行"), F5(BigDecimal.valueOf(16), "可SMS状态报告"), F6(
					BigDecimal.valueOf(32),
					"可否批量上行"), F7(BigDecimal.valueOf(64), "可否批量状态报告"), F8(BigDecimal.valueOf(128), "上行是否携带时间"), F9(
							BigDecimal.valueOf(256),
							"可填写ESM_CLASS"), F10(BigDecimal.valueOf(512), "可填写PROTOCOL_ID"), F11(
									BigDecimal.valueOf(1024),
									"可填写优先级"), F12(BigDecimal.valueOf(2048), "可填写定时发送时间"), F13(BigDecimal.valueOf(4096),
											"可填写有效时间"), F14(BigDecimal.valueOf(8192), "状态报告是否携带状态返回时间"), F15(
													BigDecimal.valueOf(16384), "可指定通道 "), F16(BigDecimal.valueOf(65536),
															"可长短信状态报告"), F17(BigDecimal.valueOf(131072),
																	"状态报告是否携带下行发送时间"), F18(BigDecimal.valueOf(262144),
																			"状态报告是否携带通道ID"), F19(
																					BigDecimal.valueOf(0x00080000),
																					"JSON状态报告"), F20(
																							BigDecimal.valueOf(
																									0x00100000),
																							"JSON下行"), F21(
																									BigDecimal.valueOf(
																											0x00200000),
																									"JSON上行");

	private BigDecimal value;
	private String name;

	private SpMtmoFlag(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static SpMtmoFlag fromValue(BigDecimal value) {
		if (value != null) {
			for (SpMtmoFlag state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return F1;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SpMtmoFlag e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
