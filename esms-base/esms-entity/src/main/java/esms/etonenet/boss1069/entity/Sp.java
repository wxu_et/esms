package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_SP database table.
 * 
 */
@LogEntity
@Entity
@Table(name = "TM_SP")
public class Sp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SP_ID")
	@Size(max = 6)
	private String spId;

	@Column(name = "ADDRESS_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal addressPolicy;

	@Column(name = "MT_TIME_WINDOW")
	@Size(max = 17)
	private String mtTimeWindow;

	@Column(name = "QUOTA_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal quotaPolicy;

	@Column(name = "ROUTE_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal routePolicy;

	@Column(name = "SMS_MO_URL")
	@Size(max = 256)
	private String smsMoUrl;

	@Column(name = "SMS_MT_URL")
	@Size(max = 256)
	private String smsMtUrl;

	@Column(name = "SMS_RT_URL")
	@Size(max = 256)
	private String smsRtUrl;

	@Column(name = "SP_EXT_NUMBER")
	@Size(max = 20)
	private String spExtNumber;

	@Column(name = "SP_MTMO_FLAG")
	@Digits(fraction = 0, integer = 10)
	private BigDecimal spMtmoFlag;

	@Column(name = "SP_NAME")
	@Size(max = 60)
	private String spName;

	@Column(name = "SP_STATE")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal spState;

	@Column(name = "TRANSMIT_CONNECTION_LIMIT")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal transmitConnectionLimit;

	@Column(name = "TRANSMIT_IP_LIMIT")
	@Size(max = 256)
	private String transmitIpLimit;

	@Column(name = "TRANSMIT_MODE")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal transmitMode;

	@Column(name = "TRANSMIT_SPEED_LIMIT")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal transmitSpeedLimit;

	@Column(name = "WHITE_LIST_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal whiteListPolicy;

	@Column(name = "USER_ID")
	@Digits(fraction = 0, integer = 10)
	private BigDecimal userId;

	@Column(name = "SP_PASSWORD")
	@Size(max = 20)
	private String spPassword;

	public Sp() {
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public BigDecimal getAddressPolicy() {
		return this.addressPolicy;
	}

	public void setAddressPolicy(BigDecimal addressPolicy) {
		this.addressPolicy = addressPolicy;
	}

	public String getMtTimeWindow() {
		return this.mtTimeWindow;
	}

	public void setMtTimeWindow(String mtTimeWindow) {
		this.mtTimeWindow = mtTimeWindow;
	}

	public BigDecimal getQuotaPolicy() {
		return this.quotaPolicy;
	}

	public void setQuotaPolicy(BigDecimal quotaPolicy) {
		this.quotaPolicy = quotaPolicy;
	}

	public BigDecimal getRoutePolicy() {
		return this.routePolicy;
	}

	public void setRoutePolicy(BigDecimal routePolicy) {
		this.routePolicy = routePolicy;
	}

	public String getSmsMoUrl() {
		return this.smsMoUrl;
	}

	public void setSmsMoUrl(String smsMoUrl) {
		this.smsMoUrl = smsMoUrl;
	}

	public String getSmsMtUrl() {
		return this.smsMtUrl;
	}

	public void setSmsMtUrl(String smsMtUrl) {
		this.smsMtUrl = smsMtUrl;
	}

	public String getSmsRtUrl() {
		return this.smsRtUrl;
	}

	public void setSmsRtUrl(String smsRtUrl) {
		this.smsRtUrl = smsRtUrl;
	}

	public String getSpExtNumber() {
		return this.spExtNumber;
	}

	public void setSpExtNumber(String spExtNumber) {
		this.spExtNumber = spExtNumber;
	}

	public BigDecimal getSpMtmoFlag() {
		return this.spMtmoFlag;
	}

	public void setSpMtmoFlag(BigDecimal spMtmoFlag) {
		this.spMtmoFlag = spMtmoFlag;
	}

	public String getSpName() {
		return this.spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public BigDecimal getSpState() {
		return this.spState;
	}

	public void setSpState(BigDecimal spState) {
		this.spState = spState;
	}

	public BigDecimal getTransmitConnectionLimit() {
		return this.transmitConnectionLimit;
	}

	public void setTransmitConnectionLimit(BigDecimal transmitConnectionLimit) {
		this.transmitConnectionLimit = transmitConnectionLimit;
	}

	public String getTransmitIpLimit() {
		return this.transmitIpLimit;
	}

	public void setTransmitIpLimit(String transmitIpLimit) {
		this.transmitIpLimit = transmitIpLimit;
	}

	public BigDecimal getTransmitMode() {
		return this.transmitMode;
	}

	public void setTransmitMode(BigDecimal transmitMode) {
		this.transmitMode = transmitMode;
	}

	public BigDecimal getTransmitSpeedLimit() {
		return this.transmitSpeedLimit;
	}

	public void setTransmitSpeedLimit(BigDecimal transmitSpeedLimit) {
		this.transmitSpeedLimit = transmitSpeedLimit;
	}

	public BigDecimal getWhiteListPolicy() {
		return this.whiteListPolicy;
	}

	public void setWhiteListPolicy(BigDecimal whiteListPolicy) {
		this.whiteListPolicy = whiteListPolicy;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public String getSpPassword() {
		return spPassword;
	}

	public void setSpPassword(String spPassword) {
		this.spPassword = spPassword;
	}

}