package esms.etonenet.boss1069.mtmo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the TT_MT database table.
 * 
 */
@Entity
@Table(name = "TT_MT")
public class Mt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SP_MT_ID")
	private Long spMtId;

	@Column(name = "AT_TIME")
	private Timestamp atTime;

	@Column(name = "BATCH_MT_ID")
	private Long batchMtId;

	@Column(name = "CARRIER_CODE")
	private String carrierCode;

	@Column(name = "CARRIER_MT_TIME")
	private Timestamp carrierMtTime;

	@Column(name = "CARRIER_RT_TIME")
	private Timestamp carrierRtTime;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "CHANNEL_MESSAGE_ID")
	private String channelMessageId;

	@Column(name = "CITY_CODE")
	private String cityCode;

	@Column(name = "COST_BATCHID")
	private Long costBatchid;

	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "DATA_CODING")
	private Short dataCoding;

	@Column(name = "DESTINATION_ADDR")
	private String destinationAddr;

	@Column(name = "ESM_CLASS")
	private Short esmClass;

	@Column(name = "MT_ERR")
	private String mtErr;

	@Column(name = "MT_STAT")
	private String mtStat;

	@Column(name = "MT_STATE")
	private Short mtState;

	@Column(name = "MT_TYPE")
	private Short mtType;

	private Short priority;

	@Column(name = "PROTOCOL_ID")
	private Short protocolId;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "REVENUE_BATCHID")
	private Long revenueBatchid;

	@Column(name = "RT_ERR")
	private String rtErr;

	@Column(name = "RT_STAT")
	private String rtStat;

	@Column(name = "RT_STATE")
	private Short rtState;

	@Column(name = "SHORT_MESSAGE")
	private String shortMessage;

	@Column(name = "SOURCE_ADDR")
	private String sourceAddr;

	@Column(name = "SP_ID")
	private String spId;

	@Column(name = "SP_MT_REF_ID")
	private Long spMtRefId;

	@Column(name = "SP_MT_REF_SEQ")
	private Short spMtRefSeq;

	@Column(name = "SP_MT_REF_TOTAL")
	private Short spMtRefTotal;

	@Column(name = "SP_MT_TIME")
	private Timestamp spMtTime;

	@Column(name = "SP_RT_TIME")
	private Timestamp spRtTime;

	@Column(name = "SP_SERVICE_CODE")
	private String spServiceCode;

	@Column(name = "USER_ID")
	private Long userId;

	@Column(name = "VALID_TIME")
	private Timestamp validTime;

	public Mt() {
	}

	public Long getSpMtId() {
		return this.spMtId;
	}

	public void setSpMtId(Long spMtId) {
		this.spMtId = spMtId;
	}

	public Timestamp getAtTime() {
		return this.atTime;
	}

	public void setAtTime(Timestamp atTime) {
		this.atTime = atTime;
	}

	public Long getBatchMtId() {
		return this.batchMtId;
	}

	public void setBatchMtId(Long batchMtId) {
		this.batchMtId = batchMtId;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Timestamp getCarrierMtTime() {
		return this.carrierMtTime;
	}

	public void setCarrierMtTime(Timestamp carrierMtTime) {
		this.carrierMtTime = carrierMtTime;
	}

	public Timestamp getCarrierRtTime() {
		return this.carrierRtTime;
	}

	public void setCarrierRtTime(Timestamp carrierRtTime) {
		this.carrierRtTime = carrierRtTime;
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelMessageId() {
		return this.channelMessageId;
	}

	public void setChannelMessageId(String channelMessageId) {
		this.channelMessageId = channelMessageId;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Long getCostBatchid() {
		return this.costBatchid;
	}

	public void setCostBatchid(Long costBatchid) {
		this.costBatchid = costBatchid;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Short getDataCoding() {
		return this.dataCoding;
	}

	public void setDataCoding(Short dataCoding) {
		this.dataCoding = dataCoding;
	}

	public String getDestinationAddr() {
		return this.destinationAddr;
	}

	public void setDestinationAddr(String destinationAddr) {
		this.destinationAddr = destinationAddr;
	}

	public Short getEsmClass() {
		return this.esmClass;
	}

	public void setEsmClass(Short esmClass) {
		this.esmClass = esmClass;
	}

	public String getMtErr() {
		return this.mtErr;
	}

	public void setMtErr(String mtErr) {
		this.mtErr = mtErr;
	}

	public String getMtStat() {
		return this.mtStat;
	}

	public void setMtStat(String mtStat) {
		this.mtStat = mtStat;
	}

	public Short getMtState() {
		return this.mtState;
	}

	public void setMtState(Short mtState) {
		this.mtState = mtState;
	}

	public Short getMtType() {
		return this.mtType;
	}

	public void setMtType(Short mtType) {
		this.mtType = mtType;
	}

	public Short getPriority() {
		return this.priority;
	}

	public void setPriority(Short priority) {
		this.priority = priority;
	}

	public Short getProtocolId() {
		return this.protocolId;
	}

	public void setProtocolId(Short protocolId) {
		this.protocolId = protocolId;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public Long getRevenueBatchid() {
		return this.revenueBatchid;
	}

	public void setRevenueBatchid(Long revenueBatchid) {
		this.revenueBatchid = revenueBatchid;
	}

	public String getRtErr() {
		return this.rtErr;
	}

	public void setRtErr(String rtErr) {
		this.rtErr = rtErr;
	}

	public String getRtStat() {
		return this.rtStat;
	}

	public void setRtStat(String rtStat) {
		this.rtStat = rtStat;
	}

	public Short getRtState() {
		return this.rtState;
	}

	public void setRtState(Short rtState) {
		this.rtState = rtState;
	}

	public String getShortMessage() {
		return this.shortMessage;
	}

	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}

	public String getSourceAddr() {
		return this.sourceAddr;
	}

	public void setSourceAddr(String sourceAddr) {
		this.sourceAddr = sourceAddr;
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public Long getSpMtRefId() {
		return this.spMtRefId;
	}

	public void setSpMtRefId(Long spMtRefId) {
		this.spMtRefId = spMtRefId;
	}

	public Short getSpMtRefSeq() {
		return this.spMtRefSeq;
	}

	public void setSpMtRefSeq(Short spMtRefSeq) {
		this.spMtRefSeq = spMtRefSeq;
	}

	public Short getSpMtRefTotal() {
		return this.spMtRefTotal;
	}

	public void setSpMtRefTotal(Short spMtRefTotal) {
		this.spMtRefTotal = spMtRefTotal;
	}

	public Timestamp getSpMtTime() {
		return this.spMtTime;
	}

	public void setSpMtTime(Timestamp spMtTime) {
		this.spMtTime = spMtTime;
	}

	public Timestamp getSpRtTime() {
		return this.spRtTime;
	}

	public void setSpRtTime(Timestamp spRtTime) {
		this.spRtTime = spRtTime;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Timestamp getValidTime() {
		return this.validTime;
	}

	public void setValidTime(Timestamp validTime) {
		this.validTime = validTime;
	}

}