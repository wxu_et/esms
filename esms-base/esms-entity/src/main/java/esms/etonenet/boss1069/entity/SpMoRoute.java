package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TM_SP_MO_ROUTE database table.
 * 
 */
@Entity
@Table(name = "TM_SP_MO_ROUTE")
public class SpMoRoute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TM_SP_MO_ROUTE_SPMOROUTEID_GENERATOR", sequenceName = "SEQ_SP_MO_ROUTE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_SP_MO_ROUTE_SPMOROUTEID_GENERATOR")
	@Column(name = "SP_MO_ROUTE_ID")
	private Long spMoRouteId;

	@Column(name = "CONTENT_MATCH_PATTERN")
	private String contentMatchPattern;

	@Column(name = "ROUTE_STATE")
	private BigDecimal routeState;

	@Column(name = "SP_CHANNEL_NUMBER")
	private String spChannelNumber;

	// bi-directional many-to-one association to SpChannel
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID"),
			@JoinColumn(name = "SP_ID", referencedColumnName = "SP_ID"),
			@JoinColumn(name = "SP_SERVICE_CODE", referencedColumnName = "SP_SERVICE_CODE") })
	private SpChannel spChannel;

	public SpMoRoute() {
	}

	public Long getSpMoRouteId() {
		return this.spMoRouteId;
	}

	public void setSpMoRouteId(Long spMoRouteId) {
		this.spMoRouteId = spMoRouteId;
	}

	public String getContentMatchPattern() {
		return this.contentMatchPattern;
	}

	public void setContentMatchPattern(String contentMatchPattern) {
		this.contentMatchPattern = contentMatchPattern;
	}

	public BigDecimal getRouteState() {
		return this.routeState;
	}

	public void setRouteState(BigDecimal routeState) {
		this.routeState = routeState;
	}

	public String getSpChannelNumber() {
		return this.spChannelNumber;
	}

	public void setSpChannelNumber(String spChannelNumber) {
		this.spChannelNumber = spChannelNumber;
	}

	public SpChannel getSpChannel() {
		return this.spChannel;
	}

	public void setSpChannel(SpChannel spChannel) {
		this.spChannel = spChannel;
	}

}