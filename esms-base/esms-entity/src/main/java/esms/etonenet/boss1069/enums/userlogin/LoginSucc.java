package esms.etonenet.boss1069.enums.userlogin;

public enum LoginSucc {

	YES(0, "成功"), NO(1, "失败");

	private final Integer value;
	private final String name;

	private LoginSucc(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return this.value;
	}

	public String getName() {
		return this.name;
	}

	public static LoginSucc fromValue(Integer value) {
		if (value != null) {
			for (LoginSucc state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return YES;
	}

}
