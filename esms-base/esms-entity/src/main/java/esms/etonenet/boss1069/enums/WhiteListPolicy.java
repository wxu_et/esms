package esms.etonenet.boss1069.enums;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum WhiteListPolicy {

	SPARE(BigDecimal.valueOf(1), "免白策略"), ENFORCE(BigDecimal.valueOf(2), "强制加白策略"), AUTO(BigDecimal.valueOf(3),
			"自动增加策略");

	private BigDecimal value;
	private String name;

	private WhiteListPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static WhiteListPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (WhiteListPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return SPARE;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (WhiteListPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
