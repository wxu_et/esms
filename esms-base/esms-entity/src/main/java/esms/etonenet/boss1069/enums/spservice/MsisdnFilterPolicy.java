package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum MsisdnFilterPolicy {

	GOBAL_BLACK(BigDecimal.valueOf(1), "过滤全局MSISDN号码黑名单"), SP_BLACK(BigDecimal.valueOf(2),
			"过滤SP的MSISDN号码黑名单 "), GOBAL_WHITE(BigDecimal.valueOf(4),
					"过滤全局MSISDN号码白名单"), SP_WHITE(BigDecimal.valueOf(8), "过滤SP的MSISDN号码白名单");

	private BigDecimal value;
	private String name;

	private MsisdnFilterPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static MsisdnFilterPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (MsisdnFilterPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return GOBAL_BLACK;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (MsisdnFilterPolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (MsisdnFilterPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
