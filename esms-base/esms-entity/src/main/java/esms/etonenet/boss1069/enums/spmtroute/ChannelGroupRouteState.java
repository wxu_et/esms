package esms.etonenet.boss1069.enums.spmtroute;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ChannelGroupRouteState {

	OPEN(BigDecimal.valueOf(1), "开启"), CLOSE(BigDecimal.valueOf(0), "关闭");

	private BigDecimal value;
	private String name;

	private ChannelGroupRouteState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ChannelGroupRouteState fromValue(BigDecimal value) {
		if (value != null) {
			for (ChannelGroupRouteState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return OPEN;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ChannelGroupRouteState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
