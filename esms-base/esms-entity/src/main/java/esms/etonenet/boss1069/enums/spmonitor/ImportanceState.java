package esms.etonenet.boss1069.enums.spmonitor;

import java.util.LinkedHashMap;
import java.util.Map;

public enum ImportanceState {

	LEVEL1(1, "普通"), LEVEL2(2, "重要"), LEVEL3(3, "高级"), LEVEL4(4, "特级");

	final String name;
	final int value;

	private ImportanceState(int value, String name) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public static ImportanceState formValue(String value) {
		if (value == null) {
			return null;
		}
		for (ImportanceState a : values()) {
			if (value.equals(a.getValue())) {
				return a;
			}
		}
		return LEVEL1;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (ImportanceState a : values()) {
			map.put(Integer.valueOf(a.getValue()).toString(), a.getName());
		}

		return map;
	}

}
