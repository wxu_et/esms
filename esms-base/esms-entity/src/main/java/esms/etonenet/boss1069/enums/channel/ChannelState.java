package esms.etonenet.boss1069.enums.channel;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ChannelState {

	EXPIRED(BigDecimal.valueOf(0), "失效的"), ACTIVED(BigDecimal.valueOf(1), "激活的"), PAUSE(BigDecimal.valueOf(2),
			"暂停的"), OVERLOAD(BigDecimal.valueOf(3), "过载的");

	private BigDecimal value;
	private String name;

	private ChannelState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ChannelState fromValue(BigDecimal value) {
		if (value != null) {
			for (ChannelState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return EXPIRED;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ChannelState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
