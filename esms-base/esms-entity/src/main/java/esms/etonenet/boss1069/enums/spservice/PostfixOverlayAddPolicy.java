package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum PostfixOverlayAddPolicy {

	NONE(BigDecimal.valueOf(0), "不叠加后缀 "), OVERLAY(BigDecimal.valueOf(8), "叠加后缀");

	private BigDecimal value;
	private String name;

	private PostfixOverlayAddPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static PostfixOverlayAddPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (PostfixOverlayAddPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return NONE;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (PostfixOverlayAddPolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (PostfixOverlayAddPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
