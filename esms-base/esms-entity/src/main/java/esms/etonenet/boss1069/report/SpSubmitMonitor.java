package esms.etonenet.boss1069.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.etonenet.anno.LogEntity;

import esms.etonenet.boss1069.entity.Sp;

/**
 * The persistent class for the TM_SP_SUBMIT_MONITOR_RP database table. OS
 */
@SuppressWarnings("serial")
@LogEntity(remark = "客户发送监控")
@Entity
@Table(name = "TM_SP_SUBMIT_MONITOR_RP")
@NamedQuery(name = "SpSubmitMonitor.findAll", query = "SELECT s FROM SpSubmitMonitor s")
public class SpSubmitMonitor implements Serializable {

	@Id
	@SequenceGenerator(name = "TM_SP_SUBMIT_MONITOR_RP_MONITORID_GENERATOR", sequenceName = "SEQ_SP_SUBMIT_MONITOR", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_SP_SUBMIT_MONITOR_RP_MONITORID_GENERATOR")
	@Column(name = "MONITOR_ID", unique = true, nullable = false, precision = 10)
	private Long monitorId;

	@Column(name = "ACTIVE_STATE", precision = 2)
	private BigDecimal activeState;

	@Column(name = "COMPANY_ID", precision = 10)
	private BigDecimal companyId;

	@Column(name = "INTERVAL", precision = 10)
	private BigDecimal interval;

	// @Temporal(TemporalType.DATE)
	@Column(name = "NORMAL_END_TIME")
	private Date normalEndTime;

	@Column(name = "NORMAL_INTERVAL", precision = 10)
	private BigDecimal normalInterval;

	// @Temporal(TemporalType.DATE)
	@Column(name = "NORMAL_START_TIME")
	private Date normalStartTime;

	// uni-directional many-to-one association to Sp
	@ManyToOne
	@JoinColumn(name = "SP_ID")
	private Sp sp;

	public SpSubmitMonitor() {
	}

	public Long getMonitorId() {
		return this.monitorId;
	}

	public void setMonitorId(Long monitorId) {
		this.monitorId = monitorId;
	}

	public BigDecimal getActiveState() {
		return this.activeState;
	}

	public void setActiveState(BigDecimal activeState) {
		this.activeState = activeState;
	}

	public BigDecimal getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public BigDecimal getInterval() {
		return this.interval;
	}

	public void setInterval(BigDecimal interval) {
		this.interval = interval;
	}

	public Date getNormalEndTime() {
		return this.normalEndTime;
	}

	public void setNormalEndTime(Date normalEndTime) {
		this.normalEndTime = normalEndTime;
	}

	public BigDecimal getNormalInterval() {
		return this.normalInterval;
	}

	public void setNormalInterval(BigDecimal normalInterval) {
		this.normalInterval = normalInterval;
	}

	public Date getNormalStartTime() {
		return this.normalStartTime;
	}

	public void setNormalStartTime(Date normalStartTime) {
		this.normalStartTime = normalStartTime;
	}

	public Sp getSp() {
		return this.sp;
	}

	public void setSp(Sp sp) {
		this.sp = sp;
	}

}