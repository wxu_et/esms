package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 过滤形式： 1:类别过滤; 2:内容过滤;
 */
public enum FiltrationType {

	CATEGORY(BigDecimal.valueOf(1), "类别"), CONTENT(BigDecimal.valueOf(2), "内容");

	private final BigDecimal value;
	private final String name;

	private FiltrationType(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public static FiltrationType fromValue(BigDecimal value) {
		if (value != null) {
			for (FiltrationType ft : values()) {
				if (value.equals(ft.value))
					return ft;
			}
		}
		return CATEGORY;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (FiltrationType ft : values()) {
			map.put(ft.value.toString(), ft.name);
		}
		return map;
	}
}
