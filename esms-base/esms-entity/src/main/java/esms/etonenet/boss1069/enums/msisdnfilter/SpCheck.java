package esms.etonenet.boss1069.enums.msisdnfilter;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SpCheck {
	YES(BigDecimal.valueOf(0), "是"), NO(BigDecimal.valueOf(1), "否");

	private final BigDecimal value;
	private final String name;

	private SpCheck(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public static SpCheck fromValue(BigDecimal value) {
		if (value != null) {
			for (SpCheck sp : values()) {
				if (value.equals(sp.value))
					return sp;
			}
		}
		return YES;
	}

	public static Map<String, String> todisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (SpCheck sp : values()) {
			map.put(sp.value.toString(), sp.name);
		}
		return map;
	}

}
