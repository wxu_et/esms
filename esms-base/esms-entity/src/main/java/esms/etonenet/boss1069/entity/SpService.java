package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_SP_SERVICE database table.
 * 
 */
@LogEntity(remark = "SP服务")
@Entity
@Table(name = "TM_SP_SERVICE")
public class SpService implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SpServicePK id;

	@Column(name = "CONTENT_FILTER_CATEGORY_FLAG")
	@Digits(fraction = 0, integer = 10)
	private BigDecimal contentFilterCategoryFlag;

	@Column(name = "CONTENT_FILTER_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal contentFilterPolicy;

	@Column(name = "CONTENT_POSTFIX_PATTERN")
	@Size(max = 60)
	private String contentPostfixPattern;

	@Column(name = "CONTENT_PREFIX_PATTERN")
	@Size(max = 60)
	private String contentPrefixPattern;

	@Column(name = "CONTENT_PROCESS_POLICY")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal contentProcessPolicy;

	@Column(name = "CONTENT_SPLIT_PATTERN")
	@Size(max = 30)
	private String contentSplitPattern;

	@Column(name = "MSISDN_FILTER_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal msisdnFilterPolicy;

	@Column(name = "PRIORITY_PATTERN")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal priorityPattern;

	@Column(name = "PRIORITY_POLICY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal priorityPolicy;

	@Column(name = "SP_SERVICE_NAME")
	@Size(max = 60)
	private String spServiceName;

	@Column(name = "SPSC_EXT_NUMBER")
	@Size(max = 20)
	private String spscExtNumber;

	// bi-directional many-to-one association to Sp
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SP_ID", insertable = false, updatable = false)
	private Sp sp;

	public SpService() {
	}

	public SpServicePK getId() {
		return this.id;
	}

	public void setId(SpServicePK id) {
		this.id = id;
	}

	public BigDecimal getContentFilterCategoryFlag() {
		return this.contentFilterCategoryFlag;
	}

	public void setContentFilterCategoryFlag(BigDecimal contentFilterCategoryFlag) {
		this.contentFilterCategoryFlag = contentFilterCategoryFlag;
	}

	public BigDecimal getContentFilterPolicy() {
		return this.contentFilterPolicy;
	}

	public void setContentFilterPolicy(BigDecimal contentFilterPolicy) {
		this.contentFilterPolicy = contentFilterPolicy;
	}

	public String getContentPostfixPattern() {
		return this.contentPostfixPattern;
	}

	public void setContentPostfixPattern(String contentPostfixPattern) {
		this.contentPostfixPattern = contentPostfixPattern;
	}

	public String getContentPrefixPattern() {
		return this.contentPrefixPattern;
	}

	public void setContentPrefixPattern(String contentPrefixPattern) {
		this.contentPrefixPattern = contentPrefixPattern;
	}

	public BigDecimal getContentProcessPolicy() {
		return this.contentProcessPolicy;
	}

	public void setContentProcessPolicy(BigDecimal contentProcessPolicy) {
		this.contentProcessPolicy = contentProcessPolicy;
	}

	public String getContentSplitPattern() {
		return this.contentSplitPattern;
	}

	public void setContentSplitPattern(String contentSplitPattern) {
		this.contentSplitPattern = contentSplitPattern;
	}

	public BigDecimal getMsisdnFilterPolicy() {
		return this.msisdnFilterPolicy;
	}

	public void setMsisdnFilterPolicy(BigDecimal msisdnFilterPolicy) {
		this.msisdnFilterPolicy = msisdnFilterPolicy;
	}

	public BigDecimal getPriorityPattern() {
		return this.priorityPattern;
	}

	public void setPriorityPattern(BigDecimal priorityPattern) {
		this.priorityPattern = priorityPattern;
	}

	public BigDecimal getPriorityPolicy() {
		return this.priorityPolicy;
	}

	public void setPriorityPolicy(BigDecimal priorityPolicy) {
		this.priorityPolicy = priorityPolicy;
	}

	public String getSpServiceName() {
		return this.spServiceName;
	}

	public void setSpServiceName(String spServiceName) {
		this.spServiceName = spServiceName;
	}

	public String getSpscExtNumber() {
		return this.spscExtNumber;
	}

	public void setSpscExtNumber(String spscExtNumber) {
		this.spscExtNumber = spscExtNumber;
	}

	public Sp getSp() {
		return this.sp;
	}

	public void setSp(Sp sp) {
		this.sp = sp;
	}

}