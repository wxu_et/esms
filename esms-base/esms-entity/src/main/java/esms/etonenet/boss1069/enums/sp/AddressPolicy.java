package esms.etonenet.boss1069.enums.sp;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum AddressPolicy {

	MONULL(BigDecimal.valueOf(0), "下行源地址/上行目的地址为空"), MOSPEXT(BigDecimal.valueOf(1),
			"下行源地址/上行目的地址为SP用户扩展号码"), MOSPSPEXT(BigDecimal.valueOf(2), "下行源地址/上行目的地址为SP通道号码+SP用户扩展号码");

	private BigDecimal value;
	private String name;

	private AddressPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static AddressPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (AddressPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return MONULL;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (AddressPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
