package esms.etonenet.boss1069.enums.msisdnSegment;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum DataLevel {

	ONE(BigDecimal.valueOf(1), "1"), TWO(BigDecimal.valueOf(2), "2"), THREE(BigDecimal.valueOf(3),
			"3"), FOUR(BigDecimal.valueOf(4), "4"),

	FIVE(BigDecimal.valueOf(5), "5"), SIX(BigDecimal.valueOf(6), "6"), SEVEN(BigDecimal.valueOf(7),
			"7"), EIGHT(BigDecimal.valueOf(8), "8"), NINE(BigDecimal.valueOf(9), "9");

	private final BigDecimal value;
	private final String name;

	private DataLevel(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static DataLevel fromValue(BigDecimal value) {
		if (value != null) {
			for (DataLevel d : values()) {
				if (value.equals(d.value))
					return d;
			}
		}
		return ONE;
	}

	public static Map<Integer, String> toDisplayMap() {
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		for (DataLevel d : values()) {
			map.put(Integer.valueOf(d.value.toString()), d.name);
		}
		return map;
	}

}
