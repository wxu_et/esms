package esms.etonenet.boss1069.enums.msisdnfilter;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum MsisdnFilterState {

	WHITELIST(BigDecimal.valueOf(1), "白名单"), BLACKLIST(BigDecimal.valueOf(2), "黑名单");
	private final BigDecimal value;
	private final String name;

	private MsisdnFilterState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static MsisdnFilterState fromValue(BigDecimal value) {
		if (value != null) {
			for (MsisdnFilterState mfs : values()) {
				if (value.equals(mfs.value))
					return mfs;
			}
		}
		return WHITELIST;
	}

	public static Map<String, String> todisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (MsisdnFilterState mfs : values()) {
			map.put(mfs.value.toString(), mfs.name);
		}
		return map;
	}

}
