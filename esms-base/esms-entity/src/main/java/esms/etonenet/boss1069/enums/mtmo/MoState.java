package esms.etonenet.boss1069.enums.mtmo;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SMS上行状态枚举类
 * 
 * @author kyu
 *
 */
public enum MoState {

	MOINIT(BigDecimal.valueOf(1), "上行消息初始状态"), MOWAITINGPPROCESSING(BigDecimal.valueOf(3), "上行等待处理"), MOTOPERSONFILTER(
			BigDecimal.valueOf(4), "上行通过号段检查"), MOPASSNUMBERSECTIONCHECK(BigDecimal.valueOf(5),
					"上行通过内容过滤"), MOPASSNUMBERFILTER(BigDecimal.valueOf(6), "上行通过号码过滤"), MOUPSTREAMROUTINGSUCCESS(
							BigDecimal.valueOf(7),
							"上行路由成功"), MOBALANCEHANDLINGSUCCESS(BigDecimal.valueOf(8), "上行余额处理成功"), MOBAN(
									BigDecimal.valueOf(30),
									"上行禁止"), MOWAITINGTOSENDTIME(BigDecimal.valueOf(31), "上行等待可发送时间"), MOWAITINGTOSEND(
											BigDecimal.valueOf(32),
											"上行等待发送"), MOWAITINGTOSENDRESULT(BigDecimal.valueOf(33),
													"上行等待发送结果"), MOSUCCESS(BigDecimal.valueOf(34), "上行成功"), MOFAILURE(
															BigDecimal.valueOf(35),
															"上行失败"), MOAGAIN(BigDecimal.valueOf(36), "上行重试"), MOSTOP(
																	BigDecimal.valueOf(37),
																	"上行终止"), MORETRYWAITINGFORRESULLT(
																			BigDecimal.valueOf(38),
																			"上行重试等待结果"), MSISDNNUMBERSECTIONERROR(
																					BigDecimal.valueOf(41),
																					"MSISDN号码段错误"), MSISDNWRONGNUMBER(
																							BigDecimal.valueOf(42),
																							"MSISDN号码错误"), CONTENTFILTERINGERROR(
																									BigDecimal.valueOf(
																											43),
																									"内容过滤错误"), ROUTEERRORS(
																											BigDecimal
																													.valueOf(
																															44),
																											"路由错误"), CONTENTPROCESSINGERROR(
																													BigDecimal
																															.valueOf(
																																	45),
																													"内容处理错误"), QUOTAPROBLEM(
																															BigDecimal
																																	.valueOf(
																																			46),
																															"配额不足"), MOWAITINGFORMASSSENDING(
																																	BigDecimal
																																			.valueOf(
																																					52),
																																	"上行等待批量发送"), WAITINGFORBULKTOENDRESULTS(
																																			BigDecimal
																																					.valueOf(
																																							53),
																																			"上行等待批量发送结果"), BATCHUPSUCCESS(
																																					BigDecimal
																																							.valueOf(
																																									54),
																																					"批量上行成功"), BATCHUPFAILURE(
																																							BigDecimal
																																									.valueOf(
																																											55),
																																							"批量上行失败"), BATCHUPAGAIN(
																																									BigDecimal
																																											.valueOf(
																																													56),
																																									"批量上行重试"), BATCHUPSTOP(
																																											BigDecimal
																																													.valueOf(
																																															57),
																																											"批量上行终止"), BATCHUPRETRYWAITINGFORRESULT(
																																													BigDecimal
																																															.valueOf(
																																																	58),
																																													"批量上行重试等待结果");

	private MoState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public static MoState fromValue(BigDecimal value) {
		if (value != null) {
			for (MoState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return MOINIT;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (MoState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}

	public static Map<String, String> toDisplayMap2() {
		Map<String, String> m = new LinkedHashMap<>();
		for (MoState e : values()) {
			m.put(e.value.toString(), e.value.toString() + "-" + e.name);
		}
		return m;
	}

	private BigDecimal value;

	private String name;

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
