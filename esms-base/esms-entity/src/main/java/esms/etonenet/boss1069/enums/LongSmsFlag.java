package esms.etonenet.boss1069.enums;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum LongSmsFlag {

	DISABLE(BigDecimal.valueOf(0), "不可以"), PARTITION(BigDecimal.valueOf(1), "分拆式长短信"), ENTIRE(BigDecimal.valueOf(2),
			"整条式长短信");

	private BigDecimal value;
	private String name;

	private LongSmsFlag(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static LongSmsFlag fromValue(BigDecimal value) {
		if (value != null) {
			for (LongSmsFlag state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return DISABLE;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (LongSmsFlag e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
