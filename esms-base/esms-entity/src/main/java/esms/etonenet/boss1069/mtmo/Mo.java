package esms.etonenet.boss1069.mtmo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TT_MO database table.
 * 
 */
@Entity
@Table(name = "TT_MO")
@NamedQuery(name = "Mo.findAll", query = "SELECT m FROM Mo m")
public class Mo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SP_MO_ID")
	private Long spMoId;

	@Column(name = "CARRIER_CODE")
	private String carrierCode;

	@Column(name = "CARRIER_MO_TIME")
	private Timestamp carrierMoTime;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "CHANNEL_MESSAGE_ID")
	private String channelMessageId;

	@Column(name = "CITY_CODE")
	private String cityCode;

	@Column(name = "COST_BATCHID")
	private Long costBatchid;

	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "DATA_CODING")
	private Short dataCoding;

	@Column(name = "DESTINATION_ADDR")
	private String destinationAddr;

	@Column(name = "ESM_CLASS")
	private Short esmClass;

	@Column(name = "MO_ERR")
	private String moErr;

	@Column(name = "MO_STAT")
	private String moStat;

	@Column(name = "MO_STATE")
	private Short moState;

	@Column(name = "PROTOCOL_ID")
	private Short protocolId;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "REVENUE_BATCHID")
	private BigDecimal revenueBatchid;

	@Column(name = "SHORT_MESSAGE")
	private String shortMessage;

	@Column(name = "SOURCE_ADDR")
	private String sourceAddr;

	@Column(name = "SP_ID")
	private String spId;

	@Column(name = "SP_MO_TIME")
	private Timestamp spMoTime;

	@Column(name = "SP_SERVICE_CODE")
	private String spServiceCode;

	@Column(name = "USER_ID")
	private Long userId;

	public Mo() {
	}

	public Long getSpMoId() {
		return this.spMoId;
	}

	public void setSpMoId(Long spMoId) {
		this.spMoId = spMoId;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Timestamp getCarrierMoTime() {
		return this.carrierMoTime;
	}

	public void setCarrierMoTime(Timestamp carrierMoTime) {
		this.carrierMoTime = carrierMoTime;
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelMessageId() {
		return this.channelMessageId;
	}

	public void setChannelMessageId(String channelMessageId) {
		this.channelMessageId = channelMessageId;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Long getCostBatchid() {
		return this.costBatchid;
	}

	public void setCostBatchid(Long costBatchid) {
		this.costBatchid = costBatchid;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Short getDataCoding() {
		return this.dataCoding;
	}

	public void setDataCoding(Short dataCoding) {
		this.dataCoding = dataCoding;
	}

	public String getDestinationAddr() {
		return this.destinationAddr;
	}

	public void setDestinationAddr(String destinationAddr) {
		this.destinationAddr = destinationAddr;
	}

	public Short getEsmClass() {
		return this.esmClass;
	}

	public void setEsmClass(Short esmClass) {
		this.esmClass = esmClass;
	}

	public String getMoErr() {
		return this.moErr;
	}

	public void setMoErr(String moErr) {
		this.moErr = moErr;
	}

	public String getMoStat() {
		return this.moStat;
	}

	public void setMoStat(String moStat) {
		this.moStat = moStat;
	}

	public Short getMoState() {
		return this.moState;
	}

	public void setMoState(Short moState) {
		this.moState = moState;
	}

	public Short getProtocolId() {
		return this.protocolId;
	}

	public void setProtocolId(Short protocolId) {
		this.protocolId = protocolId;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public BigDecimal getRevenueBatchid() {
		return this.revenueBatchid;
	}

	public void setRevenueBatchid(BigDecimal revenueBatchid) {
		this.revenueBatchid = revenueBatchid;
	}

	public String getShortMessage() {
		return this.shortMessage;
	}

	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}

	public String getSourceAddr() {
		return this.sourceAddr;
	}

	public void setSourceAddr(String sourceAddr) {
		this.sourceAddr = sourceAddr;
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public Timestamp getSpMoTime() {
		return this.spMoTime;
	}

	public void setSpMoTime(Timestamp spMoTime) {
		this.spMoTime = spMoTime;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}