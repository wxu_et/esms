package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL_BAOBEI_INFO database table.
 * 
 */
@LogEntity(remark = "通道报备")
@Entity
@Table(name = "TM_CHANNEL_BAOBEI_INFO")
@NamedQuery(name = "ChannelBaobeiInfo.findAll", query = "SELECT c FROM ChannelBaobeiInfo c")
public class ChannelBaobeiInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TM_CHANNEL_BAOBEI_INFO_CHANNELBAOBEIINFOID_GENERATOR", sequenceName = "SEQ_CHANNEL_BAOBEI_INFO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_CHANNEL_BAOBEI_INFO_CHANNELBAOBEIINFOID_GENERATOR")
	@Column(name = "CHANNEL_BAOBEI_INFO_ID", unique = true, nullable = false, precision = 10)
	private Long channelBaobeiInfoId;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "BAOBEI_DATA_STATE", precision = 4, scale = 0)
	private BigDecimal baobeiDataState;

	@Size(max = 20)
	@NotNull
	@Column(name = "BAOBEI_NUMBER", length = 20)
	private String baobeiNumber;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_LENGTH", precision = 4, scale = 0)
	private BigDecimal channelMessageLength;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_LENGTH_EN", precision = 4, scale = 0)
	private BigDecimal channelMessageLengthEn;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH", precision = 4, scale = 0)
	private BigDecimal channelMessageOrgLength;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH_EN", precision = 4, scale = 0)
	private BigDecimal channelMessageOrgLengthEn;

	@Size(max = 30)
	@Column(name = "CHANNEL_MESSAGE_SIGNATURE", length = 30)
	private String channelMessageSignature;

	@Size(max = 30)
	@Column(name = "CHANNEL_MESSAGE_SIGNATURE_EN", length = 30)
	private String channelMessageSignatureEn;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "LONG_SMS_FLAG", precision = 4, scale = 0)
	private BigDecimal longSmsFlag;

	@Size(max = 500)
	@Column(length = 500)
	private String remark;

	// bi-directional many-to-one association to Channel
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID")
	private Channel channel;

	public ChannelBaobeiInfo() {
	}

	public Long getChannelBaobeiInfoId() {
		return this.channelBaobeiInfoId;
	}

	public void setChannelBaobeiInfoId(Long channelBaobeiInfoId) {
		this.channelBaobeiInfoId = channelBaobeiInfoId;
	}

	public BigDecimal getBaobeiDataState() {
		return this.baobeiDataState;
	}

	public void setBaobeiDataState(BigDecimal baobeiDataState) {
		this.baobeiDataState = baobeiDataState;
	}

	public String getBaobeiNumber() {
		return this.baobeiNumber;
	}

	public void setBaobeiNumber(String baobeiNumber) {
		this.baobeiNumber = baobeiNumber;
	}

	public BigDecimal getChannelMessageLength() {
		return this.channelMessageLength;
	}

	public void setChannelMessageLength(BigDecimal channelMessageLength) {
		this.channelMessageLength = channelMessageLength;
	}

	public BigDecimal getChannelMessageLengthEn() {
		return this.channelMessageLengthEn;
	}

	public void setChannelMessageLengthEn(BigDecimal channelMessageLengthEn) {
		this.channelMessageLengthEn = channelMessageLengthEn;
	}

	public BigDecimal getChannelMessageOrgLength() {
		return this.channelMessageOrgLength;
	}

	public void setChannelMessageOrgLength(BigDecimal channelMessageOrgLength) {
		this.channelMessageOrgLength = channelMessageOrgLength;
	}

	public BigDecimal getChannelMessageOrgLengthEn() {
		return this.channelMessageOrgLengthEn;
	}

	public void setChannelMessageOrgLengthEn(BigDecimal channelMessageOrgLengthEn) {
		this.channelMessageOrgLengthEn = channelMessageOrgLengthEn;
	}

	public String getChannelMessageSignature() {
		return this.channelMessageSignature;
	}

	public void setChannelMessageSignature(String channelMessageSignature) {
		this.channelMessageSignature = channelMessageSignature;
	}

	public String getChannelMessageSignatureEn() {
		return this.channelMessageSignatureEn;
	}

	public void setChannelMessageSignatureEn(String channelMessageSignatureEn) {
		this.channelMessageSignatureEn = channelMessageSignatureEn;
	}

	public BigDecimal getLongSmsFlag() {
		return this.longSmsFlag;
	}

	public void setLongSmsFlag(BigDecimal longSmsFlag) {
		this.longSmsFlag = longSmsFlag;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}