package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum PriorityPolicy {

	SYSTEM(BigDecimal.valueOf(0), "优先级为系统自动设置"), STATIC(BigDecimal.valueOf(1),
			"优先级为静态设置"), DYNAMIC(BigDecimal.valueOf(2), "优先级为动态设置"), CUSTOM(BigDecimal.valueOf(3), "自定义优先级算法");

	private BigDecimal value;
	private String name;

	private PriorityPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static PriorityPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (PriorityPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return SYSTEM;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (PriorityPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}

}
