package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL_GROUP database table.
 * 
 */
@LogEntity(remark = "通道组")
@Entity
@Table(name = "TM_CHANNEL_GROUP")
@NamedQuery(name = "ChannelGroup.findAll", query = "SELECT c FROM ChannelGroup c")
public class ChannelGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Size(max = 10)
	@NotNull
	@Column(name = "CHANNEL_GROUP_ID", length = 10, unique = true, nullable = false)
	private String channelGroupId;

	@Size(max = 60)
	@NotNull
	@Column(name = "CHANNEL_GROUP_NAME", length = 60)
	private String channelGroupName;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "LOGIC_STAT", precision = 2, scale = 0)
	private BigDecimal logicStat;

	@Size(max = 500)
	@Column(length = 500)
	private String remark;

	public ChannelGroup() {
	}

	public String getChannelGroupId() {
		return this.channelGroupId;
	}

	public void setChannelGroupId(String channelGroupId) {
		this.channelGroupId = channelGroupId;
	}

	public String getChannelGroupName() {
		return this.channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}

	public BigDecimal getLogicStat() {
		return this.logicStat;
	}

	public void setLogicStat(BigDecimal logicStat) {
		this.logicStat = logicStat;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}