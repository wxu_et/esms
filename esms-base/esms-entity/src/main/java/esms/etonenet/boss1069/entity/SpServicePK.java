package esms.etonenet.boss1069.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 * The primary key class for the TM_SP_SERVICE database table.
 * 
 */
@Embeddable
public class SpServicePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "SP_SERVICE_CODE")
	@Size(max = 10)
	private String spServiceCode;

	@Column(name = "SP_ID", insertable = false, updatable = false)
	@Size(max = 6)
	private String spId;

	public SpServicePK() {
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	@Override
	public String toString() {
		return "SpServicePK [spServiceCode=" + spServiceCode + ", spId=" + spId + "]";
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SpServicePK)) {
			return false;
		}
		SpServicePK castOther = (SpServicePK) other;
		return this.spServiceCode.equals(castOther.spServiceCode) && this.spId.equals(castOther.spId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.spServiceCode.hashCode();
		hash = hash * prime + this.spId.hashCode();

		return hash;
	}
}