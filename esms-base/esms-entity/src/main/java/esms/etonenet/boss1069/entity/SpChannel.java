package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_SP_CHANNEL database table.
 * 
 */
@LogEntity(remark = "SP通道")
@Entity
@Table(name = "TM_SP_SC_CHANNEL")
public class SpChannel implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SpChannelPK id;

	@Column(name = "SP_CHANNEL_NUMBER")
	@Size(max = 21)
	private String spChannelNumber;

	@Column(name = "SP_CHANNEL_PRIORITY")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal spChannelPriority;

	@Column(name = "SP_CHANNEL_TIME_SLICE_NUMBER")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal spChannelTimeSliceNumber;

	@Column(name = "SP_CHANNEL_WEIGHT_NUMBER")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal spChannelWeightNumber;

	// bi-directional many-to-one association to Channel
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_ID", insertable = false, updatable = false)
	private Channel channel;

	// bi-directional many-to-one association to SpService
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "SP_ID", referencedColumnName = "SP_ID", insertable = false, updatable = false),
			@JoinColumn(name = "SP_SERVICE_CODE", referencedColumnName = "SP_SERVICE_CODE", insertable = false, updatable = false) })
	private SpService spService;

	public SpChannel() {
	}

	public SpChannelPK getId() {
		return this.id;
	}

	public void setId(SpChannelPK id) {
		this.id = id;
	}

	public String getSpChannelNumber() {
		return this.spChannelNumber;
	}

	public void setSpChannelNumber(String spChannelNumber) {
		this.spChannelNumber = spChannelNumber;
	}

	public BigDecimal getSpChannelPriority() {
		return this.spChannelPriority;
	}

	public void setSpChannelPriority(BigDecimal spChannelPriority) {
		this.spChannelPriority = spChannelPriority;
	}

	public BigDecimal getSpChannelTimeSliceNumber() {
		return this.spChannelTimeSliceNumber;
	}

	public void setSpChannelTimeSliceNumber(BigDecimal spChannelTimeSliceNumber) {
		this.spChannelTimeSliceNumber = spChannelTimeSliceNumber;
	}

	public BigDecimal getSpChannelWeightNumber() {
		return this.spChannelWeightNumber;
	}

	public void setSpChannelWeightNumber(BigDecimal spChannelWeightNumber) {
		this.spChannelWeightNumber = spChannelWeightNumber;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public SpService getSpService() {
		return this.spService;
	}

	public void setSpService(SpService spService) {
		this.spService = spService;
	}
}