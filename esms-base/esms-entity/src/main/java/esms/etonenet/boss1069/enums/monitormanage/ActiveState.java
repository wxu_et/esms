package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ActiveState {

	ACTIVE(BigDecimal.valueOf(1), "激活"), FORBID(BigDecimal.valueOf(0), "禁止");

	final String name;
	final BigDecimal value;

	private ActiveState(BigDecimal value, String name) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public static ActiveState formValue(String value) {
		if (value == null) {
			return null;
		}
		for (ActiveState a : values()) {
			if (value.equals(a.getValue().toString())) {
				return a;
			}
		}
		return ACTIVE;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (ActiveState a : values()) {
			map.put(a.getValue().toString(), a.getName());
		}

		return map;
	}

}
