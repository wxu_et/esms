package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL database table.
 * 
 */
@LogEntity(remark = "通道")
@Entity
@Table(name = "TM_CHANNEL")
public class Channel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Size(max = 4)
	@NotNull
	@Column(name = "CHANNEL_ID", unique = true, nullable = false, length = 4)
	@Digits(fraction = 0, integer = 4)
	private String channelId;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_BAOBEI_FLAG", precision = 4, scale = 0)
	private BigDecimal channelBaobeiFlag;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "CHANNEL_MESSAGE_LENGTH", precision = 2, scale = 0)
	private BigDecimal channelMessageLength;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_LENGTH_EN", precision = 4, scale = 0)
	private BigDecimal channelMessageLengthEn;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH", precision = 4, scale = 0)
	private BigDecimal channelMessageOrgLength;

	@Digits(fraction = 0, integer = 4)
	@Column(name = "CHANNEL_MESSAGE_ORG_LENGTH_EN", precision = 4, scale = 0)
	private BigDecimal channelMessageOrgLengthEn;

	@Size(max = 30)
	@Column(name = "CHANNEL_MESSAGE_SIGNATURE", length = 30)
	private String channelMessageSignature;

	@Size(max = 30)
	@Column(name = "CHANNEL_MESSAGE_SIGNATURE_EN", length = 30)
	private String channelMessageSignatureEn;

	@Digits(fraction = 0, integer = 10)
	@Column(name = "CHANNEL_MTMO_FLAG", precision = 10, scale = 0)
	private BigDecimal channelMtmoFlag;

	@Size(max = 60)
	@Column(name = "CHANNEL_NAME", length = 60)
	private String channelName;

	@Size(max = 21)
	@Column(name = "CHANNEL_NUMBER", length = 21)
	private String channelNumber;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "CHANNEL_NUMBER_LENGTH", precision = 2, scale = 0)
	private BigDecimal channelNumberLength;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "CHANNEL_SPEED", precision = 10, scale = 0)
	private BigDecimal channelSpeed;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "CHANNEL_STATE", precision = 2, scale = 0)
	private BigDecimal channelState;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "LONG_SMS_FLAG", precision = 2, scale = 0)
	private BigDecimal longSmsFlag;

	@NotNull
	@Digits(fraction = 0, integer = 2)
	@Column(name = "WHITE_LIST_POLICY", precision = 2, scale = 0)
	private BigDecimal whiteListPolicy;

	public Channel() {
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public BigDecimal getChannelBaobeiFlag() {
		return this.channelBaobeiFlag;
	}

	public void setChannelBaobeiFlag(BigDecimal channelBaobeiFlag) {
		this.channelBaobeiFlag = channelBaobeiFlag;
	}

	public BigDecimal getChannelMessageLength() {
		return this.channelMessageLength;
	}

	public void setChannelMessageLength(BigDecimal channelMessageLength) {
		this.channelMessageLength = channelMessageLength;
	}

	public BigDecimal getChannelMessageLengthEn() {
		return this.channelMessageLengthEn;
	}

	public void setChannelMessageLengthEn(BigDecimal channelMessageLengthEn) {
		this.channelMessageLengthEn = channelMessageLengthEn;
	}

	public BigDecimal getChannelMessageOrgLength() {
		return this.channelMessageOrgLength;
	}

	public void setChannelMessageOrgLength(BigDecimal channelMessageOrgLength) {
		this.channelMessageOrgLength = channelMessageOrgLength;
	}

	public BigDecimal getChannelMessageOrgLengthEn() {
		return this.channelMessageOrgLengthEn;
	}

	public void setChannelMessageOrgLengthEn(BigDecimal channelMessageOrgLengthEn) {
		this.channelMessageOrgLengthEn = channelMessageOrgLengthEn;
	}

	public String getChannelMessageSignature() {
		return this.channelMessageSignature;
	}

	public void setChannelMessageSignature(String channelMessageSignature) {
		this.channelMessageSignature = channelMessageSignature;
	}

	public String getChannelMessageSignatureEn() {
		return this.channelMessageSignatureEn;
	}

	public void setChannelMessageSignatureEn(String channelMessageSignatureEn) {
		this.channelMessageSignatureEn = channelMessageSignatureEn;
	}

	public BigDecimal getChannelMtmoFlag() {
		return this.channelMtmoFlag;
	}

	public void setChannelMtmoFlag(BigDecimal channelMtmoFlag) {
		this.channelMtmoFlag = channelMtmoFlag;
	}

	public String getChannelName() {
		return this.channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelNumber() {
		return this.channelNumber;
	}

	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}

	public BigDecimal getChannelNumberLength() {
		return this.channelNumberLength;
	}

	public void setChannelNumberLength(BigDecimal channelNumberLength) {
		this.channelNumberLength = channelNumberLength;
	}

	public BigDecimal getChannelSpeed() {
		return this.channelSpeed;
	}

	public void setChannelSpeed(BigDecimal channelSpeed) {
		this.channelSpeed = channelSpeed;
	}

	public BigDecimal getChannelState() {
		return this.channelState;
	}

	public void setChannelState(BigDecimal channelState) {
		this.channelState = channelState;
	}

	public BigDecimal getLongSmsFlag() {
		return this.longSmsFlag;
	}

	public void setLongSmsFlag(BigDecimal longSmsFlag) {
		this.longSmsFlag = longSmsFlag;
	}

	public BigDecimal getWhiteListPolicy() {
		return this.whiteListPolicy;
	}

	public void setWhiteListPolicy(BigDecimal whiteListPolicy) {
		this.whiteListPolicy = whiteListPolicy;
	}

}