package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author zhshen 优先级
 */
public enum Priority {

	NORMALPRIORITY(BigDecimal.valueOf(0), "普通优先级"), MEDIUMPRIORITY(BigDecimal.valueOf(1),
			"中优先级"), HIGHPRIORITY(BigDecimal.valueOf(2), "高级优先级");
	private BigDecimal value;
	private String name;

	private Priority(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public static Priority fromValue(BigDecimal value) {
		if (value != null) {
			for (Priority p : values()) {
				if (p.value.equals(value))
					return p;
			}
		}
		return NORMALPRIORITY;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (Priority p : values()) {
			map.put(p.value.toString(), p.name);
		}
		return map;
	}

}
