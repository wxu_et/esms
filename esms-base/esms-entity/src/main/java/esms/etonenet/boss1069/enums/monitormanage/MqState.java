package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 监控状态
 * 
 * @author zhshen
 *
 */
public enum MqState {

	MONITORING(BigDecimal.valueOf(0), "监控"), NOTMONITORING(BigDecimal.valueOf(1), "不监控");
	private BigDecimal value;
	private String name;

	private MqState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static MqState fromValue(BigDecimal value) {
		if (value != null) {
			for (MqState state : values()) {
				if (state.value.equals(value))
					return state;
			}
		}
		return MONITORING;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (MqState state : values())
			map.put(state.value.toString(), state.name);
		return map;
	}

}