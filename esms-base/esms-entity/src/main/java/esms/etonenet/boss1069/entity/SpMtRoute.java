package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_SP_MT_ROUTE database table.
 * 
 */
@LogEntity(remark = "SP下行路由")
@Entity
@Table(name = "TM_SP_MT_ROUTE")
@NamedQuery(name = "SpMtRoute.findAll", query = "SELECT s FROM SpMtRoute s")
public class SpMtRoute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TM_SP_MT_ROUTE_SPMTROUTEID_GENERATOR", sequenceName = "SEQ_SP_MT_ROUTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_SP_MT_ROUTE_SPMTROUTEID_GENERATOR")
	@Digits(fraction = 0, integer = 10)
	private Long spMtRouteId;

	@Column(name = "CARRIER_CODE")
	@Size(max = 6)
	private String carrierCode;

	@Column(name = "CHANNEL_GROUP_ROUTE_STATE")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal channelGroupRouteState;

	@Column(name = "CITY_CODE")
	@Size(max = 6)
	private String cityCode;

	@Column(name = "COUNTRY_CODE")
	@Size(max = 6)
	private String countryCode;

	@Column(name = "PROVINCE_CODE")
	@Size(max = 6)
	private String provinceCode;

	@Column(name = "ROUTE_STATE")
	@Digits(fraction = 0, integer = 4)
	private BigDecimal routeState;

	@Column(name = "ROUTE_TYPE")
	@Digits(fraction = 0, integer = 2)
	private BigDecimal routeType;

	@Column(name = "SP_SERVICE_CODE")
	@Size(max = 10)
	private String spServiceCode;

	// bi-directional many-to-one association to TmChannel
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID")
	private Channel channel;

	// bi-directional many-to-one association to TmSp
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "SP_ID")
	private Sp sp;

	// bi-directional many-to-one association to TmChannelGroup
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "CHANNEL_GROUP_ID")
	private ChannelGroup channelGroup;

	public SpMtRoute() {
	}

	public Long getSpMtRouteId() {
		return this.spMtRouteId;
	}

	public void setSpMtRouteId(Long spMtRouteId) {
		this.spMtRouteId = spMtRouteId;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public BigDecimal getChannelGroupRouteState() {
		return this.channelGroupRouteState;
	}

	public void setChannelGroupRouteState(BigDecimal channelGroupRouteState) {
		this.channelGroupRouteState = channelGroupRouteState;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public BigDecimal getRouteState() {
		return this.routeState;
	}

	public void setRouteState(BigDecimal routeState) {
		this.routeState = routeState;
	}

	public BigDecimal getRouteType() {
		return this.routeType;
	}

	public void setRouteType(BigDecimal routeType) {
		this.routeType = routeType;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public Sp getSp() {
		return sp;
	}

	public void setSp(Sp sp) {
		this.sp = sp;
	}

	public ChannelGroup getChannelGroup() {
		return channelGroup;
	}

	public void setChannelGroup(ChannelGroup channelGroup) {
		this.channelGroup = channelGroup;
	}

}