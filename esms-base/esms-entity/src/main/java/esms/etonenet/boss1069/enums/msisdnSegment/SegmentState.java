package esms.etonenet.boss1069.enums.msisdnSegment;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SegmentState {

	INACTIVE(BigDecimal.valueOf(0), "未启用的"), ACTIVE(BigDecimal.valueOf(1), "启用的"), PAUSE(BigDecimal.valueOf(2), "停用的");

	private final BigDecimal value;
	private final String name;

	private SegmentState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static SegmentState fromValue(BigDecimal value) {
		if (value != null) {
			for (SegmentState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return ACTIVE;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SegmentState s : values()) {
			m.put(s.value.toString(), s.name);
		}
		return m;
	}
}
