package esms.etonenet.boss1069.enums.spwarn;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SpWarnState {

	COMMON(BigDecimal.valueOf(0), "普通"), IMPORTANCE(BigDecimal.valueOf(1), "重要"), SENIOR(BigDecimal.valueOf(2),
			"高级"), SPECIAL(BigDecimal.valueOf(3), "特级");

	private BigDecimal value;
	private String name;

	private SpWarnState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static SpWarnState fromValue(BigDecimal value) {
		if (value != null) {
			for (SpWarnState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return COMMON;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SpWarnState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
