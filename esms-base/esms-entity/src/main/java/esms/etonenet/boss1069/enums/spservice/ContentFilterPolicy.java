package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ContentFilterPolicy {

	AUTO(BigDecimal.valueOf(1), "自动过滤"), MANUEL(BigDecimal.valueOf(2), "人工过滤"), AUTOMANUEL(BigDecimal.valueOf(3),
			"自动过滤+人工过滤");

	private BigDecimal value;
	private String name;

	private ContentFilterPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ContentFilterPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (ContentFilterPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return AUTO;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ContentFilterPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
