package esms.etonenet.boss1069.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

import io.swagger.annotations.ApiModelProperty;

/**
 * The persistent class for the TM_COMPANY database table.
 * 
 */
@LogEntity(remark = "公司")
@Entity
@Table(name = "TM_COMPANY")
@NamedQuery(name = "Company.findAll", query = "SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Min(value = 1)
	@ApiModelProperty(position = 1, required = true, value = "id存在将做更新操作，不然则新增")
	@Column(name = "COMPANY_ID")
	private Long companyId;

	@Size(max = 60)
	@ApiModelProperty(position = 2, value = "公司名称不超过60字符")
	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Digits(fraction = 0, integer = 2)
	@ApiModelProperty(position = 3, value = "客户状态不超过2字符")
	@Column(name = "CUSTOMER_STATE")
	private Integer customerState;

	@Digits(fraction = 0, integer = 2)
	@ApiModelProperty(position = 4, value = "重要级别不超过2字符")
	@Column(name = "IMPORTANT_TYPE")
	private Integer importantType;

	@ApiModelProperty(position = 5, value = "销售id")
	@Column(name = "SALE_ID")
	private Long saleId;

	public Company() {
	}

	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getCustomerState() {
		return this.customerState;
	}

	public void setCustomerState(Integer customerState) {
		this.customerState = customerState;
	}

	public Integer getImportantType() {
		return this.importantType;
	}

	public void setImportantType(Integer importantType) {
		this.importantType = importantType;
	}

	public Long getSaleId() {
		return this.saleId;
	}

	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}

}