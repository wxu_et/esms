package esms.etonenet.boss1069.enums.spmoroute;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum RouteState {

	ACTIVEED(BigDecimal.valueOf(1), "激活的"), BANED(BigDecimal.valueOf(0), "禁用的"), EXPIRED(BigDecimal.valueOf(-1), "无效的");

	private BigDecimal value;
	private String name;

	private RouteState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static RouteState fromValue(BigDecimal value) {
		if (value != null) {
			for (RouteState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return ACTIVEED;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RouteState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
