package esms.etonenet.boss1069.enums.channel;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ChannelBaobeiFlag {

	NORMAL(BigDecimal.valueOf(0), "普通通道 "), BAOBEI(BigDecimal.valueOf(1), "报备通道");

	private BigDecimal value;
	private String name;

	private ChannelBaobeiFlag(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ChannelBaobeiFlag fromValue(BigDecimal value) {
		if (value != null) {
			for (ChannelBaobeiFlag state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return NORMAL;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ChannelBaobeiFlag e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
