package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL_GROUP_CONFIG database table.
 * 
 */
@LogEntity(remark = "通道组配置")
@Entity
@Table(name = "TM_CHANNEL_GROUP_CONFIG")
@NamedQuery(name = "ChannelGroupConfig.findAll", query = "SELECT c FROM ChannelGroupConfig c")
public class ChannelGroupConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TM_CHANNEL_GROUP_CONFIG_CHANNELGROUPCONFIGID_GENERATOR", sequenceName = "SEQ_CHANNEL_GROUP_CONFIG", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_CHANNEL_GROUP_CONFIG_CHANNELGROUPCONFIGID_GENERATOR")
	@Column(name = "CHANNEL_GROUP_CONFIG_ID", precision = 10, unique = true, nullable = false)
	private Long channelGroupConfigId;

	@Size(max = 6)
	@Column(name = "CARRIER_CODE", length = 6)
	private String carrierCode;

	@Size(max = 6)
	@Column(name = "CITY_CODE", length = 6)
	private String cityCode;

	@Size(max = 6)
	@Column(name = "COUNTRY_CODE", length = 6)
	private String countryCode;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "LOGIC_STAT", precision = 2, scale = 0)
	private BigDecimal logicStat;

	@Size(max = 6)
	@Column(name = "PROVINCE_CODE", length = 6)
	private String provinceCode;

	@Size(max = 500)
	@Column(length = 500)
	private String remark;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "ROUTE_STATE", precision = 2, scale = 0)
	private BigDecimal routeState;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "SP_CHANNEL_PRIORITY", precision = 2, scale = 0)
	private BigDecimal spChannelPriority;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "SP_CHANNEL_TIME_SLICE_NUMBER", precision = 2, scale = 0)
	private BigDecimal spChannelTimeSliceNumber;

	@Digits(fraction = 0, integer = 2)
	@Column(name = "SP_CHANNEL_WEIGHT_NUMBER", precision = 2, scale = 0)
	private BigDecimal spChannelWeightNumber;

	// bi-directional many-to-one association to Channel
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_ID")
	private Channel channel;

	// bi-directional many-to-one association to ChannelGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_GROUP_ID")
	private ChannelGroup channelGroup;

	public ChannelGroupConfig() {
	}

	public Long getChannelGroupConfigId() {
		return this.channelGroupConfigId;
	}

	public void setChannelGroupConfigId(Long channelGroupConfigId) {
		this.channelGroupConfigId = channelGroupConfigId;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public BigDecimal getLogicStat() {
		return this.logicStat;
	}

	public void setLogicStat(BigDecimal logicStat) {
		this.logicStat = logicStat;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getRouteState() {
		return this.routeState;
	}

	public void setRouteState(BigDecimal routeState) {
		this.routeState = routeState;
	}

	public BigDecimal getSpChannelPriority() {
		return this.spChannelPriority;
	}

	public void setSpChannelPriority(BigDecimal spChannelPriority) {
		this.spChannelPriority = spChannelPriority;
	}

	public BigDecimal getSpChannelTimeSliceNumber() {
		return this.spChannelTimeSliceNumber;
	}

	public void setSpChannelTimeSliceNumber(BigDecimal spChannelTimeSliceNumber) {
		this.spChannelTimeSliceNumber = spChannelTimeSliceNumber;
	}

	public BigDecimal getSpChannelWeightNumber() {
		return this.spChannelWeightNumber;
	}

	public void setSpChannelWeightNumber(BigDecimal spChannelWeightNumber) {
		this.spChannelWeightNumber = spChannelWeightNumber;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public ChannelGroup getChannelGroup() {
		return this.channelGroup;
	}

	public void setChannelGroup(ChannelGroup channelGroup) {
		this.channelGroup = channelGroup;
	}

}