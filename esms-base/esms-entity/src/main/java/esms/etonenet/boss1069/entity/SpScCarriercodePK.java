package esms.etonenet.boss1069.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 * The primary key class for the TM_SP_SC_CARRIERCODE database table.
 * 
 */
@Embeddable
public class SpScCarriercodePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "SP_ID", insertable = false, updatable = false)
	@Size(max = 6)
	private String spId;

	@Column(name = "SP_SERVICE_CODE", insertable = false, updatable = false)
	@Size(max = 10)
	private String spServiceCode;

	@Column(name = "CARRIER_CODE")
	@Size(max = 6)
	private String carrierCode;

	public SpScCarriercodePK() {
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	@Override
	public String toString() {
		return "SpScCarriercodePK [spId=" + spId + ", spServiceCode=" + spServiceCode + ", carrierCode=" + carrierCode
				+ "]";
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SpScCarriercodePK)) {
			return false;
		}
		SpScCarriercodePK castOther = (SpScCarriercodePK) other;
		return this.spId.equals(castOther.spId) && this.spServiceCode.equals(castOther.spServiceCode)
				&& this.carrierCode.equals(castOther.carrierCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.spId.hashCode();
		hash = hash * prime + this.spServiceCode.hashCode();
		hash = hash * prime + this.carrierCode.hashCode();

		return hash;
	}
}