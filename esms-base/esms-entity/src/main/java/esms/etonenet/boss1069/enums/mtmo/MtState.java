package esms.etonenet.boss1069.enums.mtmo;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SMS下行状态枚举类
 * 
 * @author junwang
 *
 */
public enum MtState {

	MTINIT(BigDecimal.valueOf(1), "下行初始状态"), MTBALANCESUCCESS(BigDecimal.valueOf(2), "下行余额扣除成功"), MTWAITING(
			BigDecimal.valueOf(3),
			"下行等待下行处理"), MTTOPERSONFILTER(BigDecimal.valueOf(4), "下行等待下行处理"), MTPASSCONTENTFILTER(BigDecimal.valueOf(5),
					"下行通过内容过滤"), MTPASSPHONEFILTER(BigDecimal.valueOf(6), "下行通过号码过滤"), MTPASSFLOWCONTROL(
							BigDecimal.valueOf(7),
							"下行通过流量控制"), MTROUTESUCCESS(BigDecimal.valueOf(8), "下行路由成功"), MTHANGUP(
									BigDecimal.valueOf(9),
									"下行挂起状态"), TIMINGREVIEWFINISH(BigDecimal.valueOf(10), "定时检查完成"), MTWAITINGTIMER(
											BigDecimal.valueOf(11), "下行等待定时发送时间"), MTWAITINGSEND(BigDecimal.valueOf(12),
													"下行等待发送"), MTSENDSUCCESS(BigDecimal.valueOf(14),
															"下行发送成功"), MTSENDFAIL(BigDecimal.valueOf(15),
																	"下行发送失败"), MTSENDRETRY(BigDecimal.valueOf(16),
																			"下行发送重试"), MTSENDEND(BigDecimal.valueOf(17),
																					"下行发送终止"), MTWAITINGREPORT(
																							BigDecimal.valueOf(18),
																							"下行等待状态报告"), MTWAITINGREPORTOVERTIME(
																									BigDecimal.valueOf(
																											19),
																									"下行等待状态报告超时"), MTGETSUCCESSREPORT(
																											BigDecimal
																													.valueOf(
																															20),
																											"下行得到成功状态报告"), MTGETFAILREPORT(
																													BigDecimal
																															.valueOf(
																																	21),
																													"下行得到失败状态报告"), WAITINGSPWHITELISTFILTER(
																															BigDecimal
																																	.valueOf(
																																			23),
																															"等待SP白名单过滤"), SPWHITELISTFILTERFINISH(
																																	BigDecimal
																																			.valueOf(
																																					24),
																																	"SP白名单过滤完成"), WAITINTCHANNELWHITELISTFILTER(
																																			BigDecimal
																																					.valueOf(
																																							25),
																																			"等待通道白名单过滤"), CHANNELWHITELISTFILTERFINISH(
																																					BigDecimal
																																							.valueOf(
																																									26),
																																					"通道白名单过滤完成"), PHONESEGMENTERROR(
																																							BigDecimal
																																									.valueOf(
																																											41),
																																							"号码段错误"), PHONEFILTERERROR(
																																									BigDecimal
																																											.valueOf(
																																													42),
																																									"号码过滤错误"), SPTEXTFILTERERROR(
																																											BigDecimal
																																													.valueOf(
																																															43),
																																											"SP内容过滤错误"), ROUTEERROR(
																																													BigDecimal
																																															.valueOf(
																																																	44),
																																													"路由错误"), DOTEXTERROR(
																																															BigDecimal
																																																	.valueOf(
																																																			45),
																																															"内容处理错误"), QUOTADEFICIENCY(
																																																	BigDecimal
																																																			.valueOf(
																																																					46),
																																																	"配额不足"), FLOWCONTROLERROR(
																																																			BigDecimal
																																																					.valueOf(
																																																							47),
																																																			"流量控制错误"), PERSONFILTERREFUSE(
																																																					BigDecimal
																																																							.valueOf(
																																																									48),
																																																					"人工过滤拒绝"), VAILDTIMEOUTDATE(
																																																							BigDecimal
																																																									.valueOf(
																																																											49),
																																																							"有效时间过期"), PARTITION(
																																																									BigDecimal
																																																											.valueOf(
																																																													50),
																																																									"分拆式长短信"), PARTITIONCOMMON(
																																																											BigDecimal
																																																													.valueOf(
																																																															51),
																																																											"分拆式普通短信"), CHANNELTEXTFILTERERROR(
																																																													BigDecimal
																																																															.valueOf(
																																																																	53),
																																																													"通道内容过滤错误"), SPWHITELISTFILTERERROR(
																																																															BigDecimal
																																																																	.valueOf(
																																																																			54),
																																																															"SP白名单过滤错误"), CHANNELWHITELISTFILTERERROR(
																																																																	BigDecimal
																																																																			.valueOf(
																																																																					55),
																																																																	"通道白名单过滤错误"), MTSEENDSIMULATE(
																																																																			BigDecimal
																																																																					.valueOf(
																																																																							74),
																																																																			"下行发送模拟成功");

	private MtState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public static MtState fromValue(BigDecimal value) {
		if (value != null) {
			for (MtState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return MTINIT;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (MtState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}

	public static Map<String, String> toDisplayMap2() {
		Map<String, String> m = new LinkedHashMap<>();
		for (MtState e : values()) {
			m.put(e.value.toString(), e.value.toString() + "-" + e.name);
		}
		return m;
	}

	private BigDecimal value;

	private String name;

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
