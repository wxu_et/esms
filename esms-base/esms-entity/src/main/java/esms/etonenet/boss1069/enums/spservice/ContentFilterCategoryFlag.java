package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.etonenet.util.MathUtil;

public enum ContentFilterCategoryFlag {

	POLITICS(BigDecimal.valueOf(1), "政治"), INSULT(BigDecimal.valueOf(2), "侮辱"), SALACITY(BigDecimal.valueOf(3),
			"黄色"), GAMBLE(BigDecimal.valueOf(4), "赌博"), DRUG(BigDecimal.valueOf(5), "毒品"), LAND(BigDecimal.valueOf(6),
					"地产"), RELIGION(BigDecimal.valueOf(7), "宗教"), ENTERTAINMENT(BigDecimal.valueOf(8), "娱乐"), CHEAT(
							BigDecimal.valueOf(9),
							"欺诈"), ENTERPRISE(BigDecimal.valueOf(10), "企业"), ENTERPRISE2(BigDecimal.valueOf(11),
									"企业2"), RETAIN(BigDecimal.valueOf(12), "保留"), RETAIN2(BigDecimal.valueOf(13),
											"保留"), RETAIN3(BigDecimal.valueOf(14), "保留"), RETAIN4(
													BigDecimal.valueOf(15),
													"保留"), RETAIN5(BigDecimal.valueOf(16), "保留"), RETAIN6(
															BigDecimal.valueOf(17),
															"保留"), RETAIN7(BigDecimal.valueOf(18), "保留"), RETAIN8(
																	BigDecimal.valueOf(19),
																	"保留"), RETAIN9(BigDecimal.valueOf(20),
																			"保留"), RETAIN10(BigDecimal.valueOf(21),
																					"保留"), RETAIN11(
																							BigDecimal.valueOf(22),
																							"保留"), RETAIN12(
																									BigDecimal.valueOf(
																											23),
																									"保留"), RETAIN13(
																											BigDecimal
																													.valueOf(
																															24),
																											"保留"), RETAIN14(
																													BigDecimal
																															.valueOf(
																																	25),
																													"保留"), RETAIN15(
																															BigDecimal
																																	.valueOf(
																																			26),
																															"保留"), RETAIN16(
																																	BigDecimal
																																			.valueOf(
																																					27),
																																	"保留"), RETAIN17(
																																			BigDecimal
																																					.valueOf(
																																							28),
																																			"保留"), RETAIN18(
																																					BigDecimal
																																							.valueOf(
																																									29),
																																					"保留"), RETAIN19(
																																							BigDecimal
																																									.valueOf(
																																											30),
																																							"保留"), RETAIN20(
																																									BigDecimal
																																											.valueOf(
																																													31),
																																									"保留"), HXT(
																																											BigDecimal
																																													.valueOf(
																																															32),
																																											"航信通");

	private BigDecimal value;
	private String name;

	private ContentFilterCategoryFlag(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static ContentFilterCategoryFlag fromValue(BigDecimal value) {
		if (value != null) {
			for (ContentFilterCategoryFlag state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return POLITICS;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (ContentFilterCategoryFlag e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static List<String[]> to2xListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (ContentFilterCategoryFlag e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = MathUtil.getPow2(e.getValue().intValue()) + "";
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (ContentFilterCategoryFlag e : values()) {
			m.put(MathUtil.getPow2(e.value.intValue()) + "", e.name);
		}
		return m;
	}
}
