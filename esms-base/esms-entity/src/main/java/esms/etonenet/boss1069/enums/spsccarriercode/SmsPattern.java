package esms.etonenet.boss1069.enums.spsccarriercode;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SmsPattern {

	LONG(BigDecimal.valueOf(0), "长短信"), PATTERN(BigDecimal.valueOf(1), "分拆短信"), NONE(BigDecimal.valueOf(2), "不拆分");

	private BigDecimal value;
	private String name;

	private SmsPattern(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static SmsPattern fromValue(BigDecimal value) {
		if (value != null) {
			for (SmsPattern state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return LONG;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SmsPattern e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
