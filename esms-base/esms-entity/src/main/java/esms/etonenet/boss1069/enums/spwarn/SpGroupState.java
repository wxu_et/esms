package esms.etonenet.boss1069.enums.spwarn;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SpGroupState {

	OPEN(BigDecimal.valueOf(0), "关闭"), CLOSE(BigDecimal.valueOf(1), "开启");
	private BigDecimal value;
	private String name;

	private SpGroupState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static SpGroupState fromValue(BigDecimal value) {
		if (value != null) {
			for (SpGroupState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return OPEN;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SpGroupState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
