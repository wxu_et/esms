package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum PrefixPositionAddPolicy {

	NONE(BigDecimal.valueOf(0), "不加前缀"), FISRT(BigDecimal.valueOf(16), "第一条加前缀"), LAST(BigDecimal.valueOf(32),
			"最后一条加前缀"), ALL(BigDecimal.valueOf(64), "全部加前缀");

	private BigDecimal value;
	private String name;

	private PrefixPositionAddPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static PrefixPositionAddPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (PrefixPositionAddPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return NONE;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (PrefixPositionAddPolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (PrefixPositionAddPolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
