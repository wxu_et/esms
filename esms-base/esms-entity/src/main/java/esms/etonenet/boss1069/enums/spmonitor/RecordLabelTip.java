package esms.etonenet.boss1069.enums.spmonitor;

/**
 * 记录标签(区分大小写)
 * 
 * @author zfang
 * 
 */
public class RecordLabelTip {
	/**
	 * 年Y
	 */
	public static final String YEAR = "Y";

	/**
	 * 月M
	 */
	public static final String MONTH = "M";

	/**
	 * 天D
	 */
	public static final String DAY = "D";

	/**
	 * 小时h
	 */
	public static final String HOUR = "h";

	/**
	 * 分钟m
	 */
	public static final String MINUTE = "m";

	/**
	 * 分钟s
	 */
	public static final String SECOND = "s";
}
