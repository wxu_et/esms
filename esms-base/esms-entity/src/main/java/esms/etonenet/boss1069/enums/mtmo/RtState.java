package esms.etonenet.boss1069.enums.mtmo;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SMS状态报告状态枚举
 * 
 * @author junwang
 *
 */
public enum RtState {

	RTINIT(BigDecimal.valueOf(1), "状态报告初始状态"), WAITRT(BigDecimal.valueOf(2), "等待状态报告"), GETRTORERROR(
			BigDecimal.valueOf(3),
			"得到状态报告或错误"), RTTIMEOUT(BigDecimal.valueOf(4), "状态报告超时"), DELAYMATCH(BigDecimal.valueOf(5), "延迟匹配"), RTBAN(
					BigDecimal.valueOf(10),
					"状态报告禁止"), WAITSINGLETOSP(BigDecimal.valueOf(12), "等待状态报告发送到SP端"), WAITSINGLERESULT(
							BigDecimal.valueOf(13),
							"等待状态报告发送结果"), SINGLESPSUCCESS(BigDecimal.valueOf(14), "向SP端发送状态报告成功"), SINGLESPFAIL(
									BigDecimal.valueOf(15), "向SP端发送状态报告失败"), SINGLESPRETRY(BigDecimal.valueOf(16),
											"向SP端发送状态报告重试"), SINGLESPSTOP(BigDecimal.valueOf(17),
													"向SP端发送状态报告终止"), SINGLERETRYWAITRESULT(BigDecimal.valueOf(18),
															"向SP端发送状态报告重试等待结果"), WAITBATCHSP(BigDecimal.valueOf(22),
																	"等待状态报告批量发送到SP端"), WAITBATCHRESULT(
																			BigDecimal.valueOf(23),
																			"等待状态报告批量发送结果"), BATCHSPSUCCESS(
																					BigDecimal.valueOf(24),
																					"向SP端批量发送状态报告成功"), BATCHSPFAIL(
																							BigDecimal.valueOf(25),
																							"向SP端批量发送状态报告失败"), BATCHSPRETRY(
																									BigDecimal.valueOf(
																											26),
																									"向SP端批量发送状态报告重试"), BATCHSPSTOP(
																											BigDecimal
																													.valueOf(
																															27),
																											"向SP端批量发送状态报告端终止"), BATCHRETRYWAITRESULT(
																													BigDecimal
																															.valueOf(
																																	28),
																													"向SP端批量发送状态报告重试等待结果");

	private RtState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public static RtState fromValue(BigDecimal value) {
		if (value != null) {
			for (RtState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return RTINIT;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RtState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}

	public static Map<String, String> toDisplayMap2() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RtState e : values()) {
			m.put(e.value.toString(), e.value.toString() + "-" + e.name);
		}
		return m;
	}

	private BigDecimal value;

	private String name;

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
