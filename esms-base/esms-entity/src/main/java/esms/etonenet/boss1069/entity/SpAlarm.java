package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TM_CHANNEL database table.
 * 
 */
@LogEntity(remark = "spAlarm警报")
@Entity
@Table(name = "TT_SP_RP_F_ALARM")
public class SpAlarm implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@Id
	@SequenceGenerator(name = "TM_SP_RP_F_ALARM_GENERATOR", sequenceName = "SEQ_SP_RP_F_ALARM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_SP_RP_F_ALARM_GENERATOR")
	@Column(name = "SP_RP_F_ID")
	private Long spReportFid;

	/**
	 * Sp编号
	 */
	@Column(name = "SP_ID")
	private String spId;

	/**
	 * 持续时间
	 */
	@Column(name = "DURATION_TIME")
	private int durationTime;

	/**
	 * 数据生成时间
	 */
	@Column(name = "CREATE_TIME")
	private Timestamp createTime;

	/**
	 * 下行计费总量
	 */
	@Column(name = "MT_TOTAL")
	private Long mtTotal;

	/**
	 * 下行成功条数
	 */
	@Column(name = "MT_SUCCESS")
	private Long mtSuccess;

	/**
	 * 下行失败条数
	 */
	@Column(name = "MT_FAIL_TOTAL")
	private Long mtFailTotal;

	/**
	 * 移通报错
	 */
	@Column(name = "MT_FAIL_ET")
	private Long mtFailEt;

	/**
	 * 运营商报错
	 */
	@Column(name = "MT_FAIL_CARRIER")
	private Long mtFailCarrier;

	/**
	 * 无状态报告条数
	 */
	@Column(name = "MT_NO_REPORT")
	private Long mtNoReport;

	/**
	 * 重复发送被过滤流量
	 */
	@Column(name = "MT_FILTER_REP_TOTAL")
	private Long mtFilterRepTotal;

	/**
	 * 内容过滤
	 */
	@Column(name = "MT_FILTER_CONT_TOTAL")
	private Long mtFilterContTotal;

	/**
	 * 路由错误
	 */
	@Column(name = "MT_ROUTER_ERROR_TOTAL")
	private Long mtRouterErrorTotal;

	/**
	 * 黑名单过滤条数
	 */
	@Column(name = "MT_FILTER_BLACKLIST_TOTAL")
	private Long mtFilterBlackListTotal;

	/**
	 * 失败率
	 */
	@Column(name = "MT_FAIL_RATE")
	private String mtFailRate;

	/**
	 * 成功率
	 */
	@Column(name = "MT_SUCCESS_RATE")
	private String mtSuccessRate;

	/**
	 * ET拦截率
	 */
	@Column(name = "MT_FAIL_ET_RATE")
	private String mtFailEtRate;

	/**
	 * 运营商拦截率
	 */
	@Column(name = "MT_FAIL_CARRIER_RATE")
	private String mtFailCarrierRate;

	/**
	 * 无状态率
	 */
	@Column(name = "MT_NO_REPORT_RATE")
	private String mtNoReportRate;

	/**
	 * 流控过滤率
	 */
	@Column(name = "MT_FILTER_REP_RATE")
	private String mtFilterRepRate;

	/**
	 * 内容过滤率
	 */
	@Column(name = "MT_FILTER_CONT_RATE")
	private String mtFilterContRate;

	/**
	 * 路由错过滤率
	 */
	@Column(name = "MT_ROUTER_ERROR_RATE")
	private String mtRouterErrorRate;

	/**
	 * 黑名单过滤率
	 */
	@Column(name = "MT_FILTER_BLACKLIST_RATE")
	private String mtFilterBlackListRate;

	/**
	 * 重要性 1 red 2 orange 3 yellow 4 green
	 */
	@Column(name = "IMPORTANCE")
	private String importance;

	/**
	 * 原因 下行成功率（量）过低 下行失败率（量）过高 平台拦截率（量）过高 运营商拦截率（量）过高 流控错误率（量）过高 内容过滤率（量）过高
	 * 路由错误率（量）过高 黑名单过滤率（量）过高 下行无状态率（量）过高
	 */
	@Column(name = "CAUSE")
	private String cause;

	public Long getSpReportFid() {
		return spReportFid;
	}

	public void setSpReportFid(Long spReportFid) {
		this.spReportFid = spReportFid;
	}

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public int getDurationTime() {
		return durationTime;
	}

	public void setDurationTime(int durationTime) {
		this.durationTime = durationTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Long getMtTotal() {
		return mtTotal;
	}

	public void setMtTotal(Long mtTotal) {
		this.mtTotal = mtTotal;
	}

	public Long getMtSuccess() {
		return mtSuccess;
	}

	public void setMtSuccess(Long mtSuccess) {
		this.mtSuccess = mtSuccess;
	}

	public Long getMtFailTotal() {
		return mtFailTotal;
	}

	public void setMtFailTotal(Long mtFailTotal) {
		this.mtFailTotal = mtFailTotal;
	}

	public Long getMtFailEt() {
		return mtFailEt;
	}

	public void setMtFailEt(Long mtFailEt) {
		this.mtFailEt = mtFailEt;
	}

	public Long getMtFailCarrier() {
		return mtFailCarrier;
	}

	public void setMtFailCarrier(Long mtFailCarrier) {
		this.mtFailCarrier = mtFailCarrier;
	}

	public Long getMtNoReport() {
		return mtNoReport;
	}

	public void setMtNoReport(Long mtNoReport) {
		this.mtNoReport = mtNoReport;
	}

	public Long getMtFilterRepTotal() {
		return mtFilterRepTotal;
	}

	public void setMtFilterRepTotal(Long mtFilterRepTotal) {
		this.mtFilterRepTotal = mtFilterRepTotal;
	}

	public Long getMtFilterContTotal() {
		return mtFilterContTotal;
	}

	public void setMtFilterContTotal(Long mtFilterContTotal) {
		this.mtFilterContTotal = mtFilterContTotal;
	}

	public Long getMtRouterErrorTotal() {
		return mtRouterErrorTotal;
	}

	public void setMtRouterErrorTotal(Long mtRouterErrorTotal) {
		this.mtRouterErrorTotal = mtRouterErrorTotal;
	}

	public Long getMtFilterBlackListTotal() {
		return mtFilterBlackListTotal;
	}

	public void setMtFilterBlackListTotal(Long mtFilterBlackListTotal) {
		this.mtFilterBlackListTotal = mtFilterBlackListTotal;
	}

	public String getMtSuccessRate() {
		return mtSuccessRate;
	}

	public void setMtSuccessRate(String mtSuccessRate) {
		this.mtSuccessRate = mtSuccessRate;
	}

	public String getMtFailEtRate() {
		return mtFailEtRate;
	}

	public void setMtFailEtRate(String mtFailEtRate) {
		this.mtFailEtRate = mtFailEtRate;
	}

	public String getMtFailCarrierRate() {
		return mtFailCarrierRate;
	}

	public void setMtFailCarrierRate(String mtFailCarrierRate) {
		this.mtFailCarrierRate = mtFailCarrierRate;
	}

	public String getMtNoReportRate() {
		return mtNoReportRate;
	}

	public void setMtNoReportRate(String mtNoReportRate) {
		this.mtNoReportRate = mtNoReportRate;
	}

	public String getMtFilterRepRate() {
		return mtFilterRepRate;
	}

	public void setMtFilterRepRate(String mtFilterRepRate) {
		this.mtFilterRepRate = mtFilterRepRate;
	}

	public String getMtFilterContRate() {
		return mtFilterContRate;
	}

	public void setMtFilterContRate(String mtFilterContRate) {
		this.mtFilterContRate = mtFilterContRate;
	}

	public String getMtRouterErrorRate() {
		return mtRouterErrorRate;
	}

	public void setMtRouterErrorRate(String mtRouterErrorRate) {
		this.mtRouterErrorRate = mtRouterErrorRate;
	}

	public String getMtFilterBlackListRate() {
		return mtFilterBlackListRate;
	}

	public void setMtFilterBlackListRate(String mtFilterBlackListRate) {
		this.mtFilterBlackListRate = mtFilterBlackListRate;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getMtFailRate() {
		return mtFailRate;
	}

	public void setMtFailRate(String mtFailRate) {
		this.mtFailRate = mtFailRate;
	}

}