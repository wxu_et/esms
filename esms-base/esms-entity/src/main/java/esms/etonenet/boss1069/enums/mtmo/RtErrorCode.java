package esms.etonenet.boss1069.enums.mtmo;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * RT错误代码枚举类
 * 
 * @author junwang
 *
 */
public enum RtErrorCode {

	SENDSUCCESS("000", "发送成功"), PORTPARMSERROR("100", "SP API接口参数错误"), FILTERROUTE("200",
			"MLINK平台内部过滤路由信息"), SPCONFIGURATION("300", "MLINK平台内部SP配置信息"), GATEWAYSEENDERROR("400",
					"MLINK网关发送时错误"), OPERATORBACK("500", "运行商反馈的信息"), SENDINGERROR("600", "MLINK API发送时错误");

	private RtErrorCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public static RtErrorCode fromValue(String value) {
		if (value != null) {
			for (RtErrorCode state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return SENDSUCCESS;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RtErrorCode e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}

	public static Map<String, String> toDisplayMap2() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RtErrorCode e : values()) {
			m.put(e.value.toString(), e.value.toString() + "-" + e.name);
		}
		return m;
	}

	private String value;

	private String name;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
