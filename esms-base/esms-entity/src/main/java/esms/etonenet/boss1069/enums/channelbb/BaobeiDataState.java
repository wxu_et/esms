package esms.etonenet.boss1069.enums.channelbb;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum BaobeiDataState {

	VALIDATE(BigDecimal.valueOf(1), "有效"), INVALIDATE(BigDecimal.valueOf(0), "无效");

	private BigDecimal value;
	private String name;

	private BaobeiDataState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static BaobeiDataState fromValue(BigDecimal value) {
		if (value != null) {
			for (BaobeiDataState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return VALIDATE;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (BaobeiDataState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
