package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TS_MT_ADDRESS database table.
 * 
 */
@LogEntity(remark = "下发地址管理")
@Entity
@Table(name = "TS_MT_ADDRESS")
@NamedQuery(name = "MtAddress.findAll", query = "SELECT m FROM MtAddress m")
public class MtAddress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ADDRESS_NAME")
	@Size(max = 100)
	private String addressName;

	@Column(name = "CREATE_TIME")
	private Timestamp createTime;

	@Size(max = 500)
	private String target;

	@Column(name = "UPDATE_TIME")
	private Timestamp updateTime;

	public MtAddress() {
	}

	public String getAddressName() {
		return this.addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getTarget() {
		return this.target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

}