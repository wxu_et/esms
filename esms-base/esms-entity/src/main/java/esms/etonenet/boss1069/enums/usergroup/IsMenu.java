package esms.etonenet.boss1069.enums.usergroup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public enum IsMenu {

	IS(BigDecimal.valueOf(0), "是"), NOT(BigDecimal.valueOf(1), "否");

	private BigDecimal value;
	private String name;

	private IsMenu(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static IsMenu fromValue(BigDecimal value) {
		if (value != null) {
			for (IsMenu state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return IS;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (IsMenu e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}
}
