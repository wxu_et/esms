package esms.etonenet.boss1069.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 * The primary key class for the TM_SP_CHANNEL database table.
 * 
 */
@Embeddable
public class SpChannelPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "CHANNEL_ID", insertable = false, updatable = false)
	@Size(max = 4)
	private String channelId;

	@Column(name = "SP_SERVICE_CODE", insertable = false, updatable = false)
	@Size(max = 10)
	private String spServiceCode;

	@Column(name = "SP_ID", insertable = false, updatable = false)
	@Size(max = 6)
	private String spId;

	public SpChannelPK() {
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getSpServiceCode() {
		return this.spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public String getSpId() {
		return this.spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	@Override
	public String toString() {
		return "SpChannelPK [channelId=" + channelId + ", spServiceCode=" + spServiceCode + ", spId=" + spId + "]";
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SpChannelPK)) {
			return false;
		}
		SpChannelPK castOther = (SpChannelPK) other;
		return this.channelId.equals(castOther.channelId) && this.spServiceCode.equals(castOther.spServiceCode)
				&& this.spId.equals(castOther.spId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.channelId.hashCode();
		hash = hash * prime + this.spServiceCode.hashCode();
		hash = hash * prime + this.spId.hashCode();

		return hash;
	}
}