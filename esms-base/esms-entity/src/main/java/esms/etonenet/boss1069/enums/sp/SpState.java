package esms.etonenet.boss1069.enums.sp;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SpState {

	EXPIRED(BigDecimal.valueOf(0), "失效的"), ACTIVED(BigDecimal.valueOf(1), "激活的"), LOCKED(BigDecimal.valueOf(2), "锁定的");

	private BigDecimal value;
	private String name;

	private SpState(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static SpState fromValue(BigDecimal value) {
		if (value != null) {
			for (SpState state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return EXPIRED;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SpState e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
