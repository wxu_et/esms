package esms.etonenet.boss1069.enums.spservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum DecodeModePolicy {

	CHINESE(BigDecimal.valueOf(0), "前后缀中文格式"), ENGLISH(BigDecimal.valueOf(1), "前后缀英文格式");

	private BigDecimal value;
	private String name;

	private DecodeModePolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static DecodeModePolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (DecodeModePolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return CHINESE;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (DecodeModePolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (DecodeModePolicy e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
