package esms.etonenet.boss1069.enums.spmonitor;

/**
 * ORACLE 数据库 时间格式
 * 
 * @author zfang
 * 
 */
public class TimePattern {
	/**
	 * 年yyyy
	 */
	public static final String YEAR = "YYYY";

	/**
	 * 月yyyyMM
	 */
	public static final String MONTH = "YYYYMM";

	/**
	 * 天yyyyMMdd
	 */
	public static final String DAY = "YYYYMMDD";

	/**
	 * 小时yyyyMMdd HH
	 */
	public static final String HOUR = "YYYYMMDD HH24";

	/**
	 * 分钟yyyyMMdd HH:mm
	 */
	public static final String MINUTE = "YYYYMMDD HH24:MI";

	/**
	 * 秒yyyyMMdd HH:mm:ss
	 */
	public static final String SECOND = "YYYYMMDD HH24:MI:SS";
}
