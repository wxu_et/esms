package esms.etonenet.boss1069.enums.spmtroute;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public enum RouteType {

	NORMAL(BigDecimal.valueOf(0), "一般路由"), CHANNELGROUP(BigDecimal.valueOf(1), "通道组路由");

	private BigDecimal value;
	private String name;

	private RouteType(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static RouteType fromValue(BigDecimal value) {
		if (value != null) {
			for (RouteType state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return CHANNELGROUP;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (RouteType e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
