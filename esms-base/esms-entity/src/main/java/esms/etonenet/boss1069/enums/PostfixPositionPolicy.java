package esms.etonenet.boss1069.enums;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public enum PostfixPositionPolicy {

	FRONT(BigDecimal.valueOf(0), "后缀在前"), BACK(BigDecimal.valueOf(4), "后缀在后");

	private BigDecimal value;
	private String name;

	private PostfixPositionPolicy(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return name;
	}

	public static PostfixPositionPolicy fromValue(BigDecimal value) {
		if (value != null) {
			for (PostfixPositionPolicy state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return FRONT;
	}

	public static List<String[]> toListValue() {

		List<String[]> l = new ArrayList<String[]>();
		for (PostfixPositionPolicy e : values()) {
			String[] s = new String[2];
			s[0] = e.getName().toString();
			s[1] = e.getValue().toString();
			l.add(s);
		}
		return l;
	}
}
