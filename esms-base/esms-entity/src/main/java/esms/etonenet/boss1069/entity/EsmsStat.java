package esms.etonenet.boss1069.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TT_ESMS_STAT database table.
 * 
 */
@LogEntity(remark = "监控管理")
@Entity
@Table(name = "TT_ESMS_STAT")
@NamedQuery(name = "EsmsStat.findAll", query = "SELECT e FROM EsmsStat e")
public class EsmsStat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TT_ESMS_STAT_ID_GENERATOR", sequenceName = "SEQ_ESMS_STAT", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TT_ESMS_STAT_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 10)
	private Long id;

	@Column(name = "CHANNEL_NUMBER", length = 60)
	@Size(max = 60)
	private String channelNumber;

	@Column(name = "MQ_STATE", precision = 2)
	@Digits(fraction = 0, integer = 2)
	private BigDecimal mqState;

	@Column(name = "MQ_URL", length = 60)
	@Size(max = 60)
	private String mqUrl;

	@Column(precision = 2)
	@Digits(fraction = 0, integer = 2)
	private BigDecimal priority;

	@Column(name = "WINDOW_END_TIME")
	private Timestamp windowEndTime;

	@Column(name = "WINDOW_START_TIME")
	private Timestamp windowStartTime;

	public EsmsStat() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChannelNumber() {
		return this.channelNumber;
	}

	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}

	public BigDecimal getMqState() {
		return this.mqState;
	}

	public void setMqState(BigDecimal mqState) {
		this.mqState = mqState;
	}

	public String getMqUrl() {
		return this.mqUrl;
	}

	public void setMqUrl(String mqUrl) {
		this.mqUrl = mqUrl;
	}

	public BigDecimal getPriority() {
		return this.priority;
	}

	public void setPriority(BigDecimal priority) {
		this.priority = priority;
	}

	public Timestamp getWindowEndTime() {
		return this.windowEndTime;
	}

	public void setWindowEndTime(Timestamp windowEndTime) {
		this.windowEndTime = windowEndTime;
	}

	public Timestamp getWindowStartTime() {
		return this.windowStartTime;
	}

	public void setWindowStartTime(Timestamp windowStartTime) {
		this.windowStartTime = windowStartTime;
	}

	@Override
	public String toString() {
		return "EsmsStat [id=" + id + ", channelNumber=" + channelNumber + ", mqState=" + mqState + ", mqUrl=" + mqUrl
				+ ", priority=" + priority + ", windowEndTime=" + windowEndTime + ", windowStartTime=" + windowStartTime
				+ "]";
	}

}