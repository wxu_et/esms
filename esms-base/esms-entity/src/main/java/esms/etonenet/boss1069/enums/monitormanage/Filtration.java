package esms.etonenet.boss1069.enums.monitormanage;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 过滤开关： 1：是;-1：否;
 */
public enum Filtration {
	FILTER(BigDecimal.valueOf(1), "是"), NOTFILTER(BigDecimal.valueOf(-1), "否");
	private BigDecimal value;
	private String name;

	private Filtration(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static Filtration fromValue(BigDecimal value) {
		if (value != null) {
			for (Filtration f : values()) {
				if (f.value.equals(value))
					return f;
			}
		}
		return FILTER;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (Filtration f : values())
			map.put(f.value.toString(), f.name);
		return map;
	}
}
