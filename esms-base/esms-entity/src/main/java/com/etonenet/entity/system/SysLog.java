package com.etonenet.entity.system;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.etonenet.entity.system.security.UserBs;

/**
 * The persistent class for the TM_SYS_LOG database table.
 * 
 */
@Entity
@Table(name = "TM_SYS_LOG")
@NamedQuery(name = "SysLog.findAll", query = "SELECT s FROM SysLog s")
public class SysLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TM_SYS_LOG_SYSLOGID_GENERATOR", sequenceName = "SEQ_SYS_LOG", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TM_SYS_LOG_SYSLOGID_GENERATOR")
	@Column(name = "SYS_LOG_ID", unique = true, nullable = false, precision = 10)
	private Long sysLogId;

	@Column(name = "CLASS_ID", length = 100)
	private String classId;

	@Column(name = "CLASS_NAME", length = 100)
	private String className;

	@Column(name = "LOG_TIME")
	private Timestamp logTime;

	@Column(precision = 2)
	private Integer oper;

	@Column(name = "OPER_OTHER", length = 100)
	private String operOther;

	@Column(length = 1000)
	private String prop;

	// uni-directional many-to-one association to UserBs
	@ManyToOne
	@JoinColumn(name = "OPER_USER")
	private UserBs operUser;

	public SysLog() {
	}

	public Long getSysLogId() {
		return this.sysLogId;
	}

	public void setSysLogId(Long sysLogId) {
		this.sysLogId = sysLogId;
	}

	public String getClassId() {
		return this.classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Timestamp getLogTime() {
		return this.logTime;
	}

	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}

	public Integer getOper() {
		return this.oper;
	}

	public void setOper(Integer oper) {
		this.oper = oper;
	}

	public String getOperOther() {
		return this.operOther;
	}

	public void setOperOther(String operOther) {
		this.operOther = operOther;
	}

	public String getProp() {
		return this.prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}

	public UserBs getOperUser() {
		return this.operUser;
	}

	public void setOperUser(UserBs operUser) {
		this.operUser = operUser;
	}

}