package com.etonenet.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TT_SP_RP_F")
public class BossSpRealtimeMonitor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SP_RP_F_ID")
	private Long spReportFid;

	@Column(name = "UNION_FLAG")
	private String unionFlag;

	@Column(name = "COUNT_HELP")
	private Long countHelp;

	@Column(name = "JOB_PLAN_ID")
	private Long jobPlanId;

	@Column(name = "START_TIME")
	private Timestamp startTime;

	@Column(name = "END_TIME")
	private Timestamp endTime;

	@Column(name = "CREATE_START_TIME")
	private Timestamp createStartTime;

	@Column(name = "COUNT_FLAG")
	private Long countFlag;

	@Column(name = "TIME_UNIT_FLAG")
	private Long timeUnitFlag;

	@Column(name = "RECORD_FLAG")
	private String recortFlag;

	@Column(name = "PART_FLAG")
	private String partFlag;

	@Column(name = "SP_ID")
	private String spId;

	@Column(name = "SP_SERVICE_CODE")
	private String spServiceCode;

	@Column(name = "CARRIER_CODE")
	private String carrierCode;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "CITY_CODE")
	private String cityCode;

	@Column(name = "MT_TOTAL")
	private Long mtTotal;

	@Column(name = "MT_WAIT_PROC")
	private Long mtWaitProc;

	@Column(name = "MT_WAIT_SEND")
	private Long mtWaitSend;

	@Column(name = "MT_SENDED_TOTAL")
	private Long mtSendTotal;

	@Column(name = "MT_SEND_ERROR")
	private Long mtSendError;

	@Column(name = "MT_SUCCESS")
	private Long mtSuccess;

	@Column(name = "MT_FAIL_TOTAL")
	private Long mtFailTotal;

	@Column(name = "MT_FAIL_ET")
	private Long mtFailEt;

	@Column(name = "MT_FAIL_CARRIER")
	private Long mtFaliCarrier;

	@Column(name = "MT_NO_REPORT")
	private Long mtNoReport;

	@Column(name = "MT_NO_QUOTA")
	private Long mtNoQuota;

	@Column(name = "MT_FILTER_REP_TOTAL")
	private Long mtFilterRepTotal;

	@Column(name = "MT_FILTER_CONT_TOTAL")
	private Long mtFilterContTotal;

	@Column(name = "MT_SUBMIT_COUNT")
	private Long mtSubmitCountNumber;

	@Column(name = "MO_TOTAL")
	private Long moTotal;

	@Column(name = "MO_SUCCESS")
	private Long moSuccess;

	@Column(name = "MT_TOTAL_HELP")
	private Long mtTotalHelp;

	@Column(name = "MT_WAIT_PROC_HELP")
	private Long mtWaitProcHelp;

	@Column(name = "MT_WAIT_SEND_HELP")
	private Long mtWaitSendHelp;

	@Column(name = "MT_SENDED_TOTAL_HELP")
	private Long mtSendedTotalHelp;

	@Column(name = "MT_SEND_ERROR_HELP")
	private Long mtSendErrorHelp;

	@Column(name = "MT_SUCCESS_HELP")
	private Long mtSuccessHelp;

	@Column(name = "MT_FAIL_TOTAL_HELP")
	private Long mtFailTotalHelp;

	@Column(name = "MT_FAIL_ET_HELP")
	private Long mtFailEtHelp;

	@Column(name = "MT_FAIL_CARRIER_HELP")
	private Long mtFailCarrierHelp;

	@Column(name = "MT_NO_REPORT_HELP")
	private Long mtNoReportHelp;

	@Column(name = "MT_NO_QUOTA_HELP")
	private Long mtNoQuotaHelp;

	@Column(name = "MT_FILTER_REP_TOTAL_HELP")
	private Long mtFilterRepTotalHelp;

	@Column(name = "MT_FILTER_CONT_TOTAL_HELP")
	private Long mtFilterContTotalHelp;

	@Column(name = "MT_SUBMIT_COUNT_HELP")
	private Long mtSubmitCountHelp;

	@Column(name = "MO_TOTAL_HELP")
	private Long moTotalHelp;

	@Column(name = "MO_SUCCESS_HELP")
	private Long moSuccessHelp;

	public String getUnionFlag() {
		return unionFlag;
	}

	public void setUnionFlag(String unionFlag) {
		this.unionFlag = unionFlag;
	}

	public Long getCountHelp() {
		return countHelp;
	}

	public void setCountHelp(Long countHelp) {
		this.countHelp = countHelp;
	}

	public Long getSpReportFid() {
		return spReportFid;
	}

	public void setSpReportFid(Long spReportFid) {
		this.spReportFid = spReportFid;
	}

	public Long getJobPlanId() {
		return jobPlanId;
	}

	public void setJobPlanId(Long jobPlanId) {
		this.jobPlanId = jobPlanId;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Timestamp getCreateStartTime() {
		return createStartTime;
	}

	public void setCreateStartTime(Timestamp createStartTime) {
		this.createStartTime = createStartTime;
	}

	public Long getCountFlag() {
		return countFlag;
	}

	public void setCountFlag(Long countFlag) {
		this.countFlag = countFlag;
	}

	public Long getTimeUnitFlag() {
		return timeUnitFlag;
	}

	public void setTimeUnitFlag(Long timeUnitFlag) {
		this.timeUnitFlag = timeUnitFlag;
	}

	public String getRecortFlag() {
		return recortFlag;
	}

	public void setRecortFlag(String recortFlag) {
		this.recortFlag = recortFlag;
	}

	public String getPartFlag() {
		return partFlag;
	}

	public void setPartFlag(String partFlag) {
		this.partFlag = partFlag;
	}

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getSpServiceCode() {
		return spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Long getMtTotal() {
		return mtTotal;
	}

	public void setMtTotal(Long mtTotal) {
		this.mtTotal = mtTotal;
	}

	public Long getMtWaitProc() {
		return mtWaitProc;
	}

	public void setMtWaitProc(Long mtWaitProc) {
		this.mtWaitProc = mtWaitProc;
	}

	public Long getMtWaitSend() {
		return mtWaitSend;
	}

	public void setMtWaitSend(Long mtWaitSend) {
		this.mtWaitSend = mtWaitSend;
	}

	public Long getMtSendTotal() {
		return mtSendTotal;
	}

	public void setMtSendTotal(Long mtSendTotal) {
		this.mtSendTotal = mtSendTotal;
	}

	public Long getMtSendError() {
		return mtSendError;
	}

	public void setMtSendError(Long mtSendError) {
		this.mtSendError = mtSendError;
	}

	public Long getMtSuccess() {
		return mtSuccess;
	}

	public void setMtSuccess(Long mtSuccess) {
		this.mtSuccess = mtSuccess;
	}

	public Long getMtFailTotal() {
		return mtFailTotal;
	}

	public void setMtFailTotal(Long mtFailTotal) {
		this.mtFailTotal = mtFailTotal;
	}

	public Long getMtFailEt() {
		return mtFailEt;
	}

	public void setMtFailEt(Long mtFailEt) {
		this.mtFailEt = mtFailEt;
	}

	public Long getMtFaliCarrier() {
		return mtFaliCarrier;
	}

	public void setMtFaliCarrier(Long mtFaliCarrier) {
		this.mtFaliCarrier = mtFaliCarrier;
	}

	public Long getMtNoReport() {
		return mtNoReport;
	}

	public void setMtNoReport(Long mtNoReport) {
		this.mtNoReport = mtNoReport;
	}

	public Long getMtNoQuota() {
		return mtNoQuota;
	}

	public void setMtNoQuota(Long mtNoQuota) {
		this.mtNoQuota = mtNoQuota;
	}

	public Long getMtFilterRepTotal() {
		return mtFilterRepTotal;
	}

	public void setMtFilterRepTotal(Long mtFilterRepTotal) {
		this.mtFilterRepTotal = mtFilterRepTotal;
	}

	public Long getMtFilterContTotal() {
		return mtFilterContTotal;
	}

	public void setMtFilterContTotal(Long mtFilterContTotal) {
		this.mtFilterContTotal = mtFilterContTotal;
	}

	public Long getMtSubmitCountNumber() {
		return mtSubmitCountNumber;
	}

	public void setMtSubmitCountNumber(Long mtSubmitCountNumber) {
		this.mtSubmitCountNumber = mtSubmitCountNumber;
	}

	public Long getMoTotal() {
		return moTotal;
	}

	public void setMoTotal(Long moTotal) {
		this.moTotal = moTotal;
	}

	public Long getMoSuccess() {
		return moSuccess;
	}

	public void setMoSuccess(Long moSuccess) {
		this.moSuccess = moSuccess;
	}

	public Long getMtTotalHelp() {
		return mtTotalHelp;
	}

	public void setMtTotalHelp(Long mtTotalHelp) {
		this.mtTotalHelp = mtTotalHelp;
	}

	public Long getMtWaitProcHelp() {
		return mtWaitProcHelp;
	}

	public void setMtWaitProcHelp(Long mtWaitProcHelp) {
		this.mtWaitProcHelp = mtWaitProcHelp;
	}

	public Long getMtWaitSendHelp() {
		return mtWaitSendHelp;
	}

	public void setMtWaitSendHelp(Long mtWaitSendHelp) {
		this.mtWaitSendHelp = mtWaitSendHelp;
	}

	public Long getMtSendedTotalHelp() {
		return mtSendedTotalHelp;
	}

	public void setMtSendedTotalHelp(Long mtSendedTotalHelp) {
		this.mtSendedTotalHelp = mtSendedTotalHelp;
	}

	public Long getMtSendErrorHelp() {
		return mtSendErrorHelp;
	}

	public void setMtSendErrorHelp(Long mtSendErrorHelp) {
		this.mtSendErrorHelp = mtSendErrorHelp;
	}

	public Long getMtSuccessHelp() {
		return mtSuccessHelp;
	}

	public void setMtSuccessHelp(Long mtSuccessHelp) {
		this.mtSuccessHelp = mtSuccessHelp;
	}

	public Long getMtFailTotalHelp() {
		return mtFailTotalHelp;
	}

	public void setMtFailTotalHelp(Long mtFailTotalHelp) {
		this.mtFailTotalHelp = mtFailTotalHelp;
	}

	public Long getMtFailEtHelp() {
		return mtFailEtHelp;
	}

	public void setMtFailEtHelp(Long mtFailEtHelp) {
		this.mtFailEtHelp = mtFailEtHelp;
	}

	public Long getMtFailCarrierHelp() {
		return mtFailCarrierHelp;
	}

	public void setMtFailCarrierHelp(Long mtFailCarrierHelp) {
		this.mtFailCarrierHelp = mtFailCarrierHelp;
	}

	public Long getMtNoReportHelp() {
		return mtNoReportHelp;
	}

	public void setMtNoReportHelp(Long mtNoReportHelp) {
		this.mtNoReportHelp = mtNoReportHelp;
	}

	public Long getMtNoQuotaHelp() {
		return mtNoQuotaHelp;
	}

	public void setMtNoQuotaHelp(Long mtNoQuotaHelp) {
		this.mtNoQuotaHelp = mtNoQuotaHelp;
	}

	public Long getMtFilterRepTotalHelp() {
		return mtFilterRepTotalHelp;
	}

	public void setMtFilterRepTotalHelp(Long mtFilterRepTotalHelp) {
		this.mtFilterRepTotalHelp = mtFilterRepTotalHelp;
	}

	public Long getMtFilterContTotalHelp() {
		return mtFilterContTotalHelp;
	}

	public void setMtFilterContTotalHelp(Long mtFilterContTotalHelp) {
		this.mtFilterContTotalHelp = mtFilterContTotalHelp;
	}

	public Long getMtSubmitCountHelp() {
		return mtSubmitCountHelp;
	}

	public void setMtSubmitCountHelp(Long mtSubmitCountHelp) {
		this.mtSubmitCountHelp = mtSubmitCountHelp;
	}

	public Long getMoTotalHelp() {
		return moTotalHelp;
	}

	public void setMoTotalHelp(Long moTotalHelp) {
		this.moTotalHelp = moTotalHelp;
	}

	public Long getMoSuccessHelp() {
		return moSuccessHelp;
	}

	public void setMoSuccessHelp(Long moSuccessHelp) {
		this.moSuccessHelp = moSuccessHelp;
	}

}
