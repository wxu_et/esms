package com.etonenet.entity.system;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TS_URL database table.
 * 
 */
@Entity
@Table(name = "TS_URL")
@NamedQuery(name = "Url.findAll", query = "SELECT u FROM Url u")
public class Url implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_URL_URLID_GENERATOR", sequenceName = "SEQ_URL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_URL_URLID_GENERATOR")
	@Column(name = "URL_ID")
	private Long urlId;

	@Column(name = "IS_EXIST")
	private int isExist;

	private String url;

	@Column(name = "URL_DESC")
	private String urlDesc;

	public Url() {
	}

	public Long getUrlId() {
		return this.urlId;
	}

	public void setUrlId(Long urlId) {
		this.urlId = urlId;
	}

	public int getIsExist() {
		return this.isExist;
	}

	public void setIsExist(int isExist) {
		this.isExist = isExist;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlDesc() {
		return this.urlDesc;
	}

	public void setUrlDesc(String urlDesc) {
		this.urlDesc = urlDesc;
	}

}