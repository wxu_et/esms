package com.etonenet.entity.system;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TS_USER_LOGIN database table.
 * 
 */
@Entity
@Table(name = "TS_USER_LOGIN")
@NamedQuery(name = "UserLogin.findAll", query = "SELECT u FROM UserLogin u")
public class UserLogin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_USER_LOGIN_ID_GENERATOR", sequenceName = "SEQ_USER_LOGIN", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_USER_LOGIN_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 10)
	private Long id;

	@Column(length = 50)
	private String ip;

	@Column(name = "IS_SUC", precision = 2)
	private int isSuc;

	@Column(name = "LOGIN_NAME", length = 50)
	private String loginName;

	@Column(name = "LOGIN_TIME")
	private Timestamp loginTime;

	@Column(length = 50)
	private String password;

	public UserLogin() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getIsSuc() {
		return this.isSuc;
	}

	public void setIsSuc(int isSuc) {
		this.isSuc = isSuc;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Timestamp getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}