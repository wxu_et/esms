package com.etonenet.entity.system.security;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.etonenet.entity.system.UrlAction;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the TS_USER_GROUP database table.
 * 
 */
@Entity
@Table(name = "TS_USER_GROUP")
@NamedQuery(name = "UserGroup.findAll", query = "SELECT u FROM UserGroup u")
public class UserGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_USER_GROUP_USERGROUPID_GENERATOR", sequenceName = "SEQ_USER_GROUP", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_USER_GROUP_USERGROUPID_GENERATOR")
	@Column(name = "USER_GROUP_ID")
	private Long userGroupId;

	private String code;

	@Column(name = "CODE_PATH")
	private String codePath;

	@Column(name = "CREATE_TIME")
	private Timestamp createTime;

	@Column(name = "IS_MENU")
	private BigDecimal isMenu;

	@Column(name = "LOGIC_STAT")
	private BigDecimal logicStat;

	@Column(name = "MENU_NAME")
	private String menuName;

	@Column(name = "MENU_PRIORITY")
	private BigDecimal menuPriority;

	private String name;

	@Column(name = "UPDATE_TIME")
	private Timestamp updateTime;

	// uni-directional many-to-one association to UrlAction
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "url_action_id")
	private UrlAction urlAction;

	// bi-directional many-to-one association to UserGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	private UserGroup parentUserGroup;

	// bi-directional many-to-one association to UserGroup
	@JsonIgnore
	@OneToMany(mappedBy = "parentUserGroup")
	private List<UserGroup> childUserGroups;

	public UserGroup() {
	}

	public Long getUserGroupId() {
		return this.userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodePath() {
		return this.codePath;
	}

	public void setCodePath(String codePath) {
		this.codePath = codePath;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public BigDecimal getIsMenu() {
		return this.isMenu;
	}

	public void setIsMenu(BigDecimal isMenu) {
		this.isMenu = isMenu;
	}

	public BigDecimal getLogicStat() {
		return this.logicStat;
	}

	public void setLogicStat(BigDecimal logicStat) {
		this.logicStat = logicStat;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public BigDecimal getMenuPriority() {
		return this.menuPriority;
	}

	public void setMenuPriority(BigDecimal menuPriority) {
		this.menuPriority = menuPriority;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public UrlAction getUrlAction() {
		return this.urlAction;
	}

	public void setUrlAction(UrlAction urlAction) {
		this.urlAction = urlAction;
	}

	public UserGroup getParentUserGroup() {
		return this.parentUserGroup;
	}

	public void setParentUserGroup(UserGroup parentUserGroup) {
		this.parentUserGroup = parentUserGroup;
	}

	public List<UserGroup> getChildUserGroups() {
		return this.childUserGroups;
	}

	public void setChildUserGroups(List<UserGroup> childUserGroups) {
		this.childUserGroups = childUserGroups;
	}

	public UserGroup addChildUserGroup(UserGroup childUserGroup) {
		getChildUserGroups().add(childUserGroup);
		childUserGroup.setParentUserGroup(this);

		return childUserGroup;
	}

	public UserGroup removeChildUserGroup(UserGroup childUserGroup) {
		getChildUserGroups().remove(childUserGroup);
		childUserGroup.setParentUserGroup(null);

		return childUserGroup;
	}

}