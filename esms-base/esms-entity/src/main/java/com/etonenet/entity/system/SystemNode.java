package com.etonenet.entity.system;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TS_SYSTEM_NODE database table.
 * 
 */
@Entity
@Table(name = "TS_SYSTEM_NODE")
@NamedQuery(name = "SystemNode.findAll", query = "SELECT s FROM SystemNode s")
public class SystemNode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_SYSTEM_NODE_SYSTEMNODEID_GENERATOR", sequenceName = "SEQ_SYSTEM_NODE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_SYSTEM_NODE_SYSTEMNODEID_GENERATOR")
	@Column(name = "SYSTEM_NODE_ID")
	private Long systemNodeId;

	@Column(name = "DISPLAY_ORDER", precision = 5)
	private Integer displayOrder;

	@Column(length = 100)
	private String icon;

	@Column(name = "NODE_NAME", length = 50)
	private String nodeName;

	@Column(name = "NODE_TYPE", precision = 2)
	private int nodeType;

	@Column(length = 800)
	private String url;

	// bi-directional many-to-one association to SystemNode
	@ManyToOne
	@JoinColumn(name = "PARENT_ID")
	private SystemNode parent;

	// bi-directional many-to-one association to SystemNode
	@OneToMany(mappedBy = "parent")
	private List<SystemNode> childNodes;

	public SystemNode() {
	}

	public Long getSystemNodeId() {
		return this.systemNodeId;
	}

	public void setSystemNodeId(Long systemNodeId) {
		this.systemNodeId = systemNodeId;
	}

	public Integer getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getNodeName() {
		return this.nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public int getNodeType() {
		return this.nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public SystemNode getParent() {
		return this.parent;
	}

	public void setParent(SystemNode parent) {
		this.parent = parent;
	}

	public List<SystemNode> getChildNodes() {
		return this.childNodes;
	}

	public void setChildNodes(List<SystemNode> childNodes) {
		this.childNodes = childNodes;
	}

	public SystemNode addChildNode(SystemNode childNode) {
		getChildNodes().add(childNode);
		childNode.setParent(this);

		return childNode;
	}

	public SystemNode removeChildNode(SystemNode childNode) {
		getChildNodes().remove(childNode);
		childNode.setParent(null);

		return childNode;
	}

}