package com.etonenet.entity.system.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TS_ROLE database table.
 * 
 */
@Entity
@Table(name = "TS_ROLE")
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_ROLE_ROLEID_GENERATOR", sequenceName = "SEQ_ROLE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_ROLE_ROLEID_GENERATOR")
	@Column(name = "ROLE_ID", unique = true, nullable = false, precision = 10)
	private Long roleId;

	@Column(name = "DISPLAY_NAME", length = 100)
	private String displayName;

	@Column(name = "ROLE_NAME", length = 50)
	private String roleName;

	public Role() {
	}

	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}