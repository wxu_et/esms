package com.etonenet.entity.system;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TS_URL_ACTION database table.
 * 
 */
@Entity
@Table(name = "TS_URL_ACTION")
@NamedQuery(name = "UrlAction.findAll", query = "SELECT u FROM UrlAction u")
public class UrlAction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TS_URL_ACTION_URLACTIONID_GENERATOR", sequenceName = "SEQ_URL_ACTION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TS_URL_ACTION_URLACTIONID_GENERATOR")
	@Column(name = "URL_ACTION_ID")
	private Long urlActionId;

	@Column(name = "LOGIC_STAT")
	private BigDecimal logicStat;

	private String url;

	@Column(name = "URL_NAME")
	private String urlName;

	public UrlAction() {
	}

	public Long getUrlActionId() {
		return this.urlActionId;
	}

	public void setUrlActionId(Long urlActionId) {
		this.urlActionId = urlActionId;
	}

	public BigDecimal getLogicStat() {
		return this.logicStat;
	}

	public void setLogicStat(BigDecimal logicStat) {
		this.logicStat = logicStat;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlName() {
		return this.urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

}