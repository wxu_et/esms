package com.etonenet.repository.system;

import com.etonenet.entity.system.ErrorCode;

import esms.etonenet.common.repository.BaseRepository;

public interface ErrorCodeRepository extends BaseRepository<ErrorCode, Long> {

}
