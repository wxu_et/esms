package com.etonenet.repository.system.security;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.security.RoleUser;
import com.etonenet.entity.system.security.RoleUserPK;

import esms.etonenet.common.repository.BaseRepository;

public interface RoleUserRepository extends BaseRepository<RoleUser, RoleUserPK> {

	@Query(nativeQuery = true, value = "delete from ts_role_user where role_id = ?1")
	@Modifying
	void deleteByRoleId(Long roleId);

	@Query(nativeQuery = true, value = "select * from ts_role_user where user_id = ?1")
	List<RoleUser> findByUserId(Long userId);
}
