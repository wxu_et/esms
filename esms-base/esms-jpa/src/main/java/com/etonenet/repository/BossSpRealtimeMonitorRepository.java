package com.etonenet.repository;

import com.etonenet.entity.BossSpRealtimeMonitor;

import esms.etonenet.common.repository.BaseRepository;

public interface BossSpRealtimeMonitorRepository extends BaseRepository<BossSpRealtimeMonitor, Long> {

}
