package com.etonenet.repository.system;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.Url;

import esms.etonenet.common.repository.BaseRepository;

public interface UrlRepository extends BaseRepository<Url, Long> {

	Url findByUrl(String url);

	@Query(nativeQuery = true, value = "select url label,url value from TS_URL where url like %?1% and rownum <= 20")
	List<Object[]> auto(String term);
}
