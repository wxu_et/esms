package com.etonenet.repository.system;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.UrlAction;

import esms.etonenet.common.repository.BaseRepository;

public interface UrlActionRepository extends BaseRepository<UrlAction, Long> {

	UrlAction findByUrlAndLogicStat(String url, BigDecimal logicStat);

	@Query(nativeQuery = true, value = "select url label,url value from TS_URL_ACTION where url like %?1% and rownum <= 20")
	List<Object[]> autoUrlAction(String term);
}
