package com.etonenet.repository.system.security;

import com.etonenet.entity.system.security.Role;

import esms.etonenet.common.repository.BaseRepository;

public interface RoleRepository extends BaseRepository<Role, Long> {

}
