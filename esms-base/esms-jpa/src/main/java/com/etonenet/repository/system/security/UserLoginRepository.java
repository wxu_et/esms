package com.etonenet.repository.system.security;

import com.etonenet.entity.system.UserLogin;

import esms.etonenet.common.repository.BaseRepository;

public interface UserLoginRepository extends BaseRepository<UserLogin, Long> {

}
