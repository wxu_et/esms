package com.etonenet.repository.system.security;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.security.UserGroup;

import esms.etonenet.common.repository.BaseRepository;

public interface UserGroupRepository extends BaseRepository<UserGroup, Long> {

	@Query(nativeQuery = true, value = "select max(menu_priority) from ts_user_group where parent_id = ?1")
	BigDecimal getMaxMenuPriority(Long parentId);

	@Query(nativeQuery = true, value = "select code||'-'||name label,user_group_id value from ts_user_group where logic_stat = 0")
	List<?> listSelectParentForCreate();

	@Modifying
	@Query(nativeQuery = true, value = "update ts_user_group set logic_stat = 1 where user_group_id = ?1")
	void delUserGroup(Long ugid);

	@Modifying
	@Query(nativeQuery = true, value = "select * from ts_user_group where code_path like ?1%")
	List<UserGroup> getAllChildCodePath(String codePath);
}
