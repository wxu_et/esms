package com.etonenet.repository.system.security;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.security.UserBs;

import esms.etonenet.common.repository.BaseRepository;

public interface UserBsRepository extends BaseRepository<UserBs, Long> {

	UserBs findByLoginName(String loginName);

	@Query(nativeQuery = true, value = "select USER_ID||'-'||USER_NAME label,USER_ID||'-'||USER_NAME value from TS_USER_BS where (USER_ID like %?1% OR lower(USER_NAME) like '%'||lower(?1)||'%') and rownum <= 20")
	List<Object[]> auto(String term);

	@Query(nativeQuery = true, value = "SELECT distinct u.* FROM TS_USER_BS u ,TS_ROLE_USER ru where u.USER_ID=RU.USER_ID and RU.ROLE_ID = ?1 and u.user_state=1 order by login_name")
	List<UserBs> listUsersByRoleId(Long roleId);

	@Query(nativeQuery = true, value = "select * from ts_user_bs where user_state=1 order by login_name")
	List<UserBs> listAllUser();

}
