package esms.etonenet.common.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public interface BaseRepository<T, ID extends Serializable> extends PagingAndSortingRepository<T, Serializable> {

	public Page<T> findAll(Specification<T> spec, Pageable pageable);

	@Override
	<S extends T> S save(S entity);

	@Override
	<S extends T> Iterable<S> save(Iterable<S> entities);

	@Override
	T findOne(Serializable id);

	@Override
	boolean exists(Serializable id);

	@Override
	Iterable<T> findAll();

	@Override
	Iterable<T> findAll(Iterable<Serializable> ids);

	@Override
	long count();

	@Override
	void delete(Serializable id);

	@Override
	void delete(T entity);

	@Override
	void delete(Iterable<? extends T> entities);

	@Override
	void deleteAll();

	@Override
	Iterable<T> findAll(Sort sort);

	@Override
	Page<T> findAll(Pageable pageable);
}
