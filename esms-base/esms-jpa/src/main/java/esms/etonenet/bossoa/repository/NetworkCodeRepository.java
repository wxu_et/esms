package esms.etonenet.bossoa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.bossoa.entity.NetworkCode;
import esms.etonenet.common.repository.BaseRepository;

public interface NetworkCodeRepository extends BaseRepository<NetworkCode, String> {

	@Query(nativeQuery = true, value = "select DISTINCT NETWORK_CODE||'-'||NETWORK_NAME label,NETWORK_CODE||'-'||NETWORK_NAME value from TC_NETWORK_CODE where (lower(NETWORK_CODE) like '%'||lower(?1)||'%' or lower(NETWORK_NAME) like '%'||lower(?1)||'%') and ROWNUM <= 200")
	List<Object[]> auto(String term);
}