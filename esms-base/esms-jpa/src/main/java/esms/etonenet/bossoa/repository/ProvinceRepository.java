package esms.etonenet.bossoa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.bossoa.entity.Province;
import esms.etonenet.common.repository.BaseRepository;

public interface ProvinceRepository extends BaseRepository<Province, String> {

	@Query(nativeQuery = true, value = "select DISTINCT PROVINCE_CODE||'-'||PROVINCE_NAME label,PROVINCE_CODE||'-'||PROVINCE_NAME value from TC_PROVINCE where COUNTRY_CODE = ?1 and (PROVINCE_CODE like %?2% or PROVINCE_NAME like %?2%) and ROWNUM <= 200")
	List<Object[]> auto(String countryCode, String term);

	@Query(nativeQuery = true, value = "select DISTINCT PROVINCE_CODE||'-'||PROVINCE_NAME label,PROVINCE_CODE||'-'||PROVINCE_NAME value from TC_PROVINCE where (PROVINCE_CODE like %?1% or PROVINCE_NAME like %?1%) and ROWNUM <= 200")
	List<Object[]> autoSingle(String term);
}
