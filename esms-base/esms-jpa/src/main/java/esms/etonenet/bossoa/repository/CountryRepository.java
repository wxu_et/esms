package esms.etonenet.bossoa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.bossoa.entity.Country;
import esms.etonenet.common.repository.BaseRepository;

public interface CountryRepository extends BaseRepository<Country, String> {

	@Query(nativeQuery = true, value = "select DISTINCT country_code||'-'||country_name label,country_code||'-'||country_name value from tc_country where (country_code like %?1% or country_name like %?1%) and ROWNUM <= 200")
	List<Object[]> auto(String term);
}
