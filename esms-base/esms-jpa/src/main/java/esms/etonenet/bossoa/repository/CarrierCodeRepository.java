package esms.etonenet.bossoa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.bossoa.entity.CarrierCode;
import esms.etonenet.common.repository.BaseRepository;

public interface CarrierCodeRepository extends BaseRepository<CarrierCode, String> {

	@Query(nativeQuery = true, value = "select DISTINCT CARRIER_CODE||'-'||CARRIER_NAME label,CARRIER_CODE||'-'||CARRIER_NAME value from TC_CARRIER_CODE where (lower(CARRIER_CODE) like '%'||lower(?1)||'%' or lower(CARRIER_NAME) like '%'||lower(?1)||'%') and ROWNUM <= 200")
	List<Object[]> auto(String term);
}