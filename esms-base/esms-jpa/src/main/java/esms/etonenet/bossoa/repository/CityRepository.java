package esms.etonenet.bossoa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.bossoa.entity.City;
import esms.etonenet.common.repository.BaseRepository;

public interface CityRepository extends BaseRepository<City, String> {

	@Query(nativeQuery = true, value = "select DISTINCT CITY_CODE||'-'||CITY_NAME label,CITY_CODE||'-'||CITY_NAME value from TC_CITY where PROVINCE_CODE = ?1 and (CITY_CODE like %?2% or CITY_NAME like %?2%) and ROWNUM <= 200")
	List<Object[]> auto(String provinceCode, String term);

	@Query(nativeQuery = true, value = "select DISTINCT CITY_CODE||'-'||CITY_NAME label,CITY_CODE||'-'||CITY_NAME value from TC_CITY where (CITY_CODE like %?1% or CITY_NAME like %?1%) and ROWNUM <= 200")
	List<Object[]> autoSingle(String term);
}
