package esms.etonenet.boss2.repository;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.allboss.entity.BossSpRealtimeMonitor;
import esms.etonenet.common.repository.BaseRepository;

public interface SpRealtimeMonitorBoss2Repository extends BaseRepository<BossSpRealtimeMonitor, Long> {

	@Query(nativeQuery = true, value = "SELECT COALESCE(SUM(MT_TOTAL),0) FROM TT_SP_RP_F WHERE COUNT_FLAG=4 AND COUNT_FLAG=4 AND TIME_UNIT_FLAG=5 AND SP_ID<>'*' AND CHANNEL_ID<>'*' "
			+ "AND SP_SERVICE_CODE<>'*' AND CARRIER_CODE<>'*' AND COUNTRY_CODE<>'*' AND PROVINCE_CODE<>'*' AND CITY_CODE<>'*' AND TO_CHAR(start_time,'yyyy-MM-dd HH:mm')>= to_char(trunc(sysdate),'yyyy-MM-dd HH:mm') and TO_CHAR(start_time,'yyyy-MM-dd HH:mm')< to_char(sysdate,'yyyy-MM-dd HH:mm')")

	Long getBossMtTotal();

}
