package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.ChannelGroupConfig;
import esms.etonenet.common.repository.BaseRepository;

public interface ChannelGroupConfigRepository extends BaseRepository<ChannelGroupConfig, Long> {

	@Query(nativeQuery = true, value = "update tm_channel_group_config set logic_stat = 1 where channel_group_config_id = ?1")
	@Modifying
	void del(Long cgcId);

	@Query(nativeQuery = true, value = "select * from tm_channel_group_config where logic_stat = 0")
	List<ChannelGroupConfig> findAllLogic();
}
