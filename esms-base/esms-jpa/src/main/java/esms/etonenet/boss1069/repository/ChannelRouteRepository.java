package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.ChannelRoute;
import esms.etonenet.common.repository.BaseRepository;

public interface ChannelRouteRepository extends BaseRepository<ChannelRoute, Long> {

}
