package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.SpMonitorManage;
import esms.etonenet.common.repository.BaseRepository;

public interface SpMonitorManageRepository extends BaseRepository<SpMonitorManage, String> {

	@Query(nativeQuery = true, value = "select * from TT_SP_MONITORMANAGE")
	List<SpMonitorManage> queryAll();
}
