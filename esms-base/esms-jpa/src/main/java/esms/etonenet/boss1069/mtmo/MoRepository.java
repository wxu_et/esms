package esms.etonenet.boss1069.mtmo;

import esms.etonenet.common.repository.BaseRepository;

public interface MoRepository extends BaseRepository<Mo, Long> {

}
