package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.SpService;
import esms.etonenet.boss1069.entity.SpServicePK;
import esms.etonenet.common.repository.BaseRepository;

public interface SpServiceRepository extends BaseRepository<SpService, SpServicePK> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT SP_SERVICE_CODE LABEL,SP_SERVICE_CODE VALUE FROM TM_SP_SERVICE WHERE SP_SERVICE_CODE LIKE %?1% AND SP_ID = ?2 AND ROWNUM <= 20 ORDER BY LABEL")
	List<Object[]> autoSpServiceCode(String term, String spId);
}
