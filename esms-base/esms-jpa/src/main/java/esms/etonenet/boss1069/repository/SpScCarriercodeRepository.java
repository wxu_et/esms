package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.SpScCarriercode;
import esms.etonenet.boss1069.entity.SpScCarriercodePK;
import esms.etonenet.common.repository.BaseRepository;

public interface SpScCarriercodeRepository extends BaseRepository<SpScCarriercode, SpScCarriercodePK> {

}
