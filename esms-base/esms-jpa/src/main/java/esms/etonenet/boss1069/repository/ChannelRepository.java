package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.common.repository.BaseRepository;

public interface ChannelRepository extends BaseRepository<Channel, String> {

	@Query(nativeQuery = true, value = "select CHANNEL_ID||'-'||CHANNEL_NAME label,CHANNEL_ID||'-'||CHANNEL_NAME value from TM_CHANNEL where (CHANNEL_ID like %?1% OR lower(CHANNEL_NAME) like '%'||lower(?1)||'%') and rownum <= 20")
	List<Object[]> autoChannel(String term);

	@Query(nativeQuery = true, value = "select CHANNEL_ID||'-'||CHANNEL_NAME label,CHANNEL_ID||'-'||CHANNEL_NAME value from TM_CHANNEL where (CHANNEL_ID like %?1% OR lower(CHANNEL_NAME) like '%'||lower(?1)||'%') and (CHANNEL_BAOBEI_FLAG=1) and rownum <= 20")
	List<Object[]> autoChannelbb(String term);
}
