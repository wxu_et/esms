package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.SpAlarm;
import esms.etonenet.common.repository.BaseRepository;

public interface SpAlarmRepository extends BaseRepository<SpAlarm, Long> {

}
