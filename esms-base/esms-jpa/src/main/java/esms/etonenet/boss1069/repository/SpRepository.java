package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.entity.SpChannelPK;
import esms.etonenet.common.repository.BaseRepository;

public interface SpRepository extends BaseRepository<Sp, SpChannelPK> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT SP_ID||'-'||SP_NAME LABEL,SP_ID||'-'||SP_NAME VALUE FROM TM_SP WHERE (SP_ID LIKE %?1% OR LOWER(SP_NAME) LIKE '%'||LOWER(?1)||'%') AND ROWNUM <= 20 ORDER BY LABEL")
	List<Object[]> autoSP(String term);

}
