package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.MtAddress;
import esms.etonenet.common.repository.BaseRepository;

public interface MtAddressRepository extends BaseRepository<MtAddress, String> {

	@Query(value = "SELECT o FROM MtAddress o WHERE o.addressName = ?1")
	public MtAddress findByName(String addressName);

	@Query(nativeQuery = true, value = "select address_name||'-'||target label,address_name||'-'||target value from ts_mt_address where (target like %?1% or address_name like %?1%)")
	public List<Object[]> autoMtAddr(String term);

	@Query(nativeQuery = true, value = "select address_name||'-'||target label,target value from ts_mt_address where (target like %?1% or address_name like %?1%)")
	public List<Object[]> auto2MtAddr(String term);
}
