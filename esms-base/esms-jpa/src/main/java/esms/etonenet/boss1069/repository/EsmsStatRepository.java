package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.EsmsStat;
import esms.etonenet.common.repository.BaseRepository;

public interface EsmsStatRepository extends BaseRepository<EsmsStat, Long> {

}
