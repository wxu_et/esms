package esms.etonenet.boss1069.repositoryReport;

import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.report.SpSubmitMonitor;
import esms.etonenet.common.repository.BaseRepository;

public interface SpSubmitMonitorRepository extends BaseRepository<SpSubmitMonitor, Long> {

	Integer countBySp(Sp sp);

}