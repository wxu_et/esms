package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.SpMoRoute;
import esms.etonenet.common.repository.BaseRepository;

public interface SpMoRouteRepository extends BaseRepository<SpMoRoute, Long> {

	// @Modifying
	// @Query(nativeQuery = true, value =
	// "update ts_user_group set logic_stat = 1 where user_group_id = ?1")
	// void delUserGroup(Long ugid);
}
