package esms.etonenet.boss1069.repository;

import esms.etonenet.boss1069.entity.SpMtRoute;
import esms.etonenet.common.repository.BaseRepository;

public interface SpMtRouteRepository extends BaseRepository<SpMtRoute, Long> {

}
