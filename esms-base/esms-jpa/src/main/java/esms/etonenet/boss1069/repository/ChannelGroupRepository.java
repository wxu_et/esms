package esms.etonenet.boss1069.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.ChannelGroup;
import esms.etonenet.common.repository.BaseRepository;

public interface ChannelGroupRepository extends BaseRepository<ChannelGroup, String> {

	@Query(nativeQuery = true, value = "update tm_channel_group set logic_stat = 1 where channel_group_id = ?1")
	@Modifying
	void del(String cgId);

	@Query(nativeQuery = true, value = "select CHANNEL_GROUP_ID||'-'||CHANNEL_GROUP_NAME label,CHANNEL_GROUP_ID||'-'||CHANNEL_GROUP_NAME value from TM_CHANNEL_GROUP where (lower(CHANNEL_GROUP_ID) like '%'||lower(?1)||'%' OR lower(CHANNEL_GROUP_NAME) like '%'||lower(?1)||'%') and logic_stat = 0 and rownum <= 20")
	List<Object[]> auto(String term);
}
