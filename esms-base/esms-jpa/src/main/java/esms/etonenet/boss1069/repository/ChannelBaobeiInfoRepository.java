package esms.etonenet.boss1069.repository;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.entity.ChannelBaobeiInfo;
import esms.etonenet.common.repository.BaseRepository;

public interface ChannelBaobeiInfoRepository extends BaseRepository<ChannelBaobeiInfo, Long> {

	@Query(nativeQuery = true, value = "select * from TM_CHANNEL_BAOBEI_INFO where baobei_number = ?1 and channel_id = ?2")
	ChannelBaobeiInfo idcheck(String bbid, String chid);

	Integer countByChannel(Channel channel);
}
