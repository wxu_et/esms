package esms.etonenet.oldboss.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.common.repository.BaseRepository;
import esms.etonenet.oldboss.entity.OldSp;

public interface OldSpRepository extends BaseRepository<OldSp, String> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT SP_ID||'-'||SP_NAME LABEL,SP_ID||'-'||SP_NAME VALUE FROM TM_SP WHERE (SP_ID LIKE %?1% OR LOWER(SP_NAME) LIKE '%'||LOWER(?1)||'%') AND ROWNUM <= 20 ORDER BY LABEL")
	List<Object[]> autoSP(String term);

	@Query(nativeQuery = true, value = "SELECT DISTINCT SP_ID||'-'||SP_NAME LABEL,SP_ID VALUE FROM TM_SP WHERE (SP_ID LIKE %?1% OR LOWER(SP_NAME) LIKE '%'||LOWER(?1)||'%') AND ROWNUM <= 20 ORDER BY LABEL")
	List<Object[]> auto2SP(String term);

	@Query(nativeQuery = true, value = "SELECT PASSWORD FROM TM_USER WHERE USER_ID = ?1")
	public String getPwd(BigDecimal userId);
}