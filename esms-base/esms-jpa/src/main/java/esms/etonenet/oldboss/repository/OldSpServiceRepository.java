package esms.etonenet.oldboss.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import esms.etonenet.common.repository.BaseRepository;
import esms.etonenet.oldboss.entity.OldSpService;

public interface OldSpServiceRepository extends BaseRepository<OldSpService, String> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT SP_SERVICE_CODE LABEL,SP_SERVICE_CODE VALUE FROM TM_SP_SERVICE WHERE SP_SERVICE_CODE LIKE %?1% AND SP_ID = ?2 AND ROWNUM <= 20 ORDER BY LABEL")
	List<Object[]> autoSpServiceCode(String term, String spId);
}