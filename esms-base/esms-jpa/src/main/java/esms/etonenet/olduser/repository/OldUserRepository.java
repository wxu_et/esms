package esms.etonenet.olduser.repository;

import esms.etonenet.common.repository.BaseRepository;
import esms.etonenet.olduser.entity.OldUser;

public interface OldUserRepository extends BaseRepository<OldUser, Long> {

	OldUser findByUserId(Long userId);
}