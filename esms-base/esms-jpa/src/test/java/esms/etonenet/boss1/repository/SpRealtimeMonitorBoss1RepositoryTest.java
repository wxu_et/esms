package esms.etonenet.boss1.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.etonenet.config.JpaConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfig.class})
public class SpRealtimeMonitorBoss1RepositoryTest {

	@Autowired
	private SpRealtimeMonitorBoss1Repository repository;
	
	@Test
	public void test() {
		repository.count();
	}

}
