package com.etonenet.util;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil extends StringUtils {
	private static Logger logger = LoggerFactory.getLogger(StringUtil.class);
	private static Pattern mobileNumberPattern = Pattern.compile("\\d{1,20}");

	public static Date getFormateDate(String date) {

		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
		} catch (ParseException e) {
			logger.error("parse the date for formate yyyy-MM-dd HH:mm:ss failed");
			e.printStackTrace();
		}
		return new Date();
	}

	public static Date getMonthBegin(String year, String month) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(year + "-" + month + "-01 00:00:00");
		} catch (ParseException e) {
			logger.error("parse the date for formate yyyy-MM-dd HH:mm:ss failed");
			e.printStackTrace();
		}
		return new Date();
	}

	public static Date getMonthEnd(String year, String month) {

		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.parse(year + "-" + getNextMonth(year, month) + "-01 00:00:00");
		} catch (ParseException e) {
			logger.error("parse the date for formate yyyy-MM-dd HH:mm:ss failed");
			e.printStackTrace();
		}
		return new Date();
	}

	private static int getNextMonth(String year, String month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, Integer.parseInt(year));
		cal.set(Calendar.MONTH, Integer.parseInt(month));

		cal.add(Calendar.MONTH, 1);
		// 某年某月的最后一天
		return cal.get(Calendar.MONDAY);
	}

	@SuppressWarnings("unused")
	private static int getLastDayOfMonth(String year, String month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, Integer.parseInt(year));
		cal.set(Calendar.MONTH, Integer.parseInt(month));
		// 某年某月的最后一天
		return cal.getActualMaximum(Calendar.DATE);
	}

	public static String hidden(String msisden) {
		if (msisden != null && msisden.length() > 8) {
			return msisden.substring(0, msisden.length() - 8) + "*****" + msisden.substring(msisden.length() - 3);
		} else {
			return "";
		}
	}

	public static String splitMinusFirstGroup(String str) {
		String ret = "";
		if (isNotEmpty(str))
			ret = str.split("-")[0];

		return ret;
	}

	public static boolean isMobileNumber(String mobile) {
		return StringUtils.isNotBlank(mobile) && mobileNumberPattern.matcher(mobile).matches();
	}

	public static String decodeHexString(int dataCoding, String hexStr) {
		String realStr = null;
		try {
			if (hexStr != null) {
				if (dataCoding == 15) {
					realStr = new String(Hex.decodeHex(hexStr.toCharArray()), "GBK");
				} else if ((dataCoding & 0x0C) == 0x08) {
					realStr = new String(Hex.decodeHex(hexStr.toCharArray()), "UnicodeBigUnmarked");
				} else {
					realStr = new String(Hex.decodeHex(hexStr.toCharArray()), "ISO8859-1");
				}
			}
		} catch (UnsupportedEncodingException | DecoderException e) {
			logger.error(e.getMessage(), e);
		}
		return realStr;
	}

	public static String decodeStringHex(int dataCoding, String hexStr) {
		StringBuffer sb = new StringBuffer();
		char[] chars = {};
		try {
			if (dataCoding == 15)
				chars = Hex.encodeHex(hexStr.getBytes("GBK"));
			else if ((dataCoding & 0x0C) == 0x08)
				chars = Hex.encodeHex(hexStr.getBytes("UnicodeBigUnmarked"));
			else
				chars = Hex.encodeHex(hexStr.getBytes("ISO8859-1"));
			for (char c : chars)
				sb.append(c);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
		return sb.toString();
	}
}
