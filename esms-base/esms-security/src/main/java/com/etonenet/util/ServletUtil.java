package com.etonenet.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class ServletUtil {

	public static boolean isXhr(HttpServletRequest request) {
		return StringUtils.isNotEmpty(request.getHeader("X-Requested-With"))
				|| request.getHeader("accept").indexOf("application/json") != -1;
	}

	public static String URLSend(String url) throws Exception {

		BufferedReader in = null;
		String result = "";
		URL u = null;
		try {
			u = new URL(url);
			HttpURLConnection httpURLConnection = (HttpURLConnection) u.openConnection();
			httpURLConnection.setReadTimeout(20000);
			httpURLConnection.setConnectTimeout(20000);
			in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			result = in.readLine();
		} catch (Exception e) {
			throw e;
		} finally {
			if (in != null)
				in.close();
		}
		return result;

	}
}