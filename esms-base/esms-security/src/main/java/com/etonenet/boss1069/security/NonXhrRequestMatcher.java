package com.etonenet.boss1069.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;

import com.etonenet.util.ServletUtil;

public class NonXhrRequestMatcher implements RequestMatcher {

	@Override
	public boolean matches(HttpServletRequest request) {
		if (ServletUtil.isXhr(request))
			return false;
		else
			return true;
	}
}
