package com.etonenet.boss1069.oauth2;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import com.etonenet.entity.system.security.UserBs;
import com.etonenet.repository.system.security.UserBsRepository;

public class SpringDataClientDetailsService implements ClientDetailsService {

	@Resource
	UserBsRepository userBsEm;

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		BaseClientDetails bcd = null;
		UserBs user = userBsEm.findByLoginName(clientId);
		if (user != null) {
			bcd = new BaseClientDetails();
			bcd.setClientId(user.getLoginName());
			bcd.setClientSecret(user.getPassword());
			List<String> grantTypes = new ArrayList<>();
			grantTypes.add("password");
			bcd.setAuthorizedGrantTypes(grantTypes);
			List<String> scope = new ArrayList<>();
			scope.add("trust");
			bcd.setScope(scope);
		}
		return bcd;
	}

}
