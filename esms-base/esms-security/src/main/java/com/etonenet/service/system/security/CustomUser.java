package com.etonenet.service.system.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.etonenet.entity.system.security.UserBs;

/**
 * 请在CustomUserDetails中实现User的所有字段，并在CustomUserDetailsService中 cache一些用户。
 * <p>
 * 这样，当需要多次访问用户信息时，不用到数据库中查询
 * <p>
 * 如果用户太多，可以只选择几个常用的字段，以节省内存
 */
public class CustomUser extends User {
	private static final long serialVersionUID = -4217266021880842615L;

	private UserBs user;

	private String oauthToken;

	public CustomUser(UserBs user, String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.user = user;
	}

	public CustomUser(UserBs user, String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.user = user;
	}

	public UserBs getUser() {
		return user;
	}

	public CustomUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public String getOauthToken() {
		return oauthToken;
	}

	public void setOauthToken(String oauthToken) {
		this.oauthToken = oauthToken;
	}
}
