package com.etonenet.service.system.security;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class PasswordEncoder implements org.springframework.security.crypto.password.PasswordEncoder {

	private static final String salt = "";

	@Override
	public String encode(CharSequence rawPass) {

		Md5PasswordEncoder md5 = new Md5PasswordEncoder();
		return md5.encodePassword(rawPass.toString(), salt);
	}

	@Override
	public boolean matches(CharSequence rawPass, String encodePass) {

		Md5PasswordEncoder md5 = new Md5PasswordEncoder();
		return md5.isPasswordValid(encodePass, rawPass.toString(), salt);
	}

}
