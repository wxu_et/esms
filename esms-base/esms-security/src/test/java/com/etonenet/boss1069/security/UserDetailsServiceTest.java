package com.etonenet.boss1069.security;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.etonenet.config.SecurityConfig;
import com.etonenet.service.system.security.UserDetailsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SecurityConfig.class})
public class UserDetailsServiceTest {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Test
	public void test() {
		UserDetails user = userDetailsService.loadUserByUsername("admin");
		
		System.out.println(user.getPassword());
	}
	
}
