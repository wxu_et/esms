package com.etonenet.util;

import java.math.BigDecimal;

public class MathUtil {

	public static int getPow2(int item) {

		int r = 2 << item - 2;
		if (r == 0)
			r = 1;
		return r;
	}

	public static BigDecimal sum(Integer[] items) {
		if (items == null || items.length == 0)
			return null;

		int r = 0;
		for (Integer i : items)
			r += i;

		return BigDecimal.valueOf(r);
	}
}
