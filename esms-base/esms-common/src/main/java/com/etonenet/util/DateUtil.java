package com.etonenet.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class DateUtil extends DateUtils {

	public static Date getCurrentDate() {

		return new Date();
	}

	public static long getCurrentTime() {

		return System.currentTimeMillis();
	}

	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(getCurrentTime());
	}

	public static Date getTodayBegin() {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public static Date getDayBegin(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/**
	 * 获得传入date的附近几天
	 * 
	 * @author lyzhang
	 * @param date
	 *            +1 明天 -1昨天
	 * @param day
	 * @return 2012-7-27
	 */
	public static Date getNearbyDate(Date date, int day) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, day);
		return c.getTime();
	}

	public static Date getMonthBegin() {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/**
	 * 同日的附近月
	 * 
	 * @author lyzhang
	 * @param date
	 * @param month
	 * @return 2012-7-27
	 */
	public static Date getNearbyMonthDate(Date date, int month) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, month);
		return c.getTime();
	}

	/**
	 * 同日的附近小时
	 * 
	 * @author lyzhang
	 * @param date
	 * @param month
	 * @return 2012-7-27
	 */
	public static Date getNearbyHourDate(Date date, int hour) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, hour);
		return c.getTime();
	}
}
