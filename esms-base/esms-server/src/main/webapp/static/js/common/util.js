function trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");  
}

function isEmail(str) {
	if (str.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
		return true;
	else
		return false;
}

$.fn.inputText=function(val) {
	if(val==undefined)
		val='请输入内容';
	this.each(function(){
        if(this.value==''){
        	this.style.color='gray'
            this.value=val;
        }    
    });
    this.focus(function(){
        if(this.value==val){
        	this.style.color=''
            this.value='';
        }	
    });
    this.blur(function(){
        if(this.value==''){
        	this.style.color='gray'
            this.value=val;
        }    
    });
};

function post(URL, PARAMS) {      
    var temp = document.createElement("form");      
    temp.action = URL;      
    temp.method = "post";      
    temp.style.display = "none";      
    for (var x in PARAMS) {
        var opt = document.createElement("input");
        opt.type = 'hidden';
        opt.name = PARAMS[x].name;      
        opt.value = PARAMS[x].value;      
        temp.appendChild(opt);      
    }      
    document.body.appendChild(temp);      
    temp.submit();      
    return temp;      
}      

//浮点数加法运算   
function FloatAdd(arg1,arg2){
	if(arg1==undefined||arg2==undefined)
		return NaN;
   var r1,r2,m;   
   try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
   try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
   m=Math.pow(10,Math.max(r1,r2))   
   return (arg1*m+arg2*m)/m   
}   
  
//浮点数减法运算   
function FloatSub(arg1,arg2){   
	if(arg1==undefined||arg2==undefined)
		return NaN;
	var r1,r2,m,n;   
	try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
	try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
	m=Math.pow(10,Math.max(r1,r2));   
	//动态控制精度长度   
	n=(r1>=r2)?r1:r2;   
	return ((arg1*m-arg2*m)/m).toFixed(n);   
}   
  
//浮点数乘法运算   
function FloatMul(arg1,arg2){   
  if(arg1==undefined||arg2==undefined)
	  return NaN;
  var m=0,s1=arg1.toString(),s2=arg2.toString();   
  try{m+=s1.split(".")[1].length}catch(e){}   
  try{m+=s2.split(".")[1].length}catch(e){}   
  return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)   
}   
  
  
//浮点数除法运算   
function FloatDiv(arg1,arg2){   
	if(arg1==undefined||arg2==undefined)
		  return NaN;
	var t1=0,t2=0,r1,r2;   
	try{t1=arg1.toString().split(".")[1].length}catch(e){}   
	try{t2=arg2.toString().split(".")[1].length}catch(e){}   
	with(Math){   
		r1=Number(arg1.toString().replace(".",""))   
		r2=Number(arg2.toString().replace(".",""))   
		return ((r1/r2)*pow(10,t2-t1)).toFixed(4);   
	}   
}