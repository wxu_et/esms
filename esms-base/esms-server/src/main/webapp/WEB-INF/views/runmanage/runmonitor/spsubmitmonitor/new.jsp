<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="ssm">
<et:form action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_CREATE %>">
  <et:formField labelName="SP" inputName="sp.spId" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_AUTOCOMPLETE_SP %>" remote="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_REMOTECHECK_SP%>" remoteMsg="spId已存在" />
  <et:formField labelName="是否全天监控" inputName="wholeDay" value="0" type="radio" displayMap="${checkMap}" required="true"/>
  <et:formField labelName="主要监控起止时间" type="html" required="true"
		 value='
		 <div id="qizhitime">
		 <select id="startHourId" name="startHour" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="1">01</option>
          <option value="2">02</option>
          <option value="3">03</option>
          <option value="4">04</option>
          <option value="5">05</option>
          <option value="6">06</option>
          <option value="7">07</option>
          <option selected="selected" value="8">08</option>
          <option value="9">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
		 </select>
		 ：
		 <select id="startMinuteId" name="startMinute" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="5">05</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="25">25</option>
          <option selected="selected" value="30">30</option>
          <option value="35">35</option>
          <option value="40">40</option>
          <option value="45">45</option>
          <option value="50">50</option>
          <option value="55">55</option>
		 </select>
		 &nbsp;&nbsp;至&nbsp;&nbsp;
		 <select id="endHourId" name="endHour" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="1">01</option>
          <option value="2">02</option>
          <option value="3">03</option>
          <option value="4">04</option>
          <option value="5">05</option>
          <option value="6">06</option>
          <option value="7">07</option>
          <option value="8">08</option>
          <option value="9">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option selected="selected" value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
		 </select>
		 ：
		 <select id="endMinuteId" name="endMinute" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="5">05</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="25">25</option>
          <option selected="selected" value="30">30</option>
          <option value="35">35</option>
          <option value="40">40</option>
          <option value="45">45</option>
          <option value="50">50</option>
          <option value="55">55</option>
		 </select>
		 <span style="width:auto;display:inline-block">${secondDay}</span>
		 </div>
		 '
	/>
  <et:formField labelName="主要监控时间间隔(分钟)" inputName="normalInterval" digits="true" required="true" range="[1,1439]"  />
  <et:formField required="true" labelName="其他时间段是否监控" inputName="otherTime" value="1" type="radio" displayMap="${checkMap}" />
  <et:formField labelName="其他时间段监间隔时间(分钟)" inputName="interval" digits="true" required="true" range="[1,1439]" />
  <et:formField required="true" labelName="激活状态" inputName="activeState" value="1" type="select" displayMap="${activeStateMap}"/>
</et:form>
</div>
<script>
  var form=$("#ssm>form");
  var monitortime = form.find("[name='startHour']").parent().parent().parent();
  var othertime = form.find("[name='otherTime']").parent().parent().parent().parent();
  var interval = form.find("[name='interval']").parent().parent().parent();
  form.find("[name='wholeDay']").change(function(){
	  if($(this).val()==1){
		  monitortime.hide();
		  interval.hide();
		  othertime.hide();
	  }
	  else {
		  monitortime.show();
		  othertime.show();
		  var t="[name='otherTime'][value="+form.find("[name='otherTime']:checked").val()+"]"
		  form.find(t).change();
	  }
  });
  form.find("[name='otherTime']").change(function(){
	  if($(this).val()==0)
		  interval.hide();
	  else
		  interval.show();
  });
  
  var prev;
  $("#qizhitime select").focus(function(){
	  prev=$(this).val();
 	}).change(function(evt,params){
 	  var sh=parseInt(form.find("[name='startHour'] option:selected").val());
 	  var eh=parseInt(form.find("[name='endHour'] option:selected").val());
 	  var sm=parseInt(form.find("[name='startMinute'] option:selected").val());
 	  var em=parseInt(form.find("[name='endMinute'] option:selected").val());
	  if(sh==eh&&sm==em){
		  layer.msg('不能选相同的起止时间');
		  $(this).val(prev);
		  return;
	  }
	  if(sh>eh)
		  $("#qizhitime span").text('(次日)')
	  else {
		  if((sh==eh&&sm<em)||sh<eh)
		  	$("#qizhitime span").text('')
		  else if(sh==eh&&sm>em)
			  $("#qizhitime span").text('(次日)')
	  }	  
  });
 
</script>