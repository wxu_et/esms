<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form
	action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_UPDATE %>">
	<et:formField inputName="errorCodeId" labelName="错误号码编号" type="hidden"
		value="${ec.errorCodeId}" />
	<et:formField inputName="errorCode" labelName="错误代码" maxlength="30"
		required="true" value="${ec.errorCode}" />
	<et:formField inputName="host" labelName="错误定义方" required="true"
		value="${ec.errorDefine.host }" type="autocomplete"
		autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_AUTOCOMPLETE_ERRORDEFINE %>" />
	<et:formField inputName="originExplain" labelName="错误解释"
		type="textarea" maxlength="100" required="true"   value="${ec.originExplain}"/>
</et:form>