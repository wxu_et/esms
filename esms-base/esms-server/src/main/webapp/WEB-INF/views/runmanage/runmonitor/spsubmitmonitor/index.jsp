<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="esms.etonenet.boss1069.web.UrlConstants" %>
<%@ taglib uri="http://www.etonenet.com/tld" prefix = "et"%>
<script src="/static/js/common/tip.js?v=${jsVersion}"></script>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_DEL %>" >
  <et:search column="4" >
     <et:searchField labelName="SP" inputName="spId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_AUTOCOMPLETE_SP %>"/>
     <et:searchField labelName="激活状态" inputName="activeState" type="select" selectMap="${activeStateMap}" />
  </et:search>
  <et:page dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_DATA%>" dataRowStyle="ssmRowStyle" >
     <et:pageField tableHeadName="操作" value="查看" type="link" linkUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_VIEW%>" linkUrlParam="monitorId"/>
     <et:pageField tableHeadName="SP" value="sp.spId-sp.spName"  />
     <et:pageField tableHeadName="主要监控起止时间" value="" dataFormatter="startendtime"  />
     <et:pageField tableHeadName="主要监控时间间隔(分钟)" value="normalInterval"  />
     <et:pageField tableHeadName="其他时间段是否监控" dataFormatter="ismonitor" dataCellStyle="unmonitorCellStyle" />
     <et:pageField tableHeadName="其他监控时间间隔(分钟)" value="interval" dataFormatter="displayInterval" />
     <et:pageField tableHeadName="激活状态" value="activeState" dataReplace="${activeStateMap}" />
  </et:page>
</et:index>
<script type="text/javascript">
  var wholeD=86399000;
  function startendtime(v,r,i){
	  if(r.normalEndTime-r.normalStartTime>=wholeD)
		  return '全天';
	  else{
		  var start = new Date();
		  start.setTime(r.normalStartTime);
		  var end = new Date();
		  end.setTime(r.normalEndTime);
		  end.setDate(start.getDate());
		  
		  var cr=''
		  	if(end<start)
		  		cr='(次日)'
		  
		  function b0(num){
			  if(num<10)
				  return '0'+num;
			  else
				  return num;
		  }
		  return b0(start.getHours())+":"+b0(start.getMinutes())+" 至 "+b0(end.getHours())+":"+b0(end.getMinutes())+cr;
	  }
  };
  function ismonitor(v, r, i){
	  if(r.interval>=1440||r.normalEndTime-r.normalStartTime>=wholeD){
		  return '否';
	  } else {
		  return '是';
	  }
  };
  function displayInterval(v, r, i){
	  if(v>=1440||r.normalEndTime-r.normalStartTime>=wholeD){
		  return '';
	  }
	  else
		  return v;
  };
  function ssmRowStyle(r,i){
	  if(r.activeState==0){
		  return {classes:'ban-table-row'};
	  } else {
		  return {};
	  }
  };
  function unmonitorCellStyle(v,r,i){
	  if(v=='否'){
		  return {classes:'deny-table-cell'};
	  } else {
		  return {};
	  }
	  
  }
</script>




















