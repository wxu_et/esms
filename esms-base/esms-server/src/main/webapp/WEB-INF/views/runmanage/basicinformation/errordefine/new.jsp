<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form
	action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_CREATE%>">
	<et:formField inputName="errorDefineId" labelName="错误定义方编号"
		required="true" digits="true" digitsMsg="只能填整数类型" maxlength="10"
		remote="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRPRDEFINE_ERRORDEFINEIDCHECK%>"
		remoteMsg="id值不能重复" />
	<et:formField inputName="host" labelName="错误定义方" required="true"
		maxlength="15" />
</et:form>