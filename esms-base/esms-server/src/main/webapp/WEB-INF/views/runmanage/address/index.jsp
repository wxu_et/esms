<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<div id="spchannel">
	<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_ADD%>"
		delBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_DEL%>">
		<et:search column="4">
			<et:searchField labelName="名称" inputName="addressName" />
			<et:searchField labelName="下发地址" inputName="target" />
		</et:search>
		<et:page dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_DATA%>">
			<et:pageField tableHeadName="操作" value="查看" type="link"
				linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_UPDATE%>"
				linkUrlParam="addressName,target" dataSortable="false" />
			<et:pageField tableHeadName="名称" value="addressName" dataSortable="true" />
			<et:pageField tableHeadName="下发地址" value="target"
				dataSortable="false" />
		</et:page>
	</et:index>
</div>