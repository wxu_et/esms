<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>

<et:index>
	<et:search>
		<et:searchField labelName="SP*" inputName="spId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP %>"/>
		<et:searchField labelName="SP服务代码" inputName="spServiceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP_SERVICECODE %>" autocompleteCascade="spIdName"/> 
		<et:searchField labelName="通道 " inputName="channelId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_CHANNEL %>"/>
		<et:searchField labelName="手机号" inputName="destinationAddr" placeholder="多个手机号用逗号隔开"/>
		<et:searchField labelName="源地址" inputName="sourceAddr" />
		<et:searchField labelName="发送内容" inputName="mtContent" value="\"${mtContent}\"" />
		<et:searchField labelName="下发地址" inputName="mtAddr" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_MTADDR %>"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_DATA %>" dataCheckbox="false" dataShowColumns="false"  dataPagination="false" >
		<et:pageField tableHeadName="返回结果" value="resultBack" dataSortable="false" />
	</et:page>
</et:index>

<script>

$(document).ready(function(){
	$('button[data-search-btn]').html('发送')
	$("button[name='refresh']").hide();
	$(".panel-heading").hide();
	$("form .btn-group input[class='btn btn-primary']").val("发送");
	$("form .btn-group").append("<input type='reset' name='reset' value='重置' class='btn btn-primary' style='margin-left:10px'>");
    });
</script>