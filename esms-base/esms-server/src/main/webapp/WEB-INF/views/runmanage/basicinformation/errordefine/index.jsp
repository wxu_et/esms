<%@ page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html;charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="et" uri="http://www.etonenet.com/tld"%>
<et:index
	createBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_NEW%>"
	delBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DEL%>">
	<et:search>
		<et:searchField labelName="错误定义方" inputName="host" />
	</et:search>
	<et:page
		dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DATA%>"
		dataSortName="errorDefineId" dataSortOrder="desc">
		<et:pageField tableHeadName="操作" value="查看" linkTarget="_modal"
			linkUrlParam="errorDefineId" type="link"
			linkUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_VIEW%>" />
		<et:pageField tableHeadName="错误定义方编号" value="errorDefineId"
			dataWidth="18" />
		<et:pageField tableHeadName="错误定义方" value="host" />
	</et:page>
</et:index>