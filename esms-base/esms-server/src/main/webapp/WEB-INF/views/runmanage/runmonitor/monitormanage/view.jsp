<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form  action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_UPDATE%>">
<et:formField  inputName="id"   labelName="编号编号"  type="hidden"  value="${es.id}"/>
<et:formField  inputName="mqUrl"  labelName="MQ_URL" maxlength="60"  required="true"  value="${es.mqUrl}" />
<et:formField  inputName="channelNumber"  labelName="通道编号"  maxlength="60" required="true"  value="${es.channelNumber}"/>
<et:formField  inputName="mqState"   labelName="监控状态"  required="true"   type="radio"  value="${es.mqState}"  displayMap="${mqState}"/> 
<et:formField  inputName="priority"  labelName="优先级"   required="true"  type="radio"  value="${es.priority}"  displayMap="${priority}" />
</et:form>