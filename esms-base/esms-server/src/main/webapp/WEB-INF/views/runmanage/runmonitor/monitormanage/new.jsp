<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_CREATE%>">
	<et:formField inputName="mqUrl" labelName="MQ_URL" maxlength="60" required="true" />
	<et:formField inputName="channelNumber" labelName="通道编号" maxlength="50" required="true" />
	<et:formField inputName="mqState" labelName="监控状态" type="radio" displayMap="${mqState}" required="true" value="0" />
	<et:formField inputName="priority" labelName="优先级" type="radio" displayMap="${priority}" required="true" value="0" />
</et:form>

<script>
var prefix='mt_carrier_';
var preChNum=$('<div class="input-group-addon"><label class="checkbox-inline" style="padding-top:0px"><input type="checkbox" name="isMtCarrier" checked="checked">前缀'+prefix+'</label></div>')
var imc=preChNum.find("[name='isMtCarrier']");
var cn=$(".modal input[name='channelNumber']");
imc.change(function(){
	var txt=$("[showTime='1860705']").find(".layui-layer-content");
	if($(this).is(":checked"))
		txt.text(prefix+txt.text())
	else
		txt.text(txt.text().replace(prefix, '', "$1"))
})
cn.parent().prepend(preChNum);
var tipIndex;
preChNum.hover(
	function(){
		var tip='';
		if(imc.is(":checked"))
			tip+=prefix+cn.val();
		else
			tip+=cn.val();
		tipIndex=layer.tips(tip,this,{
			tips:3,
			time:1860705
		});
	},
	function(){
		layer.close(tipIndex);
	}
)

</script>