<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_ADD_DATA%>">
	<et:formField inputName="addressName" labelName="名称" required="true" remote="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_GETNAME %>" remoteMsg="名称已经存在"/>
	<et:formField inputName="target" labelName="下发地址" required="true" url="true" />
</et:form>