<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="ssmview">
<et:form action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_UPDATE %>">
  <et:formField labelName="monitorId" inputName="monitorId" type="hidden" value="${spSubmitMonitor.monitorId }"/>
  <et:formField required="true" labelName="SP" inputName="spId" value="${spSubmitMonitor.sp.spId}-${spSubmitMonitor.sp.spName}" readonly="true" />  
  <et:formField required="true" labelName="是否全天监控" inputName="wholeDay" value="${wholeDay}" type="radio" displayMap="${checkMap}"  />
  <et:formField required="true" labelName="主要监控起止时间" type="html"
		 value='
		 <div id="qizhitime">
		 <select id="startHourId" name="startHour" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="1">01</option>
          <option value="2">02</option>
          <option value="3">03</option>
          <option value="4">04</option>
          <option value="5">05</option>
          <option value="6">06</option>
          <option value="7">07</option>
          <option selected="selected" value="8">08</option>
          <option value="9">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
		 </select>
		 ：
		 <select id="startMinuteId" name="startMinute" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="5">05</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="25">25</option>
          <option selected="selected" value="30">30</option>
          <option value="35">35</option>
          <option value="40">40</option>
          <option value="45">45</option>
          <option value="50">50</option>
          <option value="55">55</option>
		 </select>
		 &nbsp;&nbsp;至&nbsp;&nbsp;
		 <select id="endHourId" name="endHour" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="1">01</option>
          <option value="2">02</option>
          <option value="3">03</option>
          <option value="4">04</option>
          <option value="5">05</option>
          <option value="6">06</option>
          <option value="7">07</option>
          <option value="8">08</option>
          <option value="9">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option selected="selected" value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
		 </select>
		 ：
		 <select id="endMinuteId" name="endMinute" class="form-control" style="width:auto;display:inline-block">
		  <option value="0">00</option>
          <option value="5">05</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="25">25</option>
          <option selected="selected" value="30">30</option>
          <option value="35">35</option>
          <option value="40">40</option>
          <option value="45">45</option>
          <option value="50">50</option>
          <option value="55">55</option>
		 </select>
		 <span style="width:auto;display:inline-block">${secondDay}</span>
		 </div>
		 '
	/>
  <et:formField labelName="主要监控时间间隔(分钟)" inputName="normalInterval" digits="true" value="${spSubmitMonitor.normalInterval>=1440 ? '' : spSubmitMonitor.normalInterval}" required="true" range="[1,1439]"  />
  <et:formField required="true" labelName="其他时间段是否监控" inputName="otherTime" value="${otherTime}" type="radio" displayMap="${checkMap}"/>
  <et:formField labelName="其他时间段监间隔时间(分钟)" inputName="interval" digits="true" value="${spSubmitMonitor.interval>=1440 ? '' : spSubmitMonitor.interval}" required="true" range="[1,1439]" />
  <et:formField required="true" labelName="激活状态" inputName="activeState" value="${spSubmitMonitor.activeState}" type="select" displayMap="${activeStateMap}" />
</et:form>
</div>
<script>
  var form=$("#ssmview>form");
  var monitortime = form.find("[name='startHour']").parent().parent().parent();
  var othertime = form.find("[name='otherTime']").parent().parent().parent().parent();
  var interval = form.find("[name='interval']").parent().parent().parent();
  
  form.find("[name='wholeDay']").change(function(){
	  if($(this).val()==1){
		  monitortime.hide();
		  othertime.hide();
		  interval.hide();
	  }
	  else {
		  monitortime.show();
		  othertime.show();
		  var t="[name='otherTime'][value="+form.find("[name='otherTime']:checked").val()+"]"
		  form.find(t).change();			 
	  }
  });
  form.find("[name='otherTime']").change(function(){
	  if($(this).val()==0)
		  interval.hide();
	  else
		  interval.show();
  });
  
  var prev;
  $("#qizhitime select").focus(function(){
	  prev=$(this).val();
 	}).change(function(evt,params){
 	  var sh=parseInt(form.find("[name='startHour'] option:selected").val());
 	  var eh=parseInt(form.find("[name='endHour'] option:selected").val());
 	  var sm=parseInt(form.find("[name='startMinute'] option:selected").val());
 	  var em=parseInt(form.find("[name='endMinute'] option:selected").val());
	  if(sh==eh&&sm==em){
		  layer.msg('不能选相同的起止时间');
		  $(this).val(prev);
		  return;
	  }
	  if(sh>eh)
		  $("#qizhitime span").text('(次日)')
	  else {
		  if((sh==eh&&sm<em)||sh<eh)
		  	$("#qizhitime span").text('')
		  else if(sh==eh&&sm>em)
			  $("#qizhitime span").text('(次日)')
	  }	  
  });
  
  $(function(){
	  if('${wholeDay}'=='1'){
		  form.find("[name='wholeDay'][value=1]").change();
	  }else{
		  form.find("[name='startHour']>option[value='${startHour}']").attr("selected",true);
		  form.find("[name='startMinute']>option[value='${startMinute}']").attr("selected",true);
		  form.find("[name='endHour']>option[value='${endHour}']").attr("selected",true);
		  form.find("[name='endMinute']>option[value='${endMinute}']").attr("selected",true);
	  }
	  if('${otherTime}'=='0'){
		  form.find("[name='otherTime'][value=0]").change();
	  };
  });
</script>