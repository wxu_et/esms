<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form
	action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_UPDATE%>">
	<et:formField inputName="errorDefineId" labelName="错误定义方编号"
		required="true" readonly="true" value="${ed.errorDefineId}" />
	<et:formField inputName="host" labelName="错误定义方" required="true"
		maxlength="15" value="${ed.host}" />
</et:form>