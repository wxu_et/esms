<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.RUNMANAGE_ADDRESS_UPDATE_DATA %>">
    <et:formField type="hidden" inputName="createTime" labelName="createTime" required="true" readonly="true" value="${address.createTime }"/>
	<et:formField inputName="addressName" labelName="名称" required="true" value="${address.addressName }" readonly="true"/>
	<et:formField inputName="target" labelName="下发地址" required="true" url="true" value="${address.target }"/>
</et:form>
