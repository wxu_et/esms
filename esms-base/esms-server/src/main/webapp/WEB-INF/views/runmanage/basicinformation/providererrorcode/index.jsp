<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index
	createBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_NEW%>"
	delBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DEL%>">
	<et:search>
		<et:searchField labelName="错误定义方" inputName="host" type="autocomplete"
			autocompleteUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_AUTOCOMPLETE_ERRORDEFINE%>" />
		<et:searchField labelName="错误代码" inputName="errorCode" />
		<et:searchField labelName="错误解释" inputName="originExplain" />
	</et:search>
	<et:page
		dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DATA%>"
		dataSortName="errorCodeId" dataSortOrder="desc">
		<et:pageField tableHeadName="操作" value="查看" type="link"
			linkTarget="_modal"
			linkUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_VIEW%>"
			linkUrlParam="errorCodeId" dataWidth="40"/>
		<et:pageField tableHeadName="错误代码" value="errorCode" dataWidth="50"/>
		<et:pageField tableHeadName="错误解释" value="originExplain" />
		<et:pageField tableHeadName="错误定义方" dataFormatter="errorDefine"/>
	</et:page>
</et:index>

<script>
function errorDefine(v,r,i){
	if(v!=null)
		return v.host
}
</script>