<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index
	createBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_NEW%>"
	delBtnUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_DEL%>">
	<et:search column="3">
		<et:searchField labelName="MQ_URL" inputName="mqUrl" />
		<et:searchField labelName="通道编号" inputName="channelNumber" />
		<et:searchField labelName="监控状态" inputName="mqState" type="select"
			selectMap="${mqState}" />
	</et:search>
	<et:page
		dataUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_DATA%>"
		dataSortName="id" dataSortOrder="desc">
		<et:pageField tableHeadName="操作" value="查看" type="link"
			linkTarget="_modal"
			linkUrl="<%=request.getContextPath()+UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_VIEW%>"
			linkUrlParam="id"  dataWidth="40"/>
		<et:pageField tableHeadName="MQ_URL" value="mqUrl" />
		<et:pageField tableHeadName="通道编号" value="channelNumber" />
		<et:pageField tableHeadName="优先级" value="priority"
			dataReplace="${priority}" />
		<et:pageField tableHeadName="mqState" value="mqState"
			dataReplace="${mqState}" />
	</et:page>
</et:index>