<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- .navbar -->
<div class="container-fluid">
	<ul class="nav navbar-nav navbar-actions navbar-left">
		<li class="visible-md visible-lg"><a href="index.html#" id="main-menu-toggle"><i class="fa fa-bars"></i></a></li>
		<li class="visible-xs visible-sm"><a href="index.html#" id="sidebar-menu"><i class="fa fa-bars"></i></a></li>
	</ul>
	<!-- <form class="navbar-form navbar-left">
		<i class="fa fa-search"></i>
		<input type="text" class="form-control" placeholder="Search...">
	</form> -->
       <ul class="nav navbar-nav navbar-right">
		<li><a href="/logout"><i class="fa fa-power-off"></i></a></li>
	</ul>
</div>