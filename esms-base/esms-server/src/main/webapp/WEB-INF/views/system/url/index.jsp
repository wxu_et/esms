<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index delBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_URL_DEL %>">
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_URL_DATA %>">
		<et:pageField tableHeadName="URL" value="url"/>
		<et:pageField tableHeadName="数据多余标识" value="isExist"/>
	</et:page>
</et:index>