<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index>
	<et:search column="4">
		<et:searchField labelName="CLASS_编号" inputName="classId" />
		<et:searchField labelName="CLASS_NAME" inputName="className" />
		<et:searchField labelName="具体内容" inputName="prop" />
		<et:searchField labelName="操作人" inputName="operUserStr" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_LOG_AUTOCOMPLETE_OPERUSER %>" />
		<et:searchField labelName="操作类型" inputName="oper" type="select" selectMap="${operType }" />
		<et:searchField labelName="查询起始时间" inputName="timeStart" type="date" />
		<et:searchField labelName="查询结束时间" inputName="timeEnd" type="date" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_LOG_DATA %>" dataCheckbox="false">
		<%-- <et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_LOG_VIEW %>" linkUrlParam="sysLogId" dataSortable="false"/> --%>
		<et:pageField tableHeadName="操作时间" value="logTime" type="dateTime"/>
		<et:pageField tableHeadName="操作人" value="operUser.userName"/>
		<et:pageField tableHeadName="操作类型" value="oper" dataReplace="${operType }"/>
		<et:pageField tableHeadName="CLASS_编号" value="classId"/>
		<et:pageField tableHeadName="CLASS_NAME" value="className"/>
		<et:pageField tableHeadName="具体内容" value="prop"/>
	</et:page>
</et:index>