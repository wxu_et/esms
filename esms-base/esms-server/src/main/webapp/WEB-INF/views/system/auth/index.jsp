<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index>
	<et:search column="4">
		<et:searchField labelName="角色名" inputName="displayName" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_DATA%>" dataCheckbox="false">
		<et:pageField tableHeadName="操作" dataFormatter="oper" />
		<et:pageField tableHeadName="角色名" value="displayName" />
	</et:page>
</et:index>
<script>
	function oper(value, row, index) {
		return '<a data-toggle="modal" data-target=".modal" href="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_ROLEUSER%>?roleid='+row.roleId+'">角色人员分配</a>';
	}
</script>