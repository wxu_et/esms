<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>

<label class="inline"></label>
<table style="margin-bottom: 30px;" class="form-inline">
	<tbody>
		<tr>
			<td>SP下行路由编号</td>
			<td><input id="spmtId" name="spmtId" type="text" class="form-control" /></td>
			<td><input id="synbtn" type="button" value="同步" class="btn btn-primary" /></td>
			<td><input id="delbtn" type="button" value="删除" class="btn btn-danger" /></td>
		</tr>
	</tbody>
</table>
<div class="btn-group">
	<button id="synallbtn" type="button" class="btn btn-warning">
		<span class="glyphicon glyphicon-refresh"></span>全部同步
	</button>
</div>
<table class="table table-hover table-striped table-bordered">
	<tbody>
		<tr>
			<td width="100px">数据库数目</td>
			<td>${dbCounts }</td>
		</tr>
		<tr>
			<td>缓存数目</td>
			<td>${hzCounts }</td>
		</tr>
	</tbody>
</table>

<script>
$("#synbtn").click(function(){
	var key=$("#spmtId").val();
	if(key!=''){
		$.post("<%=request.getContextPath()+UrlConstants.DATASYN_SPMTROUTE_SYN%>",{key:key},function(result){
		   if(result==1){
			alert('同步成功');
		   	window.location=window.location
		   }
		});		
	}
});
$("#synallbtn").click(function(){
	$.post("<%=request.getContextPath()+UrlConstants.DATASYN_SPMTROUTE_SYNALL%>",function(result){
		   alert(result);
		   window.location=window.location
		});	
});
$("#delbtn").click(function(){
	var key=$("#spmtId").val();
	if(key!=''){
		$.post("<%=request.getContextPath()+UrlConstants.DATASYN_SPMTROUTE_DEL%>",{key:key},function(result){
		   if(result==1){
			alert('删除成功');
		   	window.location=window.location
		   }
		});
	}
});
</script>