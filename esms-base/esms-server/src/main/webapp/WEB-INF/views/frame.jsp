<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="header">
    <div class="dl-title">
        <!--<img src="">-->
    </div>
    <div class="dl-log">欢迎您，${currentUser.userName }<span class="dl-log-user"></span><a href="/logout" title="退出系统" class="dl-log-quit">[退出]</a>
    </div>
</div>
<div class="content">
    <div class="dl-main-nav">
        <div class="dl-inform"><div class="dl-inform-title"><s class="dl-inform-icon dl-up"></s></div></div>
        <ul id="J_Nav"  class="nav-list ks-clear">
        	<li class="nav-item dl-selected">
				<div class="nav-item-inner">超级管理</div>
			</li>		
        	<c:forEach items="${roots }" var="r">
        		<li class="nav-item dl-selected">
					<div class="nav-item-inner"><c:out value="${r.nodeName }"></c:out></div>
				</li>		
        	</c:forEach>
        </ul>
    </div>
    <ul id="J_NavContent" class="dl-tab-conten">

    </ul>
</div>

<script>
    BUI.use('common/main',function(){
        var config;
        try{
        	config=JSON.parse('${menu}')
        }catch(e){
        	config=eval('('+'${menu}'+')');
        }
        new PageUtil.MainPage({
            modulesConfig : config
        });
    }); 
</script>
