<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_CREATE %>">
	<et:formField inputName="channelId" labelName="通道编号" maxlength="4" required="true"/>
	<et:formField inputName="channelName" labelName="通道名称" maxlength="60" required="true"/>
	<et:formField inputName="channelState" labelName="平台通道状态" type="radio" required="true" displayMap="${channelState }" value="1"/>
	<et:formField inputName="channelNumber" labelName="通道号码" maxlength="20" required="true"/>
	<et:formField inputName="channelNumberLength" labelName="通道号码长度" max="100" required="true" value="20"/>
	<et:formField inputName="channelMessageOrgLength" labelName="短信基本长度" range="[0,1000]" value="0" required="true"/>
	<et:formField inputName="channelMessageSignature" labelName="短信签名" maxlength="30"/>
	<et:formField inputName="channelMessageLength" labelName="通道消息长度" range="[1,1000]" required="true" value="70"/>
	<et:formField inputName="channelMessageOrgLengthEn" labelName="英文短信基本长度" range="[1,1000]" value="160" required="true"/>
	<et:formField inputName="channelMessageSignatureEn" labelName="英文短信签名" maxlength="30"/>
	<et:formField inputName="channelMessageLengthEn" labelName="英文通道消息长度" range="[1,1000]" />
	<et:formField inputName="channelBaobeiFlag" labelName="平台报备标志位" type="radio" displayMap="${channelBaobeiFlag }" value="0"/>
	<et:formField inputName="channelSpeed" labelName="通道速率" maxlength="10" digits="true"/>
	<et:formField inputName="channelMtmoFlagCheckbox" labelName="通道上下行标志位" value="2031631" type="checkbox" displayMap="${channelMtmoFlag }" checkboxCss="checkbox-floatleft"/>
	<et:formField inputName="longSmsFlag" labelName="是否支持长短信 " type="radio" displayMap="${longSmsFlag }" value="0"/>
	<et:formField inputName="whiteListPolicy" labelName="白名单策略" type="radio" displayMap="${whiteListPolicy }" value="1"/>
</et:form>
<style>
<!--
.tooltip-inner {
max-width:none;
padding: 2px 8px;
height: 40px;
color: #fff;
text-align: center;
text-decoration: none;
background-color: #000;
border-radius: 4px;
}
-->
</style>
<script>
var dxview=$("<button class=\"btn btn-sm btn-warning\" type=\"button\" class=\"btn btn-default\">预览</button>");
var tipIndex;
dxview.hover(
	function(){
		var c1 = $("input[name='channelMessageLength']").val();
		var c2 = $("input[name='channelMessageOrgLength']").val();
		var e1 = $("input[name='channelMessageLengthEn']").val();
		var e2 = $("input[name='channelMessageOrgLengthEn']").val();
		var con='<table><tr align="left">';
		con+='<td>中文短信:</td><td>';
		var ch='';
		if(!/^\s*$/.test(c1)&&!isNaN(c1)&&1<=c1&&c1<=1000) {
			if(!isNaN(c2)&&!/^\s*$/.test(c2)){
				if(c2==0){
					ch+='警告! “短信基本长度为0”, 计费逻辑不由通道控制';
				}else {
					ch+='1条'+c1+'字';
					if(1<=c2&&c2<=1000){
						for(var i=2;i<=5;i++){
							ch+=', '+i+'条'+((c2-3)*(i-1)+(c1-3))+'字';
						}
					}
				}
			}
		}
		con+=ch+'</td><tr align="left"><td>英文短信:</td><td>';
		if(c2==0) {
			con+='警告! “短信基本长度为0”, 计费逻辑不由通道控制';
		}else if(/^\s*$/.test(e1)){
			con+=ch;
		}else if(!isNaN(e1)&&1<=e1&&e1<=1000) {
			if(!isNaN(e2)&&!/^\s*$/.test(e2)&&0<=e2&&e2<=1000){
				con+='1条'+e1+'字';
				for(var i=2;i<=5;i++){
					con+=', '+i+'条'+((e2-3)*(i-1)+(e1-3))+'字';
				}
			}
		}
		con+="</td></tr></table>";
		tipIndex=layer.tips(con,this,{
			tips:4,
			time:10000
		})
	},
	function(){
		layer.close(tipIndex);
	}
);
$("form .modal-header .pull-right").prepend(dxview);

$("input[name='channelMessageSignature']").bind("keyup",function(){
	var c1 = $("input[name='channelMessageOrgLength']").val();
	var c2 = $("input[name='channelMessageSignature']").val();
	var length = c1 - c2.length;
	$("input[name='channelMessageLength']").val(length);
});

$("input[name='channelMessageSignatureEn']").bind("keyup",function(){
	var c1 = $("input[name='channelMessageOrgLengthEn']").val();
	var c2 = $("input[name='channelMessageSignatureEn']").val();
	var length = c1 - c2.length;
	$("input[name='channelMessageLengthEn']").val(length);
});
</script>