<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_BAOBEI_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_BAOBEI_DEL %>">
	<et:search>
		<et:searchField labelName="报备号码" inputName="baobeiNumberSearch" value="${baobeiNumberSearch }"/>
		<et:searchField labelName="短信签名" inputName="channelMessageSignature" value="${channelMessageSignature }"/>
		<et:searchField labelName="通道" inputName="channel" value="${channel }" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_BAOBEI_AUTOCOMPLETE_CHANNEL %>"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_BAOBEI_DATA %>" dataRowStyle="chbbRowStyle">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_BAOBEI_VIEW %>" linkUrlParam="channelBaobeiInfoId"/>
		<et:pageField tableHeadName="报备号码" value="baobeiNumber"/>
		<et:pageField tableHeadName="通道" value="channel.channelId-channel.channelName"/>
		<et:pageField tableHeadName="短信基本长度" value="channelMessageOrgLength"/>
		<et:pageField tableHeadName="短信签名" value="channelMessageSignature"/>
		<et:pageField tableHeadName="通道消息长度" value="channelMessageLength"/>
		<et:pageField tableHeadName="英文短信基本长度" value="channelMessageOrgLengthEn"/>
		<et:pageField tableHeadName="英文短信签名" value="channelMessageSignatureEn"/>
		<et:pageField tableHeadName="英文通道消息长度" value="channelMessageLengthEn"/>
		<et:pageField tableHeadName="是否长短信" value="longSmsFlag" dataReplace="${longSmsFlag }"/>
		<et:pageField tableHeadName="常规通道消息长度" dataFormatter="isnormalmessageLen" />
	</et:page>
</et:index>
<script>
function chbbRowStyle(row,index){
	if(row.channel.channelBaobeiFlag==0)
		return {classes:'gray-row'}
	else {
		  var c1 = row.channelMessageOrgLength;
		  var signature = row.channelMessageSignature;
		  if(signature==null){
			  var c2 = 0;
		  }else {		  
		      var c2 = signature.length;
		  }
		  var c3 = row.channelMessageLength;
		  if(c1-c2>=c3){
			  return {};
		  }else{
			  return {classes:'yellow-row'};
		  }
	}
};

function isnormalmessageLen(value,row,index){
	  var c1 = row.channelMessageOrgLength;
	  var signature = row.channelMessageSignature;
	  if(signature==null){
		  var c2 = 0;
	  }else {		  
	      var c2 = signature.length;
	  }
	  var c3 = row.channelMessageLength;
	  if(c1-c2>=c3){
		  return '是';
	  }else{
		  return '否';
	  }
};

</script>