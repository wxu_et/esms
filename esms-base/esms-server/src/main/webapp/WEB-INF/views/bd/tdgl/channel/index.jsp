<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_NEW %>">
	<et:search column="6">
		<et:searchField labelName="通道编号" inputName="channelId" />
		<et:searchField labelName="通道号码" inputName="channelNumber" />
		<et:searchField labelName="通道消息签名" inputName="channelMessageSignature" />
		<et:searchField labelName="平台通道状态" inputName="channelState" type="select" selectMap="${channelState }" />
		<et:searchField labelName="白名单策略" inputName="whiteListPolicy" type="select" selectMap="${whiteListPolicy }" />
		<et:searchField labelName="平台报备标志位" inputName="channelBaobeiFlag" type="select" selectMap="${channelBaobeiFlag }"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_DATA %>" dataRowStyle="normalMeLenRowStyle" dataCheckbox="false">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.BD_TDGL_CHANNEL_VIEW %>" linkUrlParam="channelId"/>
		<et:pageField tableHeadName="通道编号" value="channelId" />
		<et:pageField tableHeadName="通道号码" value="channelNumber"/>
		<et:pageField tableHeadName="平台通道状态" value="channelState" dataReplace="${channelState }"/>
		<et:pageField tableHeadName="平台报备标志位" value="channelBaobeiFlag" dataReplace="${channelBaobeiFlag }" dataCellStyle="channelBaobeiFlag" />
		<et:pageField tableHeadName="通道名称" value="channelName"/>
		<et:pageField tableHeadName="是否支持长短信" value="longSmsFlag" dataReplace="${longSmsFlag }"/>
		<et:pageField tableHeadName="通道消息签名" value="channelMessageSignature"/>
		<et:pageField tableHeadName="白名单策略" value="whiteListPolicy" dataReplace="${whiteListPolicy }"/>
		<et:pageField tableHeadName="常规通道消息长度" dataFormatter="isnormalmessageLen" />
	</et:page>
</et:index>
<script>
  function channelBaobeiFlag(v,r,i){
	  if(v=='报备通道')
	  	return {css:{'background-color':'#00B0B0'}};
	  else
		return {}
  }
  
  function isnormalmessageLen(value,row,index){
	  var c1 = row.channelMessageOrgLength;
	  var signature = row.channelMessageSignature;
	  if(signature==null){
		  var c2 = 0;
	  }else {		  
	      var c2 = signature.length;
	  }
	  var c3 = row.channelMessageLength;
	  if(c1-c2>=c3){
		  return '是';
	  }else{
		  return '否';
	  }
  };
  
  function normalMeLenRowStyle(row,index){
	  var c1 = row.channelMessageOrgLength;
	  var signature = row.channelMessageSignature;
	  if(signature==null){
		  var c2 = 0;
	  }else {		  
	      var c2 = signature.length;
	  }
	  var c3 = row.channelMessageLength;
	  if(c1-c2>=c3){
		  return {};
	  }else{
		  return {classes:'yellow-row'};
	  }
  };
</script>