<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index>
	<et:search>
		<et:searchField columnWidth="2" labelName="SP" inputName="spIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_SP %>"/>
		<et:searchField columnWidth="2" labelName="SP服务代码" inputName="spServiceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_SP_SERVICECODE %>" autocompleteCascade="spIdName"/> 
		<et:searchField columnWidth="2" labelName="通道 " inputName="channelId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_CHANNEL %>"/>
		<et:searchField columnWidth="2" labelName="SMS上行状态" inputName="moState" type="select" selectMap="${smsMoState}"/>
		<et:searchField columnWidth="2" labelName="MO消息状态" inputName="moStat" />
		<et:searchField columnWidth="2" labelName="MO错误代码" inputName="rtErrorCode" type="select" selectMap="${rtErrorCode }"/>
		<et:searchField labelName="运营商到MLINK端上行的时间" inputName="smStartTime,smEndTime" type="dateTimeRange"/>
		<et:searchField labelName="MLINK端到SP端上行的时间" inputName="mcStartTime,mcEndTime" type="dateTimeRange"/>
		<et:searchField columnWidth="2" labelName="国家代码" inputName="countryIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_COUNTRY2 %>"/>
		<et:searchField columnWidth="2" labelName="省代码" inputName="provinceIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_PROVINCE2 %>" autocompleteCascade="countryIdName"/>
		<et:searchField columnWidth="2" labelName="市代码" inputName="cityIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_CITY2 %>" autocompleteCascade="provinceIdName"/>
		<et:searchField labelName="目的地址" inputName="destinationAddr" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MOSELECT_DATA %>" dataCheckbox="false">
		<et:pageField tableHeadName="SMS上行编号" value="spMoId"/>
		<et:pageField tableHeadName="SP编号" value="spId"/>
		<et:pageField tableHeadName="SP服务代码" value="spServiceCode" />
		<et:pageField tableHeadName="通道编号" value="channelId"/>
		<et:pageField tableHeadName="SMS上行状态" value="moState" dataReplace="${smsMoState }"/>
		<et:pageField tableHeadName="源地址" value="sourceAddr"/>
		<et:pageField tableHeadName="目的地址" value="destinationAddr"/>
		<et:pageField tableHeadName="SMS消息" value="shortMessage"/>
		<et:pageField tableHeadName="MO消息状态" value="moStat"/>
		<et:pageField tableHeadName="MO错误代码" value="moErr"/>
		<et:pageField tableHeadName="运营商到MLINK端上行的时间" value="carrierMoTime" type="dateTime"/>
		<et:pageField tableHeadName="MLINK端到SP端上行的时间" value="spMoTime" type="dateTime"/>
		<et:pageField tableHeadName="国家代码" value="countryCode"/>
		<et:pageField tableHeadName="省代码" value="provinceCode"/>
		<et:pageField tableHeadName="市代码" value="cityCode"/>
		<et:pageField tableHeadName="运营商代码" value="carrierCode"/>
		<et:pageField tableHeadName="ESM_CLASS" value="esmClass"/>
		<et:pageField tableHeadName="PROTOCOL_编号" value="protocolId"/>
		<et:pageField tableHeadName="DATA_CODING" value="dataCoding"/>
	</et:page>
</et:index>

<script>

$(document).ready(function(){
	
	    var date = new Date();
	    var date2 = new Date();
	    date2.setDate(date2.getDate()+1);
	    var dateStart = tableSet.fn.showDate(date,"yyyy-MM-dd 00:00:00");
	    var dateEnd = tableSet.fn.showDate(date2,"yyyy-MM-dd 23:59:59");
        $("input[name='smStartTime']").val(dateStart);
        $("input[name='smEndTime']").val(dateEnd);
        
});
</script>