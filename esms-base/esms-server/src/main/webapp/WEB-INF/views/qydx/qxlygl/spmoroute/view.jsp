<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMOROUTE_UPDATE %>" id="spmo_update">
	<et:formField inputName="spMoRouteId" labelName="hid" type="hidden" value="${spmo.spMoRouteId }"/>
	<et:formField inputName="spChannel.id.channelId" required="true" labelName="通道编号" value="${spmo.spChannel.id.channelId }"/>
	<et:formField inputName="spChannel.id.spId" required="true" labelName="SP编号" readonly="true" value="${spmo.spChannel.id.spId }"/>
	<et:formField inputName="spChannel.id.spServiceCode" required="true" labelName="SP服务代码" value="${spmo.spChannel.id.spServiceCode }"/>
	<et:formField inputName="routeState" required="true" labelName="路由状态" type="radio" list="${routeState }" value="${spmo.routeState }"/>
	<et:formField inputName="spChannelNumber" required="true" labelName="SP通道号码" value="${spmo.spChannelNumber }"/>
	<et:formField inputName="contentMatchPattern" labelName="内容匹配模式" value="${spmo.contentMatchPattern }"/>
</et:form>
