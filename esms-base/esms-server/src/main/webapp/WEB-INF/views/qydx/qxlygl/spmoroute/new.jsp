<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMOROUTE_CREATE %>" id="spmo_new">
	<et:formField inputName="sp" required="true" labelName="SP" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMOROUTE_AUTOCOMPLETE_SP %>"/>
	<et:formField inputName="channel" required="true" labelName="通道" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMOROUTE_AUTOCOMPLETE_CHANNEL %>" autocompleteCascade="sp"/>
	<et:formField inputName="spChannel.id.spServiceCode" required="true" labelName="SP服务代码"/>
	<et:formField inputName="routeState" required="true" labelName="路由状态" type="radio" list="${routeState }"/>
	<et:formField inputName="spChannelNumber" required="true" labelName="SP通道号码"/>
	<et:formField inputName="contentMatchPattern" labelName="内容匹配模式"/>
</et:form>