<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index>
	<et:search>
		<et:searchField labelName="SP编号" inputName="spId" />
		<et:searchField labelName="SP名称" inputName="spName" />
		<et:searchField columnWidth="2" labelName="SP状态" inputName="spState" type="select" selectMap="${spState}" />
		<et:searchField labelName="SP扩展码" inputName="spExtNumber" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SP_DATA %>" dataCheckbox="true">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SP_VIEW %>" linkUrlParam="spId,spName" dataSortable="false"/>
		<et:pageField tableHeadName="SP编号" value="spId" dataFormatter="spxg" dataEvents="spxgEvents"/>
		<et:pageField tableHeadName="SP名称" value="spName"/>
		<et:pageField tableHeadName="SP状态" value="spState" dataReplace="${spState }"/>
		<et:pageField tableHeadName="SP扩展码" value="spExtNumber"/>
		<et:pageField tableHeadName="地址策略" value="addressPolicy" dataReplace="${addressPolicy }"/>
		<et:pageField tableHeadName="路由策略" value="routePolicy" dataReplace="${routePolicy }"/>
		<et:pageField tableHeadName="白名单策略" value="whiteListPolicy" dataReplace="${whiteListPolicy }"/>
		<et:pageField tableHeadName="下行时间窗口" value="mtTimeWindow"/>
		<et:pageField tableHeadName="短信上行url" value="smsMoUrl"/>
		<et:pageField tableHeadName="短信下行url" value="smsMtUrl"/>
		<et:pageField tableHeadName="短信状态报告URL" value="smsRtUrl"/>
		<et:pageField tableHeadName="传输连接限制" value="transmitConnectionLimit"/>
		<et:pageField tableHeadName="传输IP地址限制" value="transmitIpLimit"/>
		<et:pageField tableHeadName="传输方式" value="transmitMode"/>
		<et:pageField tableHeadName="传输速度限制" value="transmitSpeedLimit"/>
	</et:page>
</et:index>
<script>
function spxg(value){
	return [
	    '<div class="dropdown">',
	    value,
	    '<button class="btn btn-sm dropdown-toggle pull-right" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">',
	    'sp相关',
	    '<span class="caret"></span>',
	    '</button>',
	    '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">',
        '<li><a class="spwf" href="javascript:void(0)">SP服务</a></li>',
        '<li><a class="sptd ml10" href="javascript:void(0)">SP通道 </a></li>',
        '<li><a class="sxxly ml10" href="javascript:void(0)">上下行路由</a></li>',
        '</ul></div>'
    ].join('');
}

window.spxgEvents = {
     'click .spwf': function (e, value, row, index) {
         menuTo('<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_INDEX%>?sp='+row['spId'],'SP服务');
     },
     'click .sptd': function (e, value, row, index) {
    	 menuTo('<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_INDEX%>?spIdName='+row['spId'],'SP通道');
     },
     'click .sxxly': function (e, value, row, index) {
    	 menuTo('<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_INDEX%>?spIdName='+row['spId'],'上下行路由');
     }
 };
</script>