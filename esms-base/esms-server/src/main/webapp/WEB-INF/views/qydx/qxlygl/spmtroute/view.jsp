<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="spmtdiv">
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_UPDATE %>">
	<et:formField inputName="spMtRouteId" value="${spmt.spMtRouteId }" labelName="spMtRouteId" type="hidden"/>
	<et:formField inputName="spIdName" value="${spmt.sp.spId}-${spmt.sp.spName }" required="true" readonly="true" labelName="SP"/>
	<et:formField inputName="spServiceCode" value="${spmt.spServiceCode }" required="true" labelName="sp服务代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP_SERVICECODE%>" autocompleteCascade="spIdName"/>
	
	<et:formField inputName="routeType" value="${spmt.routeType }" required="true" labelName="路由类型" type="radio" displayMap="${routeType }"/>
	
	<et:formField inputName="channelIdName" value="${spmt.channel.channelId }${spmt.channel!=null?'-':''}${spmt.channel.channelName }" required="true" labelName="通道" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_CHANNEL %>"/>
	<et:formField inputName="carrierCodeIdName" required="true" value="${spmt.carrierCode }" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField inputName="routeState" required="true" value="${spmt.routeState }" labelName="路由状态" type="radio" displayMap="${routeState }"/>
	<et:formField inputName="countryIdName" required="true" value="${spmt.countryCode }" labelName="国家代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY %>"/>
	<et:formField inputName="provinceIdName" required="true" value="${spmt.provinceCode }" labelName="省代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
	<et:formField inputName="cityIdName" required="true" value="${spmt.cityCode }" labelName="市代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CITY %>" autocompleteCascade="provinceIdName"/>
	
	<et:formField inputName="channelGroupIdName" required="true" value="${spmt.channelGroup.channelGroupId }${spmt.channelGroup!=null?'-':'' }${spmt.channelGroup.channelGroupName }" labelName="通道组" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP %>"/>
	<et:formField inputName="channelGroupRouteState" required="true" labelName="通道组路由状态" type="radio" displayMap="${channelGroupRouteState }" value="${spmt.channelGroupRouteState}"/>
	
</et:form>
</div>
<script type="text/javascript">
var routeypeSwitch=function(t){
	var t0=$("#spmtdiv input[name='channelIdName'],#spmtdiv input[name='carrierCodeIdName'],#spmtdiv input[name='countryIdName'],#spmtdiv input[name='provinceIdName'],#spmtdiv input[name='cityIdName']").parent().parent().parent();
	var t02=$("#spmtdiv input[name='routeState']").parent().parent().parent().parent();
	var t1=$("#spmtdiv input[name='channelGroupIdName']").parent().parent().parent();
	var t12=$("#spmtdiv input[name='channelGroupRouteState']").parent().parent().parent().parent();
	if(t==0){
		t0.show();
		if($("#spmtdiv input[name='routeState']:checked").length==0)
			$("#spmtdiv input[name='routeState']").eq(0).attr("checked",true);
		t02.show();
		t1.hide();
		t12.hide();
	}if(t==1){
		t0.hide();
		t02.hide();
		t1.show();
		t12.show();
		if($("#spmtdiv input[name='channelGroupRouteState']:checked").length==0)
			$("#spmtdiv input[name='channelGroupRouteState']").eq(0).attr("checked",true);
	}	
};
routeypeSwitch('${spmt.routeType }');
$("#spmtdiv input[name='routeType']").change(function(){
	if($(this).val()==0)
		routeypeSwitch(0);
	else
		routeypeSwitch(1);
});
</script>