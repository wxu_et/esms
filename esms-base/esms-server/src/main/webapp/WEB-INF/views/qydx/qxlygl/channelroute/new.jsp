<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELROUTE_CREATE%>">
	<et:formField required="true" inputName="channelIdName" labelName="通道" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL %>"/>
	<et:formField required="true" inputName="carrierCodeIdName" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField required="true" inputName="routeState" labelName="路由状态" type="radio" displayMap="${routeState }" value="1"/>
	<et:formField inputName="countryIdName" required="true" labelName="国家代码" value="86-中国" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY %>"/>
	<et:formField inputName="provinceIdName" required="true" labelName="省代码" value="*" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
	<et:formField inputName="cityIdName" required="true" labelName="市代码" value="*" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CITY %>" autocompleteCascade="provinceIdName"/>
</et:form>