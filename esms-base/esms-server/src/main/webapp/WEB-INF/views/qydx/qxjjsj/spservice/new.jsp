<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_CREATE %>">
	<et:formField inputName="spIdName" labelName="SP" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_SP %>"/>
	<et:formField inputName="id.spServiceCode" labelName="SP服务代码" required="true" maxlength="10"/>
	<et:formField inputName="spServiceName" labelName="SP服务名称" maxlength="60"/>
	<et:formField inputName="msisdnFilterPolicyCheckbox" labelName="号码过滤策略" required="true" type="checkbox" displayMap="${msisdnFilterPolicy }" checkboxCss="checkbox" value="1"/>
	<et:formField inputName="contentFilterPolicy" labelName="内容过滤策略" required="true" type="radio" displayMap="${contentFilterPolicy }" value="1"/>
	<et:formField inputName="contentFilterCategoryFlagCheckbox" labelName="内容过滤分类标志" required="true" type="checkbox" displayMap="${contentFilterCategoryFlag }" checkboxCss="checkbox-floatleft" value="-1"/>
	<et:formField inputName="cpp" labelName="内容处理策略" required="true" type="radiogroup" groupList="${cpp }" valueList="${cppValue }"/>
	<et:formField inputName="priorityPolicy" labelName="优先级策略" required="true" type="radio" displayMap="${priorityPolicy }" radioCss="radio" value="0"/>
	<et:formField inputName="priorityPattern" labelName="优先级" required="true" max="10000" value="1"/>
	
	<et:formField inputName="contentPrefixPattern" labelName="内容前缀样式" maxlength="60"/>
	<et:formField inputName="contentPostfixPattern" labelName="内容后缀样式 " maxlength="60"/>
	<et:formField inputName="contentSplitPattern" labelName="内容分拆样式" maxlength="60"/>
	<et:formField inputName="spscExtNumber" labelName="SP服务扩展码" maxlength="20"/>
</et:form>