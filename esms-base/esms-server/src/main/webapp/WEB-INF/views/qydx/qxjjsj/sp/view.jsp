<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SP_UPDATE %>">
	<et:formField inputName="spId" labelName="SP编号" value="${sp.spId }" required="true" maxlength="6" readonly="true"/>
	<et:formField inputName="spName" value="${sp.spName }" labelName="SP名称" required="true" maxlength="60"/>
	<et:formField inputName="spState" value="${sp.spState }" labelName="SP状态" required="true" type="radio" displayMap="${spState }"/>
	<et:formField inputName="spPassword" labelName="密码" required="true" value="${sp.spPassword }"/>
	<et:formField inputName="addressPolicy" value="${sp.addressPolicy }" labelName="地址策略" required="true" type="radio" displayMap="${addressPolicy }" radioCss="radio"/>
	<et:formField inputName="routePolicy" value="${sp.routePolicy }" labelName="路由策略" required="true" type="radio" displayMap="${routePolicy }" radioCss="radio"/>
	<et:formField inputName="whiteListPolicy" value="${sp.whiteListPolicy }" labelName="白名单策略" required="true" type="radio" displayMap="${whiteListPolicy }"/>
	<et:formField inputName="spMtmoFlagCheckbox" value="${sp.spMtmoFlag }" labelName="SP上下行标志位" required="true" type="checkbox" displayMap="${spMtmoFlag }" checkboxCss="checkbox-floatleft"/>
	
	<et:formField inputName="mtTimeWindow" value="${sp.mtTimeWindow }" labelName="下行时间窗口" />
	<et:formField inputName="smsMoUrl" value="${sp.smsMoUrl }" labelName="SMS上行URL" maxlength="256"/>
	<et:formField inputName="smsMtUrl" value="${sp.smsMtUrl }" labelName="SMS下行URL" maxlength="256"/>
	<et:formField inputName="smsRtUrl" value="${sp.smsRtUrl }" labelName="SMS状态报告URL" maxlength="256"/>
	<et:formField inputName="transmitConnectionLimit" value="${sp.transmitConnectionLimit }" labelName="传输连接限制" max="10000"/>
	<et:formField inputName="transmitIpLimit" value="${sp.transmitIpLimit }" labelName="传输IP地址限制" maxlength="256"/>
	<et:formField inputName="transmitMode" value="${sp.transmitMode }" labelName="传输方式" type="radio"/>
	<et:formField inputName="transmitSpeedLimit" value="${sp.transmitSpeedLimit }" labelName="传输速度限制" max="10000"/>
	<et:formField inputName="spExtNumber" value="${sp.spExtNumber }" labelName="SP扩展码" maxlength="20"/>
</et:form>