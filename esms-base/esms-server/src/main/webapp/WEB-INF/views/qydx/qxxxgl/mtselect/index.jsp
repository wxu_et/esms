<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>

<div id="mtquery">
<et:index downloadBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MTSELECT_EXPORT %>">
	<et:search>
		<et:searchField columnWidth="2" labelName="SP" inputName="spIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_SP %>"/>
		<et:searchField columnWidth="2" labelName="SP服务代码" inputName="spServiceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_SP_SERVICECODE %>" autocompleteCascade="spIdName"/> 
		<et:searchField columnWidth="2" labelName="通道 " inputName="channelId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_CHANNEL %>"/>
		<et:searchField columnWidth="2" labelName="通道 " inputName="likeChannelId" />
		<et:searchField columnWidth="2" labelName="SMS下行状态" inputName="mtState" type="select" selectMap="${smsMtState}"/>
		<et:searchField columnWidth="2" labelName="MT消息状态" inputName="mtStat" />
		<et:searchField columnWidth="2" labelName="RT错误代码" inputName="rtErrorCode" type="select" selectMap="${rtErrorCode }"/>
		<et:searchField labelName="SP端到MLINK端下行的时间" inputName="smStartTime,smEndTime" type="dateTimeRange"/>
		<et:searchField labelName="MLINK端到运营商端下行时间" inputName="mcStartTime,mcEndTime" type="dateTimeRange"/>
		<et:searchField columnWidth="2" labelName="国家代码" inputName="countryIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_COUNTRY %>"/>
		<et:searchField columnWidth="2" labelName="省代码" inputName="provinceIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
		<et:searchField columnWidth="2" labelName="市代码" inputName="cityIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_CITY %>" autocompleteCascade="provinceIdName"/>
		<et:searchField columnWidth="2" labelName="运营商代码" inputName="carrierCodeIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_AUTOCOMPLETE_CARRIERCODE %>"/>
		<et:searchField labelName="目的地址" inputName="destinationAddr" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXXXGL_MTSELECT_DATA %>" dataCheckbox="false">
		<et:pageField tableHeadName="SMS下行编号" value="spMtId"/>
		<et:pageField tableHeadName="SP编号" value="spId"/>
		<et:pageField tableHeadName="SP服务代码" value="spServiceCode" />
		<et:pageField tableHeadName="通道编号" value="channelId"/>
		<et:pageField tableHeadName="SMS下行状态" value="mtState" dataReplace="${smsMtState }"/>
		<et:pageField tableHeadName="SMS状态报告状态" value="rtState" dataReplace="${smsRtState }"/>
		<%-- <et:pageField tableHeadName="SMS批量下行编号" value="batchMtId" /> --%>
		<et:pageField tableHeadName="优先级" value="priority"/>
		<et:pageField tableHeadName="源地址" value="sourceAddr"/>
		<et:pageField tableHeadName="目标地址" value="destinationAddr"/>
		<et:pageField tableHeadName="SMS消息" value="shortMessage"/>
		<et:pageField tableHeadName="MT消息状态" value="mtStat"/>
		<et:pageField tableHeadName="MT错误代码" value="mtErr"/>
		<et:pageField tableHeadName="RT消息状态" value="rtStat"/>
		<et:pageField tableHeadName="RT错误代码" value="rtErr"/>
		<et:pageField tableHeadName="SP端到MLINK端下行的时间" value="spMtTime" type="dateTime"/>
		<et:pageField tableHeadName="MLINK端到运营商端下行的时间" value="carrierMtTime" type="dateTime"/>
		<et:pageField tableHeadName="运营商端到MLINK端状态报告的时间" value="carrierRtTime" type="dateTime"/>
		<et:pageField tableHeadName="MLINK端到SP端状态报告的时间" value="spRtTime" type="dateTime"/>
		<et:pageField tableHeadName="国家代码" value="countryCode"/>
		<et:pageField tableHeadName="省代码" value="provinceCode"/>
		<et:pageField tableHeadName="市代码" value="cityCode"/>
		<et:pageField tableHeadName="运营商代码" value="carrierCode"/>
		<et:pageField tableHeadName="ESM_CLASS" value="esmClass"/>
		<et:pageField tableHeadName="PROTOCOL_编号" value="protocolId"/>
		<et:pageField tableHeadName="DATA_CODING" value="dataCoding"/>
	</et:page>
</et:index>
</div>
<script>

$(document).ready(function(){
    var date = new Date();
    var date2 = new Date();
    date2.setDate(date2.getDate()+1);
    var dateStart = tableSet.fn.showDate(date,"yyyy-MM-dd 00:00:00");
    var dateEnd = tableSet.fn.showDate(date2,"yyyy-MM-dd 00:00:00");
    $("#mtquery input[name='smStartTime']").val(dateStart);
    $("#mtquery input[name='smEndTime']").val(dateEnd);
});
</script>