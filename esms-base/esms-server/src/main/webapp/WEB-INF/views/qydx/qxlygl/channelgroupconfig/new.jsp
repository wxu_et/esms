<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_CREATE%>">
	<et:formField inputName="channelGroupIdName" labelName="通道组" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP %>"/>
	<et:formField inputName="channelIdName" labelName="通道" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_AUTOCOMPLETE_CHANNEL %>"/>
	<et:formField inputName="carrierCodeIdName" required="true" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField inputName="routeState" required="true" labelName="路由状态" type="radio" displayMap="${routeState }" value="1"/>
	<et:formField inputName="countryIdName" required="true" labelName="国家代码" value="86-中国" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY %>"/>
	<et:formField inputName="provinceIdName" required="true" labelName="省代码" value="*" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
	<et:formField inputName="cityIdName" required="true" labelName="市代码" value="*" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CITY %>" autocompleteCascade="provinceIdName"/>
	<et:formField inputName="spChannelPriority" labelName="SP通道优先级" required="true" max="99"/>
	<et:formField inputName="spChannelWeightNumber" labelName="SP通道加权系数" required="true" max="99"/>
	<et:formField inputName="remark" labelName="备注" type="textarea" maxlength="500"/>
</et:form>