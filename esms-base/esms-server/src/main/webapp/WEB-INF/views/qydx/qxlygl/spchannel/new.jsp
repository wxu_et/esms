<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_CREATE %>">
	<et:formField inputName="spIdName" labelName="SP" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP %>"/>
	<et:formField inputName="spServiceCode" labelName="SP服务代码" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP_SERVICECODE %>" autocompleteCascade="spIdName"/>
	<et:formField inputName="channelIdName" labelName="通道" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL %>"/>
	<et:formField inputName="spChannelNumber" labelName="SP通道号码" required="true" maxlength="21"/>
	<et:formField inputName="spChannelPriority" labelName="SP通道优先级" required="true" max="99"/>
	<et:formField inputName="spChannelWeightNumber" labelName="SP通道加权系数" required="true" max="99"/>
</et:form>
