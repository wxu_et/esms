<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_UPDATE%>">
	<et:formField inputName="channelGroupConfigId" labelName="channelGroupConfigId" type="hidden" value="${cgc.channelGroupConfigId }"/>
	<et:formField inputName="channelGroupIdName" value="${cgc.channelGroup.channelGroupId }-${cgc.channelGroup.channelGroupName }" labelName="通道组" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP %>"/>
	<et:formField inputName="channelIdName" value="${cgc.channel.channelId }-${cgc.channel.channelName }" labelName="通道" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_AUTOCOMPLETE_CHANNEL %>"/>
	<et:formField inputName="carrierCodeIdName" value="${cgc.carrierCode }" required="true" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField inputName="routeState" value="${cgc.routeState }" required="true" labelName="路由状态" type="radio" displayMap="${routeState }"/>
	<et:formField inputName="countryIdName" required="true" value="${cgc.countryCode }" labelName="国家代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY %>"/>
	<et:formField inputName="provinceIdName" required="true" value="${cgc.provinceCode }" labelName="省代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
	<et:formField inputName="cityIdName" required="true" value="${cgc.cityCode }" labelName="市代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CITY %>" autocompleteCascade="provinceIdName"/>
	<et:formField inputName="spChannelPriority" value="${cgc.spChannelPriority }" labelName="SP通道优先级" required="true" max="99"/>
	<et:formField inputName="spChannelWeightNumber" value="${cgc.spChannelWeightNumber }" labelName="SP通道加权系数" required="true" max="99"/>
	<et:formField inputName="remark" value="${cgc.remark }" labelName="备注" type="textarea" maxlength="500"/>
</et:form>