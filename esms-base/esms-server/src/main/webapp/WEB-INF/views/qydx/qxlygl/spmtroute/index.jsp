<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="spmtroute">
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_DEL %>">
	<et:search column="6">
		<et:searchField labelName="SP" inputName="spIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP %>"/>
		<et:searchField labelName="SP服务代码" inputName="spServiceCode"/>
		<et:searchField labelName="通道" inputName="channelIdNameSearch" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_CHANNEL %>"/>
		<et:searchField labelName="运营商代码 " inputName="carrierCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
		<et:searchField labelName="国家代码" inputName="countryIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY %>"/>
		<et:searchField labelName="省代码" inputName="provinceIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE %>" autocompleteCascade="countryIdName"/>
		<et:searchField labelName="路由状态" inputName="routeState" type="select" selectMap="${routeStateList}"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_DATA %>">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPMTROUTE_VIEW %>" linkUrlParam="spMtRouteId" dataSortable="false"/>
		<et:pageField tableHeadName="SP" value="sp.spId-sp.spName"/>
		<et:pageField tableHeadName="SP服务代码" value="spServiceCode"/>
		<et:pageField tableHeadName="路由类型" value="routeType" dataReplace="${routeTypeList }"/>
		<et:pageField tableHeadName="路由状态" value="routeState" dataReplace="${routeStateList }"/>
		<et:pageField tableHeadName="通道组" value="channelGroup.channelGroupId-channelGroup.channelGroupName" dataSortable="false"/>
		<et:pageField tableHeadName="通道组路由状态" value="channelGroupRouteState" dataReplace="${channelGroupRouteStateList }"/>
		<et:pageField tableHeadName="通道" value="channel.channelId-channel.channelName" dataSortable="false"/>
		<et:pageField tableHeadName="运营商代码" value="carrierCode"/>
		<et:pageField tableHeadName="国家代码" value="countryCode"/>
		<et:pageField tableHeadName="省代码" value="provinceCode"/>
		<et:pageField tableHeadName="市代码" value="cityCode"/>
	</et:page>
</et:index>
</div>
<script>
$(document).ready(function(){
	var  spid="${spIdName}";
	$("#spmtroute input[name='spIdName']").val(spid);
	if(spid!=''&&spid!=undefined){
		function c(){
			$('[data-toggle="table"]').bootstrapTable('refresh',{query: {spIdName: spid}});
		}
		setTimeout(c,1000);
	}
});
</script>