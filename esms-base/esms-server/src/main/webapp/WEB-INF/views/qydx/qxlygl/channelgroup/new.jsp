<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_CREATE%>">
	<et:formField inputName="channelGroupId" labelName="通道组编号" required="true" maxlength="10" remote="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_CHANNELGROUPID_CHECK %>" remoteMsg="请填写一个未曾使用过的通道组编号"/>
	<et:formField inputName="channelGroupName" labelName="通道组名称" required="true" maxlength="60"/>
	<et:formField inputName="remark" labelName="备注" type="textarea" maxlength="500"/>
</et:form>