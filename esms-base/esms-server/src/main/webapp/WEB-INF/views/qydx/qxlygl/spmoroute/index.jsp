<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<script src="/static/js/common/tip.js?v=${jsVersion}"></script>
<et:index id="sp_mangaer" createBtnUrl="${request.getContextPath()+UrlConstants.qydxQxlyglSpmorouteNew}" delBtnUrl="${request.getContextPath()+UrlConstants.qydxQxlyglSpmorouteDel}">
	<et:search>
		<et:searchField labelName="SP" inputName="spChannel.id.spId" value="${search.spChannel.id.spId }"/>
		<et:searchField labelName="SP服务代码" inputName="spChannel.id.spServiceCode" value="${search.spChannel.id.spServiceCode }"/>
		<et:searchField labelName="通道" inputName="spChannel.id.channelId" value="${search.spChannel.id.channelId }"/>
		<et:searchField labelName="路由状态" inputName="routeState" value="${search.routeState }" type="select" selectMap="${routeState}"/>
	</et:search>
	<et:page pageData="${pageData }" var="spmo">
		<et:pageField tableHeadName="" value="${spmo.spMoRouteId }" type="checkbox" name="spmoids" width="30px"/>
		<et:pageField tableHeadName="操作" value="查看" target="_modal" type="a" href="${request.getContextPath()+UrlConstants.qydxQxlyglSpmorouteView }?spmoid=${spmo.spMoRouteId }"/>
		<et:pageField tableHeadName="上行路由编号" value="${spmo.spMoRouteId }" />
		<et:pageField tableHeadName="通道" value="${spmo.spChannel.channel.channelId }-${spmo.spChannel.channel.channelName }"/>
		<et:pageField tableHeadName="SP" value="${spmo.spChannel.spService.sp.spId }-${spmo.spChannel.spService.sp.spName }"/>
		<et:pageField tableHeadName="SP服务代码" value="${spmo.spChannel.spService.id.spServiceCode }"/>
		<et:pageField tableHeadName="SP通道号码" value="${spmo.spChannelNumber}"/>
		<et:pageField tableHeadName="路由状态" value="${spmo.routeState }"/>
	</et:page>
</et:index>