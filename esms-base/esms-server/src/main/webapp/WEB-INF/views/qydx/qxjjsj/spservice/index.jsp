<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="spservice">
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_NEW %>">
	<et:search column="5">
		<et:searchField labelName="SP" inputName="sp" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_SP %>"/>
		<et:searchField labelName="SP服务代码" inputName="spServiceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_SP_SERVICECODE %>" autocompleteCascade="sp"/>
		<et:searchField labelName="SP服务名称" inputName="spServiceName" />
		<et:searchField labelName="优先级" inputName="priorityPattern" />
		<et:searchField labelName="内容后缀样式" inputName="contentPostfixPattern" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_DATA %>" dataCheckbox="false" dataSortName="id">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_VIEW %>" linkUrlParam="id.spId,id.spServiceCode"/>
		<et:pageField tableHeadName="SP" value="sp.spId-sp.spName" dataSortable="false"/>
		<et:pageField tableHeadName="SP服务代码" value="id.spServiceCode" dataSortable="false"/>
		<et:pageField tableHeadName="SP服务名称" value="spServiceName" dataSortable="false"/>
		<et:pageField dataWidth="30px" tableHeadName="SP服务扩展码" value="spscExtNumber"/>
		<et:pageField dataWidth="30px" tableHeadName="内容过滤策略" value="contentFilterPolicy" dataReplace="${contentFilterPolicy}"/>
		<et:pageField dataWidth="30px" tableHeadName="优先级" value="priorityPattern"/>
		<et:pageField tableHeadName="内容前缀样式" value="contentPrefixPattern"/>
		<et:pageField tableHeadName="内容后缀样式" value="contentPostfixPattern"/>
		<et:pageField tableHeadName="内容分拆样式" value="contentSplitPattern"/>
		<et:pageField tableHeadName="优先级策略" value="priorityPolicy" dataReplace="${priorityPolicy}"/>
	</et:page>
</et:index>
</div>
<script>
$(document).ready(function(){
   var  spid="${sp}";
	$("#spservice input[name='sp']").val(spid);
	if(spid!=''&&spid!=undefined){
		function c(){
			$('[data-toggle="table"]').bootstrapTable('refresh',{query: {sp: spid}});
		}
		setTimeout(c,1000);
	}
});
</script>