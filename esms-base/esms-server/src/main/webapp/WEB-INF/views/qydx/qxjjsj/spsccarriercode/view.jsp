<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_UPDATE%>">
	<et:formField inputName="sccid" labelName="sccid" type="hidden" value="${scc.id.spId }-${scc.id.spServiceCode }-${scc.id.carrierCode }"/>
	<et:formField readonly="true" value="${scc.spService.sp.spId }-${scc.spService.sp.spName }" inputName="spIdName" required="true" labelName="SP" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP%>"/>
	<et:formField readonly="true" value="${scc.id.spServiceCode }" inputName="id.spServiceCode" required="true" labelName="sp服务代码" type="autocomplete" autocompleteUrl="/qydx/qxjjsj/spsccarriercode/autocomplete/spservicecode?sflag=0" autocompleteCascade="spIdName"/>
	<et:formField readonly="true" value="${scc.id.carrierCode }" required="true" inputName="carrierCodeIdName" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField value="${scc.smsPattern }" required="true" inputName="smsPattern" labelName="分拆策略" type="radio" displayMap="${smsPattern }"/>
	<et:formField value="${scc.channelMessageLength }" required="true" range="[1,1000]" inputName="channelMessageLength" labelName="通道允许最大长度" readonly="true"/>
</et:form>