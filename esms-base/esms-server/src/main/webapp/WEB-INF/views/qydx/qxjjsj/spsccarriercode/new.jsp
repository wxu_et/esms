<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_CREATE%>">
	<et:formField inputName="spIdName" required="true" labelName="SP" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP%>"/>
	<et:formField inputName="id.spServiceCode" required="true" labelName="sp服务代码" type="autocomplete" autocompleteUrl="/qydx/qxjjsj/spsccarriercode/autocomplete/spservicecode?sflag=0" autocompleteCascade="spIdName"/>
	<et:formField required="true" inputName="carrierCodeIdName" labelName="运营商代码" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_AUTOCOMPLETE_CARRIERCODE %>"/>
	<et:formField required="true" inputName="smsPattern" labelName="分拆策略" type="radio" displayMap="${smsPattern }" value="0"/>
	<et:formField required="true" range="[1,1000]" inputName="channelMessageLength" labelName="通道允许最大长度" value="60"/>
</et:form>