<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="spchannel">
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_DEL %>">
	<et:search >
		<et:searchField labelName="SP" inputName="spIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP%>"/>
		<et:searchField labelName="通道" inputName="channelIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL%>"/>
		<et:searchField labelName="SP通道号码" inputName="spChannelNumber" />
		<et:searchField labelName="SP服务代码" inputName="spServiceCode" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_DATA %>" dataRowStyle="chbbRowStyle" >
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_VIEW %>" linkUrlParam="id.channelId,id.spServiceCode,id.spId" dataSortable="false"/>
		<et:pageField tableHeadName="SP" value="spService.sp.spId-spService.sp.spName" dataSortable="false"/>
		<et:pageField tableHeadName="SP服务代码" value="spService.id.spServiceCode" dataSortable="false"/>
		<et:pageField tableHeadName="通道" value="channel.channelId-channel.channelName" dataSortable="false"/>
		<et:pageField tableHeadName="SP通道号码" value="spChannelNumber"/>
		<et:pageField tableHeadName="SP通道优先级" value="spChannelPriority"/>
		<et:pageField tableHeadName="SP通道加权系数" value="spChannelWeightNumber"/>
		<et:pageField tableHeadName="是否通道报备" value="channel.channelBaobeiFlag" dataFormatter="isbbchannel" />
	</et:page>
</et:index>
</div>
<script>
$(document).ready(function(){
	var  spid="${spIdName}";
	$("#spchannel input[name='spIdName']").val(spid);
	if(spid!=''&&spid!=undefined){
		function c(){
			$('[data-toggle="table"]').bootstrapTable('refresh',{query: {spIdName: spid}});
		}
		setTimeout(c,1000);
	}	
});

  function chbbRowStyle(r,i){
	  if(r.channel.channelBaobeiFlag != 100)
		  return {classes:'yellow-row'}
	  else
		  return {}
  };
  
  function isbbchannel(value,row,index){ 	  
	  if(row.channel.channelBaobeiFlag==100){
		  return '是';
	  }else {
		  return '否';
	  } 
  };

</script>