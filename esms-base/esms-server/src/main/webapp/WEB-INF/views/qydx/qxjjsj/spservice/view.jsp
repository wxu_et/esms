<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSERVICE_UPDATE %>">
	<et:formField value="${sps.id.spId }" inputName="id.spId" labelName="SP编号" required="true" readonly="true" type="hidden"/>
	<et:formField value="${sps.sp.spId }-${sps.sp.spName }" inputName="spIdName" labelName="SP" required="true" readonly="true"/>
	<et:formField value="${sps.id.spServiceCode }" inputName="id.spServiceCode" labelName="SP服务代码" required="true" maxlength="10" readonly="true"/>
	<et:formField value="${sps.spServiceName }" inputName="spServiceName" labelName="SP服务名称" maxlength="60"/>
	<et:formField value="${sps.msisdnFilterPolicy }" inputName="msisdnFilterPolicyCheckbox" labelName="号码过滤策略" required="true" type="checkbox" displayMap="${msisdnFilterPolicy }" checkboxCss="checkbox" />
	<et:formField value="${sps.contentFilterPolicy }" inputName="contentFilterPolicy" labelName="内容过滤策略" required="true" type="radio" displayMap="${contentFilterPolicy }"/>
	<et:formField value="${sps.contentFilterCategoryFlag }" inputName="contentFilterCategoryFlagCheckbox" labelName="内容过滤分类标志" required="true" type="checkbox" displayMap="${contentFilterCategoryFlag }" checkboxCss="checkbox-floatleft"/>
	<et:formField value="${sps.contentProcessPolicy }" required="true"	inputName="cpp" labelName="内容处理策略" type="radiogroup" groupList="${cpp }"/>
	<et:formField value="${sps.priorityPolicy }" inputName="priorityPolicy" labelName="优先级策略" required="true" type="radio" displayMap="${priorityPolicy }" radioCss="radio"/>
	<et:formField value="${sps.priorityPattern }" inputName="priorityPattern" labelName="优先级" required="true" max="10000"/>
	
	<et:formField value="${sps.contentPrefixPattern }" inputName="contentPrefixPattern" labelName="内容前缀样式" maxlength="60"/>
	<et:formField value="${sps.contentPostfixPattern }" inputName="contentPostfixPattern" labelName="内容后缀样式 " maxlength="60"/>
	<et:formField value="${sps.contentSplitPattern }" inputName="contentSplitPattern" labelName="内容分拆样式" maxlength="60"/>
	<et:formField inputName="spscExtNumber" value="${sps.spscExtNumber }" labelName="SP服务扩展码" maxlength="20"/>
</et:form>