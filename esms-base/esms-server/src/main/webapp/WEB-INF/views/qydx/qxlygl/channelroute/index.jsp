<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELROUTE_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELROUTE_DEL %>">
	<et:search>
		<et:searchField value="${channelIdNameSearch}" labelName="通道" inputName="channelIdNameSearch" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL %>"/>
		<et:searchField value="${carriercodeSearch}" labelName="运营商代码" inputName="carriercodeSearch" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE %>"/>
		<et:searchField value="${routeStateSearch}" labelName="路由状态" inputName="routeStateSearch" type="select" selectMap="${routeStateList }"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELROUTE_DATA %>">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELROUTE_VIEW %>" linkUrlParam="channelRouteId" dataSortable="false"/>
		<et:pageField tableHeadName="通道" value="channel.channelId-channel.channelName" dataSortable="false"/>
		<et:pageField tableHeadName="运营商代码" value="carrierCode"/>
		<et:pageField tableHeadName="国家代码" value="countryCode"/>
		<et:pageField tableHeadName="省代码" value="provinceCode"/>
		<et:pageField tableHeadName="市代码" value="cityCode"/>
		<et:pageField tableHeadName="路由状态" value="routeState" type="radio" dataReplace="${routeStateList }"/>
	</et:page>
</et:index>