<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_DEL %>">
	<et:search >
		<et:searchField labelName="通道组" inputName="channelGroupIdName" value="${channelGroupIdName }" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP %>"/>
		<et:searchField labelName="通道" inputName="channelIdName" value="${channelIdName }" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_AUTOCOMPLETE_CHANNEL %>"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_DATA %>">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_VIEW %>" linkUrlParam="channelGroupConfigId" dataSortable="false"/>
		<et:pageField tableHeadName="通道组" value="channelGroup.channelGroupId-channelGroup.channelGroupName" dataSortable="false"/>
		<et:pageField tableHeadName="通道" value="channel.channelId-channel.channelName" dataSortable="false"/>
		<et:pageField tableHeadName="运营商代码" value="carrierCode"/>
		<et:pageField tableHeadName="国家代码" value="countryCode"/>
		<et:pageField tableHeadName="省代码" value="provinceCode"/>
		<et:pageField tableHeadName="市代码" value="cityCode"/>
		<et:pageField tableHeadName="路由状态" value="routeState" dataReplace="${routeStateList }"/>
		<et:pageField tableHeadName="SP通道优先级" value="spChannelPriority"/>
		<et:pageField tableHeadName="SP通道加权系数" value="spChannelWeightNumber"/>
	</et:page>
</et:index>