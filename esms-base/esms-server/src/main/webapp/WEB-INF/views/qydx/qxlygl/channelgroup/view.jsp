<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_UPDATE%>">
	<et:formField inputName="channelGroupId" labelName="通道组编号" readonly="true" required="true" maxlength="10" value="${cg.channelGroupId }"/>
	<et:formField inputName="channelGroupName" labelName="通道组名称" required="true" maxlength="60" value="${cg.channelGroupName }"/>
	<et:formField inputName="remark" labelName="备注" type="textarea" maxlength="500" value="${cg.remark }"/>
</et:form>