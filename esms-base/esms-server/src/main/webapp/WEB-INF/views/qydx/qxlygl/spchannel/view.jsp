<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_SPCHANNEL_UPDATE %>">
	<et:formField type="hidden" inputName="id.spId" labelName="SP编号hidden" required="true" readonly="true" value="${spch.id.spId }"/>
	<et:formField type="hidden" inputName="id.spServiceCode" labelName="SP服务代码hidden" required="true" readonly="true" value="${spch.id.spServiceCode }"/>
	<et:formField type="hidden" inputName="id.channelId" labelName="通道编号hidden" required="true" readonly="true" value="${spch.id.channelId }"/>
	
	<et:formField inputName="spIdName" labelName="SP" required="true" readonly="true" value="${spch.spService.sp.spId }-${spch.spService.sp.spName }"/>
	<et:formField inputName="spServiceCode" labelName="SP服务代码" required="true" readonly="true" value="${spch.spService.id.spServiceCode }"/>
	<et:formField inputName="channelIdName" labelName="通道" required="true" readonly="true" value="${spch.channel.channelId }-${spch.channel.channelName }"/>
	
	<et:formField inputName="spChannelNumber" labelName="SP通道号码" required="true" maxlength="21"  value="${spch.spChannelNumber }"/>
	<et:formField inputName="spChannelPriority" labelName="SP通道优先级" required="true" max="99"  value="${spch.spChannelPriority }"/>
	<et:formField inputName="spChannelWeightNumber" labelName="SP通道加权系数" required="true" max="99"  value="${spch.spChannelWeightNumber }"/>
</et:form>
