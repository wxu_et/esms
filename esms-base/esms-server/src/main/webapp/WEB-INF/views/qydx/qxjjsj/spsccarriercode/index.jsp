<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_DEL %>">
	<et:search>
		<et:searchField value="${spIdName}" labelName="SP" inputName="spIdName" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP%>"/>
		<et:searchField value="${spServiceCodeSearch}" labelName="SP服务代码" inputName="spServiceCodeSearch" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP_SERVICECODE%>" autocompleteCascade="spIdName"/>
		<et:searchField value="${carrierCodeSearch}" labelName="运营商代码" inputName="carrierCodeSearch" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_AUTOCOMPLETE_CARRIERCODE %>"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_DATA %>">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SPSCCARRIERCODE_VIEW %>" linkUrlParam="id.spId,id.spServiceCode,id.carrierCode" dataSortable="false"/>
		<et:pageField tableHeadName="SP" value="id.spId-spService.sp.spName" dataSortable="false"/>
		<et:pageField tableHeadName="SP服务代码" value="id.spServiceCode" />
		<et:pageField tableHeadName="运营商代码" value="id.carrierCode" />
		<et:pageField tableHeadName="分拆策略" value="smsPattern" dataReplace="${smsPattern }"/>
		<et:pageField tableHeadName="通道允许最大长度" value="channelMessageLength"/>
	</et:page>
</et:index>