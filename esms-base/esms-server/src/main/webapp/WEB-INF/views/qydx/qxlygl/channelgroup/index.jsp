<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_NEW %>">
	<et:search >
		<et:searchField labelName="通道组编号" inputName="channelGroupId" value="${search.channelGroupId }"/>
		<et:searchField labelName="通道组名称" inputName="channelGroupName" value="${search.channelGroupName }"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_DATA %>" dataCheckbox="false">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.QYDX_QXLYGL_CHANNELGROUP_VIEW %>" linkUrlParam="channelGroupId" dataSortable="false"/>
		<et:pageField tableHeadName="通道组编号" value="channelGroupId"/>
		<et:pageField tableHeadName="通道组名称" value="channelGroupName"/>
	</et:page>
</et:index>