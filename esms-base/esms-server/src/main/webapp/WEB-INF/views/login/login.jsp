<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="single-container login-screen animated fadeInDown">
	<section class="sign-widget-title">
		<h1>
			移通 <b>ETBOSS</b>企业管理平台
		</h1>
	</section>
	<section class="sign-widget">
		<header>
			<h4>
				登录 <b>ETBOSS</b> 用户
			</h4>
		</header>
		<div class="body">
			<form method="post" action="" class="no-margin">
				<fieldset>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-user"></i>
							</span> <input type="text" placeholder="请输入用户名"
								class="form-control nope" id="username" name="username">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-lock"></i>
							</span> <input type="password" placeholder="请输入密码"
								class="form-control nope" id="password" name="password">
						</div>
					</div>
				</fieldset>
				<div class="form-actions">
					<button class="btn btn-block btn-primary" type="submit" id="login">
						<span class="fa-icon-circle"> <i class="fa fa-sign-in"></i>
						</span> <small class="l-mar-5">登录</small>
					</button>
				</div>
							<c:if test="${param.error != null}"> ·
<p>Invalid username and password.</p>
			</c:if>
			<c:if test="${param.logout != null}"> ¸
<p>You have been logged out.</p>
			</c:if>
				
			</form>
		</div>
	</section>
	<footer class="footpg pad20"> Copyright &copy; 1999-2016
		ETONENET,INC. All Rights Reserved. </footer>
</div>
<script type="text/javascript">
<!--
	$("#welcome").html("欢迎登录" + $("title").html());
	var showTip = function(str) {
		/* var tip=$('<div class="modal hide fade">'
				+'<div class="modal-body">'
				+'<p>'+str+'</p>'
				+'</div>'
				+'<div class="modal-footer">'
				+'<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">确定</a>'
				+'</div>'
				+'</div>'
				+'</div>');
		tip.on('hidden', function () {
			normalStat();
		});
		tip.modal('show'); */
		//alert(str);
		layer.msg(str, {
			time : 1000
		});
		/* layer.alert(str, {
			skin : 'layui-layer-lan',
			closeBtn : 0,
			icon :2,
			shift : 4
		}); */
		normalStat();
	};

	var clearTip = function() {

	};

	var loginStat = function() {
		var b = $("#login");
		b.attr("disabled", "disabled");
		b.attr("value", "登录中...");
		return 0;
	};

	var normalStat = function() {
		var b = $("#login");
		b.removeAttr("disabled");
		b.attr("value", "登录");
	};
	normalStat();
	var login = function() {
		loginStat();

		var ln = trim($("#username").val());
		var pd = $("#password").val();

		if ("" == ln) {
			showTip("登录账号不能为空");
			//layer.msg('登录账号不能为空');
			return;
		}

		if ("" == pd) {
			showTip("密码不能为空");
			return;
		}

		$.ajax({
			type : "post",
			dataType : "json",
			url : "${pageContext.request.contextPath}/dologin",
			data : {
				username : ln,
				password : pd
			},
			timeout : 15000,
			success : function(data, textStatus) {
				if (data.err != null) {
					showTip(data.err);
				} else if (data.succ != null) {
					//window.location = 'boss'
					window.location = data.succ
				}
			},
			error : function(err, abc) {
				showTip("网络请求失败或超时请重新尝试")
			}
		});
	};

	// 相关事件
	$("body").keydown(function(event) {
		if (event.keyCode == 13) {
			if ($("#login").attr("disabled") != "disabled")
				login();
		}
	});

	$("#login").click(function() {
		login();
	});

	$("#username").focus();
	-->
</script>
<%
	application.setAttribute("cssVersion", "20151113");
	application.setAttribute("jsVersion", "20151113");
%>