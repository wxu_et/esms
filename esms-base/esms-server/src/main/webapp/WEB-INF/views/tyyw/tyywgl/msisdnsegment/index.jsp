<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="esms.etonenet.boss1069.web.UrlConstants" %>
<%@ taglib uri="http://www.etonenet.com/tld" prefix = "et"%>
<script src="/static/js/common/tip.js?v=${jsVersion}"></script>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_DEL %>">
   <et:search column="4">
      <et:searchField labelName="号码段前缀" inputName="msisdnSegmentPrefix" />
      <et:searchField labelName="号码段状态" inputName="msisdnSegmentState" type="select" selectMap="${segmentState}"/>
      <et:searchField labelName="运营商代码" inputName="carrierCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CARRIERCODE %>" />      
      <et:searchField labelName="网络类型" inputName="networkCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_NETWORKCODE %>" />      
      <et:searchField labelName="国家代码" inputName="countryCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_COUNTRYCODE %>" />
      <et:searchField labelName="省代码" inputName="provinceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_SINGLE_PROVINCECODE %>" />
      <et:searchField labelName="市代码" inputName="cityCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_SINGLE_CITYCODE %>" />
      <et:searchField labelName="注释" inputName="note" />     
   </et:search>   
   <et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_DATA %>" >
      <et:pageField tableHeadName="操作" value="查看" type="link" linkUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_VIEW %>" linkUrlParam="msisdnSegmentPrefix"/>
      <et:pageField tableHeadName="号码段前缀" value="msisdnSegmentPrefix"/>
      <et:pageField tableHeadName="国家代码" value="countryCode"/>
      <et:pageField tableHeadName="省代码" value="provinceCode"/>
      <et:pageField tableHeadName="市代码" value="cityCode"/>
      <et:pageField tableHeadName="运营商代码" value="carrierCode"/>
      <et:pageField tableHeadName="号码状态" value="msisdnSegmentState" dataReplace="${segmentState}"/>
      <et:pageField tableHeadName="号码最小长度" value="msisdnMinLength"/>
      <et:pageField tableHeadName="号码最大长度" value="msisdnMaxLength"/>
      <et:pageField tableHeadName="网络类型" value="networkCode"/>
      <et:pageField tableHeadName="数据级别" value="dataLevel"/>
      <et:pageField tableHeadName="创建时间" value="createTime" type="dateTime" />
      <et:pageField tableHeadName="更新时间" value="updateTime" type="dateTime" />
      <et:pageField tableHeadName="注释" value="note"/>
   </et:page>
</et:index>
