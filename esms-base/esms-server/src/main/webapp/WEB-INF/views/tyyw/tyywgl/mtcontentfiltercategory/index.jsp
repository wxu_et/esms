<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index
	createBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_NEW%>"
	delBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_DEL%>">
	<et:search>
		<et:searchField labelName="类别名" inputName="textFilterCategoryName" />
	</et:search>
	<et:page
		dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_DATA%>"
		dataSortName="textFilterCategoryId" dataSortOrder="desc">
		<et:pageField tableHeadName="操作" value="查看" type="link" dataWidth="20"
			linkTarget="_modal"
			linkUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_VIEW%>"
			linkUrlParam="textFilterCategoryId" />
		<et:pageField tableHeadName="类别编号" value="textFilterCategoryId"
			dataWidth="100" />
		<et:pageField tableHeadName="类别名" value="textFilterCategoryName" />
	</et:page>
</et:index>