<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="esms.etonenet.boss1069.web.UrlConstants" %>
<%@ taglib uri="http://www.etonenet.com/tld" prefix = "et"%>
<script src="/static/js/common/tip.js?v=${jsVersion}"></script>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_NEW%>" delBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_DEL%>">
  <et:search column="4">
     <et:searchField labelName="创建起始时间" inputName="timeStart" type="date" />
	 <et:searchField labelName="创建结束时间" inputName="timeEnd" type="date" />
     <et:searchField labelName="SP" inputName="spId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_AUTOCOMPLETE_SP%>" />
     <et:searchField labelName="MSISDN号码" inputName="msisdnFilterPattern" />
  </et:search>
  <et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_DATA %>">
     <et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_VIEW %>" linkUrlParam="id.spId,id.msisdnFilterPattern" dataSortable="false"/>
     <et:pageField tableHeadName="全局/SP编号" dataFormatter="global" />
     <et:pageField tableHeadName="MSISDN号码" value="id.msisdnFilterPattern" />
     <et:pageField tableHeadName="数据级别" value="dataLevel" />
     <et:pageField tableHeadName="创建时间" value="createTime" type="dateTime" />
     <et:pageField tableHeadName="注释" value="note" />
  </et:page>
</et:index>
<div id="progressbar"></div>
<script type="text/javascript">
<!--
	function global(v,r,i){
		if(r.id.spId==0)
			return '全局'
		else
			return 'SP编号:'+r.id.spId;	
	}
	$(function() {
		setInterval(progressbar, 1000);
	});
	function progressbar() {
		$.post("/tyyw/tyywgl/msisdnfilter/progressbar/import", {}, function(
				data) {
			data.forEach(function(element, index, array) {
				if ($("#bar" + index).length == 0) {
					$("#progressbar").append("<div id='bar"+index+"'><lable id='label"+index+"'>加载...</label></div>");
				}
				$("#bar" + index).progressbar({
					value : parseInt(element.parameter2)
				});
				$("#label"+index).html(element.parameter1+": "+element.parameter2+"%");
				if (element.parameter2 >= 100) {
					$("#label"+index).html("完成");					
					$("#bar" + index).hide(1000);
				}
			});
		});
	}
	//-->
</script>