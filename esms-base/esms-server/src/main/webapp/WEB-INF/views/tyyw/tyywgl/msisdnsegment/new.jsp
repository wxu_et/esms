<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_CREATE%>">
	<et:formField labelName="号码段前缀" inputName="msisdnSegmentPrefix" required="true" />
    <et:formField labelName="国家代码" inputName="countryCode" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_COUNTRYCODE %>" />
    <et:formField labelName="省代码" inputName="provinceCode" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_PROVINCECODE %>" autocompleteCascade="countryCode" />
    <et:formField labelName="市代码" inputName="cityCode" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CITYCODE %>" autocompleteCascade="provinceCode" />
    <et:formField labelName="运营商代码" inputName="carrierCode" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CARRIERCODE %>" />
    <et:formField labelName="号码状态" inputName="msisdnSegmentState" type="radio" required="true" displayMap="${segmentState}" value="1"/>
    <et:formField labelName="号码最小长度" inputName="msisdnMinLength" required="false" />
    <et:formField labelName="号码最大长度" inputName="msisdnMaxLength" required="false" />
    <et:formField labelName="网络类型" inputName="networkCode" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_NETWORKCODE %>" />
    <et:formField labelName="数据级别" inputName="dataLevel" type="select" displayMap="${dataLevelMap}" required="false" value="1"/>
    <et:formField labelName="注释" inputName="note" required="false" />
</et:form>
<style>
<!--
.tooltip-inner {
max-width:none;
padding: 2px 8px;
height: 40px;
color: #fff;
text-align: center;
text-decoration: none;
background-color: #000;
border-radius: 4px;
}
-->
</style>