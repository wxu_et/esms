<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<div id="spscf">
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_UPDATE%>">
	<et:formField inputName="spId" labelName="SP" value="${filter.id.spId}" type="hidden" />
	<et:formField inputName="spServiceCode" labelName="SP服务代码" value="${filter.id.spServiceCode}" type="hidden" />
	<et:formField inputName="textType" labelName="文字内容类别" value="${filter.textType}" type="hidden" />
	<et:formField inputName="text"  labelName="text" value="${filter.id.text}" type="hidden" />
	<et:formField labelName="SP" type="select" disabled="true"	required="true" value="${filter.id.spId}" displayMap="${spIdMap}" />
	<et:formField labelName="SP服务代码" type="select" disabled="true" required="true" value="${filter.id.spServiceCode}" displayMap="${spscServiceCodeMap}" />
	<et:formField labelName="文字内容类别" value="${filter.textType}" type="radio" disabled="true" required="true" displayMap="${textType}" />
	<et:formField labelName="过滤类别" type="select" disabled="true" required="true" value="${filter.id.text}" displayMap="${categoryMap}" />
	<et:formField labelName="过滤内容" disabled="true" required="true" value="${filter.id.text}" />
	<et:formField labelName="是否过滤" inputName="textState" value="${filter.textState}" type="radio" required="true" displayMap="${textState}" />
</et:form>
</div>
<script type="text/javascript">
var ft=$("#spscf").find(".form-group").eq(7)
var fn=$("#spscf").find(".form-group").eq(8)
if('${textType}'=='1'){
	ft.show();	
	fn.hide();
}else{
	ft.hide();
	fn.show();
}
</script>