<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="mf">
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_UPDATE %>">
	<et:formField inputName="id.spId" labelName="id.spId" type="hidden" value="${filter.id.spId}"/>
	<et:formField labelName="是否全局" inputName="global" type="radio" disabled="true" required="true" displayMap="${spCheckMap}" />
    <et:formField labelName="sp" inputName="sp" readonly="true" value="${filter.id.spId}-${sp.spName }" />
    <et:formField labelName="MSISDN号码" inputName="id.msisdnFilterPattern" readonly="true" required="true" value="${filter.id.msisdnFilterPattern}" maxlength="1000" />
    <et:formField labelName="数据级别" inputName="dataLevel" type="select" required="true" value="${filter.dataLevel}" displayMap="${dataLevelMap}" />
    <et:formField labelName="注释" inputName="note" value="${filter.note}" maxlength="60" />
</et:form>
</div>
<script type="text/javascript">
<!--
var sp=$("#mf").find("[name='sp']").parent().parent().parent()
if('${filter.id.spId}'=='0'){
	$("#mf").find("input[type='radio'][name='global'][value='0']").attr("checked","checked");
	sp.hide();
}else{
	$("#mf").find("input[type='radio'][name='global'][value='1']").attr("checked","checked");
	sp.show();
}	
//-->
</script>
