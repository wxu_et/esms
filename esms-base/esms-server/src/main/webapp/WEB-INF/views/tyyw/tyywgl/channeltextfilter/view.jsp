<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<div id="spscf">
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_UPDATE%>">
	<et:formField inputName="channelId" labelName="通道" value="${ctf.id.channelId}" type="hidden" />
	<et:formField inputName="textType" labelName="文字内容类别" value="${ctf.textType}" type="hidden" />
	<et:formField inputName="text" labelName="text" value="${ctf.id.text}" type="hidden" />
	<et:formField labelName="通道" type="select" disabled="true" required="true" value="${ctf.id.channelId}" displayMap="${channelIdMap}" />
	<et:formField labelName="文字内容类别" value="${ctf.textType}" type="radio" disabled="true" required="true" displayMap="${textType}" />
	<et:formField labelName="过滤类别" type="select" disabled="true"	required="true" value="${ctf.id.text}" displayMap="${categoryMap}" />
	<et:formField labelName="过滤内容" disabled="true" required="true" value="${ctf.id.text}" />
	<et:formField labelName="是否过滤" inputName="textState" value="${ctf.textState}" type="radio" required="true" displayMap="${textState}" />
</et:form>
</div>
<script type="text/javascript">
var ft=$("#spscf").find(".form-group").eq(5)
var fn=$("#spscf").find(".form-group").eq(6)
if('${textType}'=='1'){
	ft.show();	
	fn.hide();
}else{
	ft.hide();
	fn.show();
}
</script>