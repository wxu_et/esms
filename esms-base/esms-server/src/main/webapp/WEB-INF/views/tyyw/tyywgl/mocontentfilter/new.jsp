<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_CREATE%>">
	<et:formField inputName="textFilterPattern" labelName="内容过滤模式"
		required="true" maxlength="300"
		remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_TEXTFILTERPATTERNCHECK%>"
		remoteMsg="不可重复添加相同的内容过滤模式" />
	<et:formField inputName="contentFilterCategoryFlagCheckbox" labelName="内容过滤类别" 
        required="true" type="checkbox" displayMap="${contentFilterCategoryFlag }" 
        checkboxCss="checkbox-floatleft" />
</et:form>