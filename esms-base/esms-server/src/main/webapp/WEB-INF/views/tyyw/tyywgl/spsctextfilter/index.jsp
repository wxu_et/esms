<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_NEW%>" delBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_DEL%>">
	<et:search column="6">
		<et:searchField labelName="SP" inputName="spIdName" type="autocomplete"	autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP%>" />
		<et:searchField labelName="SP服务代码" inputName="spServiceCode" type="autocomplete"	autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP_SERVICECODE%>"	 autocompleteCascade="spIdName" />
		<et:searchField labelName="过滤标识" inputName="textType" type="select" selectMap="${textType}" />
		<et:searchField labelName="过滤类别" inputName="text1" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_FILTERTYPE%>" />
		<et:searchField labelName="过滤内容" inputName="text2" />
		<et:searchField labelName="是否过滤" inputName="textState" type="select" selectMap="${textState}" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_DATA%>" dataSortName="id.spId" dataSortOrder="asc">
		<et:pageField tableHeadName="操作" value="查看" type="link" dataWidth="20" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_VIEW%>" linkUrlParam="id.spId,id.text,id.spServiceCode,textType,textState" />
		<et:pageField tableHeadName="SP" value="id.spId" dataWidth="100" />
		<et:pageField tableHeadName="SP服务代码" value="id.spServiceCode" />
		<et:pageField tableHeadName="过滤类别" value="" dataFormatter="category" />
		<et:pageField tableHeadName="过滤内容" value="" dataFormatter="content" />
		<et:pageField tableHeadName="过滤标识" value="textType" dataReplace="${textType}" />
		<et:pageField tableHeadName="是否过滤" value="textState" dataReplace="${textState}" />
	</et:page>
</et:index>
<script type="text/javascript">
	function category(v, r, i) {
		if (r.textType == 1)
			return r.id.text;
		return;
	}
	function content(v, r, i) {
		if (r.textType == 2)
			return r.id.text;
		return;
	}
</script>