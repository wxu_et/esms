<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTER_UPDATE%>">
	<et:formField inputName="textFilterPattern" labelName="内容过滤模式"
		readonly="true" value="${tfd.textFilterPattern}" />
	<et:formField labelName="内容过滤类别" inputName="category"
		type="autocomplete" required="true"
		value="${category}"
		autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTER_AUTOCOMPLETE_CATEGORY%>" />
</et:form>
