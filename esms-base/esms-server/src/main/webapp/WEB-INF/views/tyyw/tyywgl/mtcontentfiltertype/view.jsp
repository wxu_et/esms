<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERTYPE_UPDATE %>">
	<et:formField inputName="textFilterCategoryId" labelName="类别编号" readonly="true"
		value="${tfc.textFilterCategoryId}" />
	<et:formField inputName="textFilterCategoryName" labelName="类别名" maxlength="60"
		required="true" value="${tfc.textFilterCategoryName}"/>
</et:form> 