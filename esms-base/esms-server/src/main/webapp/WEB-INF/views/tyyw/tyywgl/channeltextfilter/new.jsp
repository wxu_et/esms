<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<div id="spscf">
	<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_CREATE%>">
		<et:formField labelName="通道" inputName="channelName" required="true"  type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_AUTOCOMPLETE_CHANNEL%>" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_REMOTE_ID%>" remoteData="text:function(){return $(\"input[name='text']\").val();}, category:function(){return $(\"input[name='category']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();},"  remoteMsg="设置重复" />
		<et:formField labelName="文字内容类别" inputName="textType" type="radio" value="1" required="true" displayMap="${textType}" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_REMOTE_ID%>" remoteData="text:function(){return $(\"input[name='text']\").val();}, category:function(){return $(\"input[name='category']\").val();},  channelName:function(){return $(\"input[name='channelName']\").val();}," remoteMsg="设置重复" />
		<et:formField labelName="过滤类别" required="true" inputName="category" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_AUTOCOMPLETE_FILTERTYPE%>" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_REMOTE_ID%>" remoteData="text:function(){return $(\"input[name='text']\").val();},  channelName:function(){return $(\"input[name='channelName']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}," remoteMsg="设置重复" />
		<et:formField labelName="过滤内容" required="true" inputName="text" maxlength="100" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_CHANNELTEXTFILTER_REMOTE_ID%>" remoteData="category:function(){return $(\"input[name='category']\").val();}, channelName:function(){return $(\"input[name='channelName']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}," remoteMsg="设置重复" />
		<et:formField labelName="是否过滤" inputName="textState" type="radio" required="true" displayMap="${textState}" value="1" />
	</et:form>
</div>
<script type="text/javascript">
var ft=$("#spscf").find(".form-group").eq(2)
var fn=$("#spscf").find(".form-group").eq(3)
fn.hide();
$("#spscf").find("input[name='textType']").change(function(){
	if($(this).val()==1){
		ft.show();
		fn.hide();
	}else{
		ft.hide();
		fn.show();
	}	
})
</script>