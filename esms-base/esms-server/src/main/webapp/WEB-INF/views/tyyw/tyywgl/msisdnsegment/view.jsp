<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_UPDATE%>">
	<et:formField labelName="号码段前缀" readonly="true" inputName="msisdnSegmentPrefix" value="${msisdnSegment.msisdnSegmentPrefix}" required="true" />
    <et:formField labelName="国家代码" inputName="countryCode" value="${msisdnSegment.countryCode}" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_COUNTRYCODE %>" />
    <et:formField labelName="省代码" inputName="provinceCode" value="${msisdnSegment.provinceCode}" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_PROVINCECODE %>" autocompleteCascade="countryCode" />
    <et:formField labelName="市代码" inputName="cityCode" value="${msisdnSegment.cityCode}" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CITYCODE %>" autocompleteCascade="provinceCode" />
    <et:formField labelName="运营商代码" inputName="carrierCode" value="${msisdnSegment.carrierCode}" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CARRIERCODE %>" />
    <et:formField labelName="号码状态" inputName="msisdnSegmentState" value="${msisdnSegment.msisdnSegmentState}" type="radio" required="true" displayMap="${segmentState}"/>
    <et:formField labelName="号码最小长度" inputName="msisdnMinLength" value="${msisdnSegment.msisdnMinLength}" required="false" />
    <et:formField labelName="号码最大长度" inputName="msisdnMaxLength" value="${msisdnSegment.msisdnMaxLength}" required="false" />
    <et:formField labelName="网络类型" inputName="networkCode" value="${msisdnSegment.networkCode}" type="autocomplete" required="true" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_NETWORKCODE %>" />
    <et:formField labelName="数据级别" inputName="dataLevel" type="select" required="false" value="${msisdnSegment.dataLevel}" displayMap="${dataLevelMap}" />
    <et:formField labelName="注释" inputName="note" value="${msisdnSegment.note}" required="false" />
</et:form>