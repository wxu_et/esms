<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index
	createBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_NEW%>"
	delBtnUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_DEL%>">
	<et:search>
		<et:searchField labelName="内容过滤模式" inputName="textFilterPattern" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_DATA%>"
		dataSortName="textFilterPattern" dataSortOrder="desc">
		<et:pageField tableHeadName="操作" value="查看" type="link" dataWidth="20"
			linkTarget="_modal"
			linkUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTFILTER_VIEW%>"
			linkUrlParam="textFilterPattern" />
		<et:pageField tableHeadName="内容过滤模式" value="textFilterPattern"
			dataWidth="1000" />
		<et:pageField tableHeadName="内容过滤类别" value="textFilterCategoryFlagName"/>
	</et:page>
</et:index>