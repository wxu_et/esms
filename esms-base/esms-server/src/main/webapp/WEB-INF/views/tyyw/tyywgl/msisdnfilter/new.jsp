<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<div id="mf">
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_CREATE%>" tip="<p>1.针对单个sp</p><p>2.文件类型支持csv或者txt</p><p>3.号码要带86</p><p>4.加黑完成后，要在boss1，2,3上点击任意一个黑名单修改保存，即可生效</p><p>5.boss5，boss8的加黑还是在原来的界面上添加</p>">
	<et:formField labelName="是否全局" inputName="global" type="radio" displayMap="${spCheckMap}" value="0" required="true"/>
    <et:formField labelName="sp" inputName="spId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_AUTOCOMPLETE_SP%>" required="true"/>
    <et:formField labelName="MSISDN号码" inputName="msisdnFilterStr" type="textarea" maxlength="1000" />
    <et:formField labelName="文件导入" fileType=".csv,.txt" fileOnUploadComplete="addFile" inputName="files" type="file" fileUploadUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MSISDNFILTER_UPLOAD%>" />
    <et:formField labelName="数据级别" inputName="dataLevel" type="select" displayMap="${dataLevelMap}" required="true" value="1" />
    <et:formField labelName="注释" inputName="note" maxlength="60"/>
</et:form>
</div>
<script>
var form=$("#mf>form");
var sp=form.find("[name='spId']").parent().parent().parent().hide()
function addFile(f,d){
	d=eval("("+d+")");
	for(var i in d)
		form.prepend('<input type="hidden" name="fileNames" value="'+d[i]+'" />');
	
	form.find(".uploadifive").parent().parent().append(f.name);
}
form.find("[name='global']").change(function(){
	if($(this).val()==0)
		sp.hide();
	else
		sp.show();
})
</script>