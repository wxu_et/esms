<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTER_CREATE%>">
	<et:formField inputName="textFilterPattern" labelName="内容过滤模式"
		required="true" maxlength="300"
		remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTER_TEXTFILTERPATTERNCHECK%>"
		remoteMsg="不可重复添加相同的内容过滤模式" />
	<et:formField labelName="内容过滤类别" inputName="category"
		type="autocomplete" required="true"
		autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTER_AUTOCOMPLETE_CATEGORY%>" />
</et:form>