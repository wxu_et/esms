<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:form
	action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_CREATE%>">
	<et:formField inputName="textFilterCategoryId" labelName="类别编号"
		required="true" maxlength="10" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_TEXTFILTERCATEGORYIDCHECK%>"
		remoteMsg="id值不能重复" digits="true" digitsMsg="只能填整数类型"/>
	<et:formField inputName="textFilterCategoryName" labelName="类别名"	required="true" maxlength="60"/>
</et:form>