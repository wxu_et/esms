<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<div id="spscf">
<et:form action="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_CREATE%>">
	<et:formField inputName="spIdName" labelName="SP" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP%>" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID %>" remoteData="spServiceCode:function(){return $(\"input[name='spServiceCode']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}, category:function(){return $(\"input[name='category']\").val();}, text:function(){return $(\"input[name='text']\").val();}" remoteMsg="设置重复"/>
	<et:formField inputName="spServiceCode" labelName="SP服务代码" required="true" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP_SERVICECODE%>" autocompleteCascade="spIdName" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID %>" remoteData="spIdName:function(){return $(\"input[name='spIdName']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}, category:function(){return $(\"input[name='category']\").val();}, text:function(){return $(\"input[name='text']\").val();}" remoteMsg="设置重复"/>
	<et:formField labelName="文字内容类别" inputName="textType" type="radio" required="true" displayMap="${textType}" value="1" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID %>" remoteData="spServiceCode:function(){return $(\"input[name='spServiceCode']\").val();}, spIdName:function(){return $(\"input[name='spIdName']\").val();}, category:function(){return $(\"input[name='category']\").val();}, text:function(){return $(\"input[name='text']\").val();}" remoteMsg="设置重复" />
	<et:formField labelName="过滤类别" required="true" inputName="category" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_FILTERTYPE%>" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID %>" remoteData="spServiceCode:function(){return $(\"input[name='spServiceCode']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}, spIdName:function(){return $(\"input[name='spIdName']\").val();}, text:function(){return $(\"input[name='text']\").val();}" remoteMsg="设置重复" />
	<et:formField labelName="过滤内容" required="true" inputName="text" maxlength="100" remote="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID %>" remoteData="spServiceCode:function(){return $(\"input[name='spServiceCode']\").val();}, textType:function(){return $(\"input[name='textType']:checked\").val();}, category:function(){return $(\"input[name='category']\").val();}, spIdName:function(){return $(\"input[name='spIdName']\").val();}" remoteMsg="设置重复" />
	<et:formField labelName="是否过滤" inputName="textState" type="radio" required="true" displayMap="${textState}" value="1" />
</et:form>
</div>
<script type="text/javascript">
var ft=$("#spscf").find(".form-group").eq(3)
var fn=$("#spscf").find(".form-group").eq(4)
fn.hide();
$("#spscf").find("input[name='textType']").change(function(){
	if($(this).val()==1){
		ft.show();
		fn.hide();
	}else{
		ft.hide();
		fn.show();
	}	
})
</script>