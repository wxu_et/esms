<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index>
	<et:search column="2">
		<et:searchField labelName="SP" inputName="spId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_SP%>" value="*" />
		<et:searchField labelName="SP服务代码" inputName="serviceCode" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_SP_SERVICECODE%>"	autocompleteCascade="spId" />
		<et:searchField labelName="通道" inputName="channelId" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_CHANNEL%>" value="*" />
		<et:searchField labelName="查询文字" inputName="text"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_TEXTFILTERMATCH_DATA%>" dataPagination="false">
		<et:pageField tableHeadName="过滤结果类型" dataFormatter="filterType"/>
		<et:pageField tableHeadName="匹配内容" value="filterContent" />
	</et:page>
</et:index>
<script type="text/javascript">
	function filterType(v, r, i) {
		if (r.filterType == '0')
			return 'SP过滤结果';
		else if (r.filterType == '1')
			return '通道过滤结果';
	}
	function filterContent(v, r, i) {
		return r.filterContent;
	}
</script>