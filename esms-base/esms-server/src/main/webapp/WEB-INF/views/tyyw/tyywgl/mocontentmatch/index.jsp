<%@page import="esms.etonenet.boss1069.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index>
	<et:search column="2">
		<et:searchField labelName="SP" inputName="spIdName"
			type="autocomplete"
			autocompleteUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTMATCH_AUTOCOMPLETE_SP%>" />
		<et:searchField labelName="查询文字" inputName="text" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.TYYW_TYYWGL_MOCONTENTMATCH_DATA%>" dataPagination="false">
		<et:pageField tableHeadName="SP" value="spId" dataWidth="100" />
		<et:pageField tableHeadName="SP服务代码" value="spServiceCode" />
		<et:pageField tableHeadName="SP内容过滤类别" value="spContentFilterType" />
		<et:pageField tableHeadName="匹配内容过滤模式" value="patternMatchContent" />
		<et:pageField tableHeadName="匹配内容过滤类别" value="categoryMatchContent" />
	</et:page>
</et:index>