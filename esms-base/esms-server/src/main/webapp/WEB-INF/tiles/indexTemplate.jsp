<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/datatable/bootstrap-table.min.css?v=${cssVersion}">
<script src="${pageContext.request.contextPath}/static/js/datatable/bootstrap-table.js?v=${jsVersion }"></script>
<script src="${pageContext.request.contextPath}/static/js/validate/jquery.validate.min.js?v=${jsVersion}"></script>
<script src="${pageContext.request.contextPath}/static/js/validate/jquery.limitTextarea.js?v=${jsVersion}"></script>
<tiles:insertAttribute name="body" />