<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>  
<head>  
<title><tiles:getAsString name="title"/></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<c:if test="${tiles==null }">
<link rel="stylesheet" href="/static/css/main.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/css/bootstrap/bootstrap.min.css?v=${cssVersion }">
<link rel="stylesheet" href="/static/css/theme.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/css/style.min.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/css/jquery/jquery-ui-1.10.4.custom.min.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/css/datatable/bootstrap-table.css?v=${cssVersion}">

<link rel="stylesheet" href="/static/css/fonts/fontawesome.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/assert/startboot/sb-admin-2.css?v=${cssVersion}">
<link rel="stylesheet" href="/static/assert/uploadify/uploadifive.css">

<script src="/static/js/bootstrap/jquery.min.js?v=${jsVersion }"></script>
<script src="/static/js/jquery/jquery-migrate.min.js?v=${jsVersion }"></script>
<!--[if lt IE 9]>
    <script src="/static/js/bootstrap/html5shiv.min.js?v=${jsVersion }"></script>
    <script src="/static/js/bootstrap/respond.min.js?v=${jsVersion }"></script>
<![endif]-->
<script src="/static/js/jquery/jquery-ui.custom.js?v=${jsVersion }"></script>
<script src="/static/js/bootstrap/bootstrap.min.js?v=${jsVersion }"></script>
<script src="/static/js/datatable/bootstrap-table.js?v=${jsVersion }"></script>
<script src="/static/js/common/tip.js?v=${jsVersion}"></script>

<script type="text/javascript" src="/static/assert/uploadify/jquery.uploadifive.min.js"></script>
<script src="/static/assert/layer/layer.js?v=${jsVersion }"></script>
<script src="/static/assert/metisMenu/metisMenu.min.js?v=${jsVersion}"></script>
<script src="/static/assert/startboot/sb-admin-2.js?v=${jsVersion}"></script>
<script src="/static/js/jquery/jquery-ui-timepicker-addon.js?v=${jsVersion }"></script>
</c:if>
<c:if test="${tiles=='template:home' }">
<link href="/static/assert/bui/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="/static/assert/bui/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="/static/assert/bui/css/main-min.css" rel="stylesheet" type="text/css" />
<!-- <link href="/static/assert/layer/skin/layer.css" rel="stylesheet" type="text/css" /> -->

<script type="text/javascript" src="/static/assert/bui/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="/static/assert/bui/js/bui-min.js"></script>
<!-- <script type="text/javascript" src="/static/assert/layer/layer.js"></script> -->
</c:if>

<c:if test="${tiles=='template:login' }">
<link rel="stylesheet" href="/static/css/login/login.css" />
<script src="/static/js/bootstrap/jquery.min.js?v=${jsVersion }"></script>
<script src="/static/js/common/util.js?v=${jsVersion}"></script>
</c:if>
</head>  
<body screen_capture_injected="true">
<tiles:insertAttribute name="body" />
</body>  
</html>  