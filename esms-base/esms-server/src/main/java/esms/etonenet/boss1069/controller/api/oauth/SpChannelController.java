package esms.etonenet.boss1069.controller.api.oauth;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.SpChannel;
import esms.etonenet.boss1069.entity.SpChannelPK;
import esms.etonenet.boss1069.repository.ChannelBaobeiInfoRepository;
import esms.etonenet.boss1069.service.biz.impl.SpChannelService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "SpChannel" })
@ApiIgnore
@Controller("SpChannelController")
@RequestMapping(ApiConstants.BASE_PATH + "spchannel")
public class SpChannelController {

	@Resource
	private SpChannelService spChannelService;

	@Resource
	private ChannelBaobeiInfoRepository channelBBEm;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage spChannelData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("id.spId", StringUtil.splitMinusFirstGroup(param.getSpIdName())));
		conditions.add(
				SpecificationCondition.eq("id.channelId", StringUtil.splitMinusFirstGroup(param.getChannelIdName())));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("id.spServiceCode", param.getSpServiceCode()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spChannelNumber", param.getSpChannelNumber()));
		SpecificationHelper<SpChannel> sh = new SpecificationHelper<>(conditions, param.getSortOrder(),
				param.getSortName());
		Page<SpChannel> p = spChannelService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		for (SpChannel spChannel : p.getContent()) {
			int result = channelBBEm.countByChannel(spChannel.getChannel());
			if (result > 0) {
				spChannel.getChannel().setChannelBaobeiFlag(BigDecimal.valueOf(100));
			}
		}
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String spChannelCreate(@ModelAttribute @Valid SpChannel create, String spIdName, String channelIdName,
			String spServiceCode) {
		SpChannelPK id = new SpChannelPK();
		id.setSpServiceCode(spServiceCode);
		id.setChannelId(channelIdName.split("-")[0]);
		id.setSpId(spIdName.split("-")[0]);
		create.setId(id);
		spChannelService.create(create);
		return "新建成功";
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String spChannelUpdate(@ModelAttribute @Valid SpChannel update) {
		spChannelService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_DEL)
	@ResponseBody
	public String spChannelDel(@RequestBody @Valid List<SpChannel> data) {
		spChannelService.del(data);
		return "删除成功";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String spIdName;
		private String channelIdName;
		private String spChannelNumber;
		private String spServiceCode;

		public String getSpIdName() {
			return spIdName;
		}

		public void setSpIdName(String spIdName) {
			this.spIdName = spIdName;
		}

		public String getChannelIdName() {
			return channelIdName;
		}

		public void setChannelIdName(String channelIdName) {
			this.channelIdName = channelIdName;
		}

		public String getSpChannelNumber() {
			return spChannelNumber;
		}

		public void setSpChannelNumber(String spChannelNumber) {
			this.spChannelNumber = spChannelNumber;
		}

		public String getSpServiceCode() {
			return spServiceCode;
		}

		public void setSpServiceCode(String spServiceCode) {
			this.spServiceCode = spServiceCode;
		}
	}
}
