package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.MtAddress;
import esms.etonenet.boss1069.repository.MtAddressRepository;

@Service
public class MtAddressService {

	@Resource
	private MtAddressRepository addressRepository;

	public Page<MtAddress> page(PageRequest pageRequest, Specification<MtAddress> spec) {

		return addressRepository.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(MtAddress create) {

		addressRepository.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(MtAddress update) {

		addressRepository.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<MtAddress> dels) {

		for (MtAddress cr : dels)
			addressRepository.delete(cr);
	}

	public MtAddress getName(String Name) {
		return addressRepository.findByName(Name);
	}

}
