package esms.etonenet.boss1069.web.decorate;

public final class MtFilterMatch {

	private String filterType;

	private String filterContent;

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	public String getFilterContent() {
		return filterContent;
	}

	public void setFilterContent(String filterContent) {
		this.filterContent = filterContent;
	}

}
