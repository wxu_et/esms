package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.repository.ChannelBaobeiInfoRepository;
import esms.etonenet.boss1069.repository.ChannelRepository;

@Service
public class ChannelService {

	@Resource
	private ChannelRepository channelEm;

	@Resource
	ChannelBaobeiInfoRepository cbbEm;

	public Page<Channel> page(PageRequest pageRequest, Specification<Channel> spec) {

		return channelEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(Channel create) {
		channelEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(Channel update) {

		channelEm.save(update);
	}

	public Channel findOne(String id) {

		return channelEm.findOne(id);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<Channel> dels) {
		for (Channel channel : dels)
			channelEm.delete(channel);
	}

	public String findOneMatch(String id) {

		Channel channel = channelEm.findOne(id);
		if (channel != null)
			return channel.getChannelId() + "-" + channel.getChannelName();
		return null;

	}

	public List<Object[]> autoChannel(String term) {

		return channelEm.autoChannel(term);
	}
}
