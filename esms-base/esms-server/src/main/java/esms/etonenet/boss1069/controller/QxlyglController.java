package esms.etonenet.boss1069.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.entity.ChannelGroup;
import esms.etonenet.boss1069.entity.ChannelGroupConfig;
import esms.etonenet.boss1069.entity.ChannelRoute;
import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.entity.SpChannel;
import esms.etonenet.boss1069.entity.SpChannelPK;
import esms.etonenet.boss1069.entity.SpMtRoute;
import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.enums.spmoroute.RouteState;
import esms.etonenet.boss1069.enums.spmtroute.ChannelGroupRouteState;
import esms.etonenet.boss1069.enums.spmtroute.RouteType;
import esms.etonenet.boss1069.repository.ChannelBaobeiInfoRepository;
import esms.etonenet.boss1069.repository.ChannelGroupConfigRepository;
import esms.etonenet.boss1069.repository.ChannelGroupRepository;
import esms.etonenet.boss1069.repository.ChannelRepository;
import esms.etonenet.boss1069.repository.ChannelRouteRepository;
import esms.etonenet.boss1069.repository.SpChannelRepository;
import esms.etonenet.boss1069.repository.SpMoRouteRepository;
import esms.etonenet.boss1069.repository.SpMtRouteRepository;
import esms.etonenet.boss1069.repository.SpRepository;
import esms.etonenet.boss1069.repository.SpServiceRepository;
import esms.etonenet.boss1069.service.biz.impl.ChannelGroupConfigService;
import esms.etonenet.boss1069.service.biz.impl.ChannelGroupService;
import esms.etonenet.boss1069.service.biz.impl.ChannelRouteService;
import esms.etonenet.boss1069.service.biz.impl.SpChannelService;
import esms.etonenet.boss1069.service.biz.impl.SpMoRouteService;
import esms.etonenet.boss1069.service.biz.impl.SpMtRouteService;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import esms.etonenet.bossoa.entity.CarrierCode;
import esms.etonenet.bossoa.entity.City;
import esms.etonenet.bossoa.entity.Country;
import esms.etonenet.bossoa.entity.Province;
import esms.etonenet.bossoa.repository.CarrierCodeRepository;
import esms.etonenet.bossoa.repository.CityRepository;
import esms.etonenet.bossoa.repository.CountryRepository;
import esms.etonenet.bossoa.repository.ProvinceRepository;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class QxlyglController {

	@Resource
	private SpRepository spEm;

	@Resource
	private ChannelRepository channelEm;

	@Resource
	private SpChannelRepository spChannelEm;

	@Resource
	private SpServiceRepository spServiceEm;

	@Resource
	private SpRepository spRepo;

	@Resource
	private SpMoRouteRepository spMoRouteEm;

	@Resource
	private SpMtRouteRepository spMtRouteEm;

	@Resource
	private ChannelGroupRepository channelGroupEm;

	@Resource
	private ChannelGroupConfigRepository channelGroupConfigEm;

	@Resource
	private SpMoRouteService spMoRouteService;

	@Resource
	private SpMtRouteService spMtRouteService;

	@Resource
	private ChannelGroupService channelGroupService;

	@Resource
	private ChannelGroupConfigService channelGroupConfigService;

	@Resource
	private SpChannelService spChannelService;

	@Resource
	private ChannelRouteService channelRouteService;

	@Resource
	private CountryRepository countryEm;

	@Resource
	private ProvinceRepository provinceEm;

	@Resource
	private CityRepository cityEm;

	@Resource
	private CarrierCodeRepository carrierCodeEm;

	@Resource
	private ChannelRouteRepository channelRouteEm;

	@Resource
	private ChannelBaobeiInfoRepository channelBBEm;

	// spchannel
	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_INDEX)
	public ModelAndView spChannelIndex(String spIdName) {
		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.QYDX_QXLYGL_SPCHANNEL_INDEX));
		mv.addObject("spIdName", spIdName);
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_DATA)
	@ResponseBody
	public AjaxPage spChannelData(Integer pageNumber, Integer pageSize, final String spIdName,
			final String channelIdName, final String spChannelNumber, final String spServiceCode, final String sortName,
			final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("id.spId", StringUtil.splitMinusFirstGroup(spIdName)));
		conditions.add(SpecificationCondition.eq("id.channelId", StringUtil.splitMinusFirstGroup(channelIdName)));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("id.spServiceCode", spServiceCode));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spChannelNumber", spChannelNumber));

		List<String> orderNames = new ArrayList<String>();
		if ("id.spId".equals(sortName) || "id.spServiceCode".equals(sortName) || "id.carrierCode".equals(sortName)) {
			orderNames.add("id.spId");
			orderNames.add("id.spServiceCode");
			orderNames.add("id.carrierCode");
		} else
			orderNames.add(sortName);

		SpecificationHelper<SpChannel> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<SpChannel> p = spChannelService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());
		for (SpChannel spChannel : p.getContent()) {
			int result = channelBBEm.countByChannel(spChannel.getChannel());
			if (result > 0) {
				spChannel.getChannel().setChannelBaobeiFlag(BigDecimal.valueOf(100));
				;
			}
		}

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_NEW)
	public ModelAndView spChannelNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_SPCHANNEL_NEW);
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_CREATE)
	@ResponseBody
	public String spChannelCreate(SpChannel create, String spIdName, String channelIdName, String spServiceCode) {

		SpChannelPK id = new SpChannelPK();
		id.setSpServiceCode(spServiceCode);
		id.setChannelId(channelIdName.split("-")[0]);
		id.setSpId(spIdName.split("-")[0]);
		create.setId(id);
		spChannelService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_VIEW)
	public ModelAndView spChannelView(SpChannel spch) {

		SpChannelPK id = spch.getId();
		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_SPCHANNEL_VIEW);
		mv.addObject("spch", spChannelEm.findOne(id));
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_UPDATE)
	@ResponseBody
	public String spChannelUpdate(SpChannel update) {

		spChannelService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_DEL)
	@ResponseBody
	public String spChannelDel(@RequestBody List<SpChannel> data) {

		spChannelService.del(data);
		return "删除成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP)
	@ResponseBody
	public List<Autocomplete> autoSpchannelSp(String term) {

		return AutocompleteUtil.object2auto(spRepo.autoSP(term));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP_SERVICECODE)
	@ResponseBody
	public List<Autocomplete> autoSpchannelSpServiceCode(String term, String spIdName) {

		return AutocompleteUtil.object2auto(spServiceEm.autoSpServiceCode(term, spIdName.split("-")[0]));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<Autocomplete> autoSpchannelChannel(String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannel(term));
	}

	// spmtroute
	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_INDEX)
	public ModelAndView spMtRouteIndex(String spIdName) {
		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.QYDX_QXLYGL_SPMTROUTE_INDEX));
		mv.addObject("routeStateList", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		mv.addObject("channelGroupRouteStateList", ChannelGroupRouteState.toDisplayMap());
		mv.addObject("routeTypeList", RouteType.toDisplayMap());
		mv.addObject("spIdName", spIdName);
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_DATA)
	@ResponseBody
	public AjaxPage spMtRouteData(Integer pageNumber, Integer pageSize, final String routeState,
			final String carrierCode, final String spIdName, final String channelIdNameSearch,
			final String spServiceCode, final String sortName, final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("sp.spId", StringUtil.splitMinusFirstGroup(spIdName)));
		conditions.add(
				SpecificationCondition.eq("channel.channelId", StringUtil.splitMinusFirstGroup(channelIdNameSearch)));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spServiceCode", spServiceCode));
		conditions.add(SpecificationCondition.eq("carrierCode", StringUtil.splitMinusFirstGroup(carrierCode)));
		conditions.add(SpecificationCondition.eq("routeState", routeState));

		SpecificationHelper<SpMtRoute> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<SpMtRoute> p = spMtRouteService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());

		for (SpMtRoute spmt : p)
			displaySpMtBaseInfo(spmt);

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	/**
	 * 修正基础信息显示国家、省、城市、营运商
	 * 
	 * @param spmt
	 */
	private void displaySpMtBaseInfo(SpMtRoute spmt) {

		if (StringUtil.isNotEmpty(spmt.getCarrierCode())) {
			CarrierCode cc = carrierCodeEm.findOne(spmt.getCarrierCode());
			if (cc != null)
				spmt.setCarrierCode(cc.getCarrierCode() + "-" + cc.getCarrierName());
		}
		if (StringUtil.isNotEmpty(spmt.getCountryCode())) {
			Country country = countryEm.findOne(spmt.getCountryCode());
			if (country != null)
				spmt.setCountryCode(country.getCountryCode() + "-" + country.getCountryName());
		}
		if (StringUtil.isNotEmpty(spmt.getProvinceCode())) {
			Province province = provinceEm.findOne(spmt.getProvinceCode());
			if (province != null)
				spmt.setProvinceCode(province.getProvinceCode() + "-" + province.getProvinceName());
		}
		if (StringUtil.isNotEmpty(spmt.getCityCode())) {
			City city = cityEm.findOne(spmt.getCityCode());
			if (city != null)
				spmt.setCityCode(city.getCityCode() + "-" + city.getCityName());
		}
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_NEW)
	public ModelAndView spMtRouteNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_SPMTROUTE_NEW);
		mv.addObject("routeState", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		mv.addObject("routeType", RouteType.toDisplayMap());
		mv.addObject("channelGroupRouteState", ChannelGroupRouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_CREATE)
	@ResponseBody
	public String spMtRouteCreate(SpMtRoute create, String spIdName, String channelIdName, String carrierCodeIdName,
			String countryIdName, String provinceIdName, String cityIdName, String channelGroupIdName) {

		if (RouteType.NORMAL.getValue().equals(create.getRouteType())) {

			Channel ch = new Channel();
			ch.setChannelId(channelIdName.split("-")[0]);
			create.setChannel(ch);

			create.setCountryCode(countryIdName.split("-")[0]);
			create.setCityCode(cityIdName.split("-")[0]);
			create.setProvinceCode(provinceIdName.split("-")[0]);
			create.setCarrierCode(carrierCodeIdName.split("-")[0]);

			create.setChannelGroup(null);
			create.setChannelGroupRouteState(null);
		} else {

			create.setRouteState(null);

			create.setChannel(null);

			create.setCountryCode(null);
			create.setCityCode(null);
			create.setProvinceCode(null);
			create.setCarrierCode(null);

			ChannelGroup cc = new ChannelGroup();
			cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
			create.setChannelGroup(cc);
		}

		Sp sp = new Sp();
		sp.setSpId(spIdName.split("-")[0]);
		create.setSp(sp);

		spMtRouteService.create(create);

		return "新建成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE)
	@ResponseBody
	public List<?> autoCarriercode(String term) {

		return AutocompleteUtil.object2auto(carrierCodeEm.auto(term));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY)
	@ResponseBody
	public List<Autocomplete> autoCountry(String term) {

		Autocomplete d = new Autocomplete();
		d.setLabel("*");
		d.setValue("*");
		List<Autocomplete> country = new ArrayList<>();
		country.add(d);
		country.addAll(AutocompleteUtil.object2auto(countryEm.auto(term)));

		return country;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE)
	@ResponseBody
	public List<Autocomplete> autoProvince(String countryIdName, String term) {

		Autocomplete d = new Autocomplete();
		d.setLabel("*");
		d.setValue("*");
		List<Autocomplete> province = new ArrayList<>();
		province.add(d);
		province.addAll(AutocompleteUtil.object2auto(provinceEm.auto(countryIdName.split("-")[0], term)));

		return province;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CITY)
	@ResponseBody
	public List<Autocomplete> autoCity(String provinceIdName, String term) {

		Autocomplete d = new Autocomplete();
		d.setLabel("*");
		d.setValue("*");
		List<Autocomplete> city = new ArrayList<>();
		city.add(d);
		city.addAll(AutocompleteUtil.object2auto(cityEm.auto(provinceIdName.split("-")[0], term)));

		return city;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_VIEW)
	public ModelAndView spMtRouteView(Long spMtRouteId) {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_SPMTROUTE_VIEW);
		SpMtRoute spmt = spMtRouteEm.findOne(spMtRouteId);
		displaySpMtBaseInfo(spmt);
		mv.addObject("spmt", spmt);
		mv.addObject("routeState", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		mv.addObject("routeType", RouteType.toDisplayMap());
		mv.addObject("channelGroupRouteState", ChannelGroupRouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_UPDATE)
	@ResponseBody
	public String spMtRouteUpdate(SpMtRoute update, String spIdName, String channelIdName, String carrierCodeIdName,
			String countryIdName, String provinceIdName, String cityIdName, String channelGroupIdName) {

		if (RouteType.NORMAL.getValue().equals(update.getRouteType())) {

			Channel ch = new Channel();
			ch.setChannelId(channelIdName.split("-")[0]);
			update.setChannel(ch);

			update.setCountryCode(countryIdName.split("-")[0]);
			update.setCityCode(cityIdName.split("-")[0]);
			update.setProvinceCode(provinceIdName.split("-")[0]);
			update.setCarrierCode(carrierCodeIdName.split("-")[0]);

			update.setChannelGroup(null);
			update.setChannelGroupRouteState(null);
		} else {

			update.setRouteState(null);

			update.setChannel(null);

			update.setCountryCode(null);
			update.setCityCode(null);
			update.setProvinceCode(null);
			update.setCarrierCode(null);

			ChannelGroup cc = new ChannelGroup();
			cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
			update.setChannelGroup(cc);
		}
		Sp sp = new Sp();
		sp.setSpId(spIdName.split("-")[0]);
		update.setSp(sp);

		spMtRouteService.update(update);

		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_DEL)
	@ResponseBody
	public String spMtRouteDel(@RequestBody List<SpMtRoute> data) {

		spMtRouteService.del(data);

		return "删除成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP_SERVICECODE)
	@ResponseBody
	public List<Autocomplete> autoSpmtrouteSpServiceCode(String spIdName, String term) {

		Autocomplete ac = new Autocomplete();
		ac.setLabel("*");
		ac.setValue("*");
		List<Autocomplete> sps = new ArrayList<>();
		sps.add(ac);
		if (spIdName.split("-").length != 0)
			sps.addAll(AutocompleteUtil.object2auto(spServiceEm.autoSpServiceCode(term, spIdName.split("-")[0])));

		return sps;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<?> autoSpmtrouteChannel(String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannel(term));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP)
	@ResponseBody
	public List<?> autoSpmtrouteSp(String term) {

		return AutocompleteUtil.object2auto(spEm.autoSP(term));
	}

	// channelgroup
	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_INDEX)
	public ModelAndView channelGroupIndex(final ChannelGroup search) {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_INDEX));
		mv.addObject("search", search);
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_DATA)
	@ResponseBody
	public AjaxPage channelGroupData(Integer pageNumber, Integer pageSize, final ChannelGroup search,
			final String sortName, final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("logicStat", LogicStat.NORMAL.getValue()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelGroupId", search.getChannelGroupId()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelGroupName", search.getChannelGroupName()));

		SpecificationHelper<ChannelGroup> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<?> p = channelGroupService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_NEW)
	public ModelAndView channelGroupNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_NEW);

		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_CREATE)
	@ResponseBody
	public String channelGroupCreate(ChannelGroup create) {

		channelGroupService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_VIEW)
	public ModelAndView channelGroupView(String channelGroupId) {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_VIEW);
		mv.addObject("cg", channelGroupEm.findOne(channelGroupId));
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_UPDATE)
	@ResponseBody
	public String channelGroupUpdate(ChannelGroup update) {

		channelGroupService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_DEL)
	@ResponseBody
	public String channelGroupDel(@RequestBody List<ChannelGroup> data) {

		channelGroupService.del(data);
		return "删除成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUP_CHANNELGROUPID_CHECK)
	@ResponseBody
	public boolean channelGroupIdCheck(String channelGroupId) {

		if (channelGroupEm.findOne(channelGroupId) == null)
			return true;
		else
			return false;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_INDEX)
	public ModelAndView channelGroupConfigIndex(final String channelGroupIdName, final String channelIdName) {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_INDEX));
		mv.addObject("routeStateList", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_DATA)
	@ResponseBody
	public AjaxPage channelGroupConfigData(Integer pageNumber, Integer pageSize, final String channelGroupIdName,
			final String channelIdName, final String sortName, final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("logicStat", LogicStat.NORMAL.getValue()));
		conditions.add(SpecificationCondition.eq("channelGroup.channelGroupId",
				StringUtil.splitMinusFirstGroup(channelGroupIdName)));
		conditions.add(SpecificationCondition.eq("channel.channelId", StringUtil.splitMinusFirstGroup(channelIdName)));

		SpecificationHelper<ChannelGroupConfig> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<?> p = channelGroupConfigService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_NEW)
	public ModelAndView channelGroupConfigNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_NEW);
		mv.addObject("routeState", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_CREATE)
	@ResponseBody
	public String channelGroupConfigCreate(ChannelGroupConfig create, String channelGroupIdName, String channelIdName,
			String carrierCodeIdName, String countryIdName, String provinceIdName, String cityIdName) {

		ChannelGroup cc = new ChannelGroup();
		cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
		Channel c = new Channel();
		c.setChannelId(channelIdName.split("-")[0]);

		create.setChannelGroup(cc);
		create.setChannel(c);

		create.setCountryCode(countryIdName.split("-")[0]);
		create.setCityCode(cityIdName.split("-")[0]);
		create.setProvinceCode(provinceIdName.split("-")[0]);
		create.setCarrierCode(carrierCodeIdName.split("-")[0]);
		channelGroupConfigService.create(create);

		return "新建成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_VIEW)
	public ModelAndView channelGroupConfigView(Long channelGroupConfigId) {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_VIEW);
		mv.addObject("cgc", channelGroupConfigEm.findOne(channelGroupConfigId));
		mv.addObject("routeState", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_UPDATE)
	@ResponseBody
	public String channelGroupConfigUpdate(ChannelGroupConfig update, String channelGroupIdName, String channelIdName,
			String carrierCodeIdName, String countryIdName, String provinceIdName, String cityIdName) {

		ChannelGroup cc = new ChannelGroup();
		cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
		Channel c = new Channel();
		c.setChannelId(channelIdName.split("-")[0]);

		update.setChannelGroup(cc);
		update.setChannel(c);

		update.setCountryCode(countryIdName.split("-")[0]);
		update.setCityCode(cityIdName.split("-")[0]);
		update.setProvinceCode(provinceIdName.split("-")[0]);
		update.setCarrierCode(carrierCodeIdName.split("-")[0]);
		channelGroupConfigService.update(update);

		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_DEL)
	@ResponseBody
	public String channelGroupConfigDel(@RequestBody List<ChannelGroupConfig> data) {

		channelGroupConfigService.del(data);
		return "删除成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP)
	@ResponseBody
	public List<Autocomplete> autoCgcChannelGroup(String term) {

		return AutocompleteUtil.object2auto(channelGroupEm.auto(term));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELGROUPCONFIG_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<Autocomplete> autoCgcChannel(String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannel(term));
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_INDEX)
	public ModelAndView channelRouteIndex(final String channelIdNameSearch, final String carriercodeSearch,
			final String routeStateSearch) {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_INDEX));
		mv.addObject("routeStateList", esms.etonenet.boss1069.enums.RouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_DATA)
	@ResponseBody
	public AjaxPage channelRouteData(Integer pageNumber, Integer pageSize, final String channelIdNameSearch,
			final String carriercodeSearch, final String routeStateSearch, final String sortName,
			final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(
				SpecificationCondition.eq("channel.channelId", StringUtil.splitMinusFirstGroup(channelIdNameSearch)));
		conditions.add(SpecificationCondition.eq("carrierCode", StringUtil.splitMinusFirstGroup(carriercodeSearch)));
		conditions.add(SpecificationCondition.eq("routeState", routeStateSearch));

		SpecificationHelper<ChannelRoute> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<ChannelRoute> p = channelRouteService.page(new PageRequest(pageNumber, pageSize),
				sh.createSpecification());

		for (ChannelRoute cr : p.getContent())
			displayChannelRouteBaseInfo(cr);

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_NEW)
	public ModelAndView channelRouteNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_NEW);
		mv.addObject("routeState", RouteState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_CREATE)
	@ResponseBody
	public String channelRouteCreate(ChannelRoute create, String channelIdName, String carrierCodeIdName,
			String countryIdName, String provinceIdName, String cityIdName) {
		Channel ch = new Channel();
		ch.setChannelId(channelIdName.split("-")[0]);

		create.setChannel(ch);
		create.setCountryCode(countryIdName.split("-")[0]);
		create.setCityCode(cityIdName.split("-")[0]);
		create.setProvinceCode(provinceIdName.split("-")[0]);
		create.setCarrierCode(carrierCodeIdName.split("-")[0]);

		channelRouteService.create(create);

		return "新建成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_VIEW)
	public ModelAndView channelRouteView(Long channelRouteId) {

		ModelAndView mv = new ModelAndView(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_VIEW);
		mv.addObject("routeState", RouteState.toDisplayMap());
		ChannelRoute cr = channelRouteEm.findOne(channelRouteId);
		displayChannelRouteBaseInfo(cr);
		mv.addObject("cr", cr);
		return mv;
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_UPDATE)
	@ResponseBody
	public String channelRouteUpdate(ChannelRoute update, String channelIdName, String carrierCodeIdName,
			String countryIdName, String provinceIdName, String cityIdName) {

		Channel ch = new Channel();
		ch.setChannelId(channelIdName.split("-")[0]);

		update.setChannel(ch);
		update.setCountryCode(countryIdName.split("-")[0]);
		update.setCityCode(cityIdName.split("-")[0]);
		update.setProvinceCode(provinceIdName.split("-")[0]);
		update.setCarrierCode(carrierCodeIdName.split("-")[0]);

		channelRouteService.update(update);

		return "更新成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_DEL)
	@ResponseBody
	public String channelRouteDel(@RequestBody List<ChannelRoute> data) {

		channelRouteService.del(data);
		return "删除成功";
	}

	@RequestMapping(UrlConstants.QYDX_QXLYGL_CHANNELROUTE_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<?> autoChannelrouteChannel(String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannel(term));
	}

	/**
	 * 验证channelId对应的通道是否被报备
	 * 
	 * @param request
	 * @return
	 */
	// @RequestMapping(UrlConstants.QYDX_QXLYGL_SPCHANNEL_ISBBCHANNEL)
	// @ResponseBody
	// public String isBaoBeiChannel(HttpServletRequest request){
	// String channelId = request.getParameter("channelId");
	// String isbbChannel = null;
	// List<ChannelBaobeiInfo> list = channelBBEm.findByChannelId(channelId);
	// if(list.size()>0){
	// isbbChannel = "是";
	// }else {
	// isbbChannel = "否";
	// }
	//
	// return isbbChannel;
	// }

	/**
	 * 修正基础信息显示国家、省、城市、营运商
	 * 
	 * @param spmt
	 */
	private void displayChannelRouteBaseInfo(ChannelRoute cr) {

		if (StringUtil.isNotEmpty(cr.getCarrierCode())) {
			CarrierCode cc = carrierCodeEm.findOne(cr.getCarrierCode());
			if (cc != null)
				cr.setCarrierCode(cc.getCarrierCode() + "-" + cc.getCarrierName());
		}
		if (StringUtil.isNotEmpty(cr.getCountryCode())) {
			Country country = countryEm.findOne(cr.getCountryCode());
			if (country != null)
				cr.setCountryCode(country.getCountryCode() + "-" + country.getCountryName());
		}
		if (StringUtil.isNotEmpty(cr.getProvinceCode())) {
			Province province = provinceEm.findOne(cr.getProvinceCode());
			if (province != null)
				cr.setProvinceCode(province.getProvinceCode() + "-" + province.getProvinceName());
		}
		if (StringUtil.isNotEmpty(cr.getCityCode())) {
			City city = cityEm.findOne(cr.getCityCode());
			if (city != null)
				cr.setCityCode(city.getCityCode() + "-" + city.getCityName());
		}
	}

}
