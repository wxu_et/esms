package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.entity.system.ErrorCode;
import com.etonenet.service.system.ErrorCodeService;
import com.etonenet.service.system.ErrorDefineService;
import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "供应商错误代码定义" }, value = "供应商错误代码接口", description = "ErrorCodeController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "errorcode")
public class ErrorCodeController {

	@Resource
	private ErrorCodeService errorCodeService;

	@Resource
	private ErrorDefineService errorDefineService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage page(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("errorCode", param.getErrorCode()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("originExplain", param.getOriginExplain()));
		conditions.add(SpecificationCondition.eq("errorDefine.host", param.getHost()));
		SpecificationHelper<ErrorCode> sh = new SpecificationHelper<ErrorCode>(conditions, param);
		Page<ErrorCode> p = errorCodeService.page(new PageRequest(param), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());

	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.POST)
	@ResponseBody
	public ErrorCode get(@RequestParam Long id) {
		return errorCodeService.findOne(id);
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void create(@ModelAttribute @Valid ErrorCode create) {
		errorCodeService.create(create);
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@RequestBody @Valid List<ErrorCode> data) {
		errorCodeService.del(data);
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public void update(@ModelAttribute @Valid ErrorCode update) {

		errorCodeService.update(update);
	}

	private static final class PageParam extends BasePageRequestParam {

		private String errorCode;

		private String originExplain;

		private String host;

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getOriginExplain() {
			return originExplain;
		}

		public void setOriginExplain(String originExplain) {
			this.originExplain = originExplain;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

	}

}
