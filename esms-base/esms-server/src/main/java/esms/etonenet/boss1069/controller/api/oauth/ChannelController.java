package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.util.MathUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.enums.LongSmsFlag;
import esms.etonenet.boss1069.enums.WhiteListPolicy;
import esms.etonenet.boss1069.enums.channel.ChannelBaobeiFlag;
import esms.etonenet.boss1069.enums.channel.ChannelMtmoFlag;
import esms.etonenet.boss1069.enums.channel.ChannelState;
import esms.etonenet.boss1069.service.biz.impl.ChannelService;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "通道定义" }, value = "通道接口", description = "ChannelController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "channel")
public class ChannelController {

	@Resource
	private ChannelService channelService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Page<Channel> channelData(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelId", param.getChannelId()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelNumber", param.getChannelNumber()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelMessageSignature",
				param.getChannelMessageSignature()));
		conditions.add(SpecificationCondition.eq("channelState", param.getChannelState()));
		conditions.add(SpecificationCondition.eq("whiteListPolicy", param.getWhiteListPolicy()));
		conditions.add(SpecificationCondition.eq("channelBaobeiFlag", param.getChannelBaobeiFlag()));

		SpecificationHelper<Channel> sh = new SpecificationHelper<Channel>(conditions, param);

		Page<Channel> p = channelService.page(new PageRequest(param), sh.createSpecification());

		return p;
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void create(@ModelAttribute @Valid Channel create, @RequestParam Integer[] channelMtmoFlagCheckbox)
			throws Exception {

		create.setChannelMtmoFlag(MathUtil.sum(channelMtmoFlagCheckbox));
		if (channelService.findOne(create.getChannelId()) == null)
			channelService.create(create);
		else
			throw new Exception("该ID已存在!");
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Map<String, Object>> get(@RequestParam(required = true) String id) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ch", channelService.findOne(id));
		map.put("channelState", ChannelState.toDisplayMap());
		map.put("longSmsFlag", LongSmsFlag.toDisplayMap());
		map.put("whiteListPolicy", WhiteListPolicy.toDisplayMap());
		map.put("channelMtmoFlag", ChannelMtmoFlag.toDisplayMap());
		map.put("channelBaobeiFlag", ChannelBaobeiFlag.toDisplayMap());
		list.add(map);
		return list;
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public void update(@ModelAttribute @Valid Channel update, @RequestParam Integer[] channelMtmoFlagCheckbox) {

		update.setChannelMtmoFlag(MathUtil.sum(channelMtmoFlagCheckbox));
		channelService.update(update);
	}

	@ApiOperation(value = "转义")
	@RequestMapping(value = "auto", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Autocomplete> autoCgcChannel(@RequestParam(required = true) String term) {

		return AutocompleteUtil.object2auto(channelService.autoChannel(term));
	}

	private static final class PageParam extends BasePageRequestParam {

		private String channelId;

		private String channelNumber;

		private String channelMessageSignature;

		private String channelState;

		private String whiteListPolicy;

		private String channelBaobeiFlag;

		public String getChannelId() {
			return channelId;
		}

		public void setChannelId(String channelId) {
			this.channelId = channelId;
		}

		public String getChannelNumber() {
			return channelNumber;
		}

		public void setChannelNumber(String channelNumber) {
			this.channelNumber = channelNumber;
		}

		public String getChannelMessageSignature() {
			return channelMessageSignature;
		}

		public void setChannelMessageSignature(String channelMessageSignature) {
			this.channelMessageSignature = channelMessageSignature;
		}

		public String getChannelState() {
			return channelState;
		}

		public void setChannelState(String channelState) {
			this.channelState = channelState;
		}

		public String getWhiteListPolicy() {
			return whiteListPolicy;
		}

		public void setWhiteListPolicy(String whiteListPolicy) {
			this.whiteListPolicy = whiteListPolicy;
		}

		public String getChannelBaobeiFlag() {
			return channelBaobeiFlag;
		}

		public void setChannelBaobeiFlag(String channelBaobeiFlag) {
			this.channelBaobeiFlag = channelBaobeiFlag;
		}

	}

}
