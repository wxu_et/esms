package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.SpScCarriercode;
import esms.etonenet.boss1069.entity.SpScCarriercodePK;
import esms.etonenet.boss1069.service.biz.impl.SpScCarriercodeService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "SpScCarriercode" })
@ApiIgnore
@Controller("SpScCarriercodeController")
@RequestMapping(ApiConstants.BASE_PATH + "spsccarriercode")
public class SpScCarriercodeController {

	@Resource
	private SpScCarriercodeService spScCarriercodeService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage spScCarriercodeData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("id.spId", StringUtil.splitMinusFirstGroup(param.getSpIdName())));
		conditions.add(SpecificationCondition.eq("id.spServiceCode", param.getSpServiceCodeSearch()));
		conditions.add(SpecificationCondition.eq("id.carrierCode",
				StringUtil.splitMinusFirstGroup(param.getCarrierCodeSearch())));
		List<String> sortNames = new ArrayList<String>();
		if ("id.spId".equals(param.getSortName()) || "id.spServiceCode".equals(param.getSortName())
				|| "id.carrierCode".equals(param.getSortName())) {
			sortNames.add("id.spId");
			sortNames.add("id.spServiceCode");
			sortNames.add("id.carrierCode");
		} else
			sortNames.add(param.getSortName());
		SpecificationHelper<SpScCarriercode> sh = new SpecificationHelper<>(conditions, param.getSortOrder(),
				sortNames);
		Page<SpScCarriercode> p = spScCarriercodeService
				.page(new PageRequest(param.getPageNumber(), param.getPageSize()), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String spScCarriercodeUpdate(@ModelAttribute @Valid SpScCarriercode update, String sccid) {
		String[] s = sccid.split("-");
		SpScCarriercodePK id = new SpScCarriercodePK();
		id.setSpId(s[0]);
		id.setSpServiceCode(s[1]);
		id.setCarrierCode(s[2]);
		update.setId(id);
		spScCarriercodeService.update(update);
		return "更新成功";
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String spScCarriercodeDel(@RequestBody @Valid List<SpScCarriercode> data) {
		List<SpScCarriercodePK> ids = new ArrayList<>();
		for (SpScCarriercode spsc : data)
			ids.add(spsc.getId());
		if (ids.size() != 0)
			spScCarriercodeService.del(ids);
		return "删除成功";
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String spScCarriercodeCreate(@ModelAttribute @Valid SpScCarriercode create, String spIdName,
			String carrierCodeIdName) {
		create.getId().setSpId(spIdName.split("-")[0]);
		create.getId().setCarrierCode(carrierCodeIdName.split("-")[0]);
		spScCarriercodeService.create(create);
		return "新建成功";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String spIdName;
		private String spServiceCodeSearch;
		private String carrierCodeSearch;

		public String getSpIdName() {
			return spIdName;
		}

		public void setSpIdName(String spIdName) {
			this.spIdName = spIdName;
		}

		public String getSpServiceCodeSearch() {
			return spServiceCodeSearch;
		}

		public void setSpServiceCodeSearch(String spServiceCodeSearch) {
			this.spServiceCodeSearch = spServiceCodeSearch;
		}

		public String getCarrierCodeSearch() {
			return carrierCodeSearch;
		}

		public void setCarrierCodeSearch(String carrierCodeSearch) {
			this.carrierCodeSearch = carrierCodeSearch;
		}

	}
}
