package esms.etonenet.boss1069.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.entity.MtAddress;
import esms.etonenet.boss1069.repository.MtAddressRepository;
import esms.etonenet.boss1069.service.biz.impl.MtAddressService;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class MtAddressController {

	@Resource
	private MtAddressRepository addressRepository;
	@Resource
	MtAddressService addressService;

	// 跳转页面
	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_INDEX)
	public ModelAndView spChannelIndex() {
		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.RUNMANAGE_ADDRESS_INDEX));
		return mv;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_DATA)
	@ResponseBody
	public AjaxPage testData(Integer pageNumber, Integer pageSize, final String sortName, final String sortOrder,
			final String addressName, final String target) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("addressName",
				StringUtil.splitMinusFirstGroup(addressName)));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("target", StringUtil.splitMinusFirstGroup(target)));
		List<String> orderNames = new ArrayList<String>();
		if ("addressName".equals(sortName)) {
			orderNames.add("addressName");
		} else
			orderNames.add(sortName);

		SpecificationHelper<MtAddress> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<MtAddress> p = addressService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());
		List<MtAddress> list = p.getContent();
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_ADD)
	public ModelAndView testNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.RUNMANAGE_ADDRESS_ADD);
		return mv;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_ADD_DATA)
	@ResponseBody
	public String testCreate(MtAddress create) {
		Timestamp d = new Timestamp(System.currentTimeMillis());
		create.setCreateTime(d);
		create.setUpdateTime(d);
		addressService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_GETNAME)
	@ResponseBody
	public boolean getName(MtAddress address) {
		if (addressService.getName(address.getAddressName()) != null) {
			return false;
		} else {
			return true;
		}
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_UPDATE)
	public ModelAndView testUpdates(MtAddress update) {
		ModelAndView mv = new ModelAndView(UrlConstants.RUNMANAGE_ADDRESS_UPDATE);
		mv.addObject("address", addressRepository.findOne(update.getAddressName()));
		return mv;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_UPDATE_DATA)
	@ResponseBody
	public String testUpdate(MtAddress update) {
		Timestamp d = new Timestamp(System.currentTimeMillis());
		update.setUpdateTime(d);
		addressService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_ADDRESS_DEL)
	@ResponseBody
	public String testDel(@RequestBody List<MtAddress> data) {
		if (data.size() > 0) {
			addressService.del(data);
			return "删除成功";
		}
		return "没有选中数据";
	}
}
