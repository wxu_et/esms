package esms.etonenet.boss1069.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.ServletUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.repository.MtAddressRepository;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.UrlConstants;
import esms.etonenet.oldboss.entity.OldSp;
import esms.etonenet.oldboss.repository.OldChannelRepository;
import esms.etonenet.oldboss.repository.OldSpRepository;
import esms.etonenet.oldboss.repository.OldSpServiceRepository;
import esms.etonenet.olduser.entity.OldUser;
import esms.etonenet.olduser.repository.OldUserRepository;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SmsMtTestController {

	private static Logger logger = LoggerFactory.getLogger(SmsMtTestController.class);

	@Resource
	private OldSpRepository oldSpEm;

	@Resource
	private OldUserRepository oldUserEm;

	@Resource
	private OldChannelRepository oldChannelEm;

	@Resource
	private OldSpServiceRepository oldSpServiceEm;

	@Resource
	private MtAddressRepository mtAddressEm;

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_INDEX)
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView(TilesUtil.getPath(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_INDEX));
		Calendar calendar = Calendar.getInstance();
		Timestamp now = new Timestamp(calendar.getTimeInMillis());
		mav.addObject("mtContent", "测试！时间：" + now.toString());
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_DATA)
	@ResponseBody
	public AjaxPage data(final String spId, final String spServiceCode, final String channelId,
			final String destinationAddr, final String sourceAddr, final String mtContent, final String mtAddr,
			HttpServletRequest request) {

		StringBuffer sb = new StringBuffer();
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> map = new HashMap<String, String>();
		String inputLine = null;
		try {
			if (!request.getParameterMap().containsKey("mtAddr"))
				return AjaxPageUtil.toAjaxPage(1l, list);
			if (StringUtil.isNotBlank(mtAddr))
				sb.append(mtAddr);
			sb.append("?command=MULTI_MT_REQUEST");
			sb.append("&spid=" + spId);
			if (StringUtil.isNotBlank(spId)) {
				OldSp os = oldSpEm.findOne(spId);
				String pwd = "";
				if (os != null && os.getUserId() != null) {
					OldUser ou = oldUserEm.findByUserId(os.getUserId().longValue());
					if (ou != null)
						pwd = ou.getPassword();
				}
				sb.append("&sppassword=" + pwd);
			}
			if (StringUtil.isNotBlank(spServiceCode))
				sb.append("&spsc=" + spServiceCode);
			if (StringUtil.isNotBlank(channelId))
				sb.append("&chid=" + channelId);
			if (StringUtil.isNotBlank(destinationAddr))
				sb.append("&das=" + destinationAddr);
			if (StringUtil.isNotBlank(sourceAddr))
				sb.append("&sa=" + sourceAddr);
			if (StringUtil.isNotBlank(mtContent))
				sb.append("&dc=15&sm=" + StringUtil.decodeStringHex(15, mtContent));

			logger.info("url:{}", sb.toString());
			inputLine = ServletUtil.URLSend(sb.toString());
		} catch (Exception e) {
			inputLine = e.toString();
			if (!(inputLine.indexOf(sb.toString()) >= 0))
				inputLine = e.toString() + " - " + sb.toString();
			logger.error(e.getMessage(), e);
		}
		map.put("resultBack", inputLine);
		list.add(map);
		return AjaxPageUtil.toAjaxPage(1l, list);
	}

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP_SERVICECODE)
	@ResponseBody
	public List<Autocomplete> autoSpserviceSpServiceCode(String term, String spId) {

		return AutocompleteUtil.object2auto(oldSpServiceEm.autoSpServiceCode(term, spId));
	}

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP)
	@ResponseBody
	public List<Autocomplete> autoSpmtrouteSp(String term) {

		return AutocompleteUtil.object2auto(oldSpEm.auto2SP(term));
	}

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<Autocomplete> autoSpmtrouteChannel(String term) {

		return AutocompleteUtil.object2auto(oldChannelEm.auto2Channel(term));
	}

	@RequestMapping(UrlConstants.RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_MTADDR)
	@ResponseBody
	public List<Autocomplete> autoMtAddr(String term) {

		return AutocompleteUtil.object2auto(mtAddressEm.auto2MtAddr(term));
	}

}
