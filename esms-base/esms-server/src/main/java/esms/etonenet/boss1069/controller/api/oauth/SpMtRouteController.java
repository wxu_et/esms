package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.entity.ChannelGroup;
import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.entity.SpMtRoute;
import esms.etonenet.boss1069.enums.spmtroute.RouteType;
import esms.etonenet.boss1069.service.biz.impl.SpMtRouteService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import esms.etonenet.bossoa.entity.CarrierCode;
import esms.etonenet.bossoa.entity.City;
import esms.etonenet.bossoa.entity.Country;
import esms.etonenet.bossoa.entity.Province;
import esms.etonenet.bossoa.repository.CarrierCodeRepository;
import esms.etonenet.bossoa.repository.CityRepository;
import esms.etonenet.bossoa.repository.CountryRepository;
import esms.etonenet.bossoa.repository.ProvinceRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "SpMtRoute" })
@ApiIgnore
@Controller("SpMtRouteController")
@RequestMapping(ApiConstants.BASE_PATH + "spmtroute")
public class SpMtRouteController {

	@Resource
	private SpMtRouteService spMtRouteService;

	@Resource
	private CountryRepository countryEm;

	@Resource
	private ProvinceRepository provinceEm;

	@Resource
	private CityRepository cityEm;

	@Resource
	private CarrierCodeRepository carrierCodeEm;

	@RequestMapping(UrlConstants.QYDX_QXLYGL_SPMTROUTE_DATA)
	@ResponseBody
	public AjaxPage spMtRouteData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("sp.spId", StringUtil.splitMinusFirstGroup(param.getSpIdName())));
		conditions.add(SpecificationCondition.eq("channel.channelId",
				StringUtil.splitMinusFirstGroup(param.getChannelIdNameSearch())));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spServiceCode", param.getSpServiceCode()));
		conditions
				.add(SpecificationCondition.eq("carrierCode", StringUtil.splitMinusFirstGroup(param.getCarrierCode())));
		conditions.add(SpecificationCondition.eq("routeState", param.getRouteState()));
		SpecificationHelper<SpMtRoute> sh = new SpecificationHelper<>(conditions, param.getSortOrder(),
				param.getSortName());
		Page<SpMtRoute> p = spMtRouteService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		for (SpMtRoute spmt : p)
			displaySpMtBaseInfo(spmt);
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String spMtRouteCreate(@ModelAttribute @Valid SpMtRoute create, String spIdName, String channelIdName,
			String carrierCodeIdName, String countryIdName, String provinceIdName, String cityIdName,
			String channelGroupIdName) {
		if (RouteType.NORMAL.getValue().equals(create.getRouteType())) {
			Channel ch = new Channel();
			ch.setChannelId(channelIdName.split("-")[0]);
			create.setChannel(ch);
			create.setCountryCode(countryIdName.split("-")[0]);
			create.setCityCode(cityIdName.split("-")[0]);
			create.setProvinceCode(provinceIdName.split("-")[0]);
			create.setCarrierCode(carrierCodeIdName.split("-")[0]);
			create.setChannelGroup(null);
			create.setChannelGroupRouteState(null);
		} else {
			create.setRouteState(null);
			create.setChannel(null);
			create.setCountryCode(null);
			create.setCityCode(null);
			create.setProvinceCode(null);
			create.setCarrierCode(null);
			ChannelGroup cc = new ChannelGroup();
			cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
			create.setChannelGroup(cc);
		}
		Sp sp = new Sp();
		sp.setSpId(spIdName.split("-")[0]);
		create.setSp(sp);
		spMtRouteService.create(create);
		return "新建成功";
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String spMtRouteDel(@RequestBody @Valid List<SpMtRoute> data) {
		spMtRouteService.del(data);
		return "删除成功";
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String spMtRouteUpdate(@RequestBody @Valid SpMtRoute update, String spIdName, String channelIdName,
			String carrierCodeIdName, String countryIdName, String provinceIdName, String cityIdName,
			String channelGroupIdName) {
		if (RouteType.NORMAL.getValue().equals(update.getRouteType())) {
			Channel ch = new Channel();
			ch.setChannelId(channelIdName.split("-")[0]);
			update.setChannel(ch);
			update.setCountryCode(countryIdName.split("-")[0]);
			update.setCityCode(cityIdName.split("-")[0]);
			update.setProvinceCode(provinceIdName.split("-")[0]);
			update.setCarrierCode(carrierCodeIdName.split("-")[0]);
			update.setChannelGroup(null);
			update.setChannelGroupRouteState(null);
		} else {
			update.setRouteState(null);
			update.setChannel(null);
			update.setCountryCode(null);
			update.setCityCode(null);
			update.setProvinceCode(null);
			update.setCarrierCode(null);
			ChannelGroup cc = new ChannelGroup();
			cc.setChannelGroupId(channelGroupIdName.split("-")[0]);
			update.setChannelGroup(cc);
		}
		Sp sp = new Sp();
		sp.setSpId(spIdName.split("-")[0]);
		update.setSp(sp);
		spMtRouteService.update(update);
		return "更新成功";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String routeState;
		private String carrierCode;
		private String spIdName;
		private String channelIdNameSearch;
		private String spServiceCode;

		public String getRouteState() {
			return routeState;
		}

		public void setRouteState(String routeState) {
			this.routeState = routeState;
		}

		public String getCarrierCode() {
			return carrierCode;
		}

		public void setCarrierCode(String carrierCode) {
			this.carrierCode = carrierCode;
		}

		public String getSpIdName() {
			return spIdName;
		}

		public void setSpIdName(String spIdName) {
			this.spIdName = spIdName;
		}

		public String getChannelIdNameSearch() {
			return channelIdNameSearch;
		}

		public void setChannelIdNameSearch(String channelIdNameSearch) {
			this.channelIdNameSearch = channelIdNameSearch;
		}

		public String getSpServiceCode() {
			return spServiceCode;
		}

		public void setSpServiceCode(String spServiceCode) {
			this.spServiceCode = spServiceCode;
		}
	}

	/**
	 * 修正基础信息显示国家、省、城市、营运商
	 * 
	 * @param spmt
	 */
	private void displaySpMtBaseInfo(SpMtRoute spmt) {
		if (StringUtil.isNotEmpty(spmt.getCarrierCode())) {
			CarrierCode cc = carrierCodeEm.findOne(spmt.getCarrierCode());
			if (cc != null)
				spmt.setCarrierCode(cc.getCarrierCode() + "-" + cc.getCarrierName());
		}
		if (StringUtil.isNotEmpty(spmt.getCountryCode())) {
			Country country = countryEm.findOne(spmt.getCountryCode());
			if (country != null)
				spmt.setCountryCode(country.getCountryCode() + "-" + country.getCountryName());
		}
		if (StringUtil.isNotEmpty(spmt.getProvinceCode())) {
			Province province = provinceEm.findOne(spmt.getProvinceCode());
			if (province != null)
				spmt.setProvinceCode(province.getProvinceCode() + "-" + province.getProvinceName());
		}
		if (StringUtil.isNotEmpty(spmt.getCityCode())) {
			City city = cityEm.findOne(spmt.getCityCode());
			if (city != null)
				spmt.setCityCode(city.getCityCode() + "-" + city.getCityName());
		}
	}

}
