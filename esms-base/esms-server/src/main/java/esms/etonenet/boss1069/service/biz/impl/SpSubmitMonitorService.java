package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.report.SpSubmitMonitor;
import esms.etonenet.boss1069.repositoryReport.SpSubmitMonitorRepository;

@Service
public class SpSubmitMonitorService {

	@Resource
	private SpSubmitMonitorRepository spSubmitMonitorEm;

	@Transactional(rollbackFor = Exception.class)
	public void create(SpSubmitMonitor ssm) {
		spSubmitMonitorEm.save(ssm);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<SpSubmitMonitor> list) {
		for (SpSubmitMonitor s : list) {
			spSubmitMonitorEm.delete(s);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpSubmitMonitor ssm) {
		spSubmitMonitorEm.save(ssm);
	}

	public Page<SpSubmitMonitor> page(PageRequest pageRequest, Specification<SpSubmitMonitor> spec) {
		return spSubmitMonitorEm.findAll(spec, pageRequest);
	}

}
