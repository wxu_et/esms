package esms.etonenet.boss1069.web;

/**
 * URL命名规范： /父亲组/子组/子子组/子...组/自定义名称
 * 
 * 
 * @author lyzhang
 * 
 */
public class UrlConstants {

	// datasyn数据同步
	public static final String DATASYN_CHANNELBAOBEI_INDEX = "/datasyn/channelbb/index";
	public static final String DATASYN_CHANNELBAOBEI_SYN = "/datasyn/channelbb/syn";
	public static final String DATASYN_CHANNELBAOBEI_DEL = "/datasyn/channelbb/del";
	public static final String DATASYN_CHANNELBAOBEI_SYNALL = "/datasyn/channelbb/synall";

	public static final String DATASYN_CHANNEL_INDEX = "/datasyn/channel/index";
	public static final String DATASYN_CHANNEL_SYN = "/datasyn/channel/syn";
	public static final String DATASYN_CHANNEL_DEL = "/datasyn/channel/del";
	public static final String DATASYN_CHANNEL_SYNALL = "/datasyn/channel/synall";

	public static final String DATASYN_SP_INDEX = "/datasyn/sp/index";
	public static final String DATASYN_SP_SYN = "/datasyn/sp/syn";
	public static final String DATASYN_SP_SYNALL = "/datasyn/sp/synall";

	public static final String DATASYN_SPSERVICE_INDEX = "/datasyn/spservice/index";
	public static final String DATASYN_SPSERVICE_SYN = "/datasyn/spservice/syn";
	public static final String DATASYN_SPSERVICE_DEL = "/datasyn/spservice/del";
	public static final String DATASYN_SPSERVICE_SYNALL = "/datasyn/spservice/synall";

	public static final String DATASYN_SPCHANNEL_INDEX = "/datasyn/spchannel/index";
	public static final String DATASYN_SPCHANNEL_SYN = "/datasyn/spchannel/syn";
	public static final String DATASYN_SPCHANNEL_DEL = "/datasyn/spchannel/del";
	public static final String DATASYN_SPCHANNEL_SYNALL = "/datasyn/spchannel/synall";

	public static final String DATASYN_CHANNELGROUPCONFIG_INDEX = "/datasyn/channelgroupconfig/index";
	public static final String DATASYN_CHANNELGROUPCONFIG_SYN = "/datasyn/channelgroupconfig/syn";
	public static final String DATASYN_CHANNELGROUPCONFIG_DEL = "/datasyn/channelgroupconfig/del";
	public static final String DATASYN_CHANNELGROUPCONFIG_SYNALL = "/datasyn/channelgroupconfig/synall";

	public static final String DATASYN_SPSCCARRIERCODE_INDEX = "/datasyn/spsccarriercode/index";
	public static final String DATASYN_SPSCCARRIERCODE_SYN = "/datasyn/spsccarriercode/syn";
	public static final String DATASYN_SPSCCARRIERCODE_DEL = "/datasyn/spsccarriercode/del";
	public static final String DATASYN_SPSCCARRIERCODE_SYNALL = "/datasyn/spsccarriercode/synall";

	public static final String DATASYN_SPMTROUTE_INDEX = "/datasyn/spmtroute/index";
	public static final String DATASYN_SPMTROUTE_SYN = "/datasyn/spmtroute/syn";
	public static final String DATASYN_SPMTROUTE_DEL = "/datasyn/spmtroute/del";
	public static final String DATASYN_SPMTROUTE_SYNALL = "/datasyn/spmtroute/synall";

	public static final String DATASYN_CHANNELROUTE_INDEX = "/datasyn/channelroute/index";
	public static final String DATASYN_CHANNELROUTE_SYN = "/datasyn/channelroute/syn";
	public static final String DATASYN_CHANNELROUTE_DEL = "/datasyn/channelroute/del";
	public static final String DATASYN_CHANNELROUTE_SYNALL = "/datasyn/channelroute/synall";

	// system 系统
	// usergroup
	public static final String SYSTEM_USERGROUP_INDEX = "/system/usergroup/index";
	public static final String SYSTEM_USERGROUP_DATA = "/system/usergroup/data";
	public static final String SYSTEM_USERGROUP_NEW = "/system/usergroup/new";
	public static final String SYSTEM_USERGROUP_CREATE = "/system/usergroup/create";
	public static final String SYSTEM_USERGROUP_DEL = "/system/usergroup/del";
	public static final String SYSTEM_USERGROUP_VIEW = "/system/usergroup/view";
	public static final String SYSTEM_USERGROUP_UPDATE = "/system/usergroup/update";
	public static final String SYSTEM_USERGROUP_AUTOCOMPLETE_URLACTION = "/system/usergroup/autocomplete/urlaction";

	// url
	public static final String SYSTEM_URL_INDEX = "/system/url/index";
	public static final String SYSTEM_URL_DATA = "/system/url/data";
	public static final String SYSTEM_URL_DEL = "/system/url/del";
	public static final String SYSTEM_URL_AUTOCOMPLETE = "/system/url/autocomplete";

	// menu
	public static final String SYSTEM_NODE_INDEX = "/system/node/index";
	public static final String SYSTEM_NODE_NEW = "/system/node/new";
	public static final String SYSTEM_NODE_CREATE = "/system/node/create";
	public static final String SYSTEM_NODE_DEL = "/system/node/del";
	public static final String SYSTEM_NODE_VIEW = "/system/node/view";
	public static final String SYSTEM_NODE_UPDATE = "/system/node/update";

	// syslog
	public static final String SYSTEM_LOG_INDEX = "/system/log/index";
	public static final String SYSTEM_LOG_DATA = "/system/log/data";
	public static final String SYSTEM_LOG_AUTOCOMPLETE_OPERUSER = "/system/log/autocomplete/operuser";

	// role
	public static final String SYSTEM_AUTH_INDEX = "/system/auth/index";
	public static final String SYSTEM_AUTH_DATA = "/system/auth/data";
	public static final String SYSTEM_AUTH_ROLEUSER = "/system/auth/roleuser";
	public static final String SYSTEM_AUTH_SAVE = "/system/auth/save";

	// bdgl BD管理
	// tdgl 通道管理
	// channel
	public static final String BD_TDGL_CHANNEL_INDEX = "/bd/tdgl/channel/index";
	public static final String BD_TDGL_CHANNEL_DATA = "/bd/tdgl/channel/data";
	public static final String BD_TDGL_CHANNEL_NEW = "/bd/tdgl/channel/new";
	public static final String BD_TDGL_CHANNEL_CREATE = "/bd/tdgl/channel/create";
	public static final String BD_TDGL_CHANNEL_VIEW = "/bd/tdgl/channel/view";
	public static final String BD_TDGL_CHANNEL_UPDATE = "/bd/tdgl/channel/update";

	// channel baobei
	public static final String BD_TDGL_CHANNEL_BAOBEI_INDEX = "/bd/tdgl/channelbb/index";
	public static final String BD_TDGL_CHANNEL_BAOBEI_DATA = "/bd/tdgl/channelbb/data";
	public static final String BD_TDGL_CHANNEL_BAOBEI_NEW = "/bd/tdgl/channelbb/new";
	public static final String BD_TDGL_CHANNEL_BAOBEI_CREATE = "/bd/tdgl/channelbb/create";
	public static final String BD_TDGL_CHANNEL_BAOBEI_VIEW = "/bd/tdgl/channelbb/view";
	public static final String BD_TDGL_CHANNEL_BAOBEI_UPDATE = "/bd/tdgl/channelbb/update";
	public static final String BD_TDGL_CHANNEL_BAOBEI_DEL = "/bd/tdgl/channelbb/del";
	public static final String BD_TDGL_CHANNEL_BAOBEI_IDCHECK = "/bd/tdgl/channelbb/idcheck";
	public static final String BD_TDGL_CHANNEL_BAOBEI_AUTOCOMPLETE_CHANNEL = "/bd/tdgl/channelbb/autocomplete/channel";

	// qydx 企业短信
	// qxjjsj 企信基础数据
	public static final String QYDX_QXJJSJ_AUTOCOMPLETE_CARRIERCODE = "/qydx/qxjjsj/autocomplete/carriercode";

	// sp
	public static final String QYDX_QXJJSJ_SP_INDEX = "/qydx/qxjjsj/sp/index";
	public static final String QYDX_QXJJSJ_SP_DATA = "/qydx/qxjjsj/sp/data";
	public static final String QYDX_QXJJSJ_SP_NEW = "/qydx/qxjjsj/sp/new";
	public static final String QYDX_QXJJSJ_SP_CREATE = "/qydx/qxjjsj/sp/create";
	public static final String QYDX_QXJJSJ_SP_VIEW = "/qydx/qxjjsj/sp/view";
	public static final String QYDX_QXJJSJ_SP_UPDATE = "/qydx/qxjjsj/sp/update";
	public static final String QYDX_QXJJSJ_SP_SPIDCHECK = "/qydx/qxjjsj/sp/idcheck";
	// test
	public static final String QYDX_QXJJSJ_TEST_INDEX = "/qydx/qxjjsj/test/index";
	public static final String QYDX_QXJJSJ_TEST_NEW = "/qydx/qxjjsj/test/new";
	public static final String QYDX_QXJJSJ_TEST_CREATE = "/qydx/qxjjsj/test/create";
	public static final String QYDX_QXJJSJ_TEST_VIEW = "/qydx/qxjjsj/test/view";
	public static final String QYDX_QXJJSJ_TEST_DATA = "/qydx/qxjjsj/test/data";
	public static final String QYDX_QXJJSJ_TEST_DEL = "/qydx/qxjjsj/test/del";
	public static final String QYDX_QXJJSJ_TEST_UPDATE = "/qydx/qxjjsj/test/update";
	public static final String QYDX_QXJJSJ_TEST_IDCHECK = "/qydx/qxjjsj/test/idcheck";

	// spService
	public static final String QYDX_QXJJSJ_SPSERVICE_INDEX = "/qydx/qxjjsj/spservice/index";
	public static final String QYDX_QXJJSJ_SPSERVICE_DATA = "/qydx/qxjjsj/spservice/data";
	public static final String QYDX_QXJJSJ_SPSERVICE_NEW = "/qydx/qxjjsj/spservice/new";
	public static final String QYDX_QXJJSJ_SPSERVICE_CREATE = "/qydx/qxjjsj/spservice/create";
	public static final String QYDX_QXJJSJ_SPSERVICE_VIEW = "/qydx/qxjjsj/spservice/view";
	public static final String QYDX_QXJJSJ_SPSERVICE_UPDATE = "/qydx/qxjjsj/spservice/update";
	public static final String QYDX_QXJJSJ_SPSERVICE_DEL = "/qydx/qxjjsj/spservice/del";
	public static final String QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_SP = "/qydx/qxjjsj/spservice/autocomplete/sp";
	public static final String QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxjjsj/spservice/autocomplete/spservicecode";
	public static final String QYDX_QXJJSJ_SPSERVICE_AUTOCOMPLETE_CHANNEL = "/qydx/qxjjsj/spservice/autocomplete/channel";

	// spscCarrierCode
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_INDEX = "/qydx/qxjjsj/spsccarriercode/index";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_DATA = "/qydx/qxjjsj/spsccarriercode/data";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_NEW = "/qydx/qxjjsj/spsccarriercode/new";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_CREATE = "/qydx/qxjjsj/spsccarriercode/create";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_VIEW = "/qydx/qxjjsj/spsccarriercode/view";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_UPDATE = "/qydx/qxjjsj/spsccarriercode/update";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_DEL = "/qydx/qxjjsj/spsccarriercode/del";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP = "/qydx/qxjjsj/spsccarriercode/autocomplete/sp";
	public static final String QYDX_QXJJSJ_SPSCCARRIERCODE_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxjjsj/spsccarriercode/autocomplete/spservicecode";

	// qxlygl 企信路由管理
	public static final String QYDX_QXLYGL_AUTOCOMPLETE_CARRIERCODE = "/qydx/qxlygl/autocomplete/carriercode";
	public static final String QYDX_QXLYGL_AUTOCOMPLETE_COUNTRY = "/qydx/qxlygl/autocomplete/country";
	public static final String QYDX_QXLYGL_AUTOCOMPLETE_PROVINCE = "/qydx/qxlygl/autocomplete/province";
	public static final String QYDX_QXLYGL_AUTOCOMPLETE_CITY = "/qydx/qxlygl/autocomplete/city";

	// spchannel
	public static final String QYDX_QXLYGL_SPCHANNEL_INDEX = "/qydx/qxlygl/spchannel/index";
	public static final String QYDX_QXLYGL_SPCHANNEL_DATA = "/qydx/qxlygl/spchannel/data";
	public static final String QYDX_QXLYGL_SPCHANNEL_NEW = "/qydx/qxlygl/spchannel/new";
	public static final String QYDX_QXLYGL_SPCHANNEL_CREATE = "/qydx/qxlygl/spchannel/create";
	public static final String QYDX_QXLYGL_SPCHANNEL_VIEW = "/qydx/qxlygl/spchannel/view";

	public static final String QYDX_QXLYGL_SPCHANNEL_UPDATE = "/qydx/qxlygl/spchannel/update";
	public static final String QYDX_QXLYGL_SPCHANNEL_DEL = "/qydx/qxlygl/spchannel/del";
	public static final String QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP = "/qydx/qxlygl/spchannel/autocomplete/sp";
	public static final String QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxlygl/spchannel/autocomplete/spservicecode";
	public static final String QYDX_QXLYGL_SPCHANNEL_AUTOCOMPLETE_CHANNEL = "/qydx/qxlygl/spchannel/autocomplete/channel";

	// spmoroute
	public static final String QYDX_QXLYGL_SPMOROUTE_INDEX = "/qydx/qxlygl/spmoroute/index";
	public static final String QYDX_QXLYGL_SPMOROUTE_DATA = "/qydx/qxlygl/spmoroute/data";
	public static final String QYDX_QXLYGL_SPMOROUTE_NEW = "/qydx/qxlygl/spmoroute/new";
	public static final String QYDX_QXLYGL_SPMOROUTE_CREATE = "/qydx/qxlygl/spmoroute/create";
	public static final String QYDX_QXLYGL_SPMOROUTE_VIEW = "/qydx/qxlygl/spmoroute/view";
	public static final String QYDX_QXLYGL_SPMOROUTE_UPDATE = "/qydx/qxlygl/spmoroute/update";
	public static final String QYDX_QXLYGL_SPMOROUTE_DEL = "/qydx/qxlygl/spmoroute/del";

	// spmtroute
	public static final String QYDX_QXLYGL_SPMTROUTE_INDEX = "/qydx/qxlygl/spmtroute/index";
	public static final String QYDX_QXLYGL_SPMTROUTE_DATA = "/qydx/qxlygl/spmtroute/data";
	public static final String QYDX_QXLYGL_SPMTROUTE_NEW = "/qydx/qxlygl/spmtroute/new";
	public static final String QYDX_QXLYGL_SPMTROUTE_CREATE = "/qydx/qxlygl/spmtroute/create";
	public static final String QYDX_QXLYGL_SPMTROUTE_VIEW = "/qydx/qxlygl/spmtroute/view";
	public static final String QYDX_QXLYGL_SPMTROUTE_UPDATE = "/qydx/qxlygl/spmtroute/update";
	public static final String QYDX_QXLYGL_SPMTROUTE_DEL = "/qydx/qxlygl/spmtroute/del";

	public static final String QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP = "/qydx/qxlygl/spmtroute/autocomplete/sp";
	public static final String QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_CHANNEL = "/qydx/qxlygl/spmtroute/autocomplete/channel";
	public static final String QYDX_QXLYGL_SPMTROUTE_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxlygl/spmtroute/autocomplete/spservicecode";

	// channelgroup
	public static final String QYDX_QXLYGL_CHANNELGROUP_INDEX = "/qydx/qxlygl/channelgroup/index";
	public static final String QYDX_QXLYGL_CHANNELGROUP_DATA = "/qydx/qxlygl/channelgroup/data";
	public static final String QYDX_QXLYGL_CHANNELGROUP_NEW = "/qydx/qxlygl/channelgroup/new";
	public static final String QYDX_QXLYGL_CHANNELGROUP_CREATE = "/qydx/qxlygl/channelgroup/create";
	public static final String QYDX_QXLYGL_CHANNELGROUP_VIEW = "/qydx/qxlygl/channelgroup/view";
	public static final String QYDX_QXLYGL_CHANNELGROUP_UPDATE = "/qydx/qxlygl/channelgroup/update";
	public static final String QYDX_QXLYGL_CHANNELGROUP_DEL = "/qydx/qxlygl/channelgroup/del";
	public static final String QYDX_QXLYGL_CHANNELGROUP_CHANNELGROUPID_CHECK = "/qydx/qxlygl/channelgroup/idcheck";

	// channelgroupconfig
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_INDEX = "/qydx/qxlygl/channelgroupconfig/index";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_DATA = "/qydx/qxlygl/channelgroupconfig/data";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_NEW = "/qydx/qxlygl/channelgroupconfig/new";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_CREATE = "/qydx/qxlygl/channelgroupconfig/create";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_VIEW = "/qydx/qxlygl/channelgroupconfig/view";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_UPDATE = "/qydx/qxlygl/channelgroupconfig/update";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_DEL = "/qydx/qxlygl/channelgroupconfig/del";

	public static final String QYDX_QXLYGL_AUTOCOMPLETE_CHANNELGROUP = "/qydx/qxlygl/autocomplete/channelgroup";
	public static final String QYDX_QXLYGL_CHANNELGROUPCONFIG_AUTOCOMPLETE_CHANNEL = "/qydx/qxlygl/channelgroupconfig/autocomplete/channel";

	// channelroute
	public static final String QYDX_QXLYGL_CHANNELROUTE_INDEX = "/qydx/qxlygl/channelroute/index";
	public static final String QYDX_QXLYGL_CHANNELROUTE_DATA = "/qydx/qxlygl/channelroute/data";
	public static final String QYDX_QXLYGL_CHANNELROUTE_NEW = "/qydx/qxlygl/channelroute/new";
	public static final String QYDX_QXLYGL_CHANNELROUTE_CREATE = "/qydx/qxlygl/channelroute/create";
	public static final String QYDX_QXLYGL_CHANNELROUTE_VIEW = "/qydx/qxlygl/channelroute/view";
	public static final String QYDX_QXLYGL_CHANNELROUTE_UPDATE = "/qydx/qxlygl/channelroute/update";
	public static final String QYDX_QXLYGL_CHANNELROUTE_DEL = "/qydx/qxlygl/channelroute/del";
	public static final String QYDX_QXLYGL_CHANNELROUTE_AUTOCOMPLETE_CHANNEL = "/qydx/qxlygl/channelroute/autocomplete/channel";

	// mtselect
	public static final String QYDX_QXXXGL_MTSELECT_INDEX = "/qydx/qxxxgl/mtselect/index";
	public static final String QYDX_QXXXGL_MTSELECT_DATA = "/qydx/qxxxgl/mtselect/data";
	public static final String QYDX_QXXXGL_MTSELECT_EXPORT = "/qydx/qxxxgl/mtselect/export";
	public static final String QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_SP = "/qydx/qxxxgl/mtselect/autocomplete/sp";
	public static final String QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_CHANNEL = "/qydx/qxxxgl/mtselect/autocomplete/channel";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_COUNTRY = "/qydx/qxxxgl/mtselect/autocomplete/country";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_PROVINCE = "/qydx/qxxxgl/mtselect/autocomplete/province";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_CITY = "/qydx/qxxxgl/mtselect/autocomplete/city";
	public static final String QYDX_QXXXGL_MTSELECT_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxxxgl/mtselect/autocomplete/spservicecode";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_CARRIERCODE = "/qydx/qxxxgl/mtselect/autocomplete/carriercode";

	// moselect
	public static final String QYDX_QXXXGL_MOSELECT_INDEX = "/qydx/qxxxgl/moselect/index";
	public static final String QYDX_QXXXGL_MOSELECT_DATA = "/qydx/qxxxgl/moselect/data";
	public static final String QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_SP = "/qydx/qxxxgl/moselect/autocomplete/sp";
	public static final String QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_SP_SERVICECODE = "/qydx/qxxxgl/moselect/autocomplete/spservicecode";
	public static final String QYDX_QXXXGL_MOSELECT_AUTOCOMPLETE_CHANNEL = "/qydx/qxxxgl/moselect/autocomplete/channel";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_COUNTRY2 = "/qydx/qxxxgl/mtselect/autocomplete/country2";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_PROVINCE2 = "/qydx/qxxxgl/mtselect/autocomplete/province2";
	public static final String QYDX_QXXXGL_AUTOCOMPLETE_CITY2 = "/qydx/qxxxgl/mtselect/autocomplete/city2";

	// providerErrorCode
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_INDEX = "/runmanage/basicinformation/providererrorcode/index";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DATA = "/runmanage/basicinformation/providererrorcode/data";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_NEW = "/runmanage/basicinformation/providererrorcode/new";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_CREATE = "/runmanage/basicinformation/providererrorcode/create";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_VIEW = "/runmanage/basicinformation/providererrorcode/view";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_UPDATE = "/runmanage/basicinformation/providererrorcode/update";
	public static final String RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DEL = "/runmanage/basicinformation/providererrorcode/del";
	public static final String RUNMANAGE_BASICINFORMATION_AUTOCOMPLETE_ERRORDEFINE = "/runmanage/basicinformation/autocomplete/errordefine";

	// monitorManage
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_INDEX = "/runmanage/runmonitor/monitormanage/index";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_DATA = "/runmanage/runmonitor/monitormanage/data";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_NEW = "/runmanage/runmonitor/monitormanage/new";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_CREATE = "/runmanage/runmonitor/monitormanage/create";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_VIEW = "/runmanage/runmonitor/monitormanage/view";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_UPDATE = "/runmanage/runmonitor/monitormanage/update";
	public static final String RUNMANAGE_RUNMONITOR_MONITORMANAGE_DEL = "/runmanage/runmonitor/monitormanage/del";
	// sp submit monitor
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_INDEX = "/runmanage/runmonitor/spsubmitmonitor/index";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_DATA = "/runmanage/runmonitor/spsubmitmonitor/data";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_NEW = "/runmanage/runmonitor/spsubmitmonitor/new";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_CREATE = "/runmanage/runmonitor/spsubmitmonitor/create";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_VIEW = "/runmanage/runmonitor/spsubmitmonitor/view";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_UPDATE = "/runmanage/runmonitor/spsubmitmonitor/update";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_DEL = "/runmanage/runmonitor/spsubmitmonitor/del";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_AUTOCOMPLETE_SP = "/runmanage/runmonitor/spsubmitmonitor/autocomplete/sp";
	public static final String RUNMANAGE_RUNMONITOR_SPSUBMITMONITOR_REMOTECHECK_SP = "/runmanage/runmonitor/spsubmitmonitor/remotecheck/sp";

	// smsSendTest
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_INDEX = "/runmanage/smstest/smssendtest/index";
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_DATA = "/runmanage/smstest/smssendtest/data";
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP = "/runmanage/smstest/smssendtest/autocomplete/sp";
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_CHANNEL = "/runmanage/smstest/smssendtest/autocomplete/channel";
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_SP_SERVICECODE = "/runmanage/smstest/smssendtest/autocomplete/spservicecode";
	public static final String RUNMANAGE_SMSTEST_SMSSENDTEST_AUTOCOMPLETE_MTADDR = "/runmanage/smstest/smssendtest/autocomplete/mtaddr";

	// moContentFilterDetail
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_INDEX = "/tyyw/tyywgl/mocontentfilter/index";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_DATA = "/tyyw/tyywgl/mocontentfilter/data";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_NEW = "/tyyw/tyywgl/mocontentfilter/new";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_CREATE = "/tyyw/tyywgl/mocontentfilter/create";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_VIEW = "/tyyw/tyywgl/mocontentfilter/view";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_UPDATE = "/tyyw/tyywgl/mocontentfilter/update";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_DEL = "/tyyw/tyywgl/mocontentfilter/del";
	public static final String TYYW_TYYWGL_MOCONTENTFILTER_TEXTFILTERPATTERNCHECK = "/tyyw/tyywgl/mocontentfilter/textfilterpatterncheck";

	// common business manage
	// msisdnFilter
	public static final String TYYW_TYYWGL_MSISDNFILTER_INDEX = "/tyyw/tyywgl/msisdnfilter/index";
	public static final String TYYW_TYYWGL_MSISDNFILTER_DATA = "/tyyw/tyywgl/msisdnfilter/data";
	public static final String TYYW_TYYWGL_MSISDNFILTER_NEW = "/tyyw/tyywgl/msisdnfilter/new";
	public static final String TYYW_TYYWGL_MSISDNFILTER_CREATE = "/tyyw/tyywgl/msisdnfilter/create";
	public static final String TYYW_TYYWGL_MSISDNFILTER_VIEW = "/tyyw/tyywgl/msisdnfilter/view";
	public static final String TYYW_TYYWGL_MSISDNFILTER_UPDATE = "/tyyw/tyywgl/msisdnfilter/update";
	public static final String TYYW_TYYWGL_MSISDNFILTER_DEL = "/tyyw/tyywgl/msisdnfilter/del";
	public static final String TYYW_TYYWGL_MSISDNFILTER_AUTOCOMPLETE_SP = "/tyyw/tyywgl/msisdnfilter/autocomplete/sp";
	public static final String TYYW_TYYWGL_MSISDNFILTER_UPLOAD = "/tyyw/tyywgl/msisdnfilter/upload";
	public static final String TYYW_TYYWGL_MSISDNFILTER_PROGRESSBAR_IMPORT = "/tyyw/tyywgl/msisdnfilter/progressbar/import";

	// msisdnSegment
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_INDEX = "/tyyw/tyywgl/msisdnsegment/index";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_DATA = "/tyyw/tyywgl/msisdnsegment/data";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_NEW = "/tyyw/tyywgl/msisdnsegment/new";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_CREATE = "/tyyw/tyywgl/msisdnsegment/create";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_VIEW = "/tyyw/tyywgl/msisdnsegment/view";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_UPDATE = "/tyyw/tyywgl/msisdnsegment/update";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_DEL = "/tyyw/tyywgl/msisdnsegment/del";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_COUNTRYCODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/countrycode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_PROVINCECODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/provincecode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_SINGLE_PROVINCECODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/singleprovincecode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CITYCODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/citycode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_SINGLE_CITYCODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/singlecitycode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_CARRIERCODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/carriercode";
	public static final String TYYW_TYYWGL_MSISDNSEGMENT_AUTOCOMPLETE_NETWORKCODE = "/tyyw/tyywgl/msisdnsegment/autocomplete/networkcode";

	// mtContentFilterCategory
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_INDEX = "/tyyw/tyywgl/mtcontentfiltercategory/index";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_DATA = "/tyyw/tyywgl/mtcontentfiltercategory/data";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_NEW = "/tyyw/tyywgl/mtcontentfiltercategory/new";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_CREATE = "/tyyw/tyywgl/mtcontentfiltercategory/create";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_VIEW = "/tyyw/tyywgl/mtcontentfiltercategory/view";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_UPDATE = "/tyyw/tyywgl/mtcontentfiltercategory/update";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_DEL = "/tyyw/tyywgl/mtcontentfiltercategory/del";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERCATEGORY_TEXTFILTERCATEGORYIDCHECK = "/tyyw/tyywgl/mtcontentfiltercategory/textfiltercategoryidcheck";

	// mtContentFilterCategory
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_INDEX = "/tyyw/tyywgl/mtcontentfiltertype/index";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_DATA = "/tyyw/tyywgl/mtcontentfiltertype/data";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_NEW = "/tyyw/tyywgl/mtcontentfiltertype/new";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_CREATE = "/tyyw/tyywgl/mtcontentfiltertype/create";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_VIEW = "/tyyw/tyywgl/mtcontentfiltertype/view";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_UPDATE = "/tyyw/tyywgl/mtcontentfiltertype/update";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_DEL = "/tyyw/tyywgl/mtcontentfiltertype/del";
	public static final String TYYW_TYYWGL_MTCONTENTFILTERTYPE_TEXTFILTERTYPEIDCHECK = "/tyyw/tyywgl/mtcontentfiltertype/textfiltertypeidcheck";

	// mtContentFilterDetail
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_INDEX = "/tyyw/tyywgl/mtcontentfilter/index";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_DATA = "/tyyw/tyywgl/mtcontentfilter/data";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_NEW = "/tyyw/tyywgl/mtcontentfilter/new";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_CREATE = "/tyyw/tyywgl/mtcontentfilter/create";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_VIEW = "/tyyw/tyywgl/mtcontentfilter/view";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_UPDATE = "/tyyw/tyywgl/mtcontentfilter/update";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_DEL = "/tyyw/tyywgl/mtcontentfilter/del";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_AUTOCOMPLETE_CATEGORY = "/tyyw/tyywgl/mtcontentfilter/autocomplete/category";
	public static final String TYYW_TYYWGL_MTCONTENTFILTER_TEXTFILTERPATTERNCHECK = "/tyyw/tyywgl/mtcontentfilter/textfilterpatterncheck";

	// error define
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_INDEX = "/runmanage/basicinformation/errordefine/index";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DATA = "/runmanage/basicinformation/errordefine/data";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_NEW = "/runmanage/basicinformation/errordefine/new";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_CREATE = "/runmanage/basicinformation/errordefine/create";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_VIEW = "/runmanage/basicinformation/errordefine/view";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_UPDATE = "/runmanage/basicinformation/errordefine/update";
	public static final String RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DEL = "/runmanage/basicinformation/errordefine/del";
	public static final String RUNMANAGE_BASICINFORMATION_ERRPRDEFINE_ERRORDEFINEIDCHECK = "/runmanager/basicinformation/errordefine/errordefineidcheck";

	// Sp-Mt-Filter-Setting 下行SP过滤设置
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_INDEX = "/tyyw/tyywgl/spsctextfilter/index";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_DATA = "/tyyw/tyywgl/spsctextfilter/data";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_NEW = "/tyyw/tyywgl/spsctextfilter/new";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_CREATE = "/tyyw/tyywgl/spsctextfilter/create";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_VIEW = "/tyyw/tyywgl/spsctextfilter/view";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_UPDATE = "/tyyw/tyywgl/spsctextfilter/update";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_DEL = "/tyyw/tyywgl/spsctextfilter/del";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP = "/tyyw/tyywgl/spsctextfilter/autocomplete/sp";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_SP_SERVICECODE = "/tyyw/tyywgl/spsctextfilter/autocomplete/sp/servicecode";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_AUTOCOMPLETE_FILTERTYPE = "/tyyw/tyywgl/spsctextfilter/autocomplete/filtertype";
	public static final String TYYW_TYYWGL_SPSCTEXTFILTER_REMOTE_ID = "/tyyw/tyywgl/spsctextfilter/remote/id";

	// 下行通道过滤设置
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_INDEX = "/tyyw/tyywgl/channeltextfilter/index";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_DATA = "/tyyw/tyywgl/channeltextfilter/data";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_NEW = "/tyyw/tyywgl/channeltextfilter/new";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_CREATE = "/tyyw/tyywgl/channeltextfilter/create";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_VIEW = "/tyyw/tyywgl/channeltextfilter/view";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_UPDATE = "/tyyw/tyywgl/channeltextfilter/update";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_DEL = "/tyyw/tyywgl/channeltextfilter/del";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_AUTOCOMPLETE_CHANNEL = "/tyyw/tyywgl/channeltextfilter/autocomplete/channel";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_AUTOCOMPLETE_FILTERTYPE = "/tyyw/tyywgl/channeltextfilter/autocomplete/filtertype";
	public static final String TYYW_TYYWGL_CHANNELTEXTFILTER_REMOTE_ID = "/tyyw/tyywgl/channeltextfilter/remote/id";

	// 下行内容过滤匹配
	public static final String TYYW_TYYWGL_TEXTFILTERMATCH_INDEX = "/tyyw/tyywgl/textfiltermatch/index";
	public static final String TYYW_TYYWGL_TEXTFILTERMATCH_DATA = "/tyyw/tyywgl/textfiltermatch/data";
	public static final String TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_SP = "/tyyw/tyywgl/textfiltermatch/autocomplete/sp";
	public static final String TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_SP_SERVICECODE = "/tyyw/tyywgl/textfiltermatch/autocomplete/sp/servicecode";
	public static final String TYYW_TYYWGL_TEXTFILTERMATCH_AUTOCOMPLETE_CHANNEL = "/tyyw/tyywgl/textfiltermatch/autocomplete/channel";
	// 上行内容匹配
	public static final String TYYW_TYYWGL_MOCONTENTMATCH_INDEX = "/tyyw/tyywgl/mocontentmatch/index";
	public static final String TYYW_TYYWGL_MOCONTENTMATCH_DATA = "/tyyw/tyywgl/mocontentmatch/data";
	public static final String TYYW_TYYWGL_MOCONTENTMATCH_AUTOCOMPLETE_SP = "/tyyw/tyywgl/mocontentmatch/autocomplete/sp";

	// 下发地址管理
	public static final String RUNMANAGE_ADDRESS_DATA = "/runmanage/address/data";
	public static final String RUNMANAGE_ADDRESS_INDEX = "/runmanage/address/index";
	public static final String RUNMANAGE_ADDRESS_ADD = "/runmanage/address/add";
	public static final String RUNMANAGE_ADDRESS_ADD_DATA = "/runmanage/address/add/data";
	public static final String RUNMANAGE_ADDRESS_UPDATE = "/runmanage/address/update";
	public static final String RUNMANAGE_ADDRESS_UPDATE_DATA = "/runmanage/address/update/data";
	public static final String RUNMANAGE_ADDRESS_DEL = "/runmanage/address/del";
	public static final String RUNMANAGE_ADDRESS_GETNAME = "/runmanage/address/getname";

	// boss实时监控

	public static final String RUNMANAGE_SPREALTIMEMONITOR_QXPTMONITOR_INDEX = "/runmanage/sprealtimemonitor/qxptmonitor/index";
	// sp监控
	public static final String RUNMANAGE_RUNMONITOR_SPMONITOR_INDEX = "/runmanage/sprealtimemonitor/spmonitor/index";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITOR_DATA = "/runmanage/sprealtimemonitor/spmonitor/data";

	// sp监控管理
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_INDEX = "/runmanage/sprealtimemonitor/spmonitormanage/index";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_DATA = "/runmanage/sprealtimemonitor/spmonitormanage/data";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_VIEW = "/runmanage/sprealtimemonitor/spmonitormanage/view";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_NEW = "/runmanage/sprealtimemonitor/spmonitormanage/new";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_DEL = "/runmanage/sprealtimemonitor/spmonitormanage/del";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_CREATE = "/runmanage/sprealtimemonitor/spmonitormanage/create";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_UPDATE = "/runmanage/sprealtimemonitor/spmonitormanage/update";
	public static final String RUNMANAGE_RUNMONITOR_SPMONITORMANAGE_AUTOCOMPLETE_SPGROUP = "/runmanage/sprealtimemonitor/spmonitormanage/autocomplete/spgroup";
	public static final String RUNMANAGE_SPREALTIMEMONITOR_QXPTMONITOR_DATA = "/runmanage/sprealtimemonitor/qxptmonitor/data";

	// spwarnmonitor
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_INDEX = "/runmanage/spwarn/spwarnmonitor/index";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_DATA = "/runmanage/spwarn/spwarnmonitor/data";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_NEW = "/runmanage/spwarn/spwarnmonitor/new";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_CREATE = "/runmanage/spwarn/spwarnmonitor/create";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_VIEW = "/runmanage/spwarn/spwarnmonitor/view";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_UPDATE = "/runmanage/spwarn/spwarnmonitor/update";
	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_DEL = "/runmanage/spwarn/spwarnmonitor/del";

	public static final String RUNMANAGE_SPWARN_SPWARNMONITOR_AUTOCOMPLETE_SPGROUP = "/runmanage/spwarn/spwarnmonitor/autocomplete/spgroup";
	// spgroupmonitor
	public static final String RUNMANAGE_SPGROUP_SPGROUPNMONITOR_INDEX = "/runmanage/spwarn/spgroupmonitor/index";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_DATA = "/runmanage/spwarn/spgroupmonitor/data";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_NEW = "/runmanage/spwarn/spgroupmonitor/new";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_CREATE = "/runmanage/spwarn/spgroupmonitor/create";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_VIEW = "/runmanage/spwarn/spgroupmonitor/view";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_UPDATE = "/runmanage/spwarn/spgroupmonitor/update";
	public static final String RUNMANAGE_SPGROUP_SPGROUPMONITOR_DEL = "/runmanage/spwarn/spgroupmonitor/del";

}