/*
 * $Id: TextFilter.java 172 2009-07-06 07:52:54Z zxhong $ 
 * $Revision: 172 $ 
 * $Date: 2010-09-06 15:52:54 +0800 (星期一, 06 七月 2009) $
 */

package esms.etonenet.boss1069.web.decorate;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * sp,serviceCode,MD5编码的pojo类
 * 
 * @author yzhao
 */
public class SpServiceCodeMD {
	private String spId;
	private String serviceCode;
	private BigInteger cacheKey;

	public SpServiceCodeMD(String spId, String serviceCode) {
		this.spId = spId;
		this.serviceCode = serviceCode;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(spId.getBytes());
			md.update(serviceCode.getBytes());
			cacheKey = new BigInteger(md.digest());
		} catch (NoSuchAlgorithmException e) {
			System.out.println("SpServiceCodeMD MD5 error!");
		}
	}

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public BigInteger getCacheKey() {
		return cacheKey;
	}

	public void setCacheKey(BigInteger cacheKey) {
		this.cacheKey = cacheKey;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SpServiceCodeMD) {
			SpServiceCodeMD spmd = (SpServiceCodeMD) obj;
			return spmd.cacheKey.equals(this.cacheKey);
		}
		return false;

	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return cacheKey.hashCode();
	}

	// @Override
	// public int hashCode() {
	// final int prime = 31;
	// int result = 1;
	// result = prime * result + ((cacheKey == null) ? 0 : cacheKey.hashCode());
	// return result;
	// }
	//
	// @Override
	// public boolean equals(Object obj) {
	// if (this == obj)
	// return true;
	// if (obj == null)
	// return false;
	// if (getClass() != obj.getClass())
	// return false;
	// SpServiceCodeMD other = (SpServiceCodeMD) obj;
	// if (cacheKey == null) {
	// if (other.cacheKey != null)
	// return false;
	// } else if (!cacheKey.equals(other.cacheKey))
	// return false;
	// return true;
	// }

}
