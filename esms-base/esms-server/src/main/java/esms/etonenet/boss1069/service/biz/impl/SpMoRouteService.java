package esms.etonenet.boss1069.service.biz.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpMoRoute;
import esms.etonenet.boss1069.repository.SpMoRouteRepository;

@Service
public class SpMoRouteService {

	@Resource
	private SpMoRouteRepository spMoRouteEm;

	public Page<SpMoRoute> page(PageRequest pageRequest, Specification<SpMoRoute> spec) {

		return spMoRouteEm.findAll(spec, pageRequest);
	}

	public void create(SpMoRoute create) {

		spMoRouteEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpMoRoute update) {

		spMoRouteEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(Long[] spmoids) {

		for (Long spmoid : spmoids) {
			SpMoRoute spmo = new SpMoRoute();
			spmo.setSpMoRouteId(spmoid);
			spMoRouteEm.delete(spmo);
		}
	}
}
