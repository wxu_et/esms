package esms.etonenet.boss1069.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.entity.EsmsStat;
import esms.etonenet.boss1069.enums.monitormanage.MqState;
import esms.etonenet.boss1069.enums.monitormanage.Priority;
import esms.etonenet.boss1069.service.biz.impl.EsmsStatService;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author zhshen 运营监控Controller
 */
@ApiIgnore
@Controller
public class RunMonitorController {

	@Resource
	private EsmsStatService esmsStatService;

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_INDEX)
	public ModelAndView monitorManageIndex() {
		ModelAndView mav = new ModelAndView(TilesUtil.getPath(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_INDEX));
		mav.addObject("mqState", MqState.toDisplayMap());
		mav.addObject("priority", Priority.toDisplayMap());
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_DATA)
	@ResponseBody
	public AjaxPage monitorManageData(Integer pageNumber, Integer pageSize, final EsmsStat search,
			final String sortName, final String sortOrder) {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("mqUrl", search.getMqUrl()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelNumber", search.getChannelNumber()));
		conditions.add(SpecificationCondition.eq("mqState", search.getMqState()));
		SpecificationHelper<EsmsStat> sh = new SpecificationHelper<EsmsStat>(conditions, sortOrder, sortName);
		Page<EsmsStat> p = esmsStatService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_NEW)
	@ResponseBody
	public ModelAndView monitorManageNew() {
		ModelAndView mav = new ModelAndView(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_NEW);
		mav.addObject("mqState", MqState.toDisplayMap());
		mav.addObject("priority", Priority.toDisplayMap());
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_CREATE)
	@ResponseBody
	public String monitorManageCreate(EsmsStat create,
			@RequestParam(required = false, value = "isMtCarrier") String isMtCarrier) {
		if (isMtCarrier != null) {
			create.setChannelNumber("mt_carrier_" + create.getChannelNumber());
		}
		if (esmsStatService.create(create) != null)
			return "新建成功";
		return "新建失败";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_DEL)
	@ResponseBody
	public String monitorManageDel(@RequestBody List<EsmsStat> data) {
		if (data.size() > 0) {
			esmsStatService.del(data);
			return "删除成功";
		}
		return "没有选中数据";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_VIEW)
	public ModelAndView monitorManageView(String id) {
		ModelAndView mav = new ModelAndView(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_VIEW);
		EsmsStat es = esmsStatService.findOne(Long.valueOf(id.trim()));
		mav.addObject("es", es);
		mav.addObject("mqState", MqState.toDisplayMap());
		mav.addObject("priority", Priority.toDisplayMap());
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_RUNMONITOR_MONITORMANAGE_UPDATE)
	@ResponseBody
	public String monitorManageUpdate(EsmsStat update) {
		if (esmsStatService.update(update) != null)
			return "修改成功";
		return "修改失败";
	}

}
