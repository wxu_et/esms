package esms.etonenet.boss1069.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebMvc
@EnableSwagger2
@Service
@ComponentScan("esms.etonenet.boss1069.controller.api.oauth")
public class SwaggerConfig {

	/**
	 * Every Docket bean is picked up by the swagger-mvc framework - allowing
	 * for multiple swagger groups i.e. same code base multiple swagger resource
	 * listings.
	 */
	@Bean
	public Docket customDocket() {
		ApiInfo ai = new ApiInfo("boss平台 api", "", "", "", "", "", "");
		Set<String> s = new HashSet<String>();
		s.add(MediaType.APPLICATION_JSON_VALUE);
		return new Docket(DocumentationType.SWAGGER_2).produces(s).apiInfo(ai);
	}

}