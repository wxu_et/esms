package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.ChannelGroup;
import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.service.biz.impl.ChannelGroupService;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "通道组定义" }, value = "通道组接口", description = "ChannelGroupController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "channelgroup")
public class ChannelGroupController {

	@Resource
	private ChannelGroupService channelGroupService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.GET)
	@ResponseBody
	public AjaxPage channelGroupData(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.eq("logicStat", LogicStat.NORMAL.getValue()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelGroupId", param.getChannelGroupId()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelGroupName", param.getChannelGroupName()));

		SpecificationHelper<ChannelGroup> sh = new SpecificationHelper<ChannelGroup>(conditions, param.getSortOrder(),
				param.getSortName());

		Page<ChannelGroup> p = channelGroupService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void create(@ModelAttribute @Valid ChannelGroup create) throws Exception {

		if (channelGroupService.findOne(create.getChannelGroupId()) == null)
			channelGroupService.create(create);
		else
			throw new Exception("该ID已存在!");
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ChannelGroup get(@RequestParam String id) {

		return channelGroupService.findOne(id);
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.PUT)
	@ResponseBody
	public void update(@ModelAttribute @Valid ChannelGroup update) {

		channelGroupService.update(update);
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@RequestBody @Valid List<ChannelGroup> data) {

		channelGroupService.del(data);
	}

	@ApiOperation(value = "转义")
	@RequestMapping(value = "auto", method = RequestMethod.GET)
	@ResponseBody
	public List<Autocomplete> autoCgcChannelGroup(@RequestParam(required = true) String term) {

		return AutocompleteUtil.object2auto(channelGroupService.auto(term));
	}

	private static final class PageParam extends BasePageRequestParam {

		private String channelGroupId;

		private String channelGroupName;

		public String getChannelGroupId() {
			return channelGroupId;
		}

		public void setChannelGroupId(String channelGroupId) {
			this.channelGroupId = channelGroupId;
		}

		public String getChannelGroupName() {
			return channelGroupName;
		}

		public void setChannelGroupName(String channelGroupName) {
			this.channelGroupName = channelGroupName;
		}

	}

}
