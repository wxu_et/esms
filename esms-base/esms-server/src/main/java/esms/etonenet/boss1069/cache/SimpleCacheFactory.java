package esms.etonenet.boss1069.cache;

public class SimpleCacheFactory {
	private static ISimpleCache cache = null;

	/**
	 * 获取caches指定的缓存
	 * 
	 * @param caches
	 * @return
	 */
	public static ISimpleCache getCacheInstance(Class<?> caches) {
		if (cache == null) {
			try {
				cache = (ISimpleCache) caches.newInstance();
			} catch (InstantiationException e) {
				// System.out.println("指定的缓存类有误,caches参数必须是ICache的实现类");
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// System.out.println("指定的缓存类有误,caches参数必须是ICache的实现类");
				e.printStackTrace();
			}
		}
		return cache;
	}

	/**
	 * 获取系统默认的缓存
	 * 
	 * @return
	 */
	public static ISimpleCache getDefaultCache() {
		if (cache == null) {
			cache = new DefaultSimpleCache();
		} else if (!(cache instanceof DefaultSimpleCache)) {
			cache = new DefaultSimpleCache();
		}
		return cache;
	}

	public static void main(String[] args) {
		ISimpleCache cache = SimpleCacheFactory.getDefaultCache();
		if (cache.contains("area")) {
			System.out.println(cache.get("area"));
		} else {
			cache.insert("area", "福州", 120);
		}

	}
}
