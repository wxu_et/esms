package esms.etonenet.boss1069.config;

import org.springframework.beans.factory.config.MethodInvokingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;

import com.etonenet.util.SpringUtil;

@Configuration
public class BeanConfig {

	/**
	 * 用于http rest请求
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {

		return new RestTemplate();
	}

	/**
	 * 获得spring bean util类
	 * 
	 * @return
	 */
	@Bean
	public SpringUtil springUtil() {

		return new SpringUtil();
	}

	/**
	 * spring security安全继承当前线程
	 * 
	 * @return
	 */
	// @Bean
	public MethodInvokingBean methodInvokingBean() {

		MethodInvokingBean mib = new MethodInvokingBean();
		mib.setTargetClass(SecurityContextHolder.class);
		mib.setTargetMethod("setStrategyName");
		String[] args = { SecurityContextHolder.MODE_INHERITABLETHREADLOCAL };
		mib.setArguments(args);
		return mib;
	}

	public static void main(String[] args) {
		MethodInvokingBean mib = new MethodInvokingBean();
		mib.setTargetClass(SecurityContextHolder.class);
		mib.setTargetMethod("setStrategyName");
		String[] args2 = { SecurityContextHolder.MODE_INHERITABLETHREADLOCAL };
		mib.setArguments(args2);

		try {
			mib.prepare();
			mib.invoke();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
