package esms.etonenet.boss1069.service.biz.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import esms.etonenet.boss1.repository.SpRealtimeMonitorBoss1Repository;
import esms.etonenet.boss10.repository.SpRealtimeMonitorBoss10Repository;
import esms.etonenet.boss2.repository.SpRealtimeMonitorBoss2Repository;
import esms.etonenet.boss3.repository.SpRealtimeMonitorBoss3Repository;
import esms.etonenet.boss4.repository.SpRealtimeMonitorBoss4Repository;
import esms.etonenet.boss5.repository.SpRealtimeMonitorBoss5Repository;
import esms.etonenet.boss6.repository.SpRealtimeMonitorBoss6Repository;
import esms.etonenet.boss7.repository.SpRealtimeMonitorBoss7Repository;
import esms.etonenet.boss8.repository.SpRealtimeMonitorBoss8Repository;
import esms.etonenet.boss9.repository.SpRealtimeMonitorBoss9Repository;

public class SprealtimeMonitorHandleService {

	private Logger logger = LoggerFactory.getLogger(SprealtimeMonitorHandleService.class);

	public static final Long ORACLE_EXCEPTION = -955L;

	private ReadWriteLock refreshLock = new ReentrantReadWriteLock();

	private Hashtable<String, Long> bosshashtable = new Hashtable<String, Long>();

	@Resource
	private SpRealtimeMonitorBoss1Repository spRealtimeMonitorBoss1Repository;

	@Resource
	private SpRealtimeMonitorBoss2Repository spRealtimeMonitorBoss2Repository;

	@Resource
	private SpRealtimeMonitorBoss3Repository spRealtimeMonitorBoss3Repository;

	@Resource
	private SpRealtimeMonitorBoss4Repository spRealtimeMonitorBoss4Repository;

	@Resource
	private SpRealtimeMonitorBoss5Repository spRealtimeMonitorBoss5Repository;

	@Resource
	private SpRealtimeMonitorBoss6Repository spRealtimeMonitorBoss6Repository;

	@Resource
	private SpRealtimeMonitorBoss7Repository spRealtimeMonitorBoss7Repository;

	@Resource
	private SpRealtimeMonitorBoss8Repository spRealtimeMonitorBoss8Repository;

	@Resource
	private SpRealtimeMonitorBoss9Repository spRealtimeMonitorBoss9Repository;

	@Resource
	private SpRealtimeMonitorBoss10Repository spRealtimeMonitorBoss10Repository;

	private Integer selectCount;

	private Integer stepWaitTime;

	private boolean boss1 = true; // boolean boss1 = true 时，去查询boss1数据，false不查询

	private boolean boss2 = true; // boolean boss2 = true 时，去查询boss2数据，false不查询

	private boolean boss3 = true; // boolean boss3 = true 时，去查询boss3数据，false不查询

	private boolean boss4 = true; // boolean boss4 = true 时，去查询boss4数据，false不查询

	private boolean boss5 = true; // boolean boss5 = true 时，去查询boss5数据，false不查询

	private boolean boss6; // boolean boss6 = true 时，去查询boss6数据，false不查询

	private boolean boss7; // boolean boss7 = true 时，去查询boss7数据，false不查询

	private boolean boss8; // boolean boss8 = true 时，去查询boss8数据，false不查询

	private boolean boss9; // boolean boss9 = true 时，去查询boss9数据，false不查询

	private boolean boss10; // boolean boss10 = true 时，去查询boss10数据，false不查询

	private final static String boss1mt = "boss1mt";
	private final static String boss2mt = "boss2mt";
	private final static String boss3mt = "boss3mt";
	private final static String boss4mt = "boss4mt";
	private final static String boss5mt = "boss5mt";
	private final static String boss6mt = "boss6mt";
	private final static String boss7mt = "boss7mt";
	private final static String boss8mt = "boss8mt";
	private final static String boss9mt = "boss9mt";
	private final static String boss10mt = "boss10mt";
	private final static String bossmt = "bossmt";

	@PostConstruct
	public void initialize() {
		// 初始化数据，启示数据都为0
		bosshashtable.put(boss1mt, 0L);
		bosshashtable.put(boss2mt, 0L);
		bosshashtable.put(boss3mt, 0L);
		bosshashtable.put(boss4mt, 0L);
		bosshashtable.put(boss5mt, 0L);
		bosshashtable.put(boss6mt, 0L);
		bosshashtable.put(boss7mt, 0L);
		bosshashtable.put(boss8mt, 0L);
		bosshashtable.put(boss9mt, 0L);
		bosshashtable.put(boss10mt, 0L);
		bosshashtable.put(bossmt, 0L);

	}

	public void run() {

		try {

			Hashtable<String, Long> hashtable = new Hashtable<String, Long>();

			if (boss1) {
				Long boss1MtTotal = getboss1MtTotal().get(0);
				logger.info("---->boss1MtSize={}", boss1MtTotal);
				hashtable.put(boss1mt, boss1MtTotal);
			}

			if (boss2) {
				Long boss2MtTotal = getboss2MtTotal().get(0);
				logger.info("---->boss2MtSize={}", boss2MtTotal);
				hashtable.put(boss2mt, boss2MtTotal);
			}

			if (boss3) {
				Long boss3MtTotal = getboss3MtTotal().get(0);
				logger.info("---->boss3MtSize={}", boss3MtTotal);
				hashtable.put(boss3mt, boss3MtTotal);

			}

			if (boss4) {
				Long boss4MtTotal = getboss4MtTotal().get(0);
				logger.info("---->boss4MtSize={}", boss4MtTotal);
				hashtable.put(boss4mt, boss4MtTotal);
			}

			if (boss5) {
				Long boss5MtTotal = getboss5MtTotal().get(0);
				logger.info("---->boss5MtSize={}", boss5MtTotal);
				hashtable.put(boss5mt, boss5MtTotal);
			}

			if (boss6) {
				Long boss6MtTotal = getboss6MtTotal().get(0);
				logger.info("---->boss6MtSize={}", boss6MtTotal);
				hashtable.put(boss6mt, boss6MtTotal);
			}

			if (boss7) {
				Long boss7MtTotal = getboss7MtTotal().get(0);
				logger.info("---->boss7MtSize={}", boss7MtTotal);
				hashtable.put(boss7mt, boss7MtTotal);
			}

			if (boss8) {
				Long boss8MtTotal = getboss8MtTotal().get(0);
				logger.info("---->boss8MtSize={}", boss8MtTotal);
				hashtable.put(boss8mt, boss8MtTotal);
			}

			if (boss9) {
				Long boss9MtTotal = getboss9MtTotal().get(0);
				logger.info("---->boss9MtSize={}", boss9MtTotal);
				hashtable.put(boss9mt, boss9MtTotal);
			}

			if (boss10) {
				Long boss10MtTotal = getboss10MtTotal().get(0);
				logger.info("---->boss10MtSize={}", boss10MtTotal);
				hashtable.put(boss10mt, boss10MtTotal);
			}

			put(hashtable);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	// 获取boss1MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss1MtTotal() {
		List<Long> boss1List = new ArrayList<Long>();
		boolean boss1flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss1flag) {
				try {
					Long boss1MtTotal = getBoss1MtTotal();
					boss1flag = false;
					boss1List.add(boss1MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss1 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss1MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss1List.add(boss1MtTotal);
				}

			}
		}

		return boss1List;

	}

	// 获取boss2MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss2MtTotal() {

		List<Long> boss2List = new ArrayList<Long>();
		boolean boss2flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss2flag) {
				try {
					Long boss2MtTotal = getBoss2MtTotal();
					boss2flag = false;
					boss2List.add(boss2MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss2 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss2MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss2List.add(boss2MtTotal);
				}

			}
		}
		return boss2List;

	}

	// 获取boss3MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss3MtTotal() {
		List<Long> boss3List = new ArrayList<Long>();
		boolean boss3flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss3flag) {
				try {
					Long boss3MtTotal = getBoss3MtTotal();
					boss3flag = false;
					boss3List.add(boss3MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss3 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss3MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss3List.add(boss3MtTotal);
				}

			}
		}
		return boss3List;
	}

	// 获取boss4MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss4MtTotal() {
		List<Long> boss4List = new ArrayList<Long>();
		boolean boss4flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss4flag) {
				try {
					Long boss4MtTotal = getBoss4MtTotal();
					boss4flag = false;
					boss4List.add(boss4MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss4 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss4MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss4List.add(boss4MtTotal);
				}

			}
		}
		return boss4List;
	}

	// 获取boss5MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss5MtTotal() {

		List<Long> boss5List = new ArrayList<Long>();
		boolean boss5flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss5flag) {
				try {
					Long boss5MtTotal = getBoss5MtTotal();
					boss5flag = false;
					boss5List.add(boss5MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss5 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss5MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss5List.add(boss5MtTotal);
				}

			}
		}
		return boss5List;
	}

	// 获取boss6MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss6MtTotal() {

		List<Long> boss6List = new ArrayList<Long>();

		boolean boss6flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss6flag) {
				try {
					Long boss6MtTotal = getBoss6MtTotal();
					boss6flag = false;
					boss6List.add(boss6MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss1 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss6MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss6List.add(boss6MtTotal);
				}

			}
		}
		return boss6List;
	}

	// 获取boss7MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss7MtTotal() {

		List<Long> boss7List = new ArrayList<Long>();

		boolean boss7flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss7flag) {
				try {
					Long boss7MtTotal = getBoss7MtTotal();
					boss7flag = false;
					boss7List.add(boss7MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss7 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss7MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss7List.add(boss7MtTotal);
				}

			}
		}
		return boss7List;
	}

	// 获取boss8MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss8MtTotal() {

		List<Long> boss8List = new ArrayList<Long>();

		boolean boss8flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss8flag) {
				try {
					Long boss8MtTotal = getBoss8MtTotal();
					boss8flag = false;
					boss8List.add(boss8MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss8 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss8MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss8List.add(boss8MtTotal);
				}

			}
		}
		return boss8List;
	}

	// 获取boss9MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss9MtTotal() {

		List<Long> boss9List = new ArrayList<Long>();

		boolean boss9flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss9flag) {
				try {
					Long boss9MtTotal = getBoss9MtTotal();
					boss9flag = false;
					boss9List.add(boss9MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss9 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss9MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss9List.add(boss9MtTotal);
				}

			}
		}
		return boss9List;
	}

	// 获取boss10MtTotal 如果查询数据库抛出异常，一秒后再次去查询一次数据库，如果再次发生异常
	public List<Long> getboss10MtTotal() {

		List<Long> boss10List = new ArrayList<Long>();

		boolean boss10flag = true;
		for (int i = 0; i < selectCount; i++) {
			if (boss10flag) {
				try {
					Long boss10MtTotal = getBoss10MtTotal();
					boss10flag = false;
					boss10List.add(boss10MtTotal);
					break;
				} catch (Exception e) {
					logger.info("---------->boss10 select oracle happen exception,try  again,exception={}",
							e.getMessage());
					try {
						Thread.sleep(stepWaitTime);
					} catch (InterruptedException e1) {
						logger.info(e1.getMessage());
					}
					Long boss10MtTotal = ORACLE_EXCEPTION; // 2次查询数据库都发生异常，将boss1MtTotal设为负值，表示不写入，用之前缓存的值代替更新缓存
					boss10List.add(boss10MtTotal);
				}

			}
		}
		return boss10List;
	}

	public void put(Hashtable<String, Long> hashtable) {

		refreshLock.writeLock().lock();
		Long bossMtTotal = 0L;
		for (String key : hashtable.keySet()) {
			if (hashtable.get(key) == SprealtimeMonitorHandleService.ORACLE_EXCEPTION)
				continue;

			bosshashtable.put(key, hashtable.get(key));

		}
		for (String key : bosshashtable.keySet()) {
			if (key != bossmt) {
				bossMtTotal = bossMtTotal + bosshashtable.get(key);
			}
		}
		bosshashtable.put(bossmt, bossMtTotal);

		refreshLock.writeLock().unlock();

	}

	public Hashtable<String, Long> get() {
		refreshLock.readLock().lock();
		refreshLock.readLock().unlock();
		return bosshashtable;

	}

	public Long getBoss1MtTotal() {

		return spRealtimeMonitorBoss1Repository.getBossMtTotal();
	}

	public Long getBoss2MtTotal() {

		return spRealtimeMonitorBoss2Repository.getBossMtTotal();
	}

	public Long getBoss3MtTotal() {

		return spRealtimeMonitorBoss3Repository.getBossMtTotal();
	}

	public Long getBoss4MtTotal() {

		return spRealtimeMonitorBoss4Repository.getBossMtTotal();
	}

	public Long getBoss5MtTotal() {

		return spRealtimeMonitorBoss5Repository.getBossMtTotal();
	}

	public Long getBoss6MtTotal() {

		return spRealtimeMonitorBoss6Repository.getBossMtTotal();
	}

	public Long getBoss7MtTotal() {

		return spRealtimeMonitorBoss7Repository.getBossMtTotal();
	}

	public Long getBoss8MtTotal() {

		return spRealtimeMonitorBoss8Repository.getBossMtTotal();
	}

	public Long getBoss9MtTotal() {

		return spRealtimeMonitorBoss9Repository.getBossMtTotal();
	}

	public Long getBoss10MtTotal() {

		return spRealtimeMonitorBoss10Repository.getBossMtTotal();
	}

	public void setBoss1(boolean boss1) {
		this.boss1 = boss1;
	}

	public void setBoss2(boolean boss2) {
		this.boss2 = boss2;
	}

	public void setBoss3(boolean boss3) {
		this.boss3 = boss3;
	}

	public void setBoss4(boolean boss4) {
		this.boss4 = boss4;
	}

	public void setBoss5(boolean boss5) {
		this.boss5 = boss5;
	}

	public void setBoss6(boolean boss6) {
		this.boss6 = boss6;
	}

	public void setBoss7(boolean boss7) {
		this.boss7 = boss7;
	}

	public void setBoss8(boolean boss8) {
		this.boss8 = boss8;
	}

	public void setBoss9(boolean boss9) {
		this.boss9 = boss9;
	}

	public void setBoss10(boolean boss10) {
		this.boss10 = boss10;
	}

	public void setStepWaitTime(Integer stepWaitTime) {
		this.stepWaitTime = stepWaitTime;
	}

	public Integer getSelectCount() {
		return selectCount;
	}

	public void setSelectCount(Integer selectCount) {
		this.selectCount = selectCount;
	}

}
