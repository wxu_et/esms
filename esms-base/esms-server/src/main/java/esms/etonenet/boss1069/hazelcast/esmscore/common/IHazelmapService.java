package esms.etonenet.boss1069.hazelcast.esmscore.common;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface IHazelmapService<K, V> {

	public void init();

	public int syn(K key);

	public int del(K key);

	public void synAll(boolean reload);

	public Map<K, V> getMap();

}
