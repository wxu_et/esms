package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.entity.ChannelBaobeiInfo;
import esms.etonenet.boss1069.repository.ChannelBaobeiInfoRepository;

@Service
public class ChannelBaobeiInfoService {

	@Resource
	private ChannelBaobeiInfoRepository channelBaobeiInfoEm;

	public Page<ChannelBaobeiInfo> page(PageRequest pageRequest, Specification<ChannelBaobeiInfo> spec) {

		return channelBaobeiInfoEm.findAll(spec, pageRequest);
	}

	public void create(ChannelBaobeiInfo create) {

		Channel ch = new Channel();
		ch.setChannelId(create.getChannel().getChannelId().split("-")[0]);
		create.setChannel(ch);
		channelBaobeiInfoEm.save(create);
	}

	public ChannelBaobeiInfo findOne(Long id) {

		return channelBaobeiInfoEm.findOne(id);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(ChannelBaobeiInfo update) {
		Channel ch = new Channel();
		ch.setChannelId(update.getChannel().getChannelId().split("-")[0]);
		update.setChannel(ch);
		channelBaobeiInfoEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<ChannelBaobeiInfo> dels) {

		for (ChannelBaobeiInfo chbb : dels)
			channelBaobeiInfoEm.delete(chbb);
	}
}
