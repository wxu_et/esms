package esms.etonenet.boss1069.controller.api.oauth;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.MathUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.SpService;
import esms.etonenet.boss1069.service.biz.impl.SpServiceService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "SpService" })
@ApiIgnore
@Controller("SpServiceController")
@RequestMapping(ApiConstants.BASE_PATH + "spservice")
public class SpServiceController {

	@Resource
	private SpServiceService spServiceService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage spServiceData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("id.spId", StringUtil.splitMinusFirstGroup(param.getSp())));
		conditions.add(SpecificationCondition.eq("id.spServiceCode", param.getSpServiceCode()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spServiceName", param.getSpServiceName()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("contentPostfixPattern",
				param.getContentPostfixPattern()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("priorityPattern", param.getPriorityPattern()));
		List<String> sortNames = new ArrayList<String>();
		if ("id.spId".equals(param.getSortName()) || "id.spServiceCode".equals(param.getSortName())) {
			sortNames.add("id.spId");
			sortNames.add("id.spServiceCode");
		} else
			sortNames.add(param.getSortName());
		SpecificationHelper<esms.etonenet.boss1069.entity.SpService> sh = new SpecificationHelper<>(conditions,
				param.getSortOrder(), sortNames);
		Page<SpService> p = spServiceService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String spServiceCreate(@ModelAttribute @Valid SpService create, String spIdName,
			@RequestParam Integer[] msisdnFilterPolicyCheckbox,
			@RequestParam Integer[] contentFilterCategoryFlagCheckbox, Integer cpp0, Integer cpp1, Integer cpp2,
			Integer cpp3, Integer cpp4, Integer cpp5) {
		create.getId().setSpId(spIdName.split("-")[0]);
		create.setMsisdnFilterPolicy(MathUtil.sum(msisdnFilterPolicyCheckbox));
		create.setContentFilterCategoryFlag(MathUtil.sum(contentFilterCategoryFlagCheckbox));
		create.setContentProcessPolicy(BigDecimal.valueOf(cpp0 + cpp1 + cpp2 + cpp3 + cpp4 + cpp5));
		spServiceService.create(create);
		return "新建成功";
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String spServiceUpdate(@ModelAttribute @Valid SpService update,
			@RequestParam Integer[] msisdnFilterPolicyCheckbox,
			@RequestParam Integer[] contentFilterCategoryFlagCheckbox, Integer cpp0, Integer cpp1, Integer cpp2,
			Integer cpp3, Integer cpp4, Integer cpp5) {
		update.setMsisdnFilterPolicy(MathUtil.sum(msisdnFilterPolicyCheckbox));
		update.setContentFilterCategoryFlag(MathUtil.sum(contentFilterCategoryFlagCheckbox));
		update.setContentProcessPolicy(BigDecimal.valueOf(cpp0 + cpp1 + cpp2 + cpp3 + cpp4 + cpp5));
		spServiceService.update(update);
		return "更新成功";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String sp;
		private String spServiceCode;
		private String spServiceName;
		private String priorityPattern;
		private String contentPostfixPattern;

		public String getSp() {
			return sp;
		}

		public void setSp(String sp) {
			this.sp = sp;
		}

		public String getSpServiceCode() {
			return spServiceCode;
		}

		public void setSpServiceCode(String spServiceCode) {
			this.spServiceCode = spServiceCode;
		}

		public String getSpServiceName() {
			return spServiceName;
		}

		public void setSpServiceName(String spServiceName) {
			this.spServiceName = spServiceName;
		}

		public String getPriorityPattern() {
			return priorityPattern;
		}

		public void setPriorityPattern(String priorityPattern) {
			this.priorityPattern = priorityPattern;
		}

		public String getContentPostfixPattern() {
			return contentPostfixPattern;
		}

		public void setContentPostfixPattern(String contentPostfixPattern) {
			this.contentPostfixPattern = contentPostfixPattern;
		}
	}

}
