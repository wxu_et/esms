package esms.etonenet.boss1069.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.MathUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.entity.Channel;
import esms.etonenet.boss1069.entity.ChannelBaobeiInfo;
import esms.etonenet.boss1069.enums.LongSmsFlag;
import esms.etonenet.boss1069.enums.WhiteListPolicy;
import esms.etonenet.boss1069.enums.channel.ChannelBaobeiFlag;
import esms.etonenet.boss1069.enums.channel.ChannelMtmoFlag;
import esms.etonenet.boss1069.enums.channel.ChannelState;
import esms.etonenet.boss1069.enums.channelbb.BaobeiDataState;
import esms.etonenet.boss1069.repository.ChannelBaobeiInfoRepository;
import esms.etonenet.boss1069.repository.ChannelRepository;
import esms.etonenet.boss1069.service.biz.impl.ChannelBaobeiInfoService;
import esms.etonenet.boss1069.service.biz.impl.ChannelService;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class TdglController {

	@Resource
	private ChannelRepository channelEm;

	@Resource
	private ChannelBaobeiInfoRepository channelbbEm;

	@Resource
	private ChannelService channelService;

	@Resource
	private ChannelBaobeiInfoService channelbbService;

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_INDEX)
	public ModelAndView channelIndex() {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.BD_TDGL_CHANNEL_INDEX));
		mv.addObject("channelState", ChannelState.toDisplayMap());
		mv.addObject("whiteListPolicy", WhiteListPolicy.toDisplayMap());
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		mv.addObject("channelBaobeiFlag", ChannelBaobeiFlag.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_DATA)
	@ResponseBody
	public AjaxPage channelData(Integer pageNumber, Integer pageSize, final Channel search, final String sortName,
			final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelId", search.getChannelId()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelNumber", search.getChannelNumber()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelMessageSignature",
				search.getChannelMessageSignature()));
		conditions.add(SpecificationCondition.eq("channelState", search.getChannelState()));
		conditions.add(SpecificationCondition.eq("whiteListPolicy", search.getWhiteListPolicy()));
		conditions.add(SpecificationCondition.eq("channelBaobeiFlag", search.getChannelBaobeiFlag()));

		SpecificationHelper<Channel> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<?> p = channelService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_NEW)
	public ModelAndView channelNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.BD_TDGL_CHANNEL_NEW);
		mv.addObject("channelState", ChannelState.toDisplayMap());
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		mv.addObject("whiteListPolicy", WhiteListPolicy.toDisplayMap());
		mv.addObject("channelMtmoFlag", ChannelMtmoFlag.toDisplayMap());
		mv.addObject("channelBaobeiFlag", ChannelBaobeiFlag.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_CREATE)
	@ResponseBody
	public String channelCreate(Channel create, Integer[] channelMtmoFlagCheckbox) {

		create.setChannelMtmoFlag(MathUtil.sum(channelMtmoFlagCheckbox));
		channelService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_VIEW)
	public ModelAndView channelView(String channelId) {

		ModelAndView mv = new ModelAndView(UrlConstants.BD_TDGL_CHANNEL_VIEW);
		mv.addObject("ch", channelEm.findOne(channelId));
		mv.addObject("channelState", ChannelState.toDisplayMap());
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		mv.addObject("whiteListPolicy", WhiteListPolicy.toDisplayMap());
		mv.addObject("channelMtmoFlag", ChannelMtmoFlag.toDisplayMap());
		mv.addObject("channelBaobeiFlag", ChannelBaobeiFlag.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_UPDATE)
	@ResponseBody
	public String channelUpdate(Channel update, Integer[] channelMtmoFlagCheckbox) {

		update.setChannelMtmoFlag(MathUtil.sum(channelMtmoFlagCheckbox));
		channelService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_INDEX)
	public ModelAndView channelbbIndex() {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_INDEX));
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_DATA)
	@ResponseBody
	public AjaxPage channelbbData(Integer pageNumber, Integer pageSize, final String channel,
			final String baobeiNumberSearch, final String channelMessageSignature, final String sortName,
			final String sortOrder) {

		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.eq("channel.channelId", StringUtil.splitMinusFirstGroup(channel)));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("baobeiNumber", baobeiNumberSearch));
		conditions
				.add(SpecificationCondition.equalOrLikeIgnoreCase("channelMessageSignature", channelMessageSignature));

		SpecificationHelper<ChannelBaobeiInfo> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

		Page<?> p = channelbbService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_NEW)
	public ModelAndView channelbbNew() {

		ModelAndView mv = new ModelAndView(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_NEW);
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		mv.addObject("bbDataState", BaobeiDataState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_CREATE)
	@ResponseBody
	public String channelbbCreate(ChannelBaobeiInfo create, String channelIdName) {

		Channel ch = new Channel();
		ch.setChannelId(channelIdName.split("-")[0]);
		create.setChannel(ch);

		channelbbService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_VIEW)
	public ModelAndView channelbbView(Long channelBaobeiInfoId) {

		ModelAndView mv = new ModelAndView(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_VIEW);

		mv.addObject("chbb", channelbbEm.findOne(channelBaobeiInfoId));
		mv.addObject("longSmsFlag", LongSmsFlag.toDisplayMap());
		mv.addObject("bbDataState", BaobeiDataState.toDisplayMap());
		return mv;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_UPDATE)
	@ResponseBody
	public String channelbbUpdate(ChannelBaobeiInfo update, String channelIdName) {

		Channel ch = new Channel();
		ch.setChannelId(channelIdName.split("-")[0]);
		update.setChannel(ch);

		channelbbService.update(update);
		return "更新成功";
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_DEL)
	@ResponseBody
	public String channelbbDel(@RequestBody List<ChannelBaobeiInfo> data) {

		channelbbService.del(data);
		return "删除成功";
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_IDCHECK)
	@ResponseBody
	public boolean channelbbIdCheck(String baobeiNumber, String channelIdName) {

		if (StringUtil.isBlank(baobeiNumber) || StringUtil.isBlank(channelIdName))
			return true;

		if (channelbbEm.idcheck(baobeiNumber, channelIdName.split("-")[0]) == null)
			return true;
		else
			return false;
	}

	@RequestMapping(UrlConstants.BD_TDGL_CHANNEL_BAOBEI_AUTOCOMPLETE_CHANNEL)
	@ResponseBody
	public List<Autocomplete> autoChannel(String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannelbb(term));
	}

}
