package esms.etonenet.boss1069.util;

import java.util.ArrayList;
import java.util.List;

import esms.etonenet.boss1069.web.Autocomplete;

public class AutocompleteUtil {

	public static List<Autocomplete> object2auto(List<Object[]> objs) {

		List<Autocomplete> acs = new ArrayList<>();

		if (objs == null || objs.isEmpty())
			return acs;

		for (Object[] obj : objs) {
			Autocomplete ac = new Autocomplete();
			ac.setLabel(obj[0].toString());
			ac.setValue(obj[1].toString());

			acs.add(ac);
		}
		return acs;
	}
}
