package esms.etonenet.boss1069.service.biz.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import esms.etonenet.boss1069.mtmo.Mt;
import esms.etonenet.boss1069.mtmo.MtRepository;

@Service
public class MtService {

	@Resource
	private MtRepository ttMtRe;

	public Page<Mt> page(Specification<Mt> spec, Pageable pageable) {

		return ttMtRe.findAll(spec, pageable);
	}

	public Mt findOne(Long spMtId) {

		return ttMtRe.findOne(spMtId);
	}

}
