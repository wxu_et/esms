package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.ChannelGroupConfig;
import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.service.biz.impl.ChannelGroupConfigService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "通道组配置定义" }, value = "通道组配置接口", description = "ChannelGroupConfigController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "channelgroupconfig")
public class ChannelGroupConfigController {

	@Resource
	private ChannelGroupConfigService channelGroupConfigService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage channelGroupConfigData(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.eq("logicStat", LogicStat.NORMAL.getValue()));
		conditions.add(SpecificationCondition.eq("channelGroup.channelGroupId",
				StringUtil.splitMinusFirstGroup(param.getChannelGroupIdName())));
		conditions.add(SpecificationCondition.eq("channel.channelId",
				StringUtil.splitMinusFirstGroup(param.getChannelIdName())));

		SpecificationHelper<ChannelGroupConfig> sh = new SpecificationHelper<ChannelGroupConfig>(conditions, param);

		Page<ChannelGroupConfig> p = channelGroupConfigService.page(new PageRequest(param), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void create(@ModelAttribute @Valid ChannelGroupConfig create) {

		channelGroupConfigService.create(create);
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.POST)
	@ResponseBody
	public ChannelGroupConfig get(@RequestParam Long id) {

		return channelGroupConfigService.findOne(id);
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public void update(@ModelAttribute @Valid ChannelGroupConfig update) {

		channelGroupConfigService.update(update);
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@RequestBody @Valid List<ChannelGroupConfig> data) {

		channelGroupConfigService.del(data);
	}

	private final static class PageParam extends BasePageRequestParam {

		private String channelGroupIdName;

		private String channelIdName;

		public String getChannelGroupIdName() {
			return channelGroupIdName;
		}

		public void setChannelGroupIdName(String channelGroupIdName) {
			this.channelGroupIdName = channelGroupIdName;
		}

		public String getChannelIdName() {
			return channelIdName;
		}

		public void setChannelIdName(String channelIdName) {
			this.channelIdName = channelIdName;
		}

	}
}
