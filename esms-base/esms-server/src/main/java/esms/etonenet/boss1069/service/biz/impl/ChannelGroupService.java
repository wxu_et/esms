package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.ChannelGroup;
import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.repository.ChannelGroupRepository;

@Service
public class ChannelGroupService {

	@Resource
	private ChannelGroupRepository channelGroupEm;

	public Page<ChannelGroup> page(PageRequest pageRequest, Specification<ChannelGroup> spec) {

		return channelGroupEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(ChannelGroup create) {

		create.setLogicStat(LogicStat.NORMAL.getValue());
		channelGroupEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(ChannelGroup update) {

		update.setLogicStat(LogicStat.NORMAL.getValue());
		channelGroupEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<ChannelGroup> dels) {

		for (ChannelGroup cg : dels)
			channelGroupEm.del(cg.getChannelGroupId());
	}

	public ChannelGroup findOne(String id) {

		return channelGroupEm.findOne(id);
	}

	public List<Object[]> auto(String term) {

		return channelGroupEm.auto(term);
	}
}
