package esms.etonenet.boss1069.controller.api.oauth;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.entity.system.security.UserBs;
import com.etonenet.service.system.UserBsService;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = { "用户相关操作" })
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "user")
public class UserBsController {

	@Resource
	private UserBsService userService;

	@ApiOperation(value = "同步单例用户")
	@RequestMapping(value = "syn", method = RequestMethod.POST)
	@ResponseBody
	public UserBs page(@RequestBody @Valid UserBs user) {

		return userService.synUser(user);
	}
}
