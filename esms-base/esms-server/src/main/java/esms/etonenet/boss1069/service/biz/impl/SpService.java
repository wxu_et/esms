package esms.etonenet.boss1069.service.biz.impl;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.repository.SpRepository;

@Service
public class SpService {

	@Resource
	private SpRepository spEm;

	public Page<Sp> page(PageRequest pageRequest, Specification<Sp> spec) {

		return spEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(Sp create) {

		// quotaPolicy全功能隐藏(包括创建,修改,查询). 新建时默认填写1,编辑操作不做修改.
		create.setQuotaPolicy(BigDecimal.valueOf(1));

		spEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(Sp update) {

		// quotaPolicy全功能隐藏(包括创建,修改,查询). 新建时默认填写1,编辑操作不做修改.
		Sp spOld = spEm.findOne(update.getSpId());
		update.setQuotaPolicy(spOld.getQuotaPolicy());
		update.setUserId(spOld.getUserId());

		spEm.save(update);
	}

	public String findOneMatch(String id) {
		Sp sp = spEm.findOne(id);
		return sp != null ? sp.getSpId() + "-" + sp.getSpName() : null;
	}
}
