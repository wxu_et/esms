package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.ChannelRoute;
import esms.etonenet.boss1069.repository.ChannelRouteRepository;

@Service
public class ChannelRouteService {

	@Resource
	private ChannelRouteRepository channelRouteEm;

	public Page<ChannelRoute> page(PageRequest pageRequest, Specification<ChannelRoute> spec) {

		return channelRouteEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(ChannelRoute create) {

		channelRouteEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(ChannelRoute update) {

		channelRouteEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<ChannelRoute> dels) {

		for (ChannelRoute cr : dels)
			channelRouteEm.delete(cr);
	}

	public ChannelRoute findOne(Long id) {

		return channelRouteEm.findOne(id);
	}
}
