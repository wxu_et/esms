package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.entity.system.ErrorDefine;
import com.etonenet.service.system.ErrorDefineService;
import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "错误定义" }, value = "错误接口", description = "ErrorDefineController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "errordefine")
public class ErrorDefineController {

	@Resource
	private ErrorDefineService errorDefineService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.GET)
	@ResponseBody
	public AjaxPage page(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLike("host", param.getHost()));
		SpecificationHelper<ErrorDefine> sh = new SpecificationHelper<ErrorDefine>(conditions, param);
		Page<ErrorDefine> p = errorDefineService.page(new PageRequest(param), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@RequestBody @Valid List<ErrorDefine> data) {

		errorDefineService.delete(data);
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ErrorDefine get(@RequestParam Long id) {

		return errorDefineService.findById(id);
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void create(@ModelAttribute @Valid ErrorDefine create) {

		errorDefineService.create(create);
	}

	private static final class PageParam extends BasePageRequestParam {

		private String host;

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}
	}

}
