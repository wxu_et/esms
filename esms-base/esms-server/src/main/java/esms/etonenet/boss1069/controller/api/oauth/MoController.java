package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.mtmo.Mo;
import esms.etonenet.boss1069.service.biz.impl.MoService;
import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.web.Autocomplete;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.bossoa.entity.CarrierCode;
import esms.etonenet.bossoa.entity.City;
import esms.etonenet.bossoa.entity.Country;
import esms.etonenet.bossoa.entity.Province;
import esms.etonenet.bossoa.repository.CarrierCodeRepository;
import esms.etonenet.bossoa.repository.CityRepository;
import esms.etonenet.bossoa.repository.CountryRepository;
import esms.etonenet.bossoa.repository.ProvinceRepository;
import esms.etonenet.oldboss.entity.OldSpService;
import esms.etonenet.oldboss.entity.OldSpServicePK;
import esms.etonenet.oldboss.repository.OldChannelRepository;
import esms.etonenet.oldboss.repository.OldSpRepository;
import esms.etonenet.oldboss.repository.OldSpServiceRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "企信上行查询" })
@ApiIgnore
@Controller("QxxxglMoApiController")
@RequestMapping(ApiConstants.BASE_PATH + "businesslettermoquery")
public class MoController {
	@Resource
	private MoService ttMoService;

	@Resource
	private OldSpRepository spEm;

	@Resource
	private OldChannelRepository channelEm;

	@Resource
	private CountryRepository countryEm;

	@Resource
	private ProvinceRepository provinceEm;

	@Resource
	private CityRepository cityEm;

	@Resource
	private CarrierCodeRepository carrierCodeEm;

	@Resource
	private OldSpServiceRepository spServiceEm;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage page(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		if (param.getSmStartTime() != null)
			conditions.add(SpecificationCondition.greaterOrEqual("carrierMoTime", param.getSmStartTime()));
		if (param.getSmEndTime() != null)
			conditions.add(SpecificationCondition.lessOrEqual("carrierMoTime", param.getSmEndTime()));
		if (param.getMcStartTime() != null)
			conditions.add(SpecificationCondition.greaterOrEqual("spMoTime", param.getMcStartTime()));
		if (param.getMcEndTime() != null)
			conditions.add(SpecificationCondition.lessOrEqual("spMoTime", param.getMcEndTime()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("destinationAddr", param.getDestinationAddr()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("moStat", param.getMoStat()));
		SpecificationHelper<Mo> sh = new SpecificationHelper<>(conditions, param);
		Page<Mo> p = ttMoService.page(sh.createSpecification(), new PageRequest(param));
		for (Mo mo2 : p) {
			displaySpMoBaseInfo(mo2);
		}
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	private void displaySpMoBaseInfo(Mo mo) {

		if (StringUtil.isNotEmpty(mo.getCarrierCode())) {
			CarrierCode cc = carrierCodeEm.findOne(mo.getCarrierCode());
			if (cc != null)
				mo.setCarrierCode(cc.getCarrierCode() + "-" + cc.getCarrierName());
		}
		if (StringUtil.isNotEmpty(mo.getCountryCode())) {
			Country country = countryEm.findOne(mo.getCountryCode());
			if (country != null)
				mo.setCountryCode(country.getCountryCode() + "-" + country.getCountryName());
		}
		if (StringUtil.isNotEmpty(mo.getProvinceCode())) {
			Province province = provinceEm.findOne(mo.getProvinceCode());
			if (province != null)
				mo.setProvinceCode(province.getProvinceCode() + "-" + province.getProvinceName());
		}
		if (StringUtil.isNotEmpty(mo.getCityCode())) {
			City city = cityEm.findOne(mo.getCityCode());
			if (city != null)
				mo.setCityCode(city.getCityCode() + "-" + city.getCityName());
		}
		if (StringUtil.isNotEmpty(mo.getShortMessage()) && mo.getDataCoding() != null) {
			mo.setShortMessage(StringUtil.decodeHexString(mo.getDataCoding(), mo.getShortMessage()));
		}
		if (StringUtil.isNotEmpty(mo.getSpId())) {
			OldSpServicePK ssp = new OldSpServicePK();
			ssp.setSpId(mo.getSpId());
			ssp.setSpServiceCode(mo.getSpServiceCode());
			OldSpService spService = spServiceEm.findOne(ssp);
			if (spService != null)
				mo.setSpServiceCode(mo.getSpServiceCode() + "(" + spService.getSpServiceName() + ")");
		}
	}

	@ApiOperation(value = "sp服务代码查询")
	@RequestMapping(value = "getSpServiceCode", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getSpServiceCode(@RequestParam String term, @RequestParam String spIdName) {

		return AutocompleteUtil.object2auto(spServiceEm.autoSpServiceCode(term, spIdName.split("-")[0]));
	}

	@ApiOperation(value = "sp查询")
	@RequestMapping(value = "getSp", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getSp(@RequestParam String term) {
		return AutocompleteUtil.object2auto(spEm.autoSP(term));
	}

	@ApiOperation(value = "通道查询")
	@RequestMapping(value = "getChannel", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getChannel(@RequestParam String term) {

		return AutocompleteUtil.object2auto(channelEm.autoChannel(term));
	}

	@ApiOperation(value = "国家代码查询")
	@RequestMapping(value = "getCountry", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getCountry(@RequestParam String term) {

		List<Autocomplete> country = new ArrayList<>();
		country.addAll(AutocompleteUtil.object2auto(countryEm.auto(term)));

		return country;
	}

	@ApiOperation(value = "省代码查询")
	@RequestMapping(value = "getProvince", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getProvince(@RequestParam String countryIdName, @RequestParam String term) {

		List<Autocomplete> province = new ArrayList<>();
		province.addAll(AutocompleteUtil.object2auto(provinceEm.auto(countryIdName.split("-")[0], term)));

		return province;
	}

	@ApiOperation(value = "市代码查询")
	@RequestMapping(value = "getCity", method = RequestMethod.POST)
	@ResponseBody
	public List<Autocomplete> getCity(@RequestParam String provinceIdName, @RequestParam String term) {

		List<Autocomplete> city = new ArrayList<>();
		city.addAll(AutocompleteUtil.object2auto(cityEm.auto(provinceIdName.split("-")[0], term)));

		return city;
	}

	private static final class PageParam extends BasePageRequestParam {
		private String destinationAddr;
		private String moStat;
		private Date smStartTime;
		private Date smEndTime;
		private Date mcStartTime;
		private Date mcEndTime;

		public String getDestinationAddr() {
			return destinationAddr;
		}

		public void setDestinationAddr(String destinationAddr) {
			this.destinationAddr = destinationAddr;
		}

		public String getMoStat() {
			return moStat;
		}

		public void setMoStat(String moStat) {
			this.moStat = moStat;
		}

		public Date getSmStartTime() {
			return smStartTime;
		}

		public void setSmStartTime(Date smStartTime) {
			this.smStartTime = smStartTime;
		}

		public Date getSmEndTime() {
			return smEndTime;
		}

		public void setSmEndTime(Date smEndTime) {
			this.smEndTime = smEndTime;
		}

		public Date getMcStartTime() {
			return mcStartTime;
		}

		public void setMcStartTime(Date mcStartTime) {
			this.mcStartTime = mcStartTime;
		}

		public Date getMcEndTime() {
			return mcEndTime;
		}

		public void setMcEndTime(Date mcEndTime) {
			this.mcEndTime = mcEndTime;
		}

	}

}
