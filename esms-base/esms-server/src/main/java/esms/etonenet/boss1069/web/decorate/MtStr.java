package esms.etonenet.boss1069.web.decorate;

import esms.etonenet.boss1069.mtmo.Mt;

public class MtStr {

	private static final long serialVersionUID = -4769011291839066849L;

	public MtStr() {

	}

	private Mt mt;

	public Mt getMt() {
		return mt;
	}

	public void setMt(Mt mt) {
		this.mt = mt;
	}

	private String mtStateStr;

	public String getMtStateStr() {
		return mtStateStr;
	}

	public void setMtStateStr(String mtStateStr) {
		this.mtStateStr = mtStateStr;
	}
}
