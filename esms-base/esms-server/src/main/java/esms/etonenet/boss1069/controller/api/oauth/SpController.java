package esms.etonenet.boss1069.controller.api.oauth;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.MathUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.Sp;
import esms.etonenet.boss1069.repository.SpRepository;
import esms.etonenet.boss1069.service.biz.impl.SpService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "Sp" })
@ApiIgnore
@Controller("SpController")
@RequestMapping(ApiConstants.BASE_PATH + "sp")
public class SpController {
	@Resource
	private SpRepository spEm;
	@Resource
	private SpService spService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public AjaxPage spData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spId", param.getSpId()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spName", param.getSpName()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("spExtNumber", param.getSpExtNumber()));
		conditions.add(SpecificationCondition.eq("spState", param.getSpState()));
		SpecificationHelper<Sp> sh = new SpecificationHelper<>(conditions, param.getSortOrder(), param.getSortName());
		Page<Sp> p = spService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String userGroupUpdate(@ModelAttribute @Valid Sp update, @RequestParam Integer[] spMtmoFlagCheckbox) {
		update.setSpMtmoFlag(MathUtil.sum(spMtmoFlagCheckbox));
		spService.update(update);
		return "更新成功";
	}

	@ApiOperation(value = "检查spId")
	@RequestMapping(value = "checkSpId", method = RequestMethod.GET)
	@ResponseBody
	public boolean checkSpId(@RequestParam String spId) {
		if (spEm.findOne(spId) == null)
			return true;
		else
			return false;
	}

	private static final class PageParam extends BasePageRequestParam {
		private String spId;
		private String spName;
		private String spExtNumber;
		private BigDecimal spState;

		public BigDecimal getSpState() {
			return spState;
		}

		public void setSpState(BigDecimal spState) {
			this.spState = spState;
		}

		public String getSpId() {
			return spId;
		}

		public void setSpId(String spId) {
			this.spId = spId;
		}

		public String getSpName() {
			return spName;
		}

		public void setSpName(String spName) {
			this.spName = spName;
		}

		public String getSpExtNumber() {
			return spExtNumber;
		}

		public void setSpExtNumber(String spExtNumber) {
			this.spExtNumber = spExtNumber;
		}

	}

}
