package esms.etonenet.boss1069.expection;

public class BossException extends RuntimeException {

	private static final long serialVersionUID = 5412734392265148386L;

	public BossException(String message) {
		super(message);
	}

	public String getType() {

		return "BOSS_UNKNOWN_EXCEPTION";
	}
}
