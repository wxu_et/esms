package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.EsmsStat;
import esms.etonenet.boss1069.service.biz.impl.EsmsStatService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "EsmsStat" })
@ApiIgnore
@Controller("EsmsStatController")
@RequestMapping(ApiConstants.BASE_PATH + "esmsstat")
public class EsmsStatController {

	@Resource
	private EsmsStatService esmsStatService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage monitorManageData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("mqUrl", param.getMqUrl()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelNumber", param.getChannelNumber()));
		conditions.add(SpecificationCondition.eq("mqState", param.getMqState()));
		SpecificationHelper<EsmsStat> sh = new SpecificationHelper<EsmsStat>(conditions, param.getSortOrder(),
				param.getSortName());
		Page<EsmsStat> p = esmsStatService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String monitorManageDel(@RequestBody @Valid List<EsmsStat> data) {
		if (data.size() > 0) {
			esmsStatService.del(data);
			return "删除成功";
		}
		return "没有选中数据";
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@ModelAttribute @Valid EsmsStat update) {
		if (esmsStatService.update(update) != null)
			return "修改成功";
		return "修改失败";
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(@ModelAttribute @Valid EsmsStat create, String isMtCarrier) {
		if (isMtCarrier != null) {
			create.setChannelNumber("mt_carrier_" + create.getChannelNumber());
		}
		if (esmsStatService.create(create) != null)
			return "新建成功";
		return "新建失败";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String mqUrl;
		private String channelNumber;
		private String mqState;

		public String getMqUrl() {
			return mqUrl;
		}

		public void setMqUrl(String mqUrl) {
			this.mqUrl = mqUrl;
		}

		public String getChannelNumber() {
			return channelNumber;
		}

		public void setChannelNumber(String channelNumber) {
			this.channelNumber = channelNumber;
		}

		public String getMqState() {
			return mqState;
		}

		public void setMqState(String mqState) {
			this.mqState = mqState;
		}
	}

}
