package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.ChannelBaobeiInfo;
import esms.etonenet.boss1069.enums.LongSmsFlag;
import esms.etonenet.boss1069.enums.channelbb.BaobeiDataState;
import esms.etonenet.boss1069.service.biz.impl.ChannelBaobeiInfoService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "通道报备定义" }, value = "通道报备接口", description = "ChannelBaobeiInfoController")
@ApiIgnore
@Controller(value = "dsdsd")
@RequestMapping(ApiConstants.BASE_PATH + "channelbaobeiinfo")
public class ChannelBaobeiInfoController {

	@Resource
	private ChannelBaobeiInfoService channelbbService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.GET)
	@ResponseBody
	public AjaxPage channelbbData(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(
				SpecificationCondition.eq("channel.channelId", StringUtil.splitMinusFirstGroup(param.getChannelId())));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("baobeiNumber", param.getBaobeiNumberSearch()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("channelMessageSignature",
				param.getChannelMessageSignature()));

		SpecificationHelper<ChannelBaobeiInfo> sh = new SpecificationHelper<ChannelBaobeiInfo>(conditions, param);

		Page<ChannelBaobeiInfo> p = channelbbService.page(new PageRequest(param), sh.createSpecification());

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void create(@ModelAttribute @Valid ChannelBaobeiInfo create) {

		channelbbService.create(create);
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>> get(@RequestParam Long id) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("chbb", channelbbService.findOne(id));
		map.put("longSmsFlag", LongSmsFlag.toDisplayMap());
		map.put("bbDataState", BaobeiDataState.toDisplayMap());
		return list;
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.PUT)
	@ResponseBody
	public void update(@ModelAttribute @Valid ChannelBaobeiInfo update) {

		channelbbService.update(update);
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@RequestBody @Valid List<ChannelBaobeiInfo> data) {

		channelbbService.del(data);
	}

	private final static class PageParam extends BasePageRequestParam {

		private String channelId;

		private String baobeiNumberSearch;

		private String channelMessageSignature;

		public String getChannelId() {
			return channelId;
		}

		public void setChannelId(String channelId) {
			this.channelId = channelId;
		}

		public String getBaobeiNumberSearch() {
			return baobeiNumberSearch;
		}

		public void setBaobeiNumberSearch(String baobeiNumberSearch) {
			this.baobeiNumberSearch = baobeiNumberSearch;
		}

		public String getChannelMessageSignature() {
			return channelMessageSignature;
		}

		public void setChannelMessageSignature(String channelMessageSignature) {
			this.channelMessageSignature = channelMessageSignature;
		}

	}
}
