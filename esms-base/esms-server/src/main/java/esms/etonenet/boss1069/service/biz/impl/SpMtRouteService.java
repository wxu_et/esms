package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpMtRoute;
import esms.etonenet.boss1069.repository.SpMtRouteRepository;

@Service
public class SpMtRouteService {

	@Resource
	private SpMtRouteRepository spMtRouteEm;

	public Page<SpMtRoute> page(PageRequest pageRequest, Specification<SpMtRoute> spec) {

		return spMtRouteEm.findAll(spec, pageRequest);
	}

	public void create(SpMtRoute create) {

		spMtRouteEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpMtRoute update) {

		spMtRouteEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<SpMtRoute> dels) {

		for (SpMtRoute spmt : dels)
			spMtRouteEm.delete(spmt);
	}
}
