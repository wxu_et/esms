package esms.etonenet.boss1069.controller.api.oauth;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.Company;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = { "公司" })
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "company")
public class CompanyController {

	@ApiOperation(value = "同步单例公司")
	@RequestMapping(value = "syn", method = RequestMethod.POST)
	@ResponseBody
	public Company syn(@RequestBody @Valid Company company) {

		return company;
	}
}
