package esms.etonenet.boss1069.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.entity.system.ErrorCode;
import com.etonenet.entity.system.ErrorDefine;
import com.etonenet.service.system.ErrorCodeService;
import com.etonenet.service.system.ErrorDefineService;
import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;

import esms.etonenet.boss1069.util.AutocompleteUtil;
import esms.etonenet.boss1069.util.TilesUtil;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.boss1069.web.UrlConstants;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class BasicInformationController {
	@Resource
	private ErrorCodeService errorCodeService;
	@Resource
	private ErrorDefineService errorDefineService;

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_INDEX)
	public ModelAndView providerErrorCodeIndex() {
		return new ModelAndView(TilesUtil.getPath(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_INDEX));
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DATA)
	@ResponseBody
	public AjaxPage providerErrorCodeData(Integer pageNumber, Integer pageSize, final ErrorCode search,
			final String host, final String sortName, final String sortOrder) throws Exception {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("errorCode", search.getErrorCode()));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("originExplain", search.getOriginExplain()));
		conditions.add(SpecificationCondition.eq("errorDefine.host", host));
		SpecificationHelper<ErrorCode> sh = new SpecificationHelper<ErrorCode>(conditions, sortOrder, sortName);
		Page<ErrorCode> p = errorCodeService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_AUTOCOMPLETE_ERRORDEFINE)
	@ResponseBody
	public List<?> autoErrorDefine(String term) {
		return AutocompleteUtil.object2auto(errorDefineService.auto(term));
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_NEW)
	public ModelAndView providerErrorCodeNew() {
		return new ModelAndView(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_NEW);
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_CREATE)
	@ResponseBody
	public String providerErrorCodeCreate(final ErrorCode create, final String host) {
		create.setErrorDefine(errorDefineService.findByHost(host));
		errorCodeService.create(create);
		return "新建成功";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_VIEW)
	public ModelAndView providerErrorCodeView(@RequestParam(value = "errorCodeId", required = false) Long errorCodeId) {
		ModelAndView mav = new ModelAndView(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_VIEW);
		ErrorCode errorCode = errorCodeService.findOne(errorCodeId);
		mav.addObject("ec", errorCode);
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_UPDATE)
	@ResponseBody
	public String providerErrorCodeUpdate(final ErrorCode update, final String host) {
		update.setErrorDefine(errorDefineService.findByHost(host));
		if (errorCodeService.update(update) != null)
			return "修改成功";
		return "修改失败";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_PROVIDERERRORCODE_DEL)
	@ResponseBody
	public String monitorManageDel(@RequestBody List<ErrorCode> data) {
		if (data.size() > 0) {
			errorCodeService.del(data);
			return "删除成功";
		}
		return "没有选中数据";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_INDEX)
	public ModelAndView errorDefineIndex() {
		return new ModelAndView(TilesUtil.getPath(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_INDEX));

	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DATA)
	@ResponseBody
	public AjaxPage errorDefineData(Integer pageNumber, Integer pageSize, final ErrorDefine search, final String host,
			final String sortName, final String sortOrder) {
		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.equalOrLike("host", search.getHost()));
		SpecificationHelper<ErrorDefine> sh = new SpecificationHelper<ErrorDefine>(conditions, sortOrder, sortName);
		Page<ErrorDefine> p = errorDefineService.page(new PageRequest(pageNumber, pageSize), sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());

	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_NEW)
	public ModelAndView errorDefineNew() {
		return new ModelAndView(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_NEW);

	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_CREATE)
	@ResponseBody
	public String create(ErrorDefine create) {
		if (errorDefineService.create(create) != null)
			return "新建成功";
		return "新建失败";

	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_VIEW)
	public ModelAndView view(Long errorDefineId) {
		ModelAndView mav = new ModelAndView(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_VIEW);
		mav.addObject("ed", errorDefineService.findById(errorDefineId));
		return mav;
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_UPDATE)
	@ResponseBody
	public String errorDefineUpdate(ErrorDefine update) {
		if (errorDefineService.updaye(update) != null)
			return "修改成功";
		return "修改失败";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRORDEFINE_DEL)
	@ResponseBody
	public String errorDefineDel(@RequestBody List<ErrorDefine> list) {
		if (list.size() > 0) {
			errorDefineService.delete(list);
			return "删除成功";
		}
		return "没有选中数据";
	}

	@RequestMapping(UrlConstants.RUNMANAGE_BASICINFORMATION_ERRPRDEFINE_ERRORDEFINEIDCHECK)
	@ResponseBody
	public boolean errorDefineIdCheck(final Long errorDefineId) {
		if (errorDefineService.findById(errorDefineId) == null)
			return true;
		return false;
	}
}
