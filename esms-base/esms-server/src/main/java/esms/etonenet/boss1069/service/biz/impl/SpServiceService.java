package esms.etonenet.boss1069.service.biz.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpService;
import esms.etonenet.boss1069.repository.SpServiceRepository;

@Service
public class SpServiceService {

	@Resource
	private SpServiceRepository spServiceEm;

	public Page<SpService> page(PageRequest pageRequest, Specification<SpService> spec) {

		return spServiceEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(SpService create) {

		spServiceEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpService update) {

		SpService old = spServiceEm.findOne(update.getId());

		spServiceEm.save(update);
	}
}
