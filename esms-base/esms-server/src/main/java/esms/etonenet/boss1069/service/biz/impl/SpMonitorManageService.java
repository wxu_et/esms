package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpMonitorManage;
import esms.etonenet.boss1069.repository.SpMonitorManageRepository;
import esms.etonenet.boss1069.web.PageRequest;

@Service
public class SpMonitorManageService {

	private static Logger logger = LoggerFactory.getLogger(SpMonitorManageService.class);

	@Resource
	SpMonitorManageRepository spMonitorManageRepository;

	public Page<SpMonitorManage> page(PageRequest pageRequest, Specification<SpMonitorManage> spec) {
		return spMonitorManageRepository.findAll(spec, pageRequest);
	}

	public SpMonitorManage create(SpMonitorManage create) {
		return spMonitorManageRepository.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public SpMonitorManage update(SpMonitorManage update) {
		return spMonitorManageRepository.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<SpMonitorManage> dels) {
		for (SpMonitorManage spMonitorManage : dels)
			spMonitorManageRepository.delete(spMonitorManage);
	}

	public SpMonitorManage findOne(String spId) {
		return spMonitorManageRepository.findOne(spId);
	}
}
