package esms.etonenet.boss1069.job;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etonenet.entity.system.SysLog;
import com.etonenet.repository.system.SysLogRepository;
import com.etonenet.util.StringUtil;

public class LogJob {

	@Resource
	private SysLogRepository sysLogEm;

	private ReentrantLock lock = new ReentrantLock();

	public ReentrantLock getLock() {
		return lock;
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private List<SysLog> sysLogs = Collections.synchronizedList(new LinkedList<SysLog>());

	public List<SysLog> getSysLogs() {
		return sysLogs;
	}

	public void saveLog() {
		SysLog sl = null;
		List<SysLog> copyList = new LinkedList<SysLog>(sysLogs);
		lock.lock();
		sysLogs.clear();
		lock.unlock();
		Iterator<SysLog> it = copyList.iterator();
		while (it.hasNext()) {
			try {
				sl = it.next();
				if (StringUtil.isNotEmpty(sl.getProp())) {
					sysLogEm.save(sl);
				}
			} catch (Exception e) {
				logger.error("logsave err:" + sl.getProp());
				continue;
			}
		}
	}
}
