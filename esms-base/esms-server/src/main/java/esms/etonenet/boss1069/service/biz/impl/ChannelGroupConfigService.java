package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.ChannelGroupConfig;
import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.repository.ChannelGroupConfigRepository;

@Service
public class ChannelGroupConfigService {

	@Resource
	private ChannelGroupConfigRepository channelGroupConfigEm;

	public Page<ChannelGroupConfig> page(PageRequest pageRequest, Specification<ChannelGroupConfig> spec) {

		return channelGroupConfigEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(ChannelGroupConfig create) {

		create.setLogicStat(LogicStat.NORMAL.getValue());
		channelGroupConfigEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(ChannelGroupConfig update) {

		update.setLogicStat(LogicStat.NORMAL.getValue());
		channelGroupConfigEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<ChannelGroupConfig> dels) {

		for (ChannelGroupConfig cgc : dels) {
			channelGroupConfigEm.del(cgc.getChannelGroupConfigId());
		}
	}

	public ChannelGroupConfig findOne(Long id) {

		return channelGroupConfigEm.findOne(id);
	}
}
