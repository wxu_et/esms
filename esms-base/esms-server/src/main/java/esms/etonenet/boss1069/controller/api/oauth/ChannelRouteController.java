package esms.etonenet.boss1069.controller.api.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.ChannelRoute;
import esms.etonenet.boss1069.service.biz.impl.ChannelRouteService;
import esms.etonenet.boss1069.service.biz.impl.SpMtRouteService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import esms.etonenet.bossoa.entity.CarrierCode;
import esms.etonenet.bossoa.entity.City;
import esms.etonenet.bossoa.entity.Country;
import esms.etonenet.bossoa.entity.Province;
import esms.etonenet.bossoa.repository.CarrierCodeRepository;
import esms.etonenet.bossoa.repository.CityRepository;
import esms.etonenet.bossoa.repository.CountryRepository;
import esms.etonenet.bossoa.repository.ProvinceRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "通道路由定义" }, value = "通道路由接口", description = "ChannelRouteController")
@ApiIgnore
@Controller
@RequestMapping(ApiConstants.BASE_PATH + "channelroute")
public class ChannelRouteController {

	@Resource
	private SpMtRouteService spMtRouteService;

	@Resource
	private CountryRepository countryEm;

	@Resource
	private ProvinceRepository provinceEm;

	@Resource
	private CityRepository cityEm;

	@Resource
	private CarrierCodeRepository carrierCodeEm;

	@Resource
	private ChannelRouteService channelRouteService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.GET)
	@ResponseBody
	public AjaxPage channelRouteData(@ModelAttribute @Valid PageParam param) {

		List<SpecificationCondition> conditions = new ArrayList<SpecificationCondition>();
		conditions.add(SpecificationCondition.eq("channel.channelId",
				StringUtil.splitMinusFirstGroup(param.getChannelIdNameSearch())));
		conditions.add(SpecificationCondition.eq("carrierCode",
				StringUtil.splitMinusFirstGroup(param.getCarriercodeSearch())));
		conditions.add(SpecificationCondition.eq("routeState", param.getRouteStateSearch()));

		SpecificationHelper<ChannelRoute> sh = new SpecificationHelper<ChannelRoute>(conditions, param);

		Page<ChannelRoute> p = channelRouteService.page(new PageRequest(param), sh.createSpecification());

		for (ChannelRoute cr : p.getContent())
			displayChannelRouteBaseInfo(cr);

		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.PUT)
	@ResponseBody
	public void channelRouteUpdate(@ModelAttribute @Valid ChannelRoute update) {

		channelRouteService.update(update);
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public void channelRouteCreate(@ModelAttribute @Valid ChannelRoute create) {

		channelRouteService.create(create);
	}

	@ApiOperation(value = "单例查询")
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ChannelRoute channelRouteView(@RequestParam Long id) {

		ChannelRoute cr = channelRouteService.findOne(id);
		displayChannelRouteBaseInfo(cr);
		return cr;
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void channelRouteDel(@RequestBody @Valid List<ChannelRoute> data) {

		channelRouteService.del(data);
	}

	/**
	 * 修正基础信息显示国家、省、城市、营运商
	 * 
	 * @param spmt
	 */
	private void displayChannelRouteBaseInfo(ChannelRoute cr) {

		if (StringUtil.isNotEmpty(cr.getCarrierCode())) {
			CarrierCode cc = carrierCodeEm.findOne(cr.getCarrierCode());
			if (cc != null)
				cr.setCarrierCode(cc.getCarrierCode() + "-" + cc.getCarrierName());
		}
		if (StringUtil.isNotEmpty(cr.getCountryCode())) {
			Country country = countryEm.findOne(cr.getCountryCode());
			if (country != null)
				cr.setCountryCode(country.getCountryCode() + "-" + country.getCountryName());
		}
		if (StringUtil.isNotEmpty(cr.getProvinceCode())) {
			Province province = provinceEm.findOne(cr.getProvinceCode());
			if (province != null)
				cr.setProvinceCode(province.getProvinceCode() + "-" + province.getProvinceName());
		}
		if (StringUtil.isNotEmpty(cr.getCityCode())) {
			City city = cityEm.findOne(cr.getCityCode());
			if (city != null)
				cr.setCityCode(city.getCityCode() + "-" + city.getCityName());
		}
	}

	private final static class PageParam extends BasePageRequestParam {

		private String channelIdNameSearch;

		private String carriercodeSearch;

		private String routeStateSearch;

		public String getChannelIdNameSearch() {
			return channelIdNameSearch;
		}

		public void setChannelIdNameSearch(String channelIdNameSearch) {
			this.channelIdNameSearch = channelIdNameSearch;
		}

		public String getCarriercodeSearch() {
			return carriercodeSearch;
		}

		public void setCarriercodeSearch(String carriercodeSearch) {
			this.carriercodeSearch = carriercodeSearch;
		}

		public String getRouteStateSearch() {
			return routeStateSearch;
		}

		public void setRouteStateSearch(String routeStateSearch) {
			this.routeStateSearch = routeStateSearch;
		}

	}
}
