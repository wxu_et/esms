package esms.etonenet.boss1069.controller.api.oauth;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.StringUtil;

import esms.etonenet.boss1069.controller.api.oauth.common.ApiConstants;
import esms.etonenet.boss1069.entity.MtAddress;
import esms.etonenet.boss1069.repository.MtAddressRepository;
import esms.etonenet.boss1069.service.biz.impl.MtAddressService;
import esms.etonenet.boss1069.web.BasePageRequestParam;
import esms.etonenet.boss1069.web.PageRequest;
import esms.etonenet.boss1069.web.SpecificationCondition;
import esms.etonenet.boss1069.web.SpecificationHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = { "MtAddress" })
@ApiIgnore
@Controller("MtAddressController")
@RequestMapping(ApiConstants.BASE_PATH + "mtaddress")
public class MtAddressController {
	@Resource
	private MtAddressRepository addressRepository;
	@Resource
	MtAddressService addressService;

	@ApiOperation(value = "带条件及翻页查询")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	@ResponseBody
	public AjaxPage testData(@ModelAttribute @Valid PageParam param) {
		List<SpecificationCondition> conditions = new ArrayList<>();
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("addressName",
				StringUtil.splitMinusFirstGroup(param.getAddressName())));
		conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("target",
				StringUtil.splitMinusFirstGroup(param.getTarget())));
		SpecificationHelper<MtAddress> sh = new SpecificationHelper<>(conditions, param.getSortOrder(),
				param.getSortName());
		Page<MtAddress> p = addressService.page(new PageRequest(param.getPageNumber(), param.getPageSize()),
				sh.createSpecification());
		return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
	}

	@ApiOperation(value = "单例新增")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String testCreate(@ModelAttribute @Valid MtAddress create) {
		Timestamp d = new Timestamp(System.currentTimeMillis());
		create.setCreateTime(d);
		create.setUpdateTime(d);
		addressService.create(create);
		return "新建成功";
	}

	@ApiOperation(value = "单例修改")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String testUpdate(@ModelAttribute @Valid MtAddress update) {
		Timestamp d = new Timestamp(System.currentTimeMillis());
		update.setUpdateTime(d);
		addressService.update(update);
		return "更新成功";
	}

	@ApiOperation(value = "(批量)删除")
	@RequestMapping(value = "delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String testDel(@RequestBody List<MtAddress> data) {
		if (data.size() > 0) {
			addressService.del(data);
			return "删除成功";
		}
		return "没有选中数据";
	}

	private static final class PageParam extends BasePageRequestParam {
		private String addressName;
		private String target;

		public String getAddressName() {
			return addressName;
		}

		public void setAddressName(String addressName) {
			this.addressName = addressName;
		}

		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}
	}

}
