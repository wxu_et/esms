package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.EsmsStat;
import esms.etonenet.boss1069.repository.EsmsStatRepository;

@Service
public class EsmsStatService {

	@Resource
	private EsmsStatRepository esmsStatEm;

	public Page<EsmsStat> page(PageRequest pageRequest, Specification<EsmsStat> spsc) {
		return esmsStatEm.findAll(spsc, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public EsmsStat create(EsmsStat create) {
		return esmsStatEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<EsmsStat> dels) {
		for (EsmsStat esmsStat : dels)
			esmsStatEm.delete(esmsStat);
	}

	public EsmsStat findOne(Long id) {
		return esmsStatEm.findOne(id);
	}

	@Transactional(rollbackFor = Exception.class)
	public EsmsStat update(EsmsStat update) {
		return esmsStatEm.save(update);
	}

}
