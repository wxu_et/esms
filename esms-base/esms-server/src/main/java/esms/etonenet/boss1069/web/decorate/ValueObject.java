package esms.etonenet.boss1069.web.decorate;

/**
 * @author zhshen VO对象，用于存值，支持扩充, 在实际运用上给各参数附上实际意义
 */
public class ValueObject {
	private String parameter1;
	private String parameter2;

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	public String getParameter2() {
		return parameter2;
	}

	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}

}
