package esms.etonenet.boss1069.service.biz.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import esms.etonenet.boss1069.mtmo.Mo;
import esms.etonenet.boss1069.mtmo.MoRepository;

@Service
public class MoService {

	@Resource
	private MoRepository ttMoRe;

	public Page<Mo> page(Specification<Mo> spec, Pageable pageable) {

		return ttMoRe.findAll(spec, pageable);
	}

	public Mo findOne(Long spMoId) {

		return ttMoRe.findOne(spMoId);
	}

}
