package esms.etonenet.boss1069.service.biz.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpScCarriercode;
import esms.etonenet.boss1069.entity.SpScCarriercodePK;
import esms.etonenet.boss1069.repository.SpScCarriercodeRepository;

@Service
public class SpScCarriercodeService {

	@Resource
	private SpScCarriercodeRepository spScCarriercodeEm;

	public Page<SpScCarriercode> page(PageRequest pageRequest, Specification<SpScCarriercode> spec) {

		return spScCarriercodeEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(SpScCarriercode create) {

		spScCarriercodeEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpScCarriercode update) {

		spScCarriercodeEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(java.util.List<SpScCarriercodePK> crids) {

		for (SpScCarriercodePK id : crids) {
			SpScCarriercode cr = new SpScCarriercode();
			cr.setId(id);
			spScCarriercodeEm.delete(cr);
		}
	}
}
