package esms.etonenet.boss1069.service.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esms.etonenet.boss1069.entity.SpChannel;
import esms.etonenet.boss1069.repository.SpChannelRepository;

@Service
public class SpChannelService {

	@Resource
	private SpChannelRepository spChannelEm;

	public Page<SpChannel> page(PageRequest pageRequest, Specification<SpChannel> spec) {

		return spChannelEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(SpChannel create) {

		// (2) 隐藏(新建/修改/查询结果)中的所有”SP通道时间片号码”, 新建默认填空.修改保留原来属性.
		create.setSpChannelTimeSliceNumber(null);

		spChannelEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(SpChannel update) {

		// (2) 隐藏(新建/修改/查询结果)中的所有”SP通道时间片号码”, 新建默认填空.修改保留原来属性.
		SpChannel spch = spChannelEm.findOne(update.getId());
		update.setSpChannelTimeSliceNumber(spch.getSpChannelTimeSliceNumber());

		spChannelEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<SpChannel> data) {

		for (SpChannel spch : data) {

			spChannelEm.delete(spch);
		}
	}
}
