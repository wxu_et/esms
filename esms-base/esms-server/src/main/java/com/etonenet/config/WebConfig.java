package com.etonenet.config;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.etonenet.controller" })
public class WebConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.tiles();
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setOrder(3);
		registry.viewResolver(resolver);
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer config = new TilesConfigurer();
		config.setDefinitions("/WEB-INF/tiles/bossTemplate.xml");
		return config;
	}

	/**
	 * 通过默认 servlet 访问静态资源<br>
	 * http://localhost:8080/security/HelloWorld.txt
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	/**
	 * 配置消息转换
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		// jackson2 json mapping
		MappingJackson2HttpMessageConverter jackson2Converter = new MappingJackson2HttpMessageConverter();
		jackson2Converter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));
		jackson2Converter.setObjectMapper(objectMapper);
		converters.add(jackson2Converter);

		// bufferedImage
		converters.add(new BufferedImageHttpMessageConverter());

		// byte array
		ByteArrayHttpMessageConverter byteArrayConverter = new ByteArrayHttpMessageConverter();
		// byteArrayConverter
		// .setSupportedMediaTypes(Arrays.asList(MediaType.IMAGE_GIF,
		// MediaType.IMAGE_JPEG, MediaType.IMAGE_PNG));
		converters.add(byteArrayConverter);

		// string
		StringHttpMessageConverter stringConverter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		stringConverter.setWriteAcceptCharset(false);
		converters.add(stringConverter);

	}

}
