package com.etonenet.controller.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etonenet.service.system.MenuService;
import com.etonenet.service.system.security.CustomUser;
import com.etonenet.util.SpringUtil;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller()
@RequestMapping(value = "/common")
public class CommonController {

	@Resource
	private MenuService menuService;

	@RequestMapping(value = "header")
	public String header(HttpServletRequest request) {

		return "/common/header";
	}

	@RequestMapping(value = "footer")
	public String footer() {

		return "/common/footer";
	}

	@RequestMapping(value = "menu")
	public String menul(HttpServletRequest request) {

		request.setAttribute("currentUser", ((CustomUser) SpringUtil.getUser()).getUser());
		request.setAttribute("menuHtml", menuService.getMenuHtml().toString());
		return "/common/menu";
	}

	@RequestMapping(value = "csswitch")
	public String csswitch() {

		return "/common/csswitch";
	}

	@RequestMapping(value = "breadcrumb")
	public String breadcrumb() {

		return "/common/breadcrumb";
	}

	@RequestMapping(value = "page")
	public String page() {

		return "/common/page";
	}

}
