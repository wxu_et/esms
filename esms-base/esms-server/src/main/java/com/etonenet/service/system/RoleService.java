package com.etonenet.service.system;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.entity.system.security.Role;
import com.etonenet.repository.system.security.RoleRepository;

@Service
public class RoleService {

	@Resource
	private RoleRepository roleEm;

	public Page<Role> page(PageRequest pageRequest, Specification<Role> spec) {

		return roleEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(Role create) {

		roleEm.save(create);
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(Role update) {

		roleEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<Role> dels) {

		for (Role cr : dels)
			roleEm.delete(cr);
	}
}
