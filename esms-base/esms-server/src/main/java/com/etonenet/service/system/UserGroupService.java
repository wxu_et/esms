package com.etonenet.service.system;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.entity.system.security.UserGroup;
import com.etonenet.repository.system.security.UserGroupRepository;
import com.etonenet.util.DateUtil;

import esms.etonenet.boss1069.enums.LogicStat;
import esms.etonenet.boss1069.enums.usergroup.IsMenu;

@Service
public class UserGroupService {

	@Resource
	private UserGroupRepository userGroupEm;

	@Resource
	private MenuService menuService;

	public Page<UserGroup> page(PageRequest pageRequest, Specification<UserGroup> spec) {

		return userGroupEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(UserGroup userGroup) {

		Timestamp cur = DateUtil.getCurrentTimestamp();
		userGroup.setCreateTime(cur);
		userGroup.setUpdateTime(cur);
		userGroup.setLogicStat(LogicStat.NORMAL.getValue());

		// 默认是menu
		userGroup.setIsMenu(IsMenu.IS.getValue());
		// 菜单名称就是组名
		userGroup.setMenuName(userGroup.getName());

		// mvc封装出现全属性为空的非null对象，无法正常执行插入，修正
		if (userGroup.getParentUserGroup().getUserGroupId() == null) {
			userGroup.setParentUserGroup(null);
			userGroup.setMenuPriority(BigDecimal.valueOf(1));
			userGroup.setCodePath("/" + userGroup.getCode() + "/");
		} else {
			// 菜单显示权重，同级最大数+1
			BigDecimal maxp = userGroupEm.getMaxMenuPriority(userGroup.getParentUserGroup().getUserGroupId());
			if (maxp == null)
				maxp = BigDecimal.valueOf(1);
			else
				maxp = BigDecimal.valueOf(maxp.intValue() + 1);
			userGroup.setMenuPriority(maxp);

			UserGroup parent = userGroupEm.findOne(userGroup.getParentUserGroup().getUserGroupId());
			userGroup.setCodePath(parent.getCodePath() + userGroup.getCode() + "/");
		}

		userGroupEm.save(userGroup);

		menuService.initMenu();
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<UserGroup> dels) {

		for (UserGroup ug : dels)
			userGroupEm.delUserGroup(ug.getUserGroupId());

		menuService.initMenu();
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(UserGroup update) {

		UserGroup old = userGroupEm.findOne(update.getUserGroupId());

		String codePath = old.getCodePath();
		String newCodePath = codePath.replace("/" + old.getCode() + "/", "/" + update.getCode() + "/");

		old.setCode(update.getCode());
		old.setName(update.getName());
		if (update.getParentUserGroup().getUserGroupId() != null)
			old.setParentUserGroup(update.getParentUserGroup());
		else
			old.setParentUserGroup(null);
		old.setIsMenu(update.getIsMenu());
		old.setMenuName(old.getMenuName());
		old.setMenuPriority(update.getMenuPriority());
		old.setUpdateTime(DateUtil.getCurrentTimestamp());
		old.setUrlAction(update.getUrlAction());

		userGroupEm.save(old);

		menuService.initMenu();

		if (codePath.equals(newCodePath))
			return;
		// codePath 相关都更新
		for (UserGroup ug : userGroupEm.getAllChildCodePath(codePath)) {
			ug.setCodePath(ug.getCodePath().replace(codePath, newCodePath));
			userGroupEm.save(ug);
		}
	}
}
