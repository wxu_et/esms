package com.etonenet.service.system;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.etonenet.entity.system.SysLog;
import com.etonenet.repository.system.SysLogRepository;

@Service
public class SysLogService {

	@Resource
	private SysLogRepository sysLogEm;

	public Page<SysLog> page(PageRequest pageRequest, Specification<SysLog> spec) {

		return sysLogEm.findAll(spec, pageRequest);
	}
}
