# 相关URL
* http://localhost:8080/esms-server/boss
* http://localhost:8080/esms-server/login
* http://localhost:8080/esms-server/logout

# 功能列表

* done:用户登陆验证
* done:用户登陆日志
* 用户权限管理
* 用户菜单管理
* 系统日志

# 技术点列表
* spring-security
* spring-jpa
* tiles
* hibernate validator
* TODO:swagger-RESTful java doc
* TODO:oauth2
