function initPage(pageNo,totalPage,fnPage) {
	pageNo=parseInt(pageNo);
	totalPage=parseInt(totalPage);
	
	var ul=$('<ul class="pagination"></ul>');
	var pre=$('<li><a href="javascript:void(0)">&laquo;</a></li>');
	var next=$('<li><a href="javascript:void(0)">&raquo;</a></li>');
	ul.append(pre);
	ul.append(next);
	
	if(totalPage<=1)
		$("#smstempate_page").hide();
	else
		$("#smstempate_page").html(ul).show();
	
	if(pageNo<=1)
		pre.attr("class","disabled");
	else
		pre.click(function(){
			fnPage(pageNo-1);
		});
	
	if(pageNo==totalPage)
		next.attr("class","disabled");
	else
		next.click(function(){
			fnPage(pageNo+1);
		});
	
	var i = 1, j = totalPage;
	if(j > 5){
		i = pageNo - 2;
		if(i < 1){
			i = 1;
		}
		j = i + 4;
	}
	if(j > totalPage){
		j = totalPage;
		i = j - 4;
		if(i < 1){
			i = 1;
		};
	}
	if (i>1){
		var m = i - 3;
		if (m < 1)
			m = 1;
	}
	for(;i<=j;i++){
		var li=$('<li'+(pageNo == i?" class='active'":"")+'></li>');
		var a=$('<a href="javascript:void(0)">'+i+'</a>');
		if(pageNo!=i){
			a.click(function(){
				fnPage(this.innerHTML);
			});
		}
		li.append(a);
		li.insertBefore(next);
	}	

	if(j != totalPage){
		var n = i + 2;
		if(n > totalPage){
			n = totalPage;
		};
	}
}