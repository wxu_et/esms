<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_NEW %>" delBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_DEL %>">
	<et:search>
		<et:searchField labelName="code" inputName="code" value="${search.code }"/>
		<et:searchField labelName="用户组名称" inputName="name" value="${search.name }"/>
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_DATA %>">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_VIEW %>" linkUrlParam="userGroupId"/>
		<et:pageField tableHeadName="code" value="code"/>
		<et:pageField tableHeadName="用户组名称" value="name" />
		<et:pageField tableHeadName="codePath" value="codePath" />
		<et:pageField tableHeadName="创建时间" value="createTime" type="dateTime"/>
		<et:pageField tableHeadName="最后更新时间" value="updateTime" type="dateTime"/>
	</et:page>
</et:index>