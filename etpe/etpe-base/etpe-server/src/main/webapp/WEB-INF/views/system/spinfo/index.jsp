<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_NEW %>" 
			delBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_DEL %>">
	<et:search column="4">
		<et:searchField labelName="商户名称" inputName="spName" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_DATA%>" dataCheckbox="true">
		<et:pageField tableHeadName="操作"  value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_VIEW %>" linkUrlParam="spId" dataSortable="false" />
		<et:pageField tableHeadName="商户编号" value="spId" />
		<et:pageField tableHeadName="绑定ip" value="bindingIpAddr" />
		<et:pageField tableHeadName="单日调用上限" value="rationNum" />
		<et:pageField tableHeadName="商户名称" value="spName" />
		<et:pageField tableHeadName="商户密码" value="spPassword" />
		<et:pageField tableHeadName="BOSS用户编号" value="userId" />
	</et:page>
</et:index>
<%-- <script>
	function oper(value, row, index) {
		return '<a data-toggle="modal" data-target=".modal" href="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_ROLEUSER%>?roleid='+row.roleId+'">角色人员分配</a>';
	}
</script> --%>