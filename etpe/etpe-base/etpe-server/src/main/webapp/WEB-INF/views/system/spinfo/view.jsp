<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_UPDATE %>">
<%-- 	<et:formField inputName="roleId" labelName="systemNodeId" type="hidden" value="${role.roleId }"/>
	<et:formField inputName="displayName" labelName="角色名" required="true" value="${role.displayName }"/>
	<et:formField inputName="roleName" labelName="显示名称" required="true"  value="${role.roleName}"/> --%>
	
	<et:formField inputName="spId" labelName="商户编号" required="true" value="${spInfo.spId }"/>
	<et:formField inputName="bindingIpAddr" labelName="绑定ip" required="true" value="${spInfo.bindingIpAddr }"/>
	<et:formField inputName="rationNum" labelName="单日调用上限" required="true" value="${spInfo.rationNum }"/>
	<et:formField inputName="spName" labelName="商户名称" required="true" value="${spInfo.spName }"/>
	<et:formField inputName="spPassword" labelName="商户密码" required="true" value="${spInfo.spPassword }"/>
	<et:formField inputName="userId" labelName="BOSS用户编号" required="true" value="${spInfo.userId }"/>
</et:form>
