<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.etonenet.web.UrlConstants"%>

<form class="form-horizontal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="pull-right">
					<input id="svbtn" type="button" class="btn btn-sm btn-primary" value="保存">
					<button id="cbtn" class="btn btn-sm" data-dismiss="modal" aria-hidden="true">取消</button>
				</div>
				<h4 class="modal-title">&nbsp;</h4>
			</div>
			<div class="modal-body">

				<div class="tab-pane active">
					<div class="row ">
						<div class="form-group col-sm-6">
							<label class="control-label"> 人员列表</label>
							<input id="searchlist1" type="text"/>
							<div class="controls">
								<select id="list1" class="form-control" multiple="multiple" style="height: 400px;">
									<c:forEach items="${noroleUsers }" var="u">
										<option value="${u.userId }">${u.loginName}-${u.userName }</option>
									</c:forEach>
								</select>
								
							</div>
						</div>
						<div class="form-group col-sm-1" style="position:absolute;margin-left:269px;margin-top:200px">
							<input id="leftbtn" type="button" value="&gt;&gt;">
							<input id="rightbtn" type="button" value="&lt;&lt;">
						</div>
						<div class="form-group col-sm-6">
							<label class="control-label"> 已选列表</label>
							<input id="searchlist2" type="text"/>
							<div class="controls">
								<select id="list2" class="form-control" multiple="multiple" style="height: 400px;">
									<c:forEach items="${roleUsers }" var="u">
										<option value="${u.userId }">${u.loginName}-${u.userName }</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
<!--
$("#leftbtn").click(function(){
	$("#list1 option:selected").appendTo($("#list2"))
})
$("#rightbtn").click(function(){
	$("#list2 option:selected").appendTo($("#list1"))
})

$("#searchlist1").keyup(function(){
	var input=$(this).val();
	$("#list1 option").each(function(){
		if($(this).html().indexOf(input)==-1)
			$(this).hide();
		else
			$(this).show();
	})
})
$("#searchlist2").keyup(function(){
	var input=$(this).val();
	$("#list2 option").each(function(){
		if($(this).html().indexOf(input)==-1)
			$(this).hide();
		else
			$(this).show();
	})
})
$("#svbtn").click(function(){
	
	var data='';
	var i=0;
	$("#list2 option").each(function(){
		data+=$(this).val()+',';
	})
	$.post('<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_SAVE%>',{roleid:'${roleid}',data:data}, function(data) {
		showTip(data)
		$("#cbtn").click();
	});
})
//-->
</script>
