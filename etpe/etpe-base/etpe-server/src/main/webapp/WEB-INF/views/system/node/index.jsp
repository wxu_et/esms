<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/treegrid/jquery.treegrid.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/treegrid/jquery.treegrid.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/treegrid/jquery.treegrid.bootstrap3.js"></script>
</head>

<body>
   <div id="modals" class="modal fade in"></div>	
   <div>
   	 <a type="button" href="<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_NEW %>" data-target="#modals" data-toggle="modal" class="btn btn-primary">+根节点</a>
   </div>
   <table class="table tree">
   	  <tr>
   	  	<td width="500px">节点名</td>
   	  	<td width="100px">展示顺序</td>
   	  	<td width="50px">节点类型</td>
   	  	<td width="50px">带URL</td>
   	  	<td width="100px">操作</td>
   	  </tr>
      <c:forEach items="${nodetree }" var="node">
       	<c:if test="${node.parent==null  }">
        <tr class="treegrid-${node.systemNodeId }">
        </c:if>
        <c:if test="${node.parent!=null }">
        <tr class="treegrid-${node.systemNodeId } treegrid-parent-${node.parent.systemNodeId}">
        </c:if>
            <td>${fn:substring(node.nodeName,0,10)  }</td>
            <td>${node.displayOrder }</td>
            <td>${node.nodeType=='0'?'菜单':'功能' }</td>
            <td>${empty node.url?'否':'是' }</td>
            <td>
	            <a href="<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_NEW %>?parentid=${node.systemNodeId }" data-target="#modals" data-toggle="modal">+子节点</a>
	            <a href="<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_VIEW %>?nodeid=${node.systemNodeId }" data-target="#modals" data-toggle="modal">修改</a>
	            <a name="delmenu" delid="${node.systemNodeId }" href="javascript:void(0)">删除</a>
            </td>
        </tr>
       </c:forEach>    
   </table>
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid();
        
        $("a[name='delmenu']").click(function(){
        	if(window.confirm('删除该菜单将删除菜单下的所有子菜单')){
	        	var url='<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_DEL %>?nodeid='+$(this).attr("delid");
	        	$.ajax({
	        		url: url,
	        		success: function(s){
	        		  alert(s);
	        		  window.location=window.location;
		            }
	        	});
        	}
        });
    });
    
    $('body').on('hidden.bs.modal','#modals',function (){$(this).removeData('bs.modal');});
</script>
</html>
