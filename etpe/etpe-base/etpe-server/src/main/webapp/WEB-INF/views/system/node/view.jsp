<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_UPDATE %>">
	<et:formField inputName="systemNodeId" labelName="systemNodeId" type="hidden" value="${node.systemNodeId }"/>
	<et:formField inputName="nname" labelName="节点名" required="true" maxlength="50" value="${node.nodeName }"/>
	<et:formField inputName="ntype" labelName="节点类型" required="true" type="radio" displayMap="${systemNodeType }" value="${node.nodeType }"/>
	<et:formField inputName="displayOrder" labelName="展示顺序" required="true" max="100000" value="${node.displayOrder }"/>
	<et:formField inputName="url" labelName="URL" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_URL_AUTOCOMPLETE %>" value="${node.url }"/>
	<et:formField inputName="icon" labelName="ICON css" value="${node.icon }" />
</et:form>
