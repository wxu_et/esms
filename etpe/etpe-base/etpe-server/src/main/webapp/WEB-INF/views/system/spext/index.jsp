<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index>
	<et:search column="4">
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPEXT_DATA%>" dataCheckbox="false">
		<%-- <et:pageField tableHeadName="操作"  value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_VIEW %>" linkUrlParam="spId" dataSortable="false" /> --%>
		<et:pageField tableHeadName="商户编号" value="spId" />
		<et:pageField tableHeadName="已消费配额" value="costRationNum" />
		<et:pageField tableHeadName="更新时间" value="updateTm" type="dateTime" />
	</et:page>
</et:index>
<%-- <script>
	function oper(value, row, index) {
		return '<a data-toggle="modal" data-target=".modal" href="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_ROLEUSER%>?roleid='+row.roleId+'">角色人员分配</a>';
	}
</script> --%>