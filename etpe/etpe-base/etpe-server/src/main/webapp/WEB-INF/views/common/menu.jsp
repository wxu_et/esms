<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav id="sidebar-nav" class="sidebar-nav">
	<div id="sidebar-wrapper" class="sidebar-wrapper"
		data-background="dark">
		<component>sidebar-nav</component>
		<ul class="side-nav-top nav">
			<li class="sidebar-nav-logo"><component>sidebar-nav-logo</component>
				<span class="logo-text"> <b>ETBOSS</b> 企业管理平台
			</span></li>
			<li class="sidebar-nav-profile"><component>sidebar-nav-profile</component>
				<div class="profile-right">
					<span class="name">您好，${currentUser.userName }<i
						id="user-profile-settings-toggler"
						class="fa fa-caret-down l-pad-5"></i>
					</span> <span class="info" id="dateNow"></span>
				</div></li>
			<!-- Profile settings -->
			<li class="side-nav-profile-settings" style="display: none;">
				<ul class="inline-list">
					<!-- <li><a class="btn btn-white-hover mb-5" data-placement="top"
						data-toggle="tooltip" data-original-title="修改密码"> <i
							class="fa fa-lock"></i>
					</a></li>
					<li><a class="btn btn-white-hover mb-5" data-placement="top"
						data-toggle="tooltip" data-original-title="系统帮助"> <i
							class="fa fa-support"></i>
					</a></li> -->
					<li><a class="btn btn-white-hover mb-5" data-placement="top"
						data-toggle="tooltip" data-original-title="退出系统" href="${pageContext.request.contextPath }/logout">
							<i class="fa fa-sign-out l-pad-5"></i>
					</a></li>
				</ul>
			</li>
			<!-- / Profile settings -->
		</ul>
		<ul class="side-nav-top nav">
			<li class="side-nav-group searchbar" id="menuClear"><span
				class="closebtn" data-placement="top" data-toggle="tooltip"
				data-original-title="搜索菜单"> <i class="fa fa-close"></i>
			</span> <input id="menuSelect" type="text" placeholder="搜索菜单"
				class="form-control input-sm mb-5"></li>
		</ul>

		<ul id="hiden" style="display: none" data-background="dark"
			class="side-nav side-nav-info active nav nav-third-level"></ul>

		<ul id="side-nav" class="side-nav nav">
			${menuHtml }
			<li class="side-nav-info">
				<div class="row">
					<div class="col-md-12 footbgleft">Copyright &copy; 1999-2016
						ETONENET,INC. All Rights Reserved.</div>
				</div>
			</li>
		</ul>
	</div>
</nav>
<script>
	$("#menuClear").click(function() {
		$('#menuSelect').val('');
		$("#hiden").hide();
		$("#side-nav").show();
	});

	$('#menuSelect').bind(
			'input propertychange',
			function() {
				var arr = new Array();
				var menuSelect = $("#menuSelect").val();
				$(".nav-second-level ").each(
						function() {
							$(this).find("li").each(
									function() {
										var text = $(this).text();
										var html = $(this).html();
										if (text.toLowerCase().indexOf(
												menuSelect.toLowerCase()) >= 0
												&& html.indexOf('#') < 0
												&& menuSelect != null
												&& menuSelect != '') {
											arr.unshift(html);
										}
									});
						});
				if (arr.length > 0) {
					$("#hiden").show();
					$("#side-nav").hide();
					var html = "";
					for (var a = 0; a < arr.length; a++) {
						html = '<li>' + arr[a] + '<br/>' + html + '</li>';
					}
					$("#hiden").html(html);
				} else if (menuSelect == null || menuSelect == '') {
					$("#hiden").hide();
					$("#side-nav").show();
				} else {
					$("#side-nav").hide();
					$("#hiden").show();
					$("#hiden").html('');
				}

			});
	function menuTo(url, id) {
		url = '${pageContext.request.contextPath}'+url
		var navParent = $("#nav-tabs");
		var tabParent = $('#tab-content');

		var div = tabParent.find('#' + id);
		var a = $("a[data-toggle='tab'][href='#" + id + "']")
		if (div.length == 0 && a.length == 0) {
			navParent.find('.active').removeAttr('class')
			tabParent.find('.tab-pane').removeClass('active')

			div = $('<div class="tab-pane active"></div>');
			div.attr("id", id);

			var li = $('<li class="active"><a data-toggle="tab" href="#'+id+'"aria-expanded="true"> '
					+ id + '</a><span class="tab-close">X</span></li>');
			navParent.append(li);
			li.find('.tab-close').click(function() {
				if (li.hasClass('active')) {
					if (li.next().length != 0)
						li.next().find('a').click();
					else if (li.prev().length != 0)
						li.prev().find('a').click();
				}
				li.remove();
				div.remove();
			})
			$.ajax({
				type : "post",
				url : url,
				async : false,
				dataType : 'html',
				success : function(result) {
					var result = $(result);
					result.find('script').appendTo(div);
					result.appendTo(div);
					tabParent.append(div)
				}
			});
		} else
			a.click();

	}

	function showTime() {
		var dateTime = new Date();
		var week;
		switch (dateTime.getDay()) {
		case 1:
			week = "星期一 ";
			break;
		case 2:
			week = "星期二 ";
			break;
		case 3:
			week = "星期三 ";
			break;
		case 4:
			week = "星期四 ";
			break;
		case 5:
			week = "星期五 ";
			break;
		case 6:
			week = "星期六 ";
			break;
		default:
			week = "星期天 ";
			break;
		}

		var year = dateTime.getFullYear();
		if (year < 10)
			year = "0" + year;

		var month = dateTime.getMonth() + 1;
		if (month < 10)
			month = "0" + month;

		var day = dateTime.getDate();
		if (day < 10)
			day = "0" + day;

		var hours = dateTime.getHours();
		if (hours < 10)
			hours = "0" + hours;

		var minutes = dateTime.getMinutes();
		if (minutes < 10)
			minutes = "0" + minutes;

		var seconds = dateTime.getSeconds();
		if (seconds < 10)
			seconds = "0" + seconds;

		var dateStr = year + "年" + month + "月" + day + "日 " + week + hours
				+ ":" + minutes + ":" + seconds + " ";
		$("#dateNow").html(dateStr);
	}

	setInterval("showTime()", 1000);

	$(document).ready(function() {
		$(".tab-arrow-left").click(function() {
			var scrleft = $("#nav-tabs").scrollLeft();
			if (scrleft > 0)
				$("#nav-tabs").animate({
					scrollLeft : (scrleft - 120)
				}, 150);
		});
		$(".tab-arrow-right").click(function() {
			var scrleft = $("#nav-tabs").scrollLeft();
			var liwidths = $("#nav-tabs")[0].scrollWidth;
			if (scrleft < liwidths)
				$("#nav-tabs").animate({
					scrollLeft : (scrleft + 120)
				}, 150);
		})
	});
</script>