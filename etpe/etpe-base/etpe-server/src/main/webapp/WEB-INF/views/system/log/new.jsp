<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<script src="/static/js/validate/jquery.validate.min.js?v=${jsVersion}"></script>
<et:form action="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SP_CREATE %>">
	<et:formField inputName="spId" labelName="SP编号" required="true" maxlength="6" remote="<%=request.getContextPath()+UrlConstants.QYDX_QXJJSJ_SP_SPIDCHECK %>" remoteMsg="SP编号已经存在"/>
	<et:formField inputName="spName" labelName="SP名称" required="true" maxlength="60"/>
	<et:formField inputName="spState" labelName="SP状态" required="true" type="radio" displayMap="${spState }" value="1"/>
	<et:formField inputName="spPassword" labelName="密码" required="true" />
	<et:formField inputName="addressPolicy" labelName="地址策略" required="true" type="radio" displayMap="${addressPolicy }" radioCss="radio" value="0"/>
	<et:formField inputName="routePolicy" labelName="路由策略" required="true" type="radio" displayMap="${routePolicy }" radioCss="radio" value="0"/>
	<et:formField inputName="whiteListPolicy" labelName="白名单策略" required="true" type="radio" displayMap="${whiteListPolicy }" value="1"/>
	<et:formField inputName="spMtmoFlagCheckbox" labelName="SP上下行标志位" required="true" type="checkbox" displayMap="${spMtmoFlag }" checkboxCss="checkbox-floatleft" value="7"/>
	
	<et:formField inputName="mtTimeWindow" labelName="下行时间窗口" />
	<et:formField inputName="smsMtUrl" labelName="SMS上行URL" maxlength="256"/>
	<et:formField inputName="smsMoUrl" labelName="SMS下行URL" maxlength="256"/>
	<et:formField inputName="smsRtUrl" labelName="SMS状态报告URL" maxlength="256"/>
	<et:formField inputName="transmitConnectionLimit" labelName="传输连接限制" max="10000"/>
	<et:formField inputName="transmitIpLimit" labelName="传输IP地址限制" maxlength="256"/>
	<et:formField inputName="transmitMode" labelName="传输方式" type="radio"/>
	<et:formField inputName="transmitSpeedLimit" labelName="传输速度限制" max="10000"/>
	<et:formField inputName="spExtNumber" labelName="SP扩展码" maxlength="20"/>
</et:form>