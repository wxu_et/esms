<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<script src="/static/js/validate/jquery.validate.min.js?v=${jsVersion}"></script>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_UPDATE %>">
	<et:formField inputName="userGroupId" labelName="userGroupId" value="${ug.userGroupId }" type="hidden"/>
	<et:formField inputName="name" labelName="用户组名称" required="true" maxlength="50" value="${ug.name }"/>
	<et:formField inputName="code" labelName="code" required="true" maxlength="20" value="${ug.code }"/>
	<et:formField inputName="parentUserGroup.userGroupId" labelName="上级code-用户组名称" type="select" list="${selectParent }" value="${ug.parentUserGroup.userGroupId }"/>
	<et:formField inputName="isMenu" labelName="是否作为菜单展示" required="true" type="radio" list="${isMenuList }" value="${ug.isMenu }"/>
	<et:formField inputName="menuName" labelName="菜单名" value="${ug.menuName }"/>
	<et:formField inputName="menuPriority" labelName="菜单显示优先级" value="${ug.menuPriority }"/>
	<et:formField inputName="url" value="${ug.urlAction.url }" labelName="url" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERGROUP_AUTOCOMPLETE_URLACTION %>"/>
</et:form>