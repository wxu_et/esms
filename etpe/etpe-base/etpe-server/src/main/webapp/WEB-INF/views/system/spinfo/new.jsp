<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%-- <script src="/static/js/validate/jquery.validate.min.js?v=${jsVersion}"></script> --%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_SPINFO_CREATE %>">
	<et:formField inputName="spId" labelName="商户编号" required="true" />
	<et:formField inputName="bindingIpAddr" labelName="绑定ip" required="true" placeholder="多ip用英文分号;隔开" />
	<et:formField inputName="rationNum" labelName="单日调用上限" required="true" />
	<et:formField inputName="spName" labelName="商户名称" required="true" />
	<et:formField inputName="spPassword" labelName="商户密码" required="true" />
	<et:formField inputName="userId" labelName="BOSS用户编号" required="true" />
	<%-- <et:formField inputName="parentUserGroup.userGroupId" labelName="上级code-用户组名称" type="select" list="${selectParent }"/> --%>
</et:form>