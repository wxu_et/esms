/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : ums

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-05-25 23:23:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_batch_info`
-- ----------------------------
--DROP TABLE IF EXISTS `t_batch_info`;
CREATE TABLE `t_batch_info` (
  `batch_id` bigint(20) NOT NULL COMMENT '批次号',
  `sp_id` bigint(20) NOT NULL COMMENT 'API用户编号',
  `api_tm` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '批次处理时间',
  `status_code` int(1) DEFAULT NULL COMMENT '返回的状态码',
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API调用批次信息';

-- ----------------------------
-- Records of t_batch_info
-- ----------------------------

-- ----------------------------
-- Table structure for `t_phone_api_log`
-- ----------------------------
--DROP TABLE IF EXISTS `t_phone_api_log`;
CREATE TABLE `t_phone_api_log` (
  `log_id` bigint(20) NOT NULL,
  `sp_id` bigint(20) NOT NULL,
  `batch_id` bigint(20) NOT NULL COMMENT '批次编号',
  `phone` varchar(16) NOT NULL COMMENT '调用号码',
  `api_tm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '调用时间',
  `api_status` int(11) NOT NULL COMMENT '返回状态（无匹配/成功匹配/余额不足）',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API调用日志';

-- ----------------------------
-- Records of t_phone_api_log
-- ----------------------------

-- ----------------------------
-- Table structure for `phone_map`
-- ----------------------------
--DROP TABLE IF EXISTS `phone_map`;
/*
CREATE TABLE `phone_map` (
  `phone` char(16) NOT NULL COMMENT '原始的手机号码，以86开头。注意这个与JSON调用的格式有所差异',
  `encoded_phone` char(46) NOT NULL COMMENT '加密后的字符串',
  KEY `idx_phone_map_phone` (`phone`),
  KEY `idx_phone_map_encoded` (`encoded_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='手机号码加密映射表';
*/

-- ----------------------------
-- Table structure for `t_sp_extention`
-- ----------------------------
--DROP TABLE IF EXISTS `t_sp_extention`;
CREATE TABLE `t_sp_extention` (
  `sp_id` bigint(20) NOT NULL COMMENT 'API用户编号',
  `cost_ration_num` bigint(20) NOT NULL COMMENT '已消耗额度',
  `update_tm` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`sp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API用户扩展表';

-- ----------------------------
-- Records of t_sp_extention
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sp_info`
-- ----------------------------
--DROP TABLE IF EXISTS `t_sp_info`;
CREATE TABLE `t_sp_info` (
  `sp_id` bigint(20) NOT NULL,
  `sp_name` varchar(50) NOT NULL,
  `sp_password` varchar(50) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT 'BOSS用户编号',
  `ration_num` bigint(20) NOT NULL COMMENT '额度限制,单日调用次数上限',
  `binding_ip_addr` varchar(20) DEFAULT NULL COMMENT '绑定的ip',
  PRIMARY KEY (`sp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API用户配置';

-- ----------------------------
-- Records of t_sp_info
-- ----------------------------
