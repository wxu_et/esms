package com.etonenet.etpe.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the t_sp_extention database table.
 * 
 */
@Entity
@Table(name="t_sp_extention")
@NamedQuery(name="SpExtention.findAll", query="SELECT s FROM SpExtention s")
public class SpExtention implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="sp_id", unique=true, nullable=false)
	private long spId;

	@Column(name="cost_ration_num", nullable=false)
	private long costRationNum;

	@Column(name="update_tm")
	private Timestamp updateTm;

	public SpExtention() {
	}

	public long getSpId() {
		return this.spId;
	}

	public void setSpId(long spId) {
		this.spId = spId;
	}

	public long getCostRationNum() {
		return this.costRationNum;
	}

	public void setCostRationNum(long costRationNum) {
		this.costRationNum = costRationNum;
	}

	public Timestamp getUpdateTm() {
		return this.updateTm;
	}

	public void setUpdateTm(Timestamp updateTm) {
		this.updateTm = updateTm;
	}

}