package com.etonenet.etpe.repository;

import com.etonenet.etpe.entity.SpInfo;
import com.etonenet.repository.BaseRepository;

public interface SpInfoRepository extends BaseRepository<SpInfo, Long> {

	public SpInfo findBySpId(long spId);

}
