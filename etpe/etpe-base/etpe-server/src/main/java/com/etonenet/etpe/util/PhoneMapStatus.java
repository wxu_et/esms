package com.etonenet.etpe.util;

/**
 * 状态返回码定义
 * 
 * @author wxu
 *
 */
public enum PhoneMapStatus {
	// @formatter:off
	/**OK*/
	CODE_200(200, "OK", ""),
	/**JSON格式错误*/
	CODE_400(400, "Json wrong", "JSON格式错误"),
	/**密码验证失败*/
	CODE_401(401, "Password auth error", "密码验证失败"),
	/**IP不合法*/
	CODE_404(404, "Ip auth error", "IP不合法"),
	/**超出每日调用限额*/
	CODE_500(500, "Out of daily quota", "超出每日调用限额"),
	;
	// @formatter:on

	private int code;
	private String info;
	private String desc;

	private PhoneMapStatus(int code, String info, String desc) {
		this.code = code;
		this.info = info;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
