package com.etonenet.etpe.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_sp_info database table.
 * 
 */
@Entity
@Table(name = "t_sp_info")
@NamedQuery(name = "SpInfo.findAll", query = "SELECT s FROM SpInfo s")
public class SpInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sp_id", unique = true, nullable = false)
	private long spId;

	@Column(name = "binding_ip_addr", length = 20)
	private String bindingIpAddr;

	@Column(name = "ration_num", nullable = false)
	private long rationNum;

	@Column(name = "sp_name", nullable = false, length = 50)
	private String spName;

	@Column(name = "sp_password", nullable = false, length = 50)
	private String spPassword;

	@Column(name = "user_id", nullable = false)
	private long userId;

	public SpInfo() {
	}

	public long getSpId() {
		return this.spId;
	}

	public void setSpId(long spId) {
		this.spId = spId;
	}

	public String getBindingIpAddr() {
		return this.bindingIpAddr;
	}

	public void setBindingIpAddr(String bindingIpAddr) {
		this.bindingIpAddr = bindingIpAddr;
	}

	public long getRationNum() {
		return this.rationNum;
	}

	public void setRationNum(long rationNum) {
		this.rationNum = rationNum;
	}

	public String getSpName() {
		return this.spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public String getSpPassword() {
		return this.spPassword;
	}

	public void setSpPassword(String spPassword) {
		this.spPassword = spPassword;
	}

	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}