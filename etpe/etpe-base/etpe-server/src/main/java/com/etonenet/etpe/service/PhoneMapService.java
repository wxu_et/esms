package com.etonenet.etpe.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etonenet.etpe.entity.SpInfo;
import com.etonenet.etpe.exception.EtpeException;
import com.etonenet.etpe.jdbc.PhoneMapDao;
import com.etonenet.etpe.util.ApiStatus;
import com.etonenet.etpe.util.PhoneMapStatus;

@Service
public class PhoneMapService {

	private static Logger logger = LoggerFactory.getLogger(PhoneMapService.class);

	@Autowired
	private PhoneApiLogService phoneApiLogService;
	@Autowired
	private SpService spService;
	@Autowired
	private PhoneMapDao phoneMapDao;

	/**
	 * 手机号码加密
	 * 
	 * @param phoneList
	 * @param batchId
	 * @param spId
	 * 
	 * @return
	 */
	public List<String> encode(List<String> phoneList, long batchId, long spId) {
		List<String> encodedPhoneList = new ArrayList<>();
		int seq = 0;
		for (String phone : phoneList) {
			ApiStatus apiStatus = ApiStatus.S_1;
			String encodedPhone = phoneMapDao.encodedPhone("86" + phone);
			encodedPhoneList.add(encodedPhone);
			if (encodedPhone != null) {
				apiStatus = ApiStatus.S_0;
			}
			// TODO 判断是否欠费
			phoneApiLogService.log(spId, phone, apiStatus, batchId, seq);
			seq++;
		}
		return encodedPhoneList;
	}

	/**
	 * 批量加密
	 * 
	 * @param phoneList
	 * @param batchId
	 * @param spId
	 * @return
	 * @throws EtpeException
	 */
	public List<String> batchEncode(List<String> phoneList, long batchId, long spId) throws EtpeException {
		List<String> encodedPhoneList = new ArrayList<>();
		try {
			encodedPhoneList = phoneMapDao.encodedPhone(phoneList);
			int seq = 0;
			for (String encodedPhone : encodedPhoneList) {
				ApiStatus apiStatus = ApiStatus.S_1;
				if (encodedPhone != null) {
					apiStatus = ApiStatus.S_0;
				}
				// TODO 判断是否欠费
				phoneApiLogService.log(spId, phoneList.get(seq), apiStatus, batchId, seq);
				seq++;
			}
		} catch (SQLException e) {
			throw new EtpeException(e);
		}
		return encodedPhoneList;

	}

	/**
	 * 获取API用户信息
	 * 
	 * @param spId
	 * @return
	 */
	public SpInfo getSpInfo(long spId) {
		return spService.findBySpId(spId);
	}

	/**
	 * API用户验证：
	 * <p>
	 * 
	 * 账号密码验证
	 * <p>
	 * IP地址绑定验证
	 * <p>
	 * 配额验证
	 * <p>
	 * 
	 * @param spId
	 * @param ip
	 * @param phonesNum
	 *            需要加密的手机号码数量
	 * @return
	 */
	public PhoneMapStatus validation(long spId, String spPassword, String ip, int phonesNum) {
		// 手机号列表不能超过10000
		if (10000 < phonesNum || 0 >= phonesNum) {
			return PhoneMapStatus.CODE_400;
		}

		SpInfo spInfo = getSpInfo(spId);
		// 账号密码验证
		if (spInfo == null || !spInfo.getSpPassword().equals(spPassword)) {
			return PhoneMapStatus.CODE_401;
		}
		// IP地址绑定验证
		for (String bindingIpAddr : spInfo.getBindingIpAddr().split(";")) {
			if (StringUtils.isEmpty(bindingIpAddr) || !StringUtils.equals(ip, bindingIpAddr)) {
				return PhoneMapStatus.CODE_404;
			}
		}

		// 当日配额验证
		long rationNum = spInfo.getRationNum();// 单日可用额度上限
		long rationNumCosted = spService.getRationNum(spId);// 当日已消费额度
		logger.debug("Validation rationNum:{} cost:{}", rationNum, rationNumCosted);
		if (rationNum <= 0 || rationNum <= rationNumCosted) {
			return PhoneMapStatus.CODE_500;
		}

		return PhoneMapStatus.CODE_200;
	}

}
