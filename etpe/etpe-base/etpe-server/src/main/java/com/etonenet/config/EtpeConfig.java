package com.etonenet.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.etonenet.etpe.util.TimeIdService;

@Configuration
@EnableAsync
@EnableScheduling
@EnableAspectJAutoProxy
@ComponentScan(basePackages = { "com.etonenet.etpe.schedule", "com.etonenet.etpe.controller",
		"com.etonenet.etpe.service" })
@PropertySources(value = { @PropertySource("classpath:/threadpool.properties") })
public class EtpeConfig {

	@Autowired
	private Environment env;

	@Bean
	public ThreadPoolTaskExecutor poolTaskExecutor() {
		ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
		poolTaskExecutor.setThreadNamePrefix("ETPE-");
		// 线程池所使用的缓冲队列
		poolTaskExecutor.setQueueCapacity(env.getProperty("pool.queueCapacity", Integer.class));
		// 线程池维护线程的最少数量
		poolTaskExecutor.setCorePoolSize(env.getProperty("pool.corePoolSize", Integer.class));
		// 线程池维护线程的最大数量
		poolTaskExecutor.setMaxPoolSize(env.getProperty("pool.maxPoolSize", Integer.class));
		// 线程池维护线程所允许的空闲时间
		poolTaskExecutor.setKeepAliveSeconds(env.getProperty("pool.keepAliveSeconds", Integer.class));
		poolTaskExecutor.initialize();
		return poolTaskExecutor;
	}

	@Bean(name = "batchIdService")
	public TimeIdService batchIdService() {
		return new TimeIdService(100, 100);
	}

}
