package com.etonenet.etpe.entity;

import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-25T21:27:00.905+0800")
@StaticMetamodel(BatchInfo.class)
public class BatchInfo_ {
	public static volatile SingularAttribute<BatchInfo, Long> batchId;
	public static volatile SingularAttribute<BatchInfo, Timestamp> apiTm;
	public static volatile SingularAttribute<BatchInfo, Long> spId;
	public static volatile SingularAttribute<BatchInfo, Integer> statusCode;
}
