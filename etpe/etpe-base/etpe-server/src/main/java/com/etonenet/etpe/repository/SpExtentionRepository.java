package com.etonenet.etpe.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.etpe.entity.SpExtention;
import com.etonenet.repository.BaseJpaRepository;

public interface SpExtentionRepository extends BaseJpaRepository<SpExtention, Long> {

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "update t_sp_extention set cost_ration_num = cost_ration_num + ?2 where sp_id = ?1")
	public void addRationNum(long spId, long rationNum);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "update t_sp_extention set cost_ration_num = ?2 where sp_id = ?1")
	public void updateRationNum(long spId, long rationNum);
}
