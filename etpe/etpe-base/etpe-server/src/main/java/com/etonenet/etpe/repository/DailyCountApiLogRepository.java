package com.etonenet.etpe.repository;

import com.etonenet.etpe.entity.DailyCountApiLog;
import com.etonenet.repository.BaseRepository;

public interface DailyCountApiLogRepository extends BaseRepository<DailyCountApiLog, Long> {
  
}
