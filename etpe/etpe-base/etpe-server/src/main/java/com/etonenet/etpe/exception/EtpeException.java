package com.etonenet.etpe.exception;

public class EtpeException extends Exception {

	public EtpeException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = 1344875996878808025L;

}
