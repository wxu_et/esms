package com.etonenet.etpe.repository;

import com.etonenet.etpe.entity.BatchInfo;
import com.etonenet.repository.BaseRepository;

public interface BatchInfoRepository extends BaseRepository<BatchInfo, Long> {

}
