package com.etonenet.etpe.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-25T09:41:33.431+0800")
@StaticMetamodel(SpInfo.class)
public class SpInfo_ {
	public static volatile SingularAttribute<SpInfo, Long> spId;
	public static volatile SingularAttribute<SpInfo, String> bindingIpAddr;
	public static volatile SingularAttribute<SpInfo, Long> rationNum;
	public static volatile SingularAttribute<SpInfo, String> spName;
	public static volatile SingularAttribute<SpInfo, String> spPassword;
	public static volatile SingularAttribute<SpInfo, Long> userId;
}
