package com.etonenet.etpe.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the t_phone_map database table.
 * 
 */
@Entity
@Table(name="phone_map")
@NamedQuery(name="PhoneMap.findAll", query="SELECT p FROM PhoneMap p")
public class PhoneMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=16)
	private String phone;

	@Column(name="encoded_phone", nullable=false, length=64)
	private String encodedPhone;

	public PhoneMap() {
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEncodedPhone() {
		return this.encodedPhone;
	}

	public void setEncodedPhone(String encodedPhone) {
		this.encodedPhone = encodedPhone;
	}

}