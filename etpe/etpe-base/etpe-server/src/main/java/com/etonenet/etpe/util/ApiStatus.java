package com.etonenet.etpe.util;

/**
 * phone map api调用结果代码
 * @author wxu
 *
 */
public enum ApiStatus {
	// @formatter:off
	/**成功匹配*/
	S_0(0, "成功匹配", ""),
	/**无匹配*/
	S_1(1, "无匹配", ""),
	/**余额不足*/
	S_2(2, "余额不足", ""),
	;
	// @formatter:on

	private int code;
	private String info;
	private String desc;

	private ApiStatus(int code, String info, String desc) {
		this.code = code;
		this.info = info;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
