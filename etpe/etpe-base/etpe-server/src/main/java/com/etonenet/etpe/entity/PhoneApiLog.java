package com.etonenet.etpe.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_phone_api_log database table.
 * 
 */
@Entity
@Table(name = "t_phone_api_log")
@NamedQuery(name = "PhoneApiLog.findAll", query = "SELECT p FROM PhoneApiLog p")
public class PhoneApiLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "log_id", unique = true, nullable = false)
	private long logId;

	@Column(name = "api_status", nullable = false)
	private int apiStatus;

	@Column(name = "api_tm", nullable = false)
	private Timestamp apiTm;

	@Column(name = "batch_id", nullable = false)
	private long batchId;

	@Column(nullable = false, length = 16)
	private String phone;

	@Column(name = "sp_id", nullable = false)
	private long spId;

	public PhoneApiLog() {
	}

	public long getLogId() {
		return this.logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public int getApiStatus() {
		return this.apiStatus;
	}

	public void setApiStatus(int apiStatus) {
		this.apiStatus = apiStatus;
	}

	public Timestamp getApiTm() {
		return this.apiTm;
	}

	public void setApiTm(Timestamp apiTm) {
		this.apiTm = apiTm;
	}

	public long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getSpId() {
		return this.spId;
	}

	public void setSpId(long spId) {
		this.spId = spId;
	}
}