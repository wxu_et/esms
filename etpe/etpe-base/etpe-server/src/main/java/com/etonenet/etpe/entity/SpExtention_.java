package com.etonenet.etpe.entity;

import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-25T22:50:23.344+0800")
@StaticMetamodel(SpExtention.class)
public class SpExtention_ {
	public static volatile SingularAttribute<SpExtention, Long> costRationNum;
	public static volatile SingularAttribute<SpExtention, Long> spId;
	public static volatile SingularAttribute<SpExtention, Timestamp> updateTm;
}
