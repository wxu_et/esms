package com.etonenet.etpe.entity;

import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-25T22:50:23.340+0800")
@StaticMetamodel(PhoneApiLog.class)
public class PhoneApiLog_ {
	public static volatile SingularAttribute<PhoneApiLog, Long> logId;
	public static volatile SingularAttribute<PhoneApiLog, Integer> apiStatus;
	public static volatile SingularAttribute<PhoneApiLog, Timestamp> apiTm;
	public static volatile SingularAttribute<PhoneApiLog, Long> batchId;
	public static volatile SingularAttribute<PhoneApiLog, String> phone;
	public static volatile SingularAttribute<PhoneApiLog, Long> spId;
}
