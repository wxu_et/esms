package com.etonenet.etpe.service;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etonenet.etpe.entity.BatchInfo;
import com.etonenet.etpe.repository.BatchInfoRepository;
import com.etonenet.etpe.util.TimeIdService;

@Service
public class BatchInfoService {

	@Resource
	private TimeIdService batchIdService;
	@Autowired
	private BatchInfoRepository batchInfoRepository;

	/**
	 * 记录批次信息
	 * 
	 * @param spId
	 * @param batchId
	 * @param code
	 * @return
	 */
	public long log(long spId, long batchId, int code) {
		BatchInfo batchInfo = new BatchInfo();
		batchInfo.setBatchId(batchId);
		batchInfo.setApiTm(new Timestamp(System.currentTimeMillis()));
		batchInfo.setSpId(spId);
		batchInfo.setStatusCode(code);
		batchInfoRepository.save(batchInfo);
		return batchId;
	}

	/**
	 * 获取批次号
	 * 
	 * @return
	 */
	public long getBatchId() {
		return batchIdService.getId();
	}

}
