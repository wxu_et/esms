package com.etonenet.etpe.repository;

import com.etonenet.etpe.entity.PhoneMap;
import com.etonenet.repository.BaseRepository;

public interface PhoneMapRepository extends BaseRepository<PhoneMap, String> {

}
