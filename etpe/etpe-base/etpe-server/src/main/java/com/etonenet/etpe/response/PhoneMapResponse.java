package com.etonenet.etpe.response;

import java.util.List;

import com.etonenet.etpe.util.PhoneMapStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PhoneMapResponse extends BaseResponse {

	@JsonProperty(value = "code", required = true)
	private String code;
	@JsonProperty(value = "info", required = true)
	private String info;
	@JsonProperty(value = "phones", required = false)
	private List<String> phones;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public List<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		this.phones = phones;
	}

	public void setPhoneMapStatus(PhoneMapStatus status) {
		this.code = String.valueOf(status.getCode());
		this.info = status.getInfo();
	}

}
