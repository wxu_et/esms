package com.etonenet.etpe.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.etpe.entity.SpExtention;
import com.etonenet.etpe.repository.SpExtentionRepository;
import com.etonenet.etpe.util.UrlConstants;
import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.TilesUtil;
import com.etonenet.web.PageRequest;
import com.etonenet.web.SpecificationCondition;
import com.etonenet.web.SpecificationHelper;

@RestController
@RequestMapping
public class SpExtentionController {
  @Autowired
  private SpExtentionRepository spExtentionRepository;
  
  @RequestMapping(UrlConstants.SYSTEM_SPEXT_INDEX)
  public ModelAndView sysSpInfoIndex() {
    ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.SYSTEM_SPEXT_INDEX));
    return mv;
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_SPEXT_DATA)
  public AjaxPage sysSpInfoData(Integer pageNumber, Integer pageSize,
      String sortName, String sortOrder) {
    List<SpecificationCondition> conditions = new ArrayList<>();

    SpecificationHelper<SpExtention> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

    Page<SpExtention> p =
        spExtentionRepository.findAll(sh.createSpecification(), new PageRequest(pageNumber, pageSize));
    return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
  }
}
