package com.etonenet.etpe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etonenet.etpe.service.SpService;

@RestController
@RequestMapping("/console")
public class ConsoleController {

	@Autowired
	private SpService spService;

	@RequestMapping(value = "/spinfo/reload", method = RequestMethod.GET)
	public void reloadSpInfo() {
		spService.loadSpInfoAll();
	}
}
