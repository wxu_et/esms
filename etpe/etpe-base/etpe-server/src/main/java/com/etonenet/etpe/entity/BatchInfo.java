package com.etonenet.etpe.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the t_batch_info database table.
 * 
 */
@Entity
@Table(name="t_batch_info")
@NamedQuery(name="BatchInfo.findAll", query="SELECT b FROM BatchInfo b")
public class BatchInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="batch_id", unique=true, nullable=false)
	private long batchId;

	@Column(name="api_tm", nullable=false)
	private Timestamp apiTm;

	@Column(name="sp_id", nullable=false)
	private long spId;

	@Column(name="status_code")
	private int statusCode;

	public BatchInfo() {
	}

	public long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public Timestamp getApiTm() {
		return this.apiTm;
	}

	public void setApiTm(Timestamp apiTm) {
		this.apiTm = apiTm;
	}

	public long getSpId() {
		return this.spId;
	}

	public void setSpId(long spId) {
		this.spId = spId;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}