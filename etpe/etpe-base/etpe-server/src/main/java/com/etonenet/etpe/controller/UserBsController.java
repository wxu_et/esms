package com.etonenet.etpe.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.entity.system.security.UserBs;
import com.etonenet.repository.system.security.UserBsRepository;
import com.etonenet.taglibs.component.AjaxPage;
import com.etonenet.taglibs.util.AjaxPageUtil;
import com.etonenet.util.TilesUtil;
import com.etonenet.web.PageRequest;
import com.etonenet.web.SpecificationCondition;
import com.etonenet.web.SpecificationHelper;
import com.etonenet.web.UrlConstants;

@RestController
@RequestMapping
public class UserBsController {
  @Autowired
  private UserBsRepository userBsRepository;
  
  @RequestMapping(UrlConstants.SYSTEM_USERBS_INDEX)
  public ModelAndView sysUserBsIndex() {
    ModelAndView mv = new ModelAndView(TilesUtil.getPath(UrlConstants.SYSTEM_USERBS_INDEX));
    return mv;
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_DATA)
  public AjaxPage sysUserBsData(Integer pageNumber, Integer pageSize, String loginName, String sortName,
      String sortOrder) {
    List<SpecificationCondition> conditions = new ArrayList<>();
    conditions.add(SpecificationCondition.equalOrLikeIgnoreCase("loginName", loginName));

    SpecificationHelper<UserBs> sh = new SpecificationHelper<>(conditions, sortOrder, sortName);

    Page<UserBs> p =
        userBsRepository.findAll(sh.createSpecification(), new PageRequest(pageNumber, pageSize));
    return AjaxPageUtil.toAjaxPage(p.getTotalElements(), p.getContent());
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_NEW)
  public ModelAndView sysUserBsNew() {
    ModelAndView mv = new ModelAndView(UrlConstants.SYSTEM_USERBS_NEW);

    return mv;
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_VIEW)
  public ModelAndView sysUserBsView(Long userId) {
    ModelAndView mv = new ModelAndView(UrlConstants.SYSTEM_USERBS_VIEW);
    mv.addObject("userBs", userBsRepository.findOne(userId));

    return mv;
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_UPDATE)
  public String sysUserBsUpdate(UserBs userBs) {
    userBsRepository.save(userBs);

    return "修改成功";
  }

  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_CREATE)
  public String sysUserBsData(UserBs userBs) {
    userBsRepository.save(userBs);

    return "保存成功";
  }
  
  @ResponseBody
  @RequestMapping(UrlConstants.SYSTEM_USERBS_DEL)
  public String sysUserBsDel(@RequestBody List<UserBs> data) {
    userBsRepository.delete(data);
    
    return "删除成功";
  }
}
