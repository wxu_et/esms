package com.etonenet.etpe.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_daily_count_api_log")
public class DailyCountApiLog implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  /*
   * 用户账号
   */
  @Column(name = "SP_ID")
  private String spId;
  /*
   * 调用总量
   */
  @Column(name = "TOTAL_COUNT")
  private Integer totalCount;
  /*
   * 调用成功总量
   */
  @Column(name = "SUCCESS_COUNT")
  private Integer successCount;
  /*
   * 统计日期
   */
  @Column(name = "COUNT_TIME")
  private Date countTime;
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getSpId() {
    return spId;
  }
  public void setSpId(String spId) {
    this.spId = spId;
  }
  public Integer getTotalCount() {
    return totalCount;
  }
  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }
  public Integer getSuccessCount() {
    return successCount;
  }
  public void setSuccessCount(Integer successCount) {
    this.successCount = successCount;
  }
  public Date getCountTime() {
    return countTime;
  }
  public void setCountTime(Date countTime) {
    this.countTime = countTime;
  }

}
