package com.etonenet.etpe.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.etonenet.etpe.entity.PhoneApiLog;
import com.etonenet.repository.BaseRepository;

public interface PhoneApiLogRepository extends BaseRepository<PhoneApiLog, Long> {
  /*
   * 根据账号统计调用总量
   */
  @Query(nativeQuery = true,
      value = "select sp_id, count(*) from t_phone_api_log where api_tm >= ?1 and api_tm < ?2 group by sp_id")
  public List<Object> countBySpId(Date from, Date to);

  /*
   * 根据账号统计调用成功总量
   */
  @Query(nativeQuery = true,
      value = "select sp_id, count(*) from t_phone_api_log where api_tm >= ?1 and api_tm < ?2 and api_status = ?3 group by sp_id")
  public List<Object> countBySpIdSuccess(Date from, Date to, int apiStatus);

}
