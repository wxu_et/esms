package com.etonenet.etpe.util;

import com.elinku.ums.uuid.DecimalIdImpl;
import com.elinku.ums.uuid.RandomNode;

/**
 * <pre>
 * 批次号生成器uuid
 * 999999999999 timeDigits
 * 00000000000099 nodeDigits
 * 0000000000000099 sequenceDigits
 * </pre>
 * 
 * @author wxu
 *
 */
public class TimeIdService extends DecimalIdImpl {
	private long timeDigits = 10000000000L;
	private long nodeDigits = 100;
	private long sequenceDigits = 100;

	@Override
	public long getId() {
		long nodeValue = node.getNode() % nodeDigits;
		nodeValue = (nodeValue < 0) ? (nodeValue + nodeDigits) : nodeValue;
		long sequenceValue = sequence.getSequence() % sequenceDigits;
		sequenceValue = (sequenceValue < 0) ? (sequenceValue + sequenceDigits) : sequenceValue;
		// 去除毫秒值
		// long clockValue = clock.getTime() / 1000 % timeDigits;
		long clockValue = clock.getTime() / 1000;
		System.out.println(timeDigits);
		return clockValue * nodeDigits * sequenceDigits + (nodeValue * sequenceDigits) + sequenceValue;
	}

	/**
	 * clockValue * nodeDigits * sequenceDigits + (nodeValue * sequenceDigits) +
	 * sequenceValue
	 * 
	 * @param nodeDigits
	 *            节点位数
	 * @param sequenceDigits
	 *            序列位数
	 */
	public TimeIdService(long nodeDigits, long sequenceDigits) {
		this.nodeDigits = nodeDigits;
		this.sequenceDigits = sequenceDigits;
		setNode(new RandomNode(0));
		init();
	}

}
