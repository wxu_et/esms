package com.etonenet.etpe.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EtpeRequest<T extends BaseRequest> extends BaseRequest {

	@JsonProperty(value = "userKey", required = true)
	private String spId;
	@JsonProperty(value = "userVal", required = true)
	private String spPassword;
	@JsonProperty(value = "phones", required = true)
	private List<T> phones;

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getSpPassword() {
		return spPassword;
	}

	public void setSpPassword(String spPassword) {
		this.spPassword = spPassword;
	}

}
