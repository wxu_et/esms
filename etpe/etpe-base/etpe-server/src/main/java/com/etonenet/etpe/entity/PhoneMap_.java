package com.etonenet.etpe.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-25T09:41:33.430+0800")
@StaticMetamodel(PhoneMap.class)
public class PhoneMap_ {
	public static volatile SingularAttribute<PhoneMap, String> phone;
	public static volatile SingularAttribute<PhoneMap, String> encodedPhone;
}
