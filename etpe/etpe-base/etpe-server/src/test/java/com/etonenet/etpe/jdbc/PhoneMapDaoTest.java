package com.etonenet.etpe.jdbc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.etonenet.config.JpaConfigTest;
import com.etonenet.etpe.entity.PhoneApiLog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfigTest.class })
public class PhoneMapDaoTest {
	private static Logger logger = LoggerFactory.getLogger(PhoneMapDaoTest.class);

	@Autowired
	private PhoneMapDao dao;

	@Test
	public void encodedPhone() {
		System.out.println(dao.encodedPhone("12345w"));
	}

	@Test
	public void encodedPhoneList() {
		ArrayList<String> phones = randomPhoneList();
		try {
			List<String> encodedList = dao.encodedPhone(phones);

			System.out.println(Arrays.toString(encodedList.toArray()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<String> randomPhoneList() {
		Random random = new Random(System.currentTimeMillis());
		ArrayList<String> phones = new ArrayList<String>();

		for (int i = 0; i < 9999; i++) {
			long nextLong = random.nextLong();
			long tel = nextLong % 10000000000L;
			tel = tel < 0 ? tel * -1 : tel;
			String phone = "861" + tel;
			phones.add(phone);
		}
		return phones;
	}

	@Test
	public void batchUpdate() throws SQLException {

		List<PhoneApiLog> phoneApiLogs = new ArrayList<>();
		PhoneApiLog e = new PhoneApiLog();
		e.setBatchId(1);
		e.setSpId(1);
		e.setLogId(1);
		e.setPhone("23");
		e.setApiStatus(0);
		phoneApiLogs.add(e);
		int[] batchUpdate = dao.batchUpdate(phoneApiLogs);
		System.out.println(Arrays.toString(batchUpdate));

	}

	@Test
	public void radomTel() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<String> tels = randomPhoneList();
		System.out.println(mapper.writeValueAsString(tels));
	}

	@Test
	public void jdbcTest() {

		ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
		poolTaskExecutor.setThreadNamePrefix("ETPE-");
		poolTaskExecutor.setQueueCapacity(10);
		poolTaskExecutor.setCorePoolSize(10);
		poolTaskExecutor.initialize();

		Runnable task = new Runnable() {
			public void run() {
				long start = System.currentTimeMillis();
				logger.info("start...");
				Random random = new Random(start);
				ArrayList<String> tels = new ArrayList<String>();

				for (int i = 0; i < 9999; i++) {
					long nextLong = random.nextLong();
					long tel = nextLong % 10000000000L;
					tel = tel < 0 ? tel * -1 : tel;
					String phone = "861" + tel;
					tels.add(phone);
				}
				logger.info("end...cost:{}", System.currentTimeMillis() - start);
			}
		};

		for (int i = 0; i < 40; i++) {
			poolTaskExecutor.execute(task);
		}

		while (true) {
			try {
				Thread.sleep(10000);
				logger.info("pool active:{}", poolTaskExecutor.getActiveCount());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
