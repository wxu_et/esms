package com.etonenet.integration;

import java.net.URI;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.etonenet.etpe.request.PhoneMapRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 平台接口测试
 * 
 * @author wxu
 *
 */
public class PhoneMapApiTest {
	private static final String CONFIG_PORT = "9026";
	private static final String CONFIG_HOST = "10.1.51.8";

	private static final int POOL_THREAD = 30;
	private static final int Q_LIMIT = 2000;

	private static Logger logger = LoggerFactory.getLogger(PhoneMapApiTest.class);

	private AtomicLong counter = new AtomicLong(0);

	/**
	 * 登陆接口
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void login() throws JsonProcessingException {
		RestTemplate template = new RestTemplate();
		String httpUrl = "http://10.1.51.8:9026/etpe/login";
		URI uri = UriComponentsBuilder.fromHttpUrl(httpUrl).host(CONFIG_HOST).port(CONFIG_PORT).build().toUri();
		Object response = template.getForObject(uri, String.class);
		logger.info("{}", response);
	}

	@Test
	public void phoneMapEncode() throws JsonProcessingException {
		RestTemplate template = new RestTemplate();
		String httpUrl = "http://10.1.51.8:9026/etpe/interface/phonemap/encode";
		URI uri = UriComponentsBuilder.fromHttpUrl(httpUrl).host(CONFIG_HOST).port(CONFIG_PORT).build().toUri();
		String response = template.postForObject(uri, phoneMapRequest(), String.class);
		logger.debug("{}", response);
	}
	public void phoneMapBatchEncode() throws JsonProcessingException {
		RestTemplate template = new RestTemplate();
		String httpUrl = "http://10.1.51.8:9026/etpe/interface/phonemap/batchencode";
		URI uri = UriComponentsBuilder.fromHttpUrl(httpUrl).host(CONFIG_HOST).port(CONFIG_PORT).build().toUri();
		String response = template.postForObject(uri, phoneMapRequest(), String.class);
		logger.debug("{}", response);
	}

	/**
	 * 压力测试
	 */
	@Test
	public void multiThread() {
		ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
		poolTaskExecutor.setThreadNamePrefix("ETPE-");
		poolTaskExecutor.setQueueCapacity(10);
		poolTaskExecutor.setCorePoolSize(POOL_THREAD);
		poolTaskExecutor.initialize();

		Runnable task = new Runnable() {
			public void run() {
				logger.debug("{}", Thread.currentThread().getName());
				while (true) {
					try {
						phoneMapBatchEncode();
						logger.debug("count:{}", counter.addAndGet(Q_LIMIT));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
				}
			}
		};

		long start = System.currentTimeMillis();
		logger.info("{}", start);
		for (int i = 0; i < poolTaskExecutor.getCorePoolSize(); i++) {
			poolTaskExecutor.execute(task);
		}

		while (true) {
			try {
				Thread.sleep(10000);
				int activeCount = poolTaskExecutor.getActiveCount();
				long cost = (System.currentTimeMillis() - start) / 1000;
				long count = counter.get();
				long rate = count / cost;
				logger.info("pool active:{},cost time:{}s count:{}q rate:{}qps", activeCount, cost, count, rate);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private PhoneMapRequest phoneMapRequest() {
		Random random = new Random(System.currentTimeMillis());
		ArrayList<String> tels = new ArrayList<String>();

		for (int i = 0; i < Q_LIMIT; i++) {
			long nextLong = random.nextLong();
			long tel = nextLong % 10000000000L;
			tel = tel < 0 ? tel * -1 : tel;
			String phone = "1" + tel;
			tels.add(phone);
		}

		PhoneMapRequest phoneMapRequest = new PhoneMapRequest();
		phoneMapRequest.setSpId("10000");
		phoneMapRequest.setSpPassword("10000");
		phoneMapRequest.setPhones(tels);
		return phoneMapRequest;
	}
}
