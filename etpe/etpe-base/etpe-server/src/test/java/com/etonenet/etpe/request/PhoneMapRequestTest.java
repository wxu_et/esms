package com.etonenet.etpe.request;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class PhoneMapRequestTest {

	@SuppressWarnings("unchecked")
	@Test
	public void test() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		PhoneMapRequest value = new PhoneMapRequest();
		value.setSpId("spid");
		value.setSpPassword("sppassword");
		ArrayList<String> phones = new ArrayList<String>();
		phones.add("1");
		phones.add("2");
		phones.add("3");
		phones.add("4");
		phones.add("5");
		phones.add("6");
		phones.add("7");
		phones.add("8");
		value.setPhones(phones);
		String json = mapper.writeValueAsString(value);
		
		System.out.println(json);
		PhoneMapRequest readValue = mapper.readValue(json, PhoneMapRequest.class);
		System.out.println(StringUtils.join(readValue.getPhones()));
	}

}
