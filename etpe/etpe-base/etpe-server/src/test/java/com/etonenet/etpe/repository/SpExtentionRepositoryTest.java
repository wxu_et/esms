package com.etonenet.etpe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.etonenet.config.JpaConfig;
import com.etonenet.etpe.entity.SpExtention;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class })
public class SpExtentionRepositoryTest {

	@Autowired
	private SpExtentionRepository repository;

	@Test
	public void testAddRationNum() {
		int costRationNum = 0;

		// test init
		SpExtention spExtention = new SpExtention();
		spExtention.setSpId(0);
		spExtention.setCostRationNum(costRationNum);
		repository.delete(spExtention);
		repository.save(spExtention);

		// add ration num
		int adding = 10;
		repository.addRationNum(costRationNum, adding);

		// test result
		spExtention = repository.findOne(spExtention.getSpId());
		Assert.isTrue(costRationNum + adding == spExtention.getCostRationNum(), "应该相等却不等");
	}

}
