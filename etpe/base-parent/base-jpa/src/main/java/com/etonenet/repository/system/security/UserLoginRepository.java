package com.etonenet.repository.system.security;

import com.etonenet.entity.system.UserLogin;
import com.etonenet.repository.BaseRepository;

public interface UserLoginRepository extends BaseRepository<UserLogin, Long> {

}
