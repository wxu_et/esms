package com.etonenet.repository.system;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.entity.system.SysLog;
import com.etonenet.repository.BaseRepository;

public interface SysLogRepository extends BaseRepository<SysLog, Long> {

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	<S extends SysLog> S save(S entity);
}
