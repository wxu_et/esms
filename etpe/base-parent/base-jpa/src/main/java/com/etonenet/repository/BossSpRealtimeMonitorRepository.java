package com.etonenet.repository;

import com.etonenet.entity.BossSpRealtimeMonitor;

public interface BossSpRealtimeMonitorRepository extends BaseRepository<BossSpRealtimeMonitor, Long> {

}
