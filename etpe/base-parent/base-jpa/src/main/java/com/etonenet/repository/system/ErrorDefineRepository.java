package com.etonenet.repository.system;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.ErrorDefine;
import com.etonenet.repository.BaseRepository;

public interface ErrorDefineRepository extends BaseRepository<ErrorDefine, Long> {

	@Query(nativeQuery = true, value = "select DISTINCT HOST  label,HOST value from TC_ERROR_DEFINE where  lower(HOST) like '%'||lower(?1)||'%' and ROWNUM <= 200")
	List<Object[]> auto(String term);

	ErrorDefine findByHost(String host);
}
