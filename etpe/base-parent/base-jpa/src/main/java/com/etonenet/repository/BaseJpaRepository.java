package com.etonenet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@NoRepositoryBean
@Transactional(propagation = Propagation.REQUIRED)
public interface BaseJpaRepository<T, ID extends Serializable>
		extends JpaRepository<T, Serializable>, JpaSpecificationExecutor<T> {

	public Page<T> findAll(Specification<T> spec, Pageable pageable);

	@Override
	<S extends T> S save(S entity);

	@Override
	<S extends T> List<S> save(Iterable<S> entities);

	@Override
	T findOne(Serializable id);

	@Override
	boolean exists(Serializable id);

	@Override
	List<T> findAll();

	@Override
	List<T> findAll(Iterable<Serializable> ids);

	@Override
	long count();

	@Override
	void delete(Serializable id);

	@Override
	void delete(T entity);

	@Override
	void delete(Iterable<? extends T> entities);

	@Override
	void deleteAll();

	@Override
	List<T> findAll(Sort sort);

	@Override
	Page<T> findAll(Pageable pageable);
}
