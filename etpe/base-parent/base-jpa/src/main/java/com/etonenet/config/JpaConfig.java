package com.etonenet.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;
import com.etonenet.repository.system.SystemNodeRepository;
import com.etonenet.repository.system.security.UserBsRepository;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(basePackages = { "com.etonenet.repository" }, transactionManagerRef = "jpaTransactionManager")
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySources(value = { @PropertySource("classpath:/jpa.properties") })
public class JpaConfig {

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {
		DruidDataSource ds = new DruidDataSource();
		ds.setDefaultAutoCommit(false);
		ds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		ds.setUrl(env.getProperty("jdbc.url"));
		ds.setUsername(env.getProperty("jdbc.username"));
		ds.setPassword(env.getProperty("jdbc.password"));

		ds.setMinIdle(env.getProperty("jdbc.minIdle", Integer.class));
		ds.setDefaultAutoCommit(false);
		ds.setPoolPreparedStatements(false);

		// EmbeddedDatabase ds = new EmbeddedDatabaseBuilder().build();

		return ds;
	}

	@Bean(name = "jpaTransactionManager")
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	@Bean
	public EntityManagerFactory entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(env.getProperty("jdbc.ddl", Boolean.class));
		vendorAdapter.setShowSql(env.getProperty("jdbc.showsql", Boolean.class));
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.etonenet.entity");
		factory.setDataSource(dataSource());

		Properties jpaProperties = new Properties();

		jpaProperties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		jpaProperties.put("hibernate.max_fetch_depth", env.getProperty("hibernate.max_fetch_depth"));
		jpaProperties.put("hibernate.jdbc.fetch_size", env.getProperty("hibernate.jdbc.fetch_size"));
		jpaProperties.put("hibernate.jdbc.batch_size", env.getProperty("hibernate.jdbc.batch_size"));
		jpaProperties.put("connection.useUnicode", env.getProperty("connection.useUnicode"));
		jpaProperties.put("connection.characterEncoding", env.getProperty("connection.characterEncoding"));
		jpaProperties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		jpaProperties.put("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
		jpaProperties.put("hibernate.use_sql_comments", env.getProperty("hibernate.use_sql_comments"));
		jpaProperties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));

		factory.setJpaProperties(jpaProperties);
		factory.afterPropertiesSet();

		return factory.getObject();
	}

	@Bean
	@DependsOn("entityManagerFactory")
	public ResourceDatabasePopulator initDatabase(DataSource dataSource) throws Exception {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		// 建表语句
		// populator.addScript(new ClassPathResource("schema.sql"));
		// 初始化数据
		// populator.addScript(new ClassPathResource("data.sql"));
		populator.populate(dataSource.getConnection());
		return populator;
	}

}