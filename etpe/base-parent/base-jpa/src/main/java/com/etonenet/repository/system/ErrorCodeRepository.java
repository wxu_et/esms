package com.etonenet.repository.system;

import com.etonenet.entity.system.ErrorCode;
import com.etonenet.repository.BaseRepository;

public interface ErrorCodeRepository extends BaseRepository<ErrorCode, Long> {

}
