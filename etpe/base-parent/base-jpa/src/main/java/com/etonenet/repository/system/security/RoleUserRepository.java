package com.etonenet.repository.system.security;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.etonenet.entity.system.security.RoleUser;
import com.etonenet.entity.system.security.RoleUserPK;
import com.etonenet.repository.BaseRepository;

public interface RoleUserRepository extends BaseRepository<RoleUser, RoleUserPK> {

	@Query(nativeQuery = true, value = "delete from TS_ROLE_USER where ROLE_ID = ?1")
	@Modifying
	void deleteByRoleId(Long roleId);

	@Query(nativeQuery = true, value = "select * from TS_ROLE_USER where USER_ID = ?1")
	List<RoleUser> findByUserId(Long userId);
}
