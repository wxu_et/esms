<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%-- <script src="/static/js/validate/jquery.validate.min.js?v=${jsVersion}"></script> --%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_CREATE %>">
	<et:formField inputName="userId" labelName="用户编号" type="hidden" />
	<et:formField inputName="loginName" labelName="登录名" required="true" />
	<et:formField inputName="password" labelName="密码" required="true" />
	<et:formField inputName="userName" labelName="用户名" />
	<et:formField inputName="userState" labelName="状态" required="true" />
	<et:formField inputName="contact" labelName="联系人" />
	<et:formField inputName="email" labelName="邮箱" />
	<et:formField inputName="mobile" labelName="手机号" />
	<et:formField inputName="phone" labelName="电话" />
	<et:formField inputName="qq" labelName="QQ" />
	<et:formField inputName="passwordExpire" labelName="passwordExpire" required="true" />
	<et:formField inputName="accountExpire" labelName="accountExpire" required="true" />
	<et:formField inputName="accountLock" labelName="accountLock" required="true" />
<%-- 	<et:formField inputName="parentUserGroup.userGroupId" labelName="上级code-用户组名称" type="select" list="${selectParent }"/> --%>
</et:form>