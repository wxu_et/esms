<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_ROLE_UPDATE %>">
	<et:formField inputName="roleId" labelName="systemNodeId" type="hidden" value="${role.roleId }"/>
	<et:formField inputName="displayName" labelName="角色名" required="true" value="${role.displayName }"/>
	<et:formField inputName="roleName" labelName="显示名称" required="true"  value="${role.roleName}"/>
</et:form>
