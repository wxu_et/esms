<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_NEW %>" 
			delBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_DEL %>">
	<et:search column="4">
		<et:searchField labelName="登录名" inputName="loginName" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_DATA%>" dataCheckbox="true">
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_VIEW %>" linkUrlParam="userId" dataSortable="false" />
		<et:pageField tableHeadName="用户编号" value="userId" />
		<et:pageField tableHeadName="登录名" value="loginName" />
		<et:pageField tableHeadName="密码" value="password" />
		<et:pageField tableHeadName="用户名" value="userName" />
		<et:pageField tableHeadName="状态" value="userState" />
		<et:pageField tableHeadName="联系人" value="contact" />
		<et:pageField tableHeadName="邮箱" value="email" />
		<et:pageField tableHeadName="手机号" value="mobile" />
		<et:pageField tableHeadName="电话" value="phone" />
		<et:pageField tableHeadName="QQ" value="qq" />
		<et:pageField tableHeadName="passwordExpire" value="passwordExpire" />
		<et:pageField tableHeadName="accountExpire" value="accountExpire" />
		<et:pageField tableHeadName="accountLock" value="accountLock" />
	</et:page>
</et:index>
<%-- <script>
	function oper(value, row, index) {
		return '<a data-toggle="modal" data-target=".modal" href="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_ROLEUSER%>?roleid='+row.roleId+'">角色人员分配</a>';
	}
</script> --%>