<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_NODE_CREATE %>">
	<et:formField inputName="parenid" labelName="父节点" type="hidden" value="${parentid }"/>
	<et:formField inputName="nname" labelName="节点名" required="true" maxlength="50"/>
	<et:formField inputName="ntype" labelName="节点类型" required="true" type="radio" displayMap="${systemNodeType }" value="0"/>
	<et:formField inputName="displayOrder" labelName="展示顺序" required="true" max="100000"/>
	<et:formField inputName="url" labelName="URL" type="autocomplete" autocompleteUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_URL_AUTOCOMPLETE %>"/>
	<et:formField inputName="icon" labelName="ICON css" />
</et:form>