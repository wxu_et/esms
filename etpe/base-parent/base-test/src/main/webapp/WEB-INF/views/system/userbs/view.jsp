<%@page import="com.etonenet.web.UrlConstants"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.etonenet.com/tld" prefix="et" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<et:form action="<%=request.getContextPath()+UrlConstants.SYSTEM_USERBS_UPDATE %>">	
	<et:formField inputName="userId" labelName="用户编号" type="hidden" value="${userBs.userId }"/>
	<et:formField inputName="loginName" labelName="登录名" required="true" value="${userBs.loginName }" />
	<et:formField inputName="password" labelName="密码" required="true" value="${userBs.password }" />
	<et:formField inputName="userName" labelName="用户名" value="${userBs.userName }" />
	<et:formField inputName="userState" labelName="状态" required="true" value="${userBs.userState }" />
	<et:formField inputName="contact" labelName="联系人" value="${userBs.contact }" />
	<et:formField inputName="email" labelName="邮箱" value="${userBs.email }" />
	<et:formField inputName="mobile" labelName="手机号" value="${userBs.mobile }" />
	<et:formField inputName="phone" labelName="电话" value="${userBs.phone }" />
	<et:formField inputName="qq" labelName="QQ" value="${userBs.qq }" />
	<et:formField inputName="passwordExpire" labelName="passwordExpire" required="true" value="${userBs.passwordExpire }" />
	<et:formField inputName="accountExpire" labelName="accountExpire" required="true" value="${userBs.accountExpire }" />
	<et:formField inputName="accountLock" labelName="accountLock" required="true" value="${userBs.accountLock }" />
</et:form>
