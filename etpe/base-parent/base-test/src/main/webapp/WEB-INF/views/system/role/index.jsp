<%@page import="com.etonenet.web.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.etonenet.com/tld" prefix="et"%>
<et:index createBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_ROLE_NEW %>" 
			delBtnUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_ROLE_DEL %>">
	<et:search column="4">
		<et:searchField labelName="角色名" inputName="displayName" />
	</et:search>
	<et:page dataUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_ROLE_DATA%>" dataCheckbox="true">
		<%-- <et:pageField tableHeadName="操作" dataFormatter="oper" /> --%>
		<et:pageField tableHeadName="操作" value="查看" type="link" linkTarget="_modal" linkUrl="<%=request.getContextPath()+UrlConstants.SYSTEM_ROLE_VIEW %>" linkUrlParam="roleId" dataSortable="false"/>
		<et:pageField tableHeadName="角色编号" value="roleId" />
		<et:pageField tableHeadName="角色名" value="displayName" />
		<et:pageField tableHeadName="显示名称" value="roleName" />
	</et:page>
</et:index>
<%-- <script>
	function oper(value, row, index) {
		return '<a data-toggle="modal" data-target=".modal" href="<%=request.getContextPath()+UrlConstants.SYSTEM_AUTH_ROLEUSER%>?roleid='+row.roleId+'">角色人员分配</a>';
	}
</script> --%>