function showTip(tip,delay,callback){
	if(delay==undefined)
		delay=1500;
	var tip_msg=$("<span class=\"alert alert-success fade in tip-msg\">"+tip+"</span>");
	$("body").append(tip_msg);
	$(tip_msg).css({
		"z-index": 10000,
	    position: "absolute", 
	    left: ($(window).width()-$(tip_msg).width())/2, 
	    top: ($(window).height()-$(tip_msg).height())/2
	});
	
	setTimeout(function () {
		tip_msg.remove();
		if(callback!=undefined)
			callback.call();
	}, delay);
}

function showConfirm(msg){
	
	
}

//时间处理
var tableSet={
		fn:{
			showDate:function(d,fmt){
				if(fmt==undefined)
					fmt='yyyy-MM-dd hh:mm:ss';
				if(d==undefined)
					return;
			    var o = {
			        "M+": d.getMonth() + 1,
			        "d+": d.getDate(),
			        "h+": d.getHours(),
			        "m+": d.getMinutes(),
			        "s+": d.getSeconds(),
			        "q+": Math.floor((d.getMonth() + 3) / 3),
			        "S": d.getMilliseconds()
			    };
			    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
			    for (var k in o)
			    	if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			    		return fmt;
			},
			ratio:function(number){
				return Math.round(number*10000)/100+'%';
			}
		}
		}
