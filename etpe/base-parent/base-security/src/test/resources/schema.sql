/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-05-22 20:09:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ts_role`
-- ----------------------------
DROP TABLE IF EXISTS `ts_role`;
CREATE TABLE `ts_role` (
  `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DISPLAY_NAME` varchar(100) DEFAULT NULL,
  `ROLE_NAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `ROLE_ID` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `ts_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `ts_role_user`;
CREATE TABLE `ts_role_user` (
  `ROLE_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  UNIQUE KEY `ROLE_ID` (`ROLE_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`),
  KEY `FK2F9430349272679` (`ROLE_ID`),
  CONSTRAINT `FK2F9430349272679` FOREIGN KEY (`ROLE_ID`) REFERENCES `ts_role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `ts_user_bs`
-- ----------------------------
DROP TABLE IF EXISTS `ts_user_bs`;
CREATE TABLE `ts_user_bs` (
  `USER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_EXPIRE` int(11) DEFAULT NULL,
  `ACCOUNT_LOCK` int(11) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `LOGIN_NAME` varchar(50) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `PASSWORD_EXPIRE` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `qq` varchar(30) DEFAULT NULL,
  `USER_NAME` varchar(50) DEFAULT NULL,
  `USER_STATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
