package com.etonenet.web;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class PageRequest extends org.springframework.data.domain.PageRequest {

	public PageRequest(int page, int size) {
		super(--page, size);
	}

	public PageRequest(int page, int size, Direction direction, String[] properties) {
		super(--page, size, direction, properties);
	}

	public PageRequest(int page, int size, Sort sort) {
		super(--page, size, sort);
	}

	public PageRequest(BasePageRequestParam prm) {
		super(prm.getPageNumber(), prm.getPageSize());
	}

}
