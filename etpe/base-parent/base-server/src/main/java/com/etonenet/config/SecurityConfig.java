package com.etonenet.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.etonenet.service.system.security.LoginFailHandler;
import com.etonenet.service.system.security.LoginSuccessHandler;
import com.etonenet.service.system.security.UserDetailsService;

/**
 * spring security配置文件
 * 
 * @author wxu
 *
 */
@Configuration
@ComponentScan(basePackageClasses = { UserDetailsService.class })
@Import(value = { JpaConfig.class })
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private LoginSuccessHandler loginSuccessHandler;
	@Autowired
	private LoginFailHandler loginFailHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth
			.userDetailsService(userDetailsService)
//			.passwordEncoder(new PasswordEncoder())
			;
		// @formatter:on
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http
		.authorizeRequests()
		.antMatchers("/","/favicon.ico").permitAll()
		.antMatchers("/static/**").permitAll()
		.antMatchers("/interface/**").permitAll() // 使用自定义验证方式
		.anyRequest().authenticated()

		.and()
		.formLogin()
		.loginPage("/login")
		.successHandler(loginSuccessHandler)
		.failureHandler(loginFailHandler)
		.permitAll()
		
		.and().logout().logoutSuccessUrl("/").permitAll()
		
//		.and()
//		.rememberMe()
//		.tokenValiditySeconds(2419200)
//		.key("spittrKey")
		
		.and()
		.csrf().disable()
		;
		// @formatter:on
	}

}
