package com.etonenet.web;

/**
 * URL命名规范： /父亲组/子组/子子组/子...组/自定义名称
 * 
 * 
 * @author lyzhang
 * 
 */
public class UrlConstants {

	// system 系统
	// menu
	public static final String SYSTEM_NODE_INDEX = "/system/node/index";
	public static final String SYSTEM_NODE_NEW = "/system/node/new";
	public static final String SYSTEM_NODE_CREATE = "/system/node/create";
	public static final String SYSTEM_NODE_DEL = "/system/node/del";
	public static final String SYSTEM_NODE_VIEW = "/system/node/view";
	public static final String SYSTEM_NODE_UPDATE = "/system/node/update";

	// syslog
	public static final String SYSTEM_LOG_INDEX = "/system/log/index";
	public static final String SYSTEM_LOG_DATA = "/system/log/data";
	public static final String SYSTEM_LOG_AUTOCOMPLETE_OPERUSER = "/system/log/autocomplete/operuser";

	// auth
	public static final String SYSTEM_AUTH_INDEX = "/system/auth/index";
	public static final String SYSTEM_AUTH_DATA = "/system/auth/data";
	public static final String SYSTEM_AUTH_ROLEUSER = "/system/auth/roleuser";
	public static final String SYSTEM_AUTH_CREATE = "/system/auth/create";
	public static final String SYSTEM_AUTH_NEW = "/system/auth/new";
	public static final String SYSTEM_AUTH_VIEW = "/system/auth/view";
	public static final String SYSTEM_AUTH_UPDATE = "/system/auth/update";
	public static final String SYSTEM_AUTH_SAVE = "/system/auth/save";

	// role
	public static final String SYSTEM_ROLE_INDEX = "/system/role/index";
	public static final String SYSTEM_ROLE_DATA = "/system/role/data";
	public static final String SYSTEM_ROLE_NEW = "/system/role/new";
	public static final String SYSTEM_ROLE_VIEW = "/system/role/view";
	public static final String SYSTEM_ROLE_UPDATE = "/system/role/update";
	public static final String SYSTEM_ROLE_CREATE = "/system/role/create";
	public static final String SYSTEM_ROLE_DEL = "/system/role/del";

	// userbs
	public static final String SYSTEM_USERBS_INDEX = "/system/userbs/index";
	public static final String SYSTEM_USERBS_DATA = "/system/userbs/data";
	public static final String SYSTEM_USERBS_SAVE = "/system/userbs/save";
	public static final String SYSTEM_USERBS_NEW = "/system/userbs/new";
	public static final String SYSTEM_USERBS_VIEW = "/system/userbs/view";
	public static final String SYSTEM_USERBS_UPDATE = "/system/userbs/update";
	public static final String SYSTEM_USERBS_CREATE = "/system/userbs/create";
	public static final String SYSTEM_USERBS_DEL = "/system/userbs/del";

	// spinfo
	public static final String SYSTEM_SPINFO_INDEX = "/system/spinfo/index";
	public static final String SYSTEM_SPINFO_DATA = "/system/spinfo/data";
	public static final String SYSTEM_SPINFO_SAVE = "/system/spinfo/save";
	public static final String SYSTEM_SPINFO_NEW = "/system/spinfo/new";
	public static final String SYSTEM_SPINFO_VIEW = "/system/spinfo/view";
	public static final String SYSTEM_SPINFO_UPDATE = "/system/spinfo/update";
	public static final String SYSTEM_SPINFO_CREATE = "/system/spinfo/create";
	public static final String SYSTEM_SPINFO_DEL = "/system/spinfo/del";

}