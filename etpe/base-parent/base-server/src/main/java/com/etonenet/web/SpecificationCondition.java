package com.etonenet.web;

import java.io.Serializable;
import java.util.Date;

public class SpecificationCondition {

	private String path;

	// like,eq
	private String type;

	private Serializable value;

	private Serializable start;

	private Serializable end;

	// pre,after,around
	private String likeType = "after";

	/**
	 * 不等于
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition notEq(String path, Serializable value) {

		return new SpecificationCondition(path, value, "notEq");
	}

	/**
	 * 完全匹配
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition eq(String path, Serializable value) {

		return new SpecificationCondition(path, value, "eq");
	}

	/**
	 * 大于
	 * 
	 * @param path
	 * @param value
	 *            Date类型、 其他类型toString()逻辑
	 * @return
	 */
	public static SpecificationCondition greater(String path, Serializable value) {

		return new SpecificationCondition(path, value, "greater");
	}

	/**
	 * 大于等于
	 * 
	 * @param path
	 * @param value
	 *            Date类型、 其他类型toString()逻辑
	 * @return
	 */
	public static SpecificationCondition greaterOrEqual(String path, Serializable value) {

		return new SpecificationCondition(path, value, "greaterOrEqual");
	}

	/**
	 * 小于
	 * 
	 * @param path
	 * @param value
	 *            Date类型、 其他类型toString()逻辑
	 * @return
	 */
	public static SpecificationCondition less(String path, Serializable value) {

		return new SpecificationCondition(path, value, "less");
	}

	/**
	 * 小于等于
	 * 
	 * @param path
	 * @param value
	 *            Date类型、 其他类型toString()逻辑
	 * @return
	 */
	public static SpecificationCondition lessOrEqual(String path, Serializable value) {

		return new SpecificationCondition(path, value, "lessOrEqual");
	}

	/**
	 * 模糊匹配(忽略大小写) like '%xxx'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likePreIgnoreCase(String path, Serializable value) {

		return new SpecificationCondition(path, value, "likeIgnoreCase", "pre");
	}

	/**
	 * 模糊匹配 (忽略大小写)like 'xxx%'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likeAfterIgnoreCase(String path, Serializable value) {

		return new SpecificationCondition(path, value, "likeIgnoreCase", "after");
	}

	/**
	 * 模糊匹配(忽略大小写) like '%xxx%'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likeAroundIgnoreCase(String path, Serializable value) {

		return new SpecificationCondition(path, value, "likeIgnoreCase", "around");
	}

	/**
	 * 模糊匹配 like '%xxx'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likePre(String path, Serializable value) {

		return new SpecificationCondition(path, value, "like", "pre");
	}

	/**
	 * 模糊匹配like 'xxx%'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likeAfter(String path, Serializable value) {

		return new SpecificationCondition(path, value, "like", "after");
	}

	/**
	 * 模糊匹配 like '%xxx%'
	 * 
	 * @param path
	 * @param value
	 *            String类型
	 * @return
	 */
	public static SpecificationCondition likeAround(String path, Serializable value) {

		return new SpecificationCondition(path, value, "like", "around");
	}

	/**
	 * 范围匹配
	 * 
	 * @param path
	 * @param start
	 *            Date类型、 其他类型toString()逻辑
	 * @param end
	 *            Date类型、 其他类型toString()逻辑
	 * @return
	 */
	public static SpecificationCondition between(String path, Date start, Date end) {

		return new SpecificationCondition(path, start, end, "between");
	}

	/**
	 * @param path
	 * @param value:String类型，如果value
	 *            包含%则模糊匹配,否则模糊匹配
	 * @Return SpecificationCondition(忽略大小写)
	 */
	public static SpecificationCondition equalOrLikeIgnoreCase(String path, Serializable value) {
		return new SpecificationCondition(path, value, "equalOrLikeIgnoreCase");
	}

	/**
	 * @param path
	 * @param value:String类型，如果value包含%则模糊匹配否则精确匹配
	 * @Return SpecificationCondition(区分大小写)
	 */
	public static SpecificationCondition equalOrLike(String path, Serializable value) {
		return new SpecificationCondition(path, value, "equalOrLike");
	}

	private SpecificationCondition(String path, Serializable value, String type) {
		super();
		this.path = path;
		this.value = value;
		this.type = type;
	}

	private SpecificationCondition(String path, Serializable start, Serializable end, String type) {
		super();
		this.value = "value";
		this.path = path;
		this.start = start;
		this.end = end;
		this.type = type;
	}

	public SpecificationCondition(String path, Serializable value, String type, String likeType) {
		super();
		this.path = path;
		this.type = type;
		this.value = value;
		this.likeType = likeType;
	}

	protected String getPath() {
		return path;
	}

	protected String getType() {
		return type;
	}

	protected Serializable getValue() {
		return value;
	}

	protected String getLikeType() {
		return likeType;
	}

	protected Serializable getStart() {
		return start;
	}

	protected Serializable getEnd() {
		return end;
	}

}
