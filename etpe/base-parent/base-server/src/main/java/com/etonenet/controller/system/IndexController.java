package com.etonenet.controller.system;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.etonenet.entity.system.SystemNode;
import com.etonenet.repository.system.SystemNodeRepository;
import com.etonenet.service.system.security.CustomUser;
import com.etonenet.util.SpringUtil;
import com.etonenet.util.TilesUtil;
import com.etonenet.web.UrlConstants;
import com.etonenet.web.decorate.Menu;
import com.etonenet.web.decorate.MenuItem;
import com.etonenet.web.decorate.MenuRoot;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Api(tags = { "系统设置" }, value = "接口页面")
@Controller
public class IndexController {

	@Resource
	private SystemNodeRepository nodeEm;

	@Resource
	private RestTemplate restTemplate;

	@ApiOperation(value = "登陆BOSS的首页")
	@RequestMapping(value = "/boss", method = RequestMethod.GET)
	public ModelAndView boss() {

		return new ModelAndView("frame:boss");
	}

	@ApiOperation(value = "菜单首页")
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {

		ModelAndView mv = new ModelAndView(TilesUtil.getPath("/frame"));
		List<SystemNode> roots = nodeEm.listAllRoot();
		mv.addObject("roots", roots);

		List<MenuRoot> mrs = new ArrayList<>();
		// 系统设置
		MenuRoot mr = new MenuRoot();
		List<Menu> menu = new ArrayList<>();
		List<MenuItem> items = new ArrayList<>();

		Menu m = new Menu();
		MenuItem mi = new MenuItem();

		mr.setId("0");
		m.setId("0");
		m.setText("系统管理");

		mi.setId("0");
		mi.setText("菜单管理");
		mi.setHref(UrlConstants.SYSTEM_NODE_INDEX);
		items.add(mi);
		mi = new MenuItem();
		mi.setId("1");
		mi.setText("日志管理");
		mi.setHref(UrlConstants.SYSTEM_LOG_INDEX);
		items.add(mi);
		mi = new MenuItem();
		mi.setId("2");
		mi.setText("权限管理");
		mi.setHref(UrlConstants.SYSTEM_AUTH_INDEX);
		items.add(mi);

		m.setItems(items);
		menu.add(m);
		mr.setMenu(menu);
		mrs.add(mr);

		for (SystemNode n : roots) {
			mr = new MenuRoot();
			mr.setId(n.getSystemNodeId().toString());

			menu = new ArrayList<>();
			for (SystemNode n2 : nodeEm.listChildren(n.getSystemNodeId())) {
				m = new Menu();
				m.setId(n2.getSystemNodeId().toString());
				m.setText(n2.getNodeName());

				items = new ArrayList<>();
				for (SystemNode n3 : nodeEm.listChildren(n2.getSystemNodeId())) {
					mi = new MenuItem();
					mi.setId(n3.getSystemNodeId().toString());
					mi.setText(n3.getNodeName());
					mi.setHref(n3.getUrl());

					items.add(mi);
				}
				m.setItems(items);
				menu.add(m);
			}
			mr.setMenu(menu);
			mrs.add(mr);
		}

		mv.addObject("tiles", "template:home");
		mv.addObject("currentUser", ((CustomUser) SpringUtil.getUser()).getUser());
		try {
			mv.addObject("menu", new ObjectMapper().writeValueAsString(mrs));
		} catch (JsonProcessingException e) {
		}
		return mv;
	}

	@ApiOperation(value = "登陆页面")
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("frame:/login/login");
		mv.addObject("tiles", "template:login");
		return mv;
	}

	@ApiOperation(value = "欢迎页面")
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView welcome() {

		return new ModelAndView(TilesUtil.getPath("/welcome"));
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView e403() {

		return new ModelAndView("403");
	}

	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public ModelAndView e404() {

		return new ModelAndView("404");
	}

	@RequestMapping(value = "/500", method = RequestMethod.GET)
	public ModelAndView e500() {

		return new ModelAndView("500");
	}
}
