package com.etonenet.expection;

public final class ServiceLogicException extends BossException {

	private static final long serialVersionUID = 4169408421196896282L;

	public static final String EXCEPTION_TYPE = "SERVICE_LOGIC_EXCEPTION";

	public ServiceLogicException(String message) {
		super(message);
	}

	@Override
	public String getType() {
		return EXCEPTION_TYPE;
	}

}
