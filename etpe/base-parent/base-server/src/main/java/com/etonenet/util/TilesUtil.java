package com.etonenet.util;

import org.springframework.stereotype.Component;

/**
 * tiles模板工具类
 * 
 * @author lyzhang
 * 
 */
@Component
public class TilesUtil {

	public static String getPath(String path) {

		path = path.trim();
		if (!path.startsWith("/"))
			path = "/" + path;
		if (path.endsWith("/"))
			path = path.substring(0, path.length() - 1);

		return "index:" + path;
	}
}
