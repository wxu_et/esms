package com.etonenet.service.system;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.entity.system.ErrorCode;
import com.etonenet.repository.system.ErrorCodeRepository;

@Service
public class ErrorCodeService {

	@Resource
	private ErrorCodeRepository errorCodeEm;

	public Page<ErrorCode> page(PageRequest pageRequest, Specification<ErrorCode> spec) {
		return errorCodeEm.findAll(spec, pageRequest);
	}

	@Transactional(rollbackFor = Exception.class)
	public void create(ErrorCode create) {
		errorCodeEm.save(create);
	}

	public ErrorCode findOne(Long errorCodeId) {
		return errorCodeEm.findOne(errorCodeId);
	}

	@Transactional(rollbackFor = Exception.class)
	public ErrorCode update(ErrorCode update) {
		return errorCodeEm.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void del(List<ErrorCode> dels) {
		for (ErrorCode errorCode : dels)
			errorCodeEm.delete(errorCode);
	}

}
