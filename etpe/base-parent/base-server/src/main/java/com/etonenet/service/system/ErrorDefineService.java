package com.etonenet.service.system;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etonenet.entity.system.ErrorDefine;
import com.etonenet.repository.system.ErrorDefineRepository;

@Service
public class ErrorDefineService {

	@Resource
	private ErrorDefineRepository errorDefineRe;

	public Page<ErrorDefine> page(PageRequest pageRequest, Specification<ErrorDefine> spsc) {
		return errorDefineRe.findAll(spsc, pageRequest);
	}

	public List<Object[]> auto(String term) {
		return errorDefineRe.auto(term);
	}

	public ErrorDefine findByHost(String host) {
		return errorDefineRe.findByHost(host);
	}

	public ErrorDefine create(ErrorDefine create) {
		return errorDefineRe.save(create);
	}

	public ErrorDefine updaye(ErrorDefine update) {
		return errorDefineRe.save(update);
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(List<ErrorDefine> list) {
		for (ErrorDefine del : list) {
			errorDefineRe.delete(del);
		}

	}

	public ErrorDefine findById(Long errorDefineId) {
		return errorDefineRe.findOne(errorDefineId);
	}

}
