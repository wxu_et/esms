package com.etonenet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebMvc
@EnableSwagger2 // Loads the spring beans required by the framework
@ComponentScan(basePackages = { "com.etonenet.controller" })
@PropertySource("classpath:swagger.properties") // 配置json路径：http://localhost:8080/t-swagger/v2/api-docs
public class SwaggerConfig extends WebMvcConfigurerAdapter {
	@Bean
	ApiInfo apiInfo() {
		String title = "ETONENET API";
		String description = "手机号码加密接口";
		String version = "1.0.0";
		String licenseUrl = "";
		String license = licenseUrl;
		Contact contactName = new Contact("", "", "");
		String termsOfServiceUrl = licenseUrl;
		return new ApiInfo(title, description, version, termsOfServiceUrl, contactName, license, licenseUrl);
	}

	@Bean
	public Docket customImplementation(ApiInfo apiInfo) {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}