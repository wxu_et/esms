package com.etonenet.web.decorate;

public class MoContentMatch {
	/**
	 * SPID
	 */
	private String spId;
	/**
	 * SP服务代码
	 */
	private String spServiceCode;

	/**
	 * SP内容过滤类别
	 */
	private String spContentFilterType;

	/**
	 * 匹配内容过滤模式
	 */
	private String patternMatchContent;
	/**
	 * 匹配内容过滤类别
	 */
	private String categoryMatchContent;

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getSpServiceCode() {
		return spServiceCode;
	}

	public void setSpServiceCode(String spServiceCode) {
		this.spServiceCode = spServiceCode;
	}

	public String getSpContentFilterType() {
		return spContentFilterType;
	}

	public void setSpContentFilterType(String spContentFilterType) {
		this.spContentFilterType = spContentFilterType;
	}

	public String getPatternMatchContent() {
		return patternMatchContent;
	}

	public void setPatternMatchContent(String patternMatchContent) {
		this.patternMatchContent = patternMatchContent;
	}

	public String getCategoryMatchContent() {
		return categoryMatchContent;
	}

	public void setCategoryMatchContent(String categoryMatchContent) {
		this.categoryMatchContent = categoryMatchContent;
	}

}
