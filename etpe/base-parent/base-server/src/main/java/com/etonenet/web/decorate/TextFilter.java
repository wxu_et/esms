package com.etonenet.web.decorate;

import java.util.Map;

public class TextFilter {

	private String textFilterPattern;

	private String textFilterCategoryFlagName;

	private Map<String, String> map;

	public String getTextFilterPattern() {
		return textFilterPattern;
	}

	public void setTextFilterPattern(String textFilterPattern) {
		this.textFilterPattern = textFilterPattern;
	}

	public String getTextFilterCategoryFlagName() {
		return textFilterCategoryFlagName;
	}

	public void setTextFilterCategoryFlagName(String textFilterCategoryFlagName) {
		this.textFilterCategoryFlagName = textFilterCategoryFlagName;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

}
