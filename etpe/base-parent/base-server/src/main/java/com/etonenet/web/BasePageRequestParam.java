package com.etonenet.web;

import javax.annotation.Nullable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class BasePageRequestParam {

	@Max(value = 500)
	@Min(value = 1)
	@NotNull
	private Integer pageSize;

	@Min(value = 1)
	@NotNull
	private Integer pageNumber;

	@Nullable
	private String sortName;

	@Nullable
	@Pattern(regexp = "desc|asc|^\\s*$")
	private String sortOrder;

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

}
