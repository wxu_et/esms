package com.etonenet.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hello")
public class HomeController {

	@RequestMapping(value = "/home", method = GET)
	public String home(Model model) {
		return "/home";
	}
	
	@RequestMapping(value = "/t/{x}", method = GET)
	public String tell(@PathVariable String x){
		return x;
	}

}
