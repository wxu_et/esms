<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>  
<head>  
<title><tiles:getAsString name="title"/></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<c:if test="${tiles==null||tiles=='template:home' }">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/libs/chosen/1.4.2/chosen.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/jquery/jquery-ui-1.10.4.custom.min.css?v=${cssVersion}">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/bootstrap/3.3.5/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/style.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/themes/theme-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/cs.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assert/uploadify/uploadifive.css">
<script src="${pageContext.request.contextPath}/static/js/bootstrap/jquery.min.js?v=${jsVersion }"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery/jquery-ui.custom.js?v=${jsVersion }"></script>
<!-- <script src="${pageContext.request.contextPath}/static/boss/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
<script src="${pageContext.request.contextPath}/static/boss/js/main.min.js"></script>
<script src="${pageContext.request.contextPath}/static/boss/libs/metisMenu/1.1.3/metisMenu.min.js"></script>

<script src="${pageContext.request.contextPath}/static/js/bootstrap/bootstrap.min.js?v=${jsVersion }"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery/jquery-ui-timepicker-addon.js?v=${jsVersion }"></script>
<script src="${pageContext.request.contextPath}/static/js/common/tip.js?v=${jsVersion}"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/assert/uploadify/jquery.uploadifive.min.js"></script>
<script src="${pageContext.request.contextPath}/static/assert/layer/layer.js?v=${jsVersion }"></script>
</c:if>
<c:if test="${tiles=='template:login' }">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/login/login.css" />
<script src="${pageContext.request.contextPath}/static/js/bootstrap/jquery.min.js?v=${jsVersion }"></script>
<script src="${pageContext.request.contextPath}/static/js/common/util.js?v=${jsVersion}"></script>
</c:if>
</head>  
<body data-theme="sky">
<c:if test="${tiles==null||tiles=='template:home' }">
<div id="wrapper">
	<tiles:insertAttribute name="menu" />
	<div class="container-wrapper">
		<div class="row">
			<div class="panel no-margin-bottom">
				<div class="cstab tab tab-theme">
					<div class="tab-bottom">
						<a class="sidebar-minimalizer btn btn-theme tab-arrow-c" href="#">
							<i class="fa"></i>
						</a>
						<a class="btn btn-default btn-bordered tab-arrow tab-arrow-left"><span class="fa fa-chevron-left"></span></a>
						<a class="btn btn-default btn-bordered tab-arrow tab-arrow-right"><span class="fa fa-chevron-right"></span></a>
						<div class="sopn margin30">
							<ul class="nav nav-tabs" id="nav-tabs"></ul>
						</div>
					</div>
					<div class="tab-content" id="tab-content"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</c:if>

<c:if test="${tiles=='template:login' }">
<tiles:insertAttribute name="body" />
<!-- Style Sheets -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/libs/pace/1.0.2/themes/black/pace-theme-minimal.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/libs/metisMenu/1.1.3/metisMenu.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/libs/animate.css/3.2.0/animate.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/libs/toastr.js/2.1.2/toastr.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/style.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/themes/theme-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/demo.min.css">
<!-- / Style Sheets -->

<!-- Style Sheets -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/login.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/boss/styles/cs.css">
<!-- / Style Sheets -->
<!-- layer -->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/assert/layer/layer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/assert/layer/extend/layer.ext.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assert/layer/skin/layer.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assert/layer/skin/layer.ext.css">
<!-- /layer -->
</c:if>
</body>  
</html>  