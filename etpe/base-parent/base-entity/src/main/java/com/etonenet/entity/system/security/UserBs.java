package com.etonenet.entity.system.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

import io.swagger.annotations.ApiModelProperty;

/**
 * The persistent class for the TS_USER_BS database table.
 * 
 */
@LogEntity(remark = "用户管理")
@Entity
@Table(name = "TS_USER_BS")
@NamedQuery(name = "UserBs.findAll", query = "SELECT u FROM UserBs u")
public class UserBs implements Serializable {
	private static final long serialVersionUID = 1L;

	// 必填
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Min(value = 1)
	@ApiModelProperty(position = 1, value = "id存在将做更新操作，不然则新增")
	@Column(name = "USER_ID", unique = true, nullable = false, precision = 10)
	private Long userId;

	@NotNull
	@Pattern(regexp = "[0-9a-zA-Z]{3,50}")
	@ApiModelProperty(position = 2, required = true, value = "登录名不少于3字符且不超过50字符，英文数字字符，不区分大小写")
	@Column(name = "LOGIN_NAME", length = 50)
	private String loginName;

	@NotNull
	@Pattern(regexp = "[0-9a-zA-Z\\D]{3,50}")
	@ApiModelProperty(position = 3, required = true, value = "密码不少于3字符且不超过50字符，英文数字字符含特殊符号，区分大小写")
	@Column(length = 50)
	private String password;

	@ApiModelProperty(position = 4, value = "昵称不超过15字符")
	@Column(name = "USER_NAME", length = 50)
	private String userName;

	@ApiModelProperty(position = 5, value = "用户状态,默认0")
	@Column(name = "USER_STATE", precision = 2)
	private int userState;

	// 非必填
	@Size(max = 100)
	@ApiModelProperty(value = "联系方式不超过100字符")
	@Column(length = 100)
	private String contact;

	@Pattern(regexp = "[0-9a-zA-Z\\D]{0,100}")
	@ApiModelProperty(value = "邮箱不超过100字符")
	@Column(length = 100)
	private String email;

	@Pattern(regexp = "[0-9\\D]{0,20}")
	@ApiModelProperty(value = "手机不超过20字符")
	@Column(length = 20)
	private String mobile;

	@Pattern(regexp = "[0-9\\D]{0,20}")
	@ApiModelProperty(value = "手机不超过20字符")
	@Column(length = 20)
	private String phone;

	@Pattern(regexp = "[0-9]{0,30}")
	@ApiModelProperty(value = "qq不超过30字符")
	@Column(length = 30)
	private String qq;

	@ApiModelProperty(hidden = true, value = "暂时不用")
	@Column(name = "PASSWORD_EXPIRE", precision = 2)
	private int passwordExpire;

	@ApiModelProperty(hidden = true, value = "暂时不用")
	@Column(name = "ACCOUNT_EXPIRE", precision = 2)
	private int accountExpire;

	@ApiModelProperty(hidden = true, value = "暂时不用")
	@Column(name = "ACCOUNT_LOCK", precision = 2)
	private int accountLock;

	public UserBs() {
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getAccountExpire() {
		return this.accountExpire;
	}

	public void setAccountExpire(int accountExpire) {
		this.accountExpire = accountExpire;
	}

	public int getAccountLock() {
		return this.accountLock;
	}

	public void setAccountLock(int accountLock) {
		this.accountLock = accountLock;
	}

	public String getContact() {
		return this.contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPasswordExpire() {
		return this.passwordExpire;
	}

	public void setPasswordExpire(int passwordExpire) {
		this.passwordExpire = passwordExpire;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQq() {
		return this.qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserState() {
		return this.userState;
	}

	public void setUserState(int userState) {
		this.userState = userState;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountExpire;
		result = prime * result + accountLock;
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((loginName == null) ? 0 : loginName.hashCode());
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + passwordExpire;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((qq == null) ? 0 : qq.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + userState;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserBs other = (UserBs) obj;
		if (accountExpire != other.accountExpire)
			return false;
		if (accountLock != other.accountLock)
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (loginName == null) {
			if (other.loginName != null)
				return false;
		} else if (!loginName.equals(other.loginName))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (passwordExpire != other.passwordExpire)
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (qq == null) {
			if (other.qq != null)
				return false;
		} else if (!qq.equals(other.qq))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userState != other.userState)
			return false;
		return true;
	}

}