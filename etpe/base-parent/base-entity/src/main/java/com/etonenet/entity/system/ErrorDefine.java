package com.etonenet.entity.system;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TC_ERROR_DEFINE database table.
 * 
 */
@LogEntity(remark = "错误定义方")
@Entity
@Table(name = "TC_ERROR_DEFINE")
@NamedQuery(name = "ErrorDefine.findAll", query = "SELECT e FROM ErrorDefine e")
public class ErrorDefine implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ERROR_DEFINE_ID", unique = true, nullable = false, precision = 10)
	private Long errorDefineId;

	@Column(name = "HOST", nullable = false, length = 50)
	private String host;

	public ErrorDefine() {
	}

	public Long getErrorDefineId() {
		return this.errorDefineId;
	}

	public void setErrorDefineId(Long errorDefineId) {
		this.errorDefineId = errorDefineId;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}
}