package com.etonenet.entity.system.security;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the TS_ROLE_USER database table.
 * 
 */
@Entity
@Table(name = "TS_ROLE_USER")
@NamedQuery(name = "RoleUser.findAll", query = "SELECT r FROM RoleUser r")
public class RoleUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RoleUserPK id;

	// uni-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "ROLE_ID", nullable = false, insertable = false, updatable = false)
	private Role role;

	public RoleUser() {
	}

	public RoleUserPK getId() {
		return this.id;
	}

	public void setId(RoleUserPK id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}