package com.etonenet.enums.systemnode;

import java.util.LinkedHashMap;
import java.util.Map;

public enum SystemNodeType {

	MENU(0, "菜单"), FUN(1, "功能");

	private final Integer value;
	private final String name;

	private SystemNodeType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return this.value;
	}

	public String getName() {
		return this.name;
	}

	public static SystemNodeType fromValue(Integer value) {
		if (value != null) {
			for (SystemNodeType state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return MENU;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (SystemNodeType e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
