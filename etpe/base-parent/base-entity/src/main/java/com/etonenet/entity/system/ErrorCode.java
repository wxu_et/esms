package com.etonenet.entity.system;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.etonenet.anno.LogEntity;

/**
 * The persistent class for the TC_ERROR_CODE database table.
 * 
 */
@LogEntity(remark = "错误代码")
@Entity
@Table(name = "TC_ERROR_CODE")
@NamedQuery(name = "ErrorCode.findAll", query = "SELECT e FROM ErrorCode e")
public class ErrorCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ERROR_CODE_ID", unique = true, nullable = false, precision = 10)
	private Long errorCodeId;

	@Size(max = 100)
	@Column(name = "COVERT_EXPLAIN", length = 100)
	private String covertExplain;

	@Size(max = 30)
	@Column(name = "ERROR_CODE", length = 30)
	private String errorCode;

	@Digits(fraction = 0, integer = 3)
	@Column(precision = 3)
	private BigDecimal grade;

	@Size(max = 100)
	@Column(name = "ORIGIN_EXPLAIN", length = 100)
	private String originExplain;

	// uni-directional many-to-one association to ErrorDefine
	@ManyToOne
	@JoinColumn(name = "ERROR_FROM")
	private ErrorDefine errorDefine;

	public ErrorCode() {
	}

	public Long getErrorCodeId() {
		return this.errorCodeId;
	}

	public void setErrorCodeId(Long errorCodeId) {
		this.errorCodeId = errorCodeId;
	}

	public String getCovertExplain() {
		return this.covertExplain;
	}

	public void setCovertExplain(String covertExplain) {
		this.covertExplain = covertExplain;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public BigDecimal getGrade() {
		return this.grade;
	}

	public void setGrade(BigDecimal grade) {
		this.grade = grade;
	}

	public String getOriginExplain() {
		return this.originExplain;
	}

	public void setOriginExplain(String originExplain) {
		this.originExplain = originExplain;
	}

	public ErrorDefine getErrorDefine() {
		return this.errorDefine;
	}

	public void setErrorDefine(ErrorDefine errorDefine) {
		this.errorDefine = errorDefine;
	}
}