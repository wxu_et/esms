package com.etonenet.enums;

import java.math.BigDecimal;

public enum LogicStat {

	NORMAL(BigDecimal.valueOf(0), "正常"), DEL(BigDecimal.valueOf(1), "删除");

	private final BigDecimal value;
	private final String name;

	private LogicStat(BigDecimal value, String name) {
		this.value = value;
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public String getName() {
		return this.name;
	}

	public static LogicStat fromValue(BigDecimal value) {
		if (value != null) {
			for (LogicStat state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return NORMAL;
	}

}
