package com.etonenet.entity.system.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the TS_ROLE_USER database table.
 * 
 */
@Embeddable
public class RoleUserPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID", insertable = false, updatable = false, nullable = false, precision = 10)
	private long roleId;

	@Column(name = "USER_ID", insertable = false, updatable = false, nullable = false, precision = 10)
	private long userId;

	public RoleUserPK() {
	}

	public long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RoleUserPK)) {
			return false;
		}
		RoleUserPK castOther = (RoleUserPK) other;
		return (this.roleId == castOther.roleId) && (this.userId == castOther.userId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.roleId ^ (this.roleId >>> 32)));
		hash = hash * prime + ((int) (this.userId ^ (this.userId >>> 32)));

		return hash;
	}
}