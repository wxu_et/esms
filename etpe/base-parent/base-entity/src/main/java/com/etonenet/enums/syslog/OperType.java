package com.etonenet.enums.syslog;

import java.util.LinkedHashMap;
import java.util.Map;

public enum OperType {

	INSERT(0, "增"), DEL(1, "删"), MODIFY(2, "改"), QUERY(3, "查"), OTHER(4, "其他");

	private final Integer value;
	private final String name;

	private OperType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return this.value;
	}

	public String getName() {
		return this.name;
	}

	public static OperType fromValue(Integer value) {
		if (value != null) {
			for (OperType state : values()) {
				if (state.value.equals(value)) {
					return state;
				}
			}
		}
		return INSERT;
	}

	public static Map<String, String> toDisplayMap() {
		Map<String, String> m = new LinkedHashMap<>();
		for (OperType e : values()) {
			m.put(e.value.toString(), e.name);
		}
		return m;
	}
}
