# ddl
* [CustomJdbcUserServiceSampleConfig.sql](https://github.com/spring-projects/spring-security/blob/master/config/src/test/resources/CustomJdbcUserServiceSampleConfig.sql)
* [createAclSchemaMySQL.sql](https://github.com/spring-projects/spring-security/blob/master/acl/src/main/resources/createAclSchemaMySQL.sql)

# questions

## ROLE 与 AUTORITY 的关系？
Q:我看在功能上是一样的。而且spring没有ROLE管理器，没有ROLE的DDL。
A:默认情况下，ROLE都是以“ROLE_”开头的，而autority没有； 实际上，ROLE与autority是一样的。

## 不知道 FilterInvocationSecurityMetadataSource 有什么作用？
* 配置文件
```java
@Configuration
@ComponentScan(basePackageClasses = { UserDetailController.class })
@Import(value = { JpaConfig.class })
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String AUTHORITY_ABC = "ROLE_ABC";

	@Autowired
	private DataSource dataSource;
	@Autowired
	private JdbcUserDetailsManager userDetailsManager;

	...

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http
		.authorizeRequests()
		.antMatchers("/view/**").hasAuthority(AUTHORITY_ABC)
//		.antMatchers("/","/favicon.ico").permitAll()
//		.antMatchers("/static/**").permitAll()
		.anyRequest().authenticated()
		.withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
		    public <O extends FilterSecurityInterceptor> O postProcess(O fsi) {
		        fsi.setSecurityMetadataSource(new MyFilterSecurityMetadataSource());
		        return fsi;
		    }
		})
```

* 不知道这里应该怎么用，如FIXME
``` java
public class MyFilterSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	public List<ConfigAttribute> getAttributes(Object object) {
		FilterInvocation fi = (FilterInvocation) object;
		String fullRequestUrl = fi.getFullRequestUrl();
		String requestUrl = fi.getRequestUrl();
		String httpMethod = fi.getRequest().getMethod();
		String contextPath = fi.getRequest().getContextPath();
		System.out.println("Full request URL: " + fullRequestUrl);
		System.out.println("Request URL: " + requestUrl);
		System.out.println("HTTP Method: " + httpMethod);
		System.out.println("Context path: " + contextPath);
		List<ConfigAttribute> configAttributes = new ArrayList<ConfigAttribute>(0);
		// Lookup your database (or other source) using this information and
		// populate the list of attributes
		
		// FIXME 如果匹配，返回“ROLE_USER"，无法审核通过。
		configAttributes.add(new SecurityConfig("*"));

		return configAttributes;
	}
	
```
A: ROLE没有配置正确，需要注意前缀；默认"ROLE_"开头。

## GROUP 中的 autority 是否不能自动归并到 userDetail 的autority中去？
Q:
* GROUP已经有了允许访问的autority ；但是userDetail 中没有，所以无法访问。  
* 如果给userdetail对应的autority，user就可以访问了。
A:默认情况下，此功能不开启
```java
@Bean
	public JdbcUserDetailsManager userDetailsManager() {
		JdbcUserDetailsManager manager = new JdbcUserDetailsManager();
		manager.setDataSource(dataSource);
		// 启用用户组功能
		manager.setEnableGroups(true);
```

## 角色继承功能没有测试成功？
Q：
* 如何使用RoleVoter?
* 为什么RoleVoter中的attributes都是WebExpressionConfigAttribute?
```java
public int vote(Authentication authentication, Object object,	Collection<ConfigAttribute> attributes) {
```

# functions

## list
* acl(Access Control List)
	* ACL基本操作
		* 使用aclService管理acl信息
		* 使用acl控制delete操作
		* 控制用户可以看到哪些信息
	* 管理acl
		* 管理多个domain类
		* 动态授权与收回授权
	* acl自动提醒
* 用户管理界面
* 角色继承	suspend
* 使用jcaptcha 实现彩色验证码
* 动态资源管理
* 锁定用户	done
```
连续输错3次密码，账号锁定；
```
* 使用用户组			done
* 自定义过滤器		
* 自定义会话管理
* 配置过滤器
* 管理会话			done
	* 后登陆的将先登录的踢出系统 done