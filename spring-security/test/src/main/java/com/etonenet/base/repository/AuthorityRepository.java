package com.etonenet.base.repository;

import com.etonenet.base.entity.Authority;

public interface AuthorityRepository extends BaseRepository<Authority, Integer> {

}
