package com.etonenet.base.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the resources database table.
 * 
 */
@Entity
@Table(name = "resources")
@NamedQuery(name = "Resource.findAll", query = "SELECT r FROM Resource r")
public class Resource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "res_id", updatable = false)
	private long resId;

	@Column(name = "res_descn")
	private String resDescn;

	@Column(name = "res_name")
	private String resName;

	@Column(name = "res_priority")
	private int resPriority;

	@Column(name = "res_string")
	private String resString;

	@Column(name = "res_type")
	private String resType;

	// bi-directional many-to-one association to ResourceRole
	@OneToMany(mappedBy = "resource", fetch = FetchType.EAGER)
	private List<ResourceRole> resourceRoles;

	public Resource() {
	}

	public long getResId() {
		return this.resId;
	}

	public void setResId(long resId) {
		this.resId = resId;
	}

	public String getResDescn() {
		return this.resDescn;
	}

	public void setResDescn(String resDescn) {
		this.resDescn = resDescn;
	}

	public String getResName() {
		return this.resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public int getResPriority() {
		return this.resPriority;
	}

	public void setResPriority(int resPriority) {
		this.resPriority = resPriority;
	}

	public String getResString() {
		return this.resString;
	}

	public void setResString(String resString) {
		this.resString = resString;
	}

	public String getResType() {
		return this.resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public List<ResourceRole> getResourceRoles() {
		return this.resourceRoles;
	}

	public void setResourceRoles(List<ResourceRole> resourceRoles) {
		this.resourceRoles = resourceRoles;
	}

	public ResourceRole addResourceRole(ResourceRole resourceRole) {
		getResourceRoles().add(resourceRole);
		resourceRole.setResource(this);

		return resourceRole;
	}

	public ResourceRole removeResourceRole(ResourceRole resourceRole) {
		getResourceRoles().remove(resourceRole);
		resourceRole.setResource(null);

		return resourceRole;
	}

}