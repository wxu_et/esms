package com.etonenet.base.test;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.etonenet.base.entity.Group;

public class UserInfo extends BaseUserDetails {
	private static final long serialVersionUID = 1L;
	private String email;
	private List<Group> groups;

	public UserInfo(String username, String password, boolean enabled,
			Collection<? extends GrantedAuthority> authorities) throws IllegalArgumentException {
		super(username, password, enabled, authorities);
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean equals(Object rhs) {
		if (!(rhs instanceof UserInfo) || (rhs == null)) {
			return false;
		}

		if (super.equals(rhs)) {
			UserInfo userInfo = (UserInfo) rhs;

			return ((this.email == null) && (userInfo.getEmail() == null))
					|| ((this.email != null) && this.email.equals(userInfo.getEmail()));
		}

		return false;
	}

	public int hashCode() {
		int code = super.hashCode();

		if (this.email != null) {
			code *= -11;
		}

		return code;
	}

	public UserInfo(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities)
			throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public UserInfo(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, Collection<? extends GrantedAuthority> authorities)
			throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, authorities);
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
}
