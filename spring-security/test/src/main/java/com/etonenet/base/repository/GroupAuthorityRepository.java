package com.etonenet.base.repository;

import com.etonenet.base.entity.GroupAuthority;

public interface GroupAuthorityRepository extends BaseRepository<GroupAuthority, Integer> {

}
