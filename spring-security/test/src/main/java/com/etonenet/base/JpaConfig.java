package com.etonenet.base;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.samples.config.DataConfiguration;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@Import(value = { DataConfiguration.class })
@PropertySources(value = { @PropertySource("classpath:/jpa.properties") })
public class JpaConfig {

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {

		DruidDataSource ds = new DruidDataSource();
		ds.setDefaultAutoCommit(true);
		ds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		ds.setUrl(env.getProperty("jdbc.url"));
		ds.setUsername(env.getProperty("jdbc.username"));
		ds.setPassword(env.getProperty("jdbc.password"));

		ds.setMaxActive(env.getProperty("jdbc.maxActive", Integer.class));
		ds.setInitialSize(env.getProperty("jdbc.initialSize", Integer.class));
		ds.setMaxWait(env.getProperty("jdbc.maxWaitMillis", Integer.class));
		ds.setMinIdle(env.getProperty("jdbc.minIdle", Integer.class));

		ds.setDefaultAutoCommit(false);
		ds.setPoolPreparedStatements(false);

		// EmbeddedDatabase ds = new EmbeddedDatabaseBuilder().build();

		return ds;
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

}