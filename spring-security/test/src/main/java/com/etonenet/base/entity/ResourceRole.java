package com.etonenet.base.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the resource_roles database table.
 * 
 */
@Entity
@Table(name="resource_roles")
@NamedQuery(name="ResourceRole.findAll", query="SELECT r FROM ResourceRole r")
public class ResourceRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false)
	private long id;

	private String role;

	//bi-directional many-to-one association to Resource
	@ManyToOne
	@JoinColumn(name="res_id")
	private Resource resource;

	public ResourceRole() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Resource getResource() {
		return this.resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

}