package com.etonenet.base.repository;

import com.etonenet.base.entity.Resource;

public interface ResourceRepository extends BaseRepository<Resource, Integer> {

}
