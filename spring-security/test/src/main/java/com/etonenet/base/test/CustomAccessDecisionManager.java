package com.etonenet.base.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;

//public class CustomAccessDecisionManager extends AbstractAccessDecisionManager {
	public class CustomAccessDecisionManager extends AffirmativeBased {

	public CustomAccessDecisionManager(List<AccessDecisionVoter<? extends Object>> decisionVoters) {
		super(decisionVoters);
	}

	public void decide(Authentication authentication, Object filter, Collection<ConfigAttribute> configAttributes)
			throws AccessDeniedException, InsufficientAuthenticationException {

		if ((filter == null) || !this.supports(filter.getClass())) {
			throw new IllegalArgumentException("Object must be a FilterInvocation");
		}

		String url = ((FilterInvocation) filter).getRequestUrl();
		String contexto = ((FilterInvocation) filter).getRequest().getContextPath();

		// FIXME 数据库访问
		// Collection<ConfigAttribute> roles =
		// service.getConfigAttributesFromSecuredUris(contexto, url);

		Collection<ConfigAttribute> roles = new ArrayList<ConfigAttribute>();

		ConfigAttribute e = new SecurityConfig("ROLE_USER");
		roles.add(e);
		
//		super.decide(authentication, filter, roles);
		super.decide(authentication, filter, configAttributes);
		
	}

}