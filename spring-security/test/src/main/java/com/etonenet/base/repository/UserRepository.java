package com.etonenet.base.repository;

import com.etonenet.base.entity.User;

public interface UserRepository extends BaseRepository<User, Integer> {

}
