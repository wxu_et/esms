package com.etonenet.base.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the groups database table.
 * 
 */
@Entity
@Table(name = "groups")
@NamedQuery(name = "Group.findAll", query = "SELECT g FROM Group g")
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "group_name")
	private String groupName;

	// bi-directional many-to-one association to GroupAuthority
	@OneToMany(mappedBy = "group")
	private List<GroupAuthority> groupAuthorities;

	// @ManyToMany注释表示Group是多对多关系的一端。
	@ManyToMany(cascade = CascadeType.PERSIST)
	// @JoinTable描述了多对多关系的数据表关系。name属性指定中间表名称
	@JoinTable(name = "group_members",
			// joinColumns定义中间表与Group表的外键关系,中间表group_members的group_id列是Group表的主键列对应的外键列
			joinColumns = { @JoinColumn(name = "group_id", referencedColumnName = "id") },
			// inverseJoinColumns属性定义了中间表与另外一端(User)的外键关系
			inverseJoinColumns = { @JoinColumn(name = "username", referencedColumnName = "username") })
	private List<User> members;

	public Group() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<GroupAuthority> getGroupAuthorities() {
		return this.groupAuthorities;
	}

	public void setGroupAuthorities(List<GroupAuthority> groupAuthorities) {
		this.groupAuthorities = groupAuthorities;
	}

	public GroupAuthority addGroupAuthority(GroupAuthority groupAuthority) {
		getGroupAuthorities().add(groupAuthority);
		groupAuthority.setGroup(this);

		return groupAuthority;
	}

	public GroupAuthority removeGroupAuthority(GroupAuthority groupAuthority) {
		getGroupAuthorities().remove(groupAuthority);
		groupAuthority.setGroup(null);

		return groupAuthority;
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

}