package com.etonenet.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.etonenet.base.test.UserInfo;

/**
 * 扩展userdetails
 * 
 * @author wxu
 *
 */
@Service
public class UserInfoService implements UserDetailsService {
	private Map<String, UserInfo> userMap = null;

	public UserInfoService() {
		userMap = new HashMap<String, UserInfo>();

		UserInfo userInfo = null;
		userInfo = new UserInfo("user", "user", true, getUser("ROLE_USER"));
		userInfo.setEmail("user@family168.com");

		userMap.put("user", userInfo);
		userInfo = new UserInfo("admin", "admin", true, getUser("ROLE_USER", "ROLE_ADMIN"));
		userInfo.setEmail("admin@family168.com");
		userMap.put("admin", userInfo);
	}

	private List<SimpleGrantedAuthority> getUser(String... autorities) {
		List<SimpleGrantedAuthority> arrayList = new ArrayList<>();
		for (int i = 0; i < autorities.length; i++) {
			arrayList.add(new SimpleGrantedAuthority(autorities[i]));
		}
		return arrayList;
	}

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		return userMap.get(username);
	}
}