package com.etonenet.base;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * security servlet
 * 
 * @author wxu
 *
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {

}
