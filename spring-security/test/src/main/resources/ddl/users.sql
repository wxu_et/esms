/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-06-02 09:05:22
*/

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` int(5) NOT NULL,
  `usernamecn` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', 'admin', '1', '系统管理员');
INSERT INTO `users` VALUES ('lxb', 'c7d3f4c857bc8c145d6e5d40c1bf23d9', '1', '登录用户');
INSERT INTO `users` VALUES ('user', 'user', '1', '普通用户');
INSERT INTO `users` VALUES ('wxu', 'wxu', '1', 'user');
INSERT INTO `users` VALUES ('xxx', 'ppp', '0', null);

-- ----------------------------
-- Table structure for `authorities`
-- ----------------------------
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_auth_username` (`username`,`authority`),
  CONSTRAINT `FK8w2newnyu9dk552fgnapdp3i3` FOREIGN KEY (`USERNAME`) REFERENCES `users` (`USERNAME`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`USERNAME`) REFERENCES `users` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of authorities
-- ----------------------------
INSERT INTO `authorities` VALUES ('1', 'admin', 'ROLE_PLATFORMADMIN');
INSERT INTO `authorities` VALUES ('2', 'admin', 'ROLE_SYSADMIN');
INSERT INTO `authorities` VALUES ('3', 'lxb', 'ROLE_LOGIN');
INSERT INTO `authorities` VALUES ('4', 'lxb', 'ROLE_LOGINTOWELCOME');
INSERT INTO `authorities` VALUES ('12', 'user', 'ROLE_ABC');
INSERT INTO `authorities` VALUES ('5', 'user', 'ROLE_USER');
INSERT INTO `authorities` VALUES ('11', 'wxu', 'ROLE_USER');

-- ----------------------------
-- Table structure for `group_authorities`
-- ----------------------------
DROP TABLE IF EXISTS `group_authorities`;
CREATE TABLE `group_authorities` (
  `group_id` int(11) NOT NULL,
  `authority` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FKruy9mx1ch59gog4lw18kgnd67` (`group_id`),
  CONSTRAINT `FKruy9mx1ch59gog4lw18kgnd67` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group_authorities
-- ----------------------------
INSERT INTO `group_authorities` VALUES ('3', 'ROLE_USER', '2');

-- ----------------------------
-- Table structure for `group_members`
-- ----------------------------
DROP TABLE IF EXISTS `group_members`;
CREATE TABLE `group_members` (
  `group_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  KEY `FKpc3ptq8m7nfr4yv9swshh7kay` (`username`),
  KEY `FKkv9vlrye4rmhqjq4qohy2n5a6` (`group_id`),
  CONSTRAINT `FKkv9vlrye4rmhqjq4qohy2n5a6` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `FKpc3ptq8m7nfr4yv9swshh7kay` FOREIGN KEY (`username`) REFERENCES `users` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group_members
-- ----------------------------
INSERT INTO `group_members` VALUES ('3', 'admin');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('3', 'GROUP_USER');


-- ----------------------------
-- Table structure for `resources`
-- ----------------------------
DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `res_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_descn` varchar(255) DEFAULT NULL COMMENT '描述',
  `res_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `res_priority` int(11) NOT NULL COMMENT '优先级',
  `res_string` varchar(255) DEFAULT NULL COMMENT '公式或内容，如URL',
  `res_type` varchar(255) DEFAULT NULL COMMENT '类型:URL , METHOD',
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='资源定义表';

-- ----------------------------
-- Records of resources
-- ----------------------------
INSERT INTO `resources` VALUES ('1', null, 'URL-静态资源', '9', '/resources/**', 'URL');
INSERT INTO `resources` VALUES ('2', null, 'URL-TEST', '9', '/view/**', 'URL');
INSERT INTO `resources` VALUES ('4', null, 'URL-ANONYMOUS', '9', '/', 'URL');

-- ----------------------------
-- Table structure for `resource_roles`
-- ----------------------------
DROP TABLE IF EXISTS `resource_roles`;
CREATE TABLE `resource_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(20) NOT NULL,
  `role` varchar(50) NOT NULL COMMENT '角色',
  PRIMARY KEY (`id`),
  KEY `FKj80mbldy0k9a2h8b01bl23sq3` (`res_id`),
  CONSTRAINT `FKj80mbldy0k9a2h8b01bl23sq3` FOREIGN KEY (`res_id`) REFERENCES `resources` (`res_id`),
  CONSTRAINT `resource_roles_ibfk_1` FOREIGN KEY (`res_id`) REFERENCES `resources` (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色- 资源关联表';

-- ----------------------------
-- Records of resource_roles
-- ----------------------------
INSERT INTO `resource_roles` VALUES ('1', '1', 'ROLE_USER');
INSERT INTO `resource_roles` VALUES ('2', '2', 'ROLE_TEST');
INSERT INTO `resource_roles` VALUES ('3', '4', 'ROLE_USER');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_name` varchar(50) NOT NULL COMMENT '角色名 - 同autority，但有"ROLE_"前缀',
  `memo` varchar(50) DEFAULT NULL COMMENT '注释功用',
  PRIMARY KEY (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色定义';

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('ROLE_ABC', '测试账号');
INSERT INTO `roles` VALUES ('ROLE_ADMIN', '管理员');
INSERT INTO `roles` VALUES ('ROLE_GUEST', '访客');
INSERT INTO `roles` VALUES ('ROLE_LOGIN', null);
INSERT INTO `roles` VALUES ('ROLE_SYSADMIN', '系统管理员');
INSERT INTO `roles` VALUES ('ROLE_USER', '普通用户');


