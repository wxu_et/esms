package com.etonenet.base.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.etonenet.base.JpaConfig;
import com.etonenet.base.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class })
public class UserRepositoryTest {

	@Autowired
	private UserRepository repository;
	
	@Test
	public void test() {
		User user = new User();
		user.setUsername("xxx");
		user.setPassword("ppp");
		user.setGroups(null);
		user.setAuthorities(null);
		repository.save(user);
	}

}
